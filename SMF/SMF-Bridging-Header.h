
//
//  IOSServiceLayerDemoCode-Bridging-Header.h
//  IOSServiceLayerDemoCode
//
//  Created by Vishal on 03/08/16.
//  Copyright © 2016 Daffodilsw Applications. All rights reserved.
//



#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <BEMCheckBox/BEMCheckBox.h>

//#import <AWSS3/AWSS3.h>
