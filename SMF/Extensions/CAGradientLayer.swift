//
//  CAGradientLayer.swift
//  SMF
//
//  Created by Jenkins on 1/3/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
import UIKit
extension CAGradientLayer {
    class func gradientLayerForBounds(bounds: CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [UIColor.appGreenColor.cgColor, UIColor.appBlueColor.cgColor]
        return layer
    }
    
}
