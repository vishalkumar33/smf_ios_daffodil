
/// This class is used for parsing error if error response is complex than developer can parse their error response within this


import UIKit

protocol ErrorInterceptor {
    func getErrorString(error:NSError, errorResolver: ErrorResolver) -> String
}

/// This class is used for Intercepting Error corresponding to NSError Object

class SimpleErrorHandler : ErrorInterceptor {
    
    /**
     This method is used for returning appropriate error message for a Error
     
     - parameter error:         NSError object contains Error Information
     - parameter errorResolver: ErrorResolver object which is used for lookup string in Error Lookup Table and returning error message corresponding to that Error Code
     
     - returns: Error String
     */
    
    func getErrorString(error:NSError, errorResolver: ErrorResolver) -> String {
        print(error.domain)
        return (errorResolver.getErrorObjectForCode(errorCode: error.code))
    }
}
