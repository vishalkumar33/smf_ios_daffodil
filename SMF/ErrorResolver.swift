
import Foundation

/// Error Resolver class is used for resolving Error Issue

class ErrorResolver {

    var errorDict: [Int: String] = [Int: String]()

    /**
     This method is used for registering error code for specific APIs
     
     - parameter errorCode: Code of Error
     - parameter message:   Message string corresponding to error
     */

    func registerErrorCode(errorCode: ErrorCodes, message: String){
        
        //Mapping messages corresponding to specific code
        self.errorDict[errorCode.rawValue] = message
    }
    
     /**
     This method is used for sending specific error message corresponding to error code
     
     - parameter errorCode: Error Code corresponding to specific error
     
     - returns: Returning Error Message
     */
    
    func getErrorObjectForCode(errorCode: Int) -> String
    {
        //This line check whether error is resolvable or not
        
        if isErrorResolvable(code: errorCode){
            
            return errorDict[errorCode]!
        }
        else{
            
           return "Some error occured"
        }
    }
    
    /**
     This method is used for checking whether error is already added or not in Error Dictionary
     
     - parameter code: Error Code
     
     - returns: Returning True or false
     */
    
    private func isErrorResolvable(code: Int) -> Bool
    {
        guard let _ = errorDict[code] else {
            return false
        }
        return true
    }

}
