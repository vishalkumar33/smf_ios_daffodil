

import UIKit
import MBProgressHUD

var spinner: UIActivityIndicatorView!

extension UIView
{
    
    // MARK:- Loader Methods
    func showLoaderWithMessage(_ message: String)
    {
        let hud: MBProgressHUD = MBProgressHUD.showAdded(to: self, animated: true)
        hud.labelText = message
    }
    
    func showLoader()
    {
        MBProgressHUD.showAdded(to: self, animated: true)
    }
    
    func showLoaderWithNoBlock(){
        let hud: MBProgressHUD = MBProgressHUD.showAdded(to: self, animated: true)
        hud.opacity = 0.1
        hud.isUserInteractionEnabled = false
    }
    
    func hideLoader()
    {
        MBProgressHUD.hideAllHUDs(for: self, animated: true)
    }
    
    func showToastWithMessage(_ msg: String)
    {
        let hud: MBProgressHUD = MBProgressHUD.showAdded(to: self, animated: true)
        hud.mode = .text
        hud.labelText = msg
        hud.margin = 10.0
        hud.removeFromSuperViewOnHide = true
        hud.hide(true, afterDelay: 20)
    }
    
    func showSpinner() {
        spinner = UIActivityIndicatorView(activityIndicatorStyle: .white)
        spinner.frame = CGRect(x: self.frame.size.width / 2 - 15, y: self.frame.size.height / 2 - 15, width: 30, height: 30)
        spinner.startAnimating()
       // self.addSubview(spinner)
       // self.bringSubviewToFront(spinner)
        let view = UIView()
        view.frame = CGRect(x: self.frame.size.width / 2 - 15, y: self.frame.size.height / 2 - 15, width: 30, height: 30)
        view.backgroundColor = UIColor.red
        self.addSubview(view)
        self.bringSubview(toFront: view)
    }
    
    func showSpinnerWithUserInteractionDisabled(_ shouldDisableUserInteraction: Bool) {
        if shouldDisableUserInteraction
        {
            self.alpha = 0.8
            self.isUserInteractionEnabled = false
        }
      
        self.hideSpinner()
        
        spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        spinner.frame = CGRect(x: self.frame.size.width/2-15, y: self.frame.size.height/2-15, width: 30, height: 30)
        spinner.startAnimating()
      //  spinner.color = UIColor.redColor()
        
        self.addSubview(spinner)
        self.bringSubview(toFront: spinner)
    }
    
    func hideSpinner()
    {
        self.isUserInteractionEnabled = true;
        self.alpha = 1.0;
        if(spinner != nil)
        {
            spinner.removeFromSuperview()
            spinner = nil
        }
        
    }

    
    
    
}

extension UIView {
    
    /// Flip view horizontally.
    func flipX() {
        transform = CGAffineTransform(scaleX: -transform.a, y: transform.d)
    }
    
    /// Flip view vertically.
    func flipY() {
        transform = CGAffineTransform(scaleX: transform.a, y: -transform.d)
    }
    
}
