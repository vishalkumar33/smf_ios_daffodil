//
//  CommentTableHelper.swift
//  SMF
//
//  Created by Jenkins on 2/21/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//



import Foundation

class CommentTableHelper {
    
    
    class func EmptyMessage(message:String, tableView:UITableView) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: tableView.bounds.size.height - 300,width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        tableView.backgroundView = messageLabel;
        tableView.separatorStyle = .none;
    }
    
    
    class func stringToDate(date:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let convertedDate = dateFormatter.date(from: date)
        print("date: \(date)")
        return convertedDate!
    }
    
    class func DateToString(date:Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, yyyy 'at' h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        let dateString = formatter.string(from: date)
        print(dateString)
        return dateString
        
    }
    
    class func getMessageSize(message:String) -> CGSize {
        let WIDTH = UIScreen.main.bounds.width
        let lblMsg = UILabel()
        lblMsg.backgroundColor = UIColor.yellow
        lblMsg.text = message
        let font = UIFont.systemFont(ofSize: 14.0)
        var desiredWidth: CGFloat = 190
        if WIDTH == 375{
            desiredWidth = 270
        }
        else if WIDTH == 414{
            desiredWidth = 300
        }
        lblMsg.font = font
        lblMsg.numberOfLines = 0;
        lblMsg.lineBreakMode = NSLineBreakMode.byWordWrapping
        let size: CGSize = lblMsg.sizeThatFits(CGSize(width: desiredWidth, height: .greatestFiniteMagnitude))
        return size
    }
    
    
}
