//
//  CollectionViewHelper.swift
//  SMF
//
//  Created by Jenkins on 1/24/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class CollectionViewHelper {
    
    
    class func EmptyMessage(message:String, collectionView:UICollectionView) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0,width: collectionView.bounds.size.width, height: collectionView.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        collectionView.backgroundView = messageLabel;
    }
    
    
}
