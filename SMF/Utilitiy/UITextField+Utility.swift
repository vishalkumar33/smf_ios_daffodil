//
//  UITextField+Utility.swift
//  SMF
//
//  Created by Jenkins on 12/28/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    
    func leftPadding(width : CGFloat){
         let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: width, height: self.frame.size.height))
        self.leftView = paddingView
       
        self.leftViewMode = UITextFieldViewMode.always
        
    }
    
}
extension UITextView{
    
    func leftPadding(width : CGFloat){
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: width, height: self.frame.size.height))
               
    }
    
}
