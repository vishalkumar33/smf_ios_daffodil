//
//  UIColor+AppColor.swift
//  SMF
//
//  Created by Jenkins on 12/29/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import Foundation
import Foundation
import UIKit

extension UIColor {
    
    static var appGrayColor:UIColor
    {
        return UIColor(red: 81/255, green: 80/255, blue: 82/255, alpha: 0.1)
    }
    static var appGrayPlaceHolder:UIColor
    {
        return UIColor(red: 81/255, green: 80/255, blue: 82/255, alpha: 0.6)
    }
    static var appGrayboderColor:UIColor
    {
        return UIColor(red: 81/255, green: 80/255, blue: 82/255, alpha: 0.2)
    }

    static var appBlueColor:UIColor
    {
        return UIColor(red: 96/255, green: 102/255, blue: 207/255, alpha: 1.0)

    }
    static var appGreenColor:UIColor
    {
         return UIColor(red: 96/255, green: 214/255, blue: 145/255, alpha: 1.0)
   }
    static var appViewborderColor:UIColor
        {
            return UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 0.7)
    }
    static var appbtnBorderColor:UIColor
    {
        return UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1.0)
    }
    
    static var appMainViewGrayColor:UIColor
    {
            return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1)
    }
    static var appBlueBorderColor:UIColor
    {
            return UIColor(red: 97/255, green: 189/255, blue: 214/255, alpha: 1.0)
    }
    static var appfillColor:UIColor
        {
            return UIColor(red: 249/255, green: 249/255, blue: 250/255, alpha: 1.0)
    }
    static var appseaformColor:UIColor
        {
            return  UIColor(red: 96/255, green: 209/255, blue: 159/255, alpha: 1.0)
    }
  
}
