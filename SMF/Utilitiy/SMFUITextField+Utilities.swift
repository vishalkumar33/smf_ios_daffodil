//
//  SMFUITextField+Utilities.swift
//  SMF
//
//  Created by Jenkins on 1/4/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class MyCustomTextField: UITextField {
    
    @IBInspectable
    var cornerRadius: CGFloat = 0 {
        didSet {
            layer.masksToBounds = true
            
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable
    var borderWidth : CGFloat = 0 {
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    
    }
    
}
