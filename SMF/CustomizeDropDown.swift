
import UIKit

class CustomizeDropDown: UIView{

    // Properties
    var dataObject                          : [DropDownDataObject] = [DropDownDataObject]()
    var dropDownTableView                   : UITableView!      = UITableView()
    var dropDownViewHeight                  : CGFloat!
    
    
    //Drop Down View Properties
    var dropDownViewBackgroundColor         : UIColor!
    var dropDownDefaultViewBackgroundColor  : UIColor!          = UIColor.clear
    var dropDownViewCornerRadius            : CGFloat!
    var dropDownDefaultViewCornerRedius     : CGFloat!          = CGFloat(0)
    var dropDownViewBorderWidth             : CGFloat!
    var dropDownDefaultViewBorderWidth      : CGFloat!          = CGFloat(0)
    var dropDownViewBorderColor             : CGColor!
    var dropDownViewDefaultBorderColor      : CGColor!          = nil
    
    //Drop Down TableView Properties
    var dropDownTableViewCornerRadius       : CGFloat!
    var dropDownTableViewBorderWidth        : CGFloat!
    var dropDownTableViewBorderColor        : CGColor!
    var dropDownTableViewCellSelectionColor : CGColor!
    
    //Drop Down Cell Properties
    var dropDownCellTextColor               : UIColor!
    var dropDownDefaultCellTextColor        : UIColor!          = UIColor.black
    var dropDownCellHeight                  : CGFloat!
    var dropDownDefaultCellHeight           : CGFloat!          = CGFloat(44)
    var dropDownCellBackgroundColor         : UIColor!
    var dropDownDefaultBackgroundColor      : UIColor!          = UIColor.appfillColor
    var dropDownCellTextAligment            : NSTextAlignment!
    var dropDownDefaultCellTextAligment     : NSTextAlignment!  = NSTextAlignment.left
    var dropDownCellLabelFont               : String!
    var dropDownDefaultLabelFont            : String!           = "Quicksand-Medium"
    var dropDownCellLabelFontSize           : CGFloat!
    var dropDownCellDefaultLabelFontSize    : CGFloat!          = CGFloat(14)
    var dropDownCellImageViewCornerRadius   : CGFloat!
    var dropDownDefaultCellImageViewCornerRadius : CGFloat!     = CGFloat(20)
    var dropDownCellImageBorderWidth        : CGFloat!
    var dropDownDefaultCellImageBorderWidth : CGFloat!          = CGFloat(0)
    var dropDownCellImageBorderColor        : CGColor!
    var dropDownDefaultCellImageBorderColor : CGColor!          = nil
    
    
    // Data Properties
    var index                               : Int!
    var value                               : AnyObject!
    

    // Animation Properties
    var interval                            : TimeInterval!
    var defaultInterval                     : TimeInterval!   = TimeInterval(0.20)
    
    // Shadow Properties
    var shadow_Offset                           = CGSize(width: 0, height: 1.4)
    var shadow_Opacity                                           = Float(1.0)
    var shadow_Radius                        : CGFloat!
    var defaultShadowRadius                 : CGFloat!          = CGFloat(10.0)
    
    override func awakeFromNib() {
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
}
