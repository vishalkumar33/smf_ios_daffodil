
import Foundation

protocol ApiResponseReceiver{
    func onSuccess<T: Mapper>(response:T) -> Void
    func onError(errorResponse:NSError) -> Void
}

 protocol Mapper {
     init?(_ response: Dictionary<String,AnyObject>)
}


class ResponseWrapper : ApiResponseReceiver  {
    
    let delegate         : AnyObject?
    let errorResolver    : ErrorResolver?
    let errorInterceptor : ErrorInterceptor?
    
    
    
    init(errorResolver:ErrorResolver , responseCallBack:ResponseCallback, errorIntercepto:ErrorInterceptor){
        self.delegate = responseCallBack
        self.errorResolver = errorResolver
        self.errorInterceptor = errorIntercepto
    }
    
    convenience init(errorResolver:ErrorResolver , responseCallBack:ResponseCallback){
        self.init(errorResolver: errorResolver, responseCallBack:responseCallBack, errorIntercepto: SimpleErrorHandler())
    }
    
    /**
     This method is used for handling Success response of an API
     
     - parameter response: Response is a kind of Generic Object
     */
    
    func onSuccess<T:Mapper>(response:T) -> Void {
        print(response)
        self.delegate?.servicesManagerSuccessResponse!(responseObject: response as AnyObject)
    }
    
    /**
     This method is used for handling Error response of an API
     
     - parameter errorResponse: NSError Object contains error info
     */
    
    func onError(errorResponse: NSError) ->Void {
        // Simple Error Handling in our Simple Error Hnadling Class
        self.delegate?.servicesManagerError!(error:
            (self.errorInterceptor?.getErrorString(error: errorResponse, errorResolver: errorResolver!))!)
        
    }
    
}

