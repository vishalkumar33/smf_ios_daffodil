//
//  LikePopUp.swift
//  SMF
//
//  Created by Jenkins on 2/17/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
protocol  ActionDelegate {
    func likeTapped(_ b:UIButton)
    func dislikeTapped(_ b:UIButton)
    func peaceTapped(_ b:UIButton)
}
protocol LikePopUpDelegate{
    func closePopUp();
    
}

class LikePopUp:UIView,LikePopUpDelegate
{
    var actionDelegate:ActionDelegate!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var dislikeImageView: UIImageView!
    @IBOutlet weak var peaceImageView: UIImageView!
    
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var dislikeCountLabel: UILabel!

    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var peaceCountLabel: UILabel!
    
    @IBOutlet weak var peaceButton: UIButton!
    @IBOutlet weak var dislikeButton: UIButton!
    var delegate : LikePopUpDelegate?
    override func awakeFromNib() {
        delegate = self
    }
    
    func closePopUp(){
        self.removeFromSuperview()
        print("close popup")
    }
    @IBAction func likePopUpAction(_ sender: Any) {
    }
    
    @IBAction func likeAction(_ sender: Any) {
        self.actionDelegate.likeTapped(sender as! UIButton)

    }
    @IBAction func dislikeAction(_ sender: Any) {
        self.actionDelegate.dislikeTapped(sender as! UIButton)
    }
    
    @IBAction func peaceAction(_ sender: Any) {
        self.actionDelegate.peaceTapped(sender as! UIButton)

    }
    
    
}
