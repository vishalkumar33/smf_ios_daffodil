//
//  SMFHomePresenter.swift
//  SMF
//
//  Created by Jenkins on 1/4/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

protocol SMFHomeViewDelegate:SMFCommonViewDelegate{
    func updateTable(newsFeedArray:[Post], totalCount:Int)
    func updateUI()
    func updateTableComment(commentArray:[Comment])
    func updateCountLike()
    func updateCountDislike()
    func updateCountPeace()
    func getCommentList()
    func refreshData()
}

class SMFHomePresenter: ResponseCallback {
        
    private var homeViewDelegate    : SMFHomeViewDelegate?
    private var homeService = HomeService()
    
    var serviceStatus:Int?

    init(delegate : SMFHomeViewDelegate){
        homeViewDelegate = delegate

    }
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        self.homeViewDelegate?.hideLoader!()
        if serviceStatus == 1 {
            //like
            homeViewDelegate?.updateCountLike()
        }
        
        if serviceStatus == 5 {
            //like
            homeViewDelegate?.updateCountDislike()
        }
        
        if serviceStatus == 6 {
            //like
            homeViewDelegate?.updateCountPeace()
        }
        if serviceStatus == 3{
            homeViewDelegate?.getCommentList()
        }
        
        if serviceStatus == 4 {
            homeViewDelegate?.refreshData()
        }
    }
    

    func servicesManagerError(error : String){
        homeViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        self.homeViewDelegate?.hideLoader!()
    }
    
    func getNewsFeed(skip:Int,lat:String, long:String, choice:String?){
        homeViewDelegate?.showLoader!()
        var userChoice:String?
        if choice == "" {
            
        }else if choice == "Nearby" {
            userChoice = "isNearby"
        }else if choice == "Favourites" {
            userChoice = "isFavourite"
        }else if choice == "Trending" {
            userChoice = "isTrending"
        }
        
        Service.getNewFeed(lat, skip: skip, choice: userChoice, long: long, success: { (response, count) in
            self.homeViewDelegate?.hideLoader!()
            print(response)
            self.parseNewsFeed(feeds: response, totalCount:  count)
           
        }) { (error) in
            print(error!)
        }
        
    }
    
    func parseNewsFeed(feeds:NSArray, totalCount:Int){
        var feedArray:[Post] = [Post]()
        if feeds.count > 0 {
            let result = feeds
            print("\(result)")
            for type in result {
                if let dict = type as? NSDictionary {
                    let obj  = Post(response: dict)
                    feedArray.append(obj)
                }
            }
            
        }
        self.homeViewDelegate?.updateTable(newsFeedArray: feedArray, totalCount:  totalCount)
    }
    
    
    
    func getCommentList(postId:String)
    {
        homeViewDelegate?.showLoader!()
        Service.getCommentList(postId, success:  { (response) in
            self.homeViewDelegate?.hideLoader!()
            print(response)
            self.parseCommentList(feeds: response)
            
        }) { (error) in
            print(error!)
        }
    }
    
    func dictForReport(postId:String) -> NSMutableDictionary{
        let dic:NSMutableDictionary = NSMutableDictionary()
        dic["postId"] = postId
        return dic
    }
    
    
    func confirmReport(postId:String){
        self.homeViewDelegate?.showLoader!()
        Service.Report(data: dictForReport(postId: postId), success: {_ in 
            self.homeViewDelegate?.hideLoader!()
            
            self.homeViewDelegate?.showErrorAlert!(alertTitle: "Success", alertMessage: "This post has been reported")
            
        }) { (error) in
            self.homeViewDelegate?.hideLoader!()
        }
    }

    func parseCommentList(feeds:NSArray){
        var feedArray:[Comment] = [Comment]()
        if feeds.count > 0 {
            let result = feeds
            print("\(result)")
            for type in result {
                if let dict = type as? NSDictionary {
                    let obj  = Comment(response: dict)
                    feedArray.append(obj)
                }
            }
        }
        self.homeViewDelegate?.updateTableComment(commentArray: feedArray)
    }

    func HidePostButtonClicked(postId:String) ->Void
    {
        self.serviceStatus = 4
        
        //Showing Loader
        homeViewDelegate?.showLoader!()
        self.homeService.performHidePutWithValidInput(inputData: HomeRequestModel.Builder().setpostId(postId: postId).addRequestHeader(key:"Content-Type" , value: "application/json").build(), homeDelegate: self)
        
    }
    func postButtonClickedWithPost(newComment:String,postId:String) ->Void
    {
        self.serviceStatus = 3
        if newComment.isEmpty {
            homeViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: "Please post something")
            return
        }
        homeViewDelegate?.showLoader!()
        let trimmedComment = newComment.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        print(trimmedComment)
        self.homeService.performPostWithValidInput(inputData: HomeRequestModel.Builder().setComment(content: trimmedComment).setpostId(postId: postId).addRequestHeader(key:"Content-Type", value: "application/json").build(), homeDelegate: self)
        
    }
    
    
    func blockUserClicked(blockUserId:String) ->Void
    {
        self.serviceStatus = 2
        //Showing Loader
        homeViewDelegate?.showLoader!()
        self.homeService.performMemberPutWithValidInput(inputData: HomeRequestModel.Builder().setBlockUserId(blockUserId: blockUserId).addRequestHeader(key:"Content-Type" , value: "application/json").build(), homeDelegate: self)
        
    }

    func tappedRelateToFlame(postId:String,action:String,content:String) ->Void
    {
        if action == "like" {
            self.serviceStatus = 1
        }
        if action == "dislike" {
            self.serviceStatus = 5
        }
        if action == "peace" {
            self.serviceStatus = 6
        }
        
        //Showing Loader
        homeViewDelegate?.showLoader!()
        self.homeService.performFlamePutWithValidInput(inputData: HomeRequestModel.Builder().setpostId(postId: postId).setAction(action:action).setComment(content:content).addRequestHeader(key:"Content-Type" , value: "application/json").build(), homeDelegate: self)
        
    }
    
    func updateUI(){
        self.homeViewDelegate?.updateUI()
    }

    func showPopUp(_ sender: AnyObject){

    }
        
}
