//
//  SMFHomeViewController.swift
//  SMF
//
//  Created by Shivam Srivastava on 22/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit
import SidePanel
import BTNavigationDropdownMenu
import AFNetworking
import Crashlytics
protocol  homeSaveValue {
    func saveIndex(index:Int)
}


class SMFHomeViewController: SMFBaseViewController,UIGestureRecognizerDelegate{
    var delegate : homeSaveValue!
    var value :Int?
    @IBOutlet weak var whatsOmindBtn: UIButton!
    @IBOutlet weak var tableView :      UITableView!
    var homePresenter            :      SMFHomePresenter!
    var isHome : Bool = true
    var menuView                 :      BTNavigationDropdownMenu!
    var feedArray                :      [Post]                          =       [Post]()
    //var count :[Int]
    var skip = 0
    var hasMoreData = false
    var refreshControl:UIRefreshControl?
    var isRefreshedData = false
   // var postObj : Post?
    var flameView :LikePopUp!
    var flag = 0
    var likeCount = 0
    var dislikeCount = 0
    var peaceCount = 0
    
    var totalCount = 0
    
     let kSeparatorId = 123
    let kSeparatorHeight: CGFloat = 10
    
    var selectedLat:Double?
    var selectedLong:Double?
    var selectedChoide:String?
    
    @IBOutlet weak var topContraintTableView: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = false
        self.homePresenter = SMFHomePresenter(delegate: self)
        homePresenter.updateUI()
        if self.isHome
        {
             self.whatsOmindBtn.isHidden = false
            getNewsFeed()
        }
        else
        {
            self.whatsOmindBtn.isHidden = true
            self.topContraintTableView.constant = 8
            getHistoryNewsFeed()
        }
        initRefreshControl()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.register(SMFHistoryTextTableViewCell.self)
        self.tableView.register(SMFHistoryImageTableViewCell.self)
        self.tableView.register(SMFHistoryVideoTableViewCell.self)
        self.tableView.register(SMFTextTableViewCell.self)
        self.tableView.register(SMFDisapperingPost.self)
        self.tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "SMFLoaderTableViewCell",bundle: nil), forCellReuseIdentifier: "SMFLoaderTableViewCell")
        self.tableView.showsVerticalScrollIndicator = false
        NotificationCenter.default.addObserver(self, selector: #selector(SMFHomeViewController.methodOfReceivedNotification), name: NSNotification.Name(rawValue: "tableReload"), object: nil)
    }
    
    func handleSwipe(gesture: UISwipeGestureRecognizer) {
        print("swipe")
    }
    
    func initForCrash(){
        let button = UIButton(type: UIButtonType.roundedRect)
        button.frame = CGRect(x:20, y:50, width:100, height:30)
        button.setTitle("Crash", for: UIControlState.normal)
        button.addTarget(self, action: #selector(SMFHomeViewController.crashButtonTapped), for: UIControlEvents.touchUpInside)
        view.addSubview(button)

    }
    
    @IBAction func crashButtonTapped() {
        Crashlytics.sharedInstance().crash()
    }

    
    func initRefreshControl(){
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.backgroundColor = UIColor.white
        self.refreshControl!.addTarget(self, action: #selector(SMFHomeViewController.refreshTable), for: .valueChanged)
        self.tableView.addSubview(refreshControl!)

    }
    
    func refreshTable(){
        if (self.refreshControl != nil) {
            let formatter: DateFormatter = DateFormatter()
            formatter.dateFormat = "MMM d, h:mm a"
            let title: String = "Last update: \(formatter.string(from: Date()))"
            let attrsDictionary: [String : AnyObject] = [
                NSForegroundColorAttributeName : UIColor.black
            ]
            let attributedTitle: NSAttributedString = NSAttributedString(string: title, attributes: attrsDictionary)
            self.refreshControl!.attributedTitle = attributedTitle
            self.refreshControl!.endRefreshing()
        }
        self.skip = 0
        self.hasMoreData = false
        self.isRefreshedData = true
        getNewsFeed()
    }
    func sharePostPush(index: Int) {
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))
        let sharePostController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFSharePostViewController") as! SMFSharePostViewController
        sharePostController.postView = getAppropriateCell(cell: cell!)
        sharePostController.postObj = feedArray[index]
        if selectedLat != nil {
            sharePostController.selectLat = self.selectedLat
        }else {
            sharePostController.selectLat = 28
        }
        if selectedLong != nil {
            sharePostController.selectLong = self.selectedLong
        }else{
            sharePostController.selectLong = 77
        }
        
        self.navigationController?.pushViewController(sharePostController, animated: true)

    }
    func getNewsFeed() {
        
        if AFNetworkReachabilityManager.shared().isReachable{
            if selectedLat != nil {
                self.homePresenter.getNewsFeed(skip: self.skip, lat: String(describing: self.selectedLat!), long: String(describing: self.selectedLong!), choice: self.selectedChoide!)
            }else{
                self.homePresenter.getNewsFeed(skip: self.skip, lat: "27", long: "28", choice: self.selectedChoide)
            }
            
        }
        else{
            self.getNewFeedsNetworkHandler()
        }
        
    }
    
    
    func getNewFeedsNetworkHandler(){
        AFNetworkReachabilityManager.shared().setReachabilityStatusChange { (status: AFNetworkReachabilityStatus) -> Void in
            switch status {
            case .notReachable:
                self.showErrorAlert(alertTitle: "Error", alertMessage: "Unable to connect to internet")
                print("Not reachable")
            case .reachableViaWiFi, .reachableViaWWAN:
                if self.selectedLat != nil {
                    self.homePresenter.getNewsFeed(skip: self.skip, lat: String(describing: self.selectedLat!), long: String(describing: self.selectedLong!), choice: self.selectedChoide!)
                }else{
                    self.homePresenter.getNewsFeed(skip: self.skip, lat: "27", long: "28", choice: self.selectedChoide)
                }
                print("Reachable")
            case .unknown:
                self.showErrorAlert(alertTitle: "Error", alertMessage: "Unable to connect to internet")
                print("Unknown")
            }
        }
    }
    
    
    func methodOfReceivedNotification(){
        tableView.reloadData()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    //for pop Over button
    @IBAction func popUpButton(_ sender: AnyObject) {
        
    }
 
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func postUpdateAction(_ sender: AnyObject) {
        
    }
 
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false

    }
    
    
    
    @IBAction func whatsOnMindAction(_ sender: Any) {
        let postAndUpdateController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFPostUpdateViewController") as! SMFPostUpdateViewController
        MyPostState.isPostSelected = false

        self.navigationController?.pushViewController(postAndUpdateController, animated: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false

    }
}



extension SMFHomeViewController:UITableViewDelegate,UITableViewDataSource {
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hasMoreData == true{
            return feedArray.count + 1
        }
        else{
            return feedArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) ->
        CGFloat {
            
            return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if  (indexPath as NSIndexPath).row < self.feedArray.count{
            return 450
        }
        else{
            return 50
        }
    }
   
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath as NSIndexPath).row < feedArray.count {
          let postObj = feedArray[indexPath.row]
            print(postObj.id)
            if postObj.videoPath != "" && postObj.hidden == false {
                if postObj.content != "" {
                    let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFHistoryVideoTableViewCell
                  cell.videoDelegate = self
                    cell.setCell1(post: postObj, indexPath: indexPath.row)
                  
                    
                    cell.selectionStyle = .none
                    cell.contentView.removeFromSuperview()
                    return cell
                }else{
                    return UITableViewCell()
                }
            }else if postObj.imagePath != "" && postObj.hidden == false {
                if postObj.content != "" {
                    let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFHistoryImageTableViewCell
                    cell.imageDelegate = self
                       cell.countLabel.text = String(postObj.likedBy.count + postObj.dislikedBy.count + postObj.peaceBy.count)
                    //   cell.btnPopup.addTarget(self, action: #selector(SMFHomeViewController.showPopUp(_:)), for: .touchUpInside)
                    cell.setCell1(post: postObj, indexPath: indexPath.row)
                    cell.selectionStyle = .none
                    cell.contentView.removeFromSuperview()
                    return cell
                }else{
                    return UITableViewCell()
                }
            }else if postObj.hidden == true {
                if postObj.content != "" {
                    let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFDisapperingPost
                    
                    //   cell.btnPopup.addTarget(self, action: #selector(SMFHomeViewController.showPopUp(_:)), for: .touchUpInside)
                    cell.selectionStyle = .none
                    cell.contentView.removeFromSuperview()
                    return cell
                }else{
                    return UITableViewCell()
                }
            }
             else{
                if postObj.content != "" && postObj.hidden == false {
                    let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFTextTableViewCell
                    cell.countLabel.text = String(postObj.likedBy.count + postObj.dislikedBy.count + postObj.peaceBy.count)
                    cell.flamePopUp.tag = indexPath.row
                    cell.hideDelegate = self
                    cell.setCell(post: postObj, indexPath: indexPath.row)
                    cell.selectionStyle = .none
                    cell.contentView.removeFromSuperview()
                    return cell
                }else{
                    return UITableViewCell()
                }
            }
        } else if hasMoreData == true{
            let cell: SMFLoaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SMFLoaderTableViewCell")! as! SMFLoaderTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }else {
            let cell:UITableViewCell = UITableViewCell()
            cell.backgroundColor = UIColor.clear
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if  (indexPath as NSIndexPath).row == self.feedArray.count - 1 && hasMoreData == true{
            self.getNewsFeed()
        }
        if cell.viewWithTag(kSeparatorId) == nil //add separator only once
        {
            let separatorView = UIView(frame: CGRect(x: 0, y: cell.frame.height - kSeparatorHeight, width: cell.frame.width, height: kSeparatorHeight))
            separatorView.tag = kSeparatorId
            separatorView.backgroundColor = UIColor.white
            separatorView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            cell.addSubview(separatorView)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SMFCommentViewController") as! SMFCommentViewController
        vc.delegate = self
       let postObj = feedArray[indexPath.row]
        if postObj.hidden == true{
            feedArray.remove(at: indexPath.row)
            tableView.reloadData()
        }
    
        tableView.reloadData()
        vc.postView = getAppropriateCell(cell: cell!)
        vc.postObjFeeds = feedArray[indexPath.row]
        
        let navigationController = UINavigationController(rootViewController: vc)
        self.present(navigationController, animated: true, completion: nil)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func getAppropriateCell(cell:UITableViewCell)->UINib
    {
        
        if let _:SMFTextTableViewCell = cell as? SMFTextTableViewCell{
            let nib = UINib(nibName: "SMFTextTableViewCell", bundle: nil)
            return nib
        }else if let _ :SMFHistoryVideoTableViewCell = cell as? SMFHistoryVideoTableViewCell
        {
            let nib = UINib(nibName: "SMFHistoryVideoTableViewCell", bundle: nil)
            return nib
        }
        else if let _ :SMFHistoryImageTableViewCell = cell as? SMFHistoryImageTableViewCell
        {
            let nib = UINib(nibName: "SMFHistoryImageTableViewCell", bundle: nil)
            return nib
        }
        else if let _ :SMFDisapperingPost = cell as? SMFDisapperingPost
        {
            let nib = UINib(nibName: "SMFTextTableViewCell", bundle: nil)
            return nib
        }
        return UINib()
    }
}



extension SMFHomeViewController: SMFHomeViewDelegate, SMFHistoryTextTableViewCellDelegate,HidePostDelegate,ActionDelegate,ReportPopUpDelegate, SMFHistoryImageTableViewCellDelegate,SMFHistoryVideoTableViewCellDelegate {
     func confirmDelegate(_ b: UIButton) {
        //self.homePresenter.confirmReport(postId: (postObj.id)!)
    }
    func getCommentList() {
        
    }
    
    func refreshData() {
        self.refreshTable()
    }

    func updateUI() {
        self.navigationController?.navigationBar.isHidden = false
        tableView.estimatedRowHeight = 300
        setNavBar()
        if isHome
        {
        dropDown()
            self.navigationItem.titleView = menuView
        }
        
    }
    func reportPost(index: Int) {
        let reportView = Bundle.main.loadNibNamed("ReportPopUp", owner: self, options: nil)?[0] as! ReportPopUp
        reportView.frame = UIScreen.main.bounds
        reportView.confirm = self
        let window = UIApplication.shared.keyWindow!
        window.addSubview(reportView)
        let postobj1 = feedArray[index]
        print(postobj1.id)
    }

    
    func updateTableComment(commentArray: [Comment]) {
    
    }
    func likeTapped(_ b: UIButton) {
        let postObj = self.feedArray[b.tag]
        let likeCount = postObj.likedBy.count
        if postObj.likedBy.contains( UserDefault.string(forKey: "userId")!)
        {
            postObj.likedBy.remove(UserDefault.string(forKey: "userId")!)
              self.flameView.likeImageView.image = #imageLiteral(resourceName: "thumbsup")
        }
        else
        {
            postObj.likedBy.add(UserDefault.string(forKey: "userId")!)
            self.flameView.likeImageView.image = #imageLiteral(resourceName: "like")
        }
        
        self.flameView.likeCountLabel.text = String(postObj.likedBy.count)
       self.tableView.reloadData()
        self.homePresenter.tappedRelateToFlame(postId: (postObj.id), action: "like", content: "")
    }
    func showLoader() {
        self.view.showLoader()
    }
    func popHome() {
        
    }
    func dislikeTapped(_ b: UIButton) {
        let postObj = self.feedArray[b.tag]
        let disLikeCount = postObj.dislikedBy.count
        if postObj.dislikedBy.contains( UserDefault.string(forKey: "userId")!)
        {
            postObj.dislikedBy.remove(UserDefault.string(forKey: "userId")!)
            self.flameView.dislikeImageView.image = #imageLiteral(resourceName: "dislike")
        }
        else
        {
            postObj.dislikedBy.add(UserDefault.string(forKey: "userId")!)
              self.flameView.dislikeImageView.image = #imageLiteral(resourceName: "thumbsdown")
        }
        var cell = tableView.cellForRow(at: IndexPath(row: b.tag, section: 0))
        if (cell?.isKind(of: SMFTextTableViewCell.self))!
        {
        let newcell = cell as! SMFTextTableViewCell
              newcell.countLabel.text = String(postObj.likedBy.count+postObj.dislikedBy.count+postObj.peaceBy.count)
        }
        else if (cell?.isKind(of: SMFHistoryImageTableViewCell.self))!
        {
             let newcell = cell as! SMFHistoryImageTableViewCell
              newcell.countLabel.text = String(postObj.likedBy.count+postObj.dislikedBy.count+postObj.peaceBy.count)
        }
        else
        {
             let newcell = cell as! SMFHistoryVideoTableViewCell
              newcell.countLabel.text = String(postObj.likedBy.count+postObj.dislikedBy.count+postObj.peaceBy.count)
        }
        self.flameView.dislikeCountLabel.text = String(postObj.dislikedBy.count)
      
        self.homePresenter.tappedRelateToFlame(postId: (postObj.id), action: "dislike", content: "")

    }
    func peaceTapped(_ b: UIButton) {
         let postObj = self.feedArray[b.tag]
        let likeCount = postObj.peaceBy.count
        if postObj.peaceBy.contains( UserDefault.string(forKey: "userId")!)
        {
            postObj.peaceBy.remove(UserDefault.string(forKey: "userId")!)
            self.flameView.peaceImageView.image = #imageLiteral(resourceName: "win")

        }
        else
        {
            postObj.peaceBy.add(UserDefault.string(forKey: "userId")!)
            flameView.peaceImageView.image = #imageLiteral(resourceName: "peaceBlue")
        }
        let cell = tableView.cellForRow(at: IndexPath(row: b.tag, section: 0)) as! SMFTextTableViewCell
        self.flameView.peaceCountLabel.text = String(postObj.peaceBy.count)
        cell.countLabel.text = String(postObj.likedBy.count+postObj.dislikedBy.count+postObj.peaceBy.count)

    }
    
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func blockUser() {
    
       // self.homePresenter.blockUserClicked(blockUserId: (postObj.author.id)!)
    }
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    func flamePopUpLoad(index:Int) {
        let postObj = self.feedArray[index]
       // tableView.addGestureRecognizer(swipeRightGesture)
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! SMFTextTableViewCell
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let flamePopup = cell.flamePopUp!
        flameView = Bundle.main.loadNibNamed("LikePopUp", owner: self, options: nil)?[0] as! LikePopUp
        flameView.frame = CGRect(x: flamePopup.frame.minX, y: flamePopup.frame.minY, width: 260, height: 49)
        flameView.actionDelegate = self
         flameView.likeCountLabel.text = String(postObj.likedBy.count)
         flameView.dislikeCountLabel.text = String(postObj.dislikedBy.count)
         flameView.peaceCountLabel.text = String(postObj.peaceBy.count)
        
        if postObj.likedBy.contains( UserDefault.string(forKey: "userId")!)
        {
           flameView.likeImageView.image = #imageLiteral(resourceName: "like")
        }
        else
        {
            flameView.likeImageView.image = #imageLiteral(resourceName: "thumbsup")
        }
        if postObj.dislikedBy.contains( UserDefault.string(forKey: "userId")!)
        {
           flameView.dislikeImageView.image = #imageLiteral(resourceName: "thumbsdown")
        }
        else
        {
            flameView.dislikeImageView.image = #imageLiteral(resourceName: "dislike")
                    }
        if postObj.peaceBy.contains( UserDefault.string(forKey: "userId")!)
        {
            flameView.peaceImageView.image = #imageLiteral(resourceName: "peaceBlue")
        }
        else
        {
            flameView.peaceImageView.image = #imageLiteral(resourceName: "win")

        }
        
        flameView.likeButton.tag = index
        flameView.dislikeButton.tag = index
        flameView.peaceButton.tag = index
        value = index
        cell.addSubview(flameView)
        tableView.bringSubview(toFront: cell)
        flag = 1
    }
    func flamePopUpLoadImage(index:Int) {
        
        let postObj = self.feedArray[index]
        // tableView.addGestureRecognizer(swipeRightGesture)
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! SMFHistoryImageTableViewCell
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let flamePopup = cell.flamePopUp!
        flameView = Bundle.main.loadNibNamed("LikePopUp", owner: self, options: nil)?[0] as! LikePopUp
        flameView.frame = CGRect(x: flamePopup.frame.minX, y: flamePopup.frame.minY, width: 260, height: 49)
        flameView.actionDelegate = self
        flameView.likeCountLabel.text = String(postObj.likedBy.count)
        flameView.dislikeCountLabel.text = String(postObj.dislikedBy.count)
        flameView.peaceCountLabel.text = String(postObj.peaceBy.count)
        
        if postObj.likedBy.contains( UserDefault.string(forKey: "userId")!)
        {
            flameView.likeImageView.image = #imageLiteral(resourceName: "like")
        }
        else
        {
            flameView.likeImageView.image = #imageLiteral(resourceName: "thumbsup")
        }
        if postObj.dislikedBy.contains( UserDefault.string(forKey: "userId")!)
        {
            flameView.dislikeImageView.image = #imageLiteral(resourceName: "thumbsdown")
        }
        else
        {
            flameView.dislikeImageView.image = #imageLiteral(resourceName: "dislike")
        }
        if postObj.peaceBy.contains( UserDefault.string(forKey: "userId")!)
        {
            flameView.peaceImageView.image = #imageLiteral(resourceName: "peaceBlue")
        }
        else
        {
            flameView.peaceImageView.image = #imageLiteral(resourceName: "win")
            
        }
        
        flameView.likeButton.tag = index
        flameView.dislikeButton.tag = index
        flameView.peaceButton.tag = index
        value = index
        cell.addSubview(flameView)
        tableView.bringSubview(toFront: cell)
        flag = 1
    }
    func flamePopUpLoad1(index:Int) {
        let baseView = UIView()
        baseView.backgroundColor = UIColor.red
        baseView.frame = self.view.bounds
        //  tableView.backgroundView = baseView
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(gesture:)))
        // tableView.addGestureRecognizer(swipeRightGesture)
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! SMFHistoryImageTableViewCell
        cell.setNeedsLayout()
        
        cell.layoutIfNeeded()
        let flamePopup = cell.flamePopUp!
        flameView = Bundle.main.loadNibNamed("LikePopUp", owner: self, options: nil)?[0] as! LikePopUp
        flameView.frame = CGRect(x: flamePopup.frame.minX, y: flamePopup.frame.minY, width: 260, height: 49)
        flameView.actionDelegate = self
        flameView.likeCountLabel.text = String(likeCount)
        flameView.dislikeCountLabel.text = String(dislikeCount)
        flameView.peaceCountLabel.text = String(peaceCount)
        value = index
        cell.addSubview(flameView)
        tableView.bringSubview(toFront: cell)
        flag = 1
    }
    

    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if (flag == 1){
        flameView.closePopUp()
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView){
        if (flag == 1){
            flameView.closePopUp()
        }
    }
    func test(){
        flameView.delegate?.closePopUp()
    }
    
    func pushCommentPage(index:Int)
    {
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SMFCommentViewController") as! SMFCommentViewController
        let navigationController = UINavigationController(rootViewController: vc)

        self.present(navigationController, animated: true, completion: nil)
    //    self.delegate.saveIndex(index: index)
        MyPostId.isPost = index
        vc.postView = getAppropriateCell(cell: cell!)
        MyPostId.postId = feedArray[index].id
        vc.postObjFeeds = feedArray[index]

    }
    
    func hidePost(index: Int) {
        let postobj1 = feedArray[index]
        print(postobj1.id)
        self.homePresenter.HidePostButtonClicked(postId: (postobj1.id) )
    }
    
    func loadPlaces() -> [SavedLocation]?{
        let placesData = UserDefaults.standard.object(forKey: "places") as? NSData
        
        if let placesData = placesData {
            let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as? [SavedLocation]
            
            if let placesArray = placesArray {
                return placesArray
            }
            
        }
        return nil
    }
    
    
    func dropDown()
    {
        var saveLocationArray:[SavedLocation] = [SavedLocation]()
        let city = UserDefault.string(forKey: "userCity")
        var items = ["Local (\(city!))", "Nearby", "Favourites", "Trending"]
        if let locationArray = loadPlaces() {
            saveLocationArray = locationArray
            for location in locationArray {
                items.append(location.name!)
            }
        }
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 249.0/255.0,
                                                                        green:249.0/255.0,
                                                                        blue:250.0/255.0,
                                                                        alpha: 1.0)
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController,
                                            containerView: self.navigationController!.view,
                                            title: "Local (\(city!))",
                                            items: items as [AnyObject])
        menuView.cellHeight = 40
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor.appBlueBorderColor
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor(red: 136.0/255.0,
                                              green: 134.0/255.0,
                                              blue: 134.0/255.0,
                                              alpha: 0.8)
        menuView.cellTextLabelFont = UIFont(name: "Quicksand-Medium",
                                            size: 14)
        menuView.cellTextLabelAlignment = .left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            print("Did select item at index: \(indexPath)")
            if indexPath > 3 {
                let location = saveLocationArray[indexPath-4]
                self.selectedLat = location.latLng?.firstObject as? Double
                self.selectedLong = location.latLng?.lastObject as? Double
                UserDefault.set(self.selectedLat, forKey: "selectedLat")
                UserDefault.set(self.selectedLong, forKey: "selectedLong")
            }else{
                if indexPath == 0 {
                    
                }else if indexPath == 1 {
                    self.selectedChoide = "Nearby"
                }else if indexPath == 2 {
                    self.selectedChoide = "Favourites"
                }else if indexPath == 3 {
                    self.selectedChoide = "Trending"
                }
            }
            self.getNewsFeed()
        }
        
    }
    
    func popUpButtonTapped(_ sender: AnyObject) {
      //  homePresenter.showPopUp(sender)
    }
    
    func  setNavBar(){
        if isHome
        {
        self.setNav(title: "Home")
        setRightNavBarButton()
        }
        else
        {   setLeftNavBarButton()
             self.setNav(title: "History")
        
        }
    }

    
    func  setRightNavBarButton(){
        let clearBtn = UIButton(type: .custom)
        clearBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 30)
        clearBtn.setImage(#imageLiteral(resourceName: "homeSearch"), for: .normal)
        let item1 = UIBarButtonItem(customView: clearBtn)
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        clearBtn.addTarget(self, action: #selector(SMFHomeViewController.searchTapped), for: UIControlEvents.touchUpInside)
    }
    
    
    func searchTapped()
    {
        let searchViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFSearchViewController") as! SMFSearchViewController
        self.navigationController?.pushViewController(searchViewController, animated: true)
    }
    
    func updateTable(newsFeedArray: [Post], totalCount: Int) {
        if isRefreshedData == true {
            self.feedArray.removeAll(keepingCapacity: false)
            isRefreshedData = false
        }
        self.feedArray.append(contentsOf: newsFeedArray)
        if totalCount > self.feedArray.count {
            hasMoreData = true
            skip = skip + 10
        }else {
            hasMoreData = false
        }
        tableView.reloadData()
    }
    
    
    func updateCountPeace() {
//        let count = postObj.peaceBy.count
//        peaceCount = count! + 1
//        flameView.peaceCountLabel.text = String(peaceCount)
    }
    
    func updateCountLike() {
//        let count = postObj.likedBy.count
//        likeCount = count! + 1
//        flameView.likeCountLabel.text = String(likeCount)
    }
    
    func updateCountDislike() {
//        let count = postObj.dislikedBy.count
//        dislikeCount = count! + 1
//        flameView.dislikeCountLabel.text = String(dislikeCount)
    }
    func getHistoryNewsFeed(){
        self.showLoader()
        let authorId = UserDefault.value(forKey: "userId") as! String
        Service.getHistoryDetail(author_id: authorId, success:{ (response) in
            print(response)
            self.hideLoader()
            self.parseHistoryNewsFeed(response: response )
        }) { (error) in
            self.hideLoader()
            print(error!)
        }
    }
    func parseHistoryNewsFeed(response:NSDictionary){
        
        let data:NSDictionary  = response
        let count:Int = data["totalfeedscount"] as! Int
        let feeds = data["result"] as! NSArray
        
        var feedArray:[Post] = [Post]()
        if feeds.count > 0 {
            let result = feeds
            print("\(result)")
            for type in result {
                if let dict = type as? NSDictionary {
                    let obj  = Post(response: dict)
                    feedArray.append(obj)
                }
            }
        }
        self.feedArray = feedArray
        tableView.reloadData()
    }
    override func setLeftNavBarButton(){
        let backBtn = UIButton(type: .custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 18, height: 18)
        backBtn.setImage(#imageLiteral(resourceName: "backIcon"), for: .normal)
        backBtn.addTarget(self, action: #selector(SMFHomeViewController.addTapped), for: UIControlEvents.touchUpInside)
        let back = UIBarButtonItem(customView: backBtn)
        self.navigationItem.setLeftBarButton(back, animated: true)
    }

}



extension SMFHomeViewController:addCommentDelegate {
    func updateTable() {
        self.tableView.reloadData()
    }
}









