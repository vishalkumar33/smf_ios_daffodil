//
//  SMFCommentTableViewCell.swift
//  SMF
//
//  Created by Jenkins on 2/18/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var flamePopUpImageView: UIImageView!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var noOfCountLabel: UILabel!
    @IBOutlet weak var CommentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
