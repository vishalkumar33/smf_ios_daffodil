//
//  SMFPopUpTableViewCell.swift
//  SMF
//
//  Created by Shivam Srivastava on 03/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFPopUpTableViewCell: UITableViewCell {

    
    @IBOutlet weak var popUpLabel: UILabel!
    
    @IBOutlet weak var popUpImageView: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
