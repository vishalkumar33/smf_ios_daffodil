//
//  SMFTableViewCell.swift
//  SMF
//
//  Created by Shivam Srivastava on 22/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

class SMFTableViewCell: UITableViewCell {

    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var postedImage: UIImageView!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var commentTextField: UITextField!
    
    @IBAction func postAction(_ sender: AnyObject) {
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        profileImage.layer.cornerRadius = 25.0
        profileImage.clipsToBounds = true
        
    }

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

     
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
