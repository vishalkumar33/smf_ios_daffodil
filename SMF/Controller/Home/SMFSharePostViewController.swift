//
//  SMFSharePostViewController.swift
//  SMF
//
//  Created by Jenkins on 2/20/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFSharePostViewController: SMFBaseViewController,SharePostDelegate1 {
    var postObj : Post?
    @IBOutlet weak var captionTextView: SMFPlaceholderTextView!
    var postView: UINib = UINib()
    var sharePostPresenter                      : SharePostPresenter?
    var heightXib = 0
    @IBOutlet weak var tableView: UITableView!
    var selectLat:Double?
    var selectLong:Double?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.sharePostPresenter = SharePostPresenter(delegate: self)
        setNav(title: "Share Post")
        setLeftNavBarButton()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.layer.borderWidth = 1.0
        tableView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
   
    }
    
    
    

    @IBAction func sharePostAction(_ sender: Any) {
        self.sharePostPresenter?.postButtonClickedWithPost(newPost: self.captionTextView.text, lat: "\(selectLat)", long: "\(selectLong)", postId: (postObj?.id)!)
    }
    
   
    func sharePost(_ b: UIButton) {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "SMFSharePostTableViewCell", for: IndexPath(row: 0, section: 0)) as! SMFSharePostTableViewCell
            self.sharePostPresenter?.postButtonClickedWithPost(newPost: self.captionTextView.text!, lat: "\((UserDefault.double(forKey: "selectedLat")))", long: "\((UserDefault.double(forKey: "selectedLong")))", postId: (postObj?.id)!)
    }
    
}
extension SMFSharePostViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SMFSharePostTableViewCell", for: indexPath) as! SMFSharePostTableViewCell
        let view = postView.instantiate(withOwner: self, options: nil)[0] as! UIView
        if view is  SMFHistoryImageTableViewCell{
 
            (view as! SMFHistoryImageTableViewCell).commentsCount.setTitle(String(describing: postObj?.commentCount), for: .normal)
            (view as! SMFHistoryImageTableViewCell).countLabel.text = String((postObj?.likedBy.count)!+(postObj?.dislikedBy.count)!+(postObj?.peaceBy.count)!)
            (view as! SMFHistoryImageTableViewCell).lblContent.text = postObj?.content
            
            (view as! SMFHistoryImageTableViewCell).lblUserName.text = "\((postObj?.author.profile.firstName.capitalized)!) \((postObj?.author.profile.lastName.capitalized)!)"
            (view as! SMFHistoryImageTableViewCell).imgUser.sd_setImage(with: URL(string: (postObj?.author.profile.profileImage)!))
            (view as! SMFHistoryImageTableViewCell).lblTime.text = TableViewHelper.DateToString(date: TableViewHelper.stringToDate(date: (postObj?.createdAt)!))
            (view as! SMFHistoryImageTableViewCell).contentView.isHidden = true
            (view as! SMFHistoryImageTableViewCell).btnPopup.isHidden = true
            (view as! SMFHistoryImageTableViewCell).sharePostbtn.isHidden = true
                
            heightXib = 500//sharePostAction
            
        }
        else if view is  SMFTextTableViewCell{
          
            
            (view as! SMFTextTableViewCell).btnComment.setTitle( "\((postObj?.commentCount)!) Comments", for: .normal)
            (view as! SMFTextTableViewCell).countLabel.text = "\((postObj?.likedBy.count)!+(postObj?.dislikedBy.count)!+(postObj?.peaceBy.count)!)  |"
            
            (view as! SMFTextTableViewCell).detailLabel.text = postObj?.content
            (view as! SMFTextTableViewCell).UserImageView.sd_setImage(with: URL(string: (postObj?.author.profile.profileImage)!))
            (view as! SMFTextTableViewCell).lblUserName.text = "\((postObj?.author.profile.firstName.capitalized)!) \((postObj?.author.profile.lastName.capitalized)!)"
            (view as! SMFTextTableViewCell).lblDateTime.text = TableViewHelper.DateToString(date: TableViewHelper.stringToDate(date: (postObj?.createdAt)!))
            (view as! SMFTextTableViewCell).bottomView.isHidden = true//btnPopup
            (view as! SMFTextTableViewCell).btnPopup.isHidden = true
            (view as! SMFTextTableViewCell).sharePostbtn.isHidden = true
            (view as! SMFTextTableViewCell).sharePostIcon.isHidden = true
            

            heightXib = 250
            
        }
        cell.isUserInteractionEnabled = false
        cell.frame = tableView.bounds
        view.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: cell.contentView.frame.width, height: view.frame.height)
        cell.contentView.addSubview(view)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if(heightXib == 0)
        {
            return 300
        }
        else
        {
            return CGFloat(heightXib)
        }
    }
    
    
}
extension SMFSharePostViewController : SharePostDelegate
{
    func showLoader() {
        self.view.showLoader()
    }
    func hideLoader() {
        self.view.hideLoader()
    }
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
}

