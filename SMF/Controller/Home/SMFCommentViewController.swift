//
//  SMFCommentViewController.swift
//  SMF
//
//  Created by Jenkins on 2/17/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage
protocol addCommentDelegate {
    func updateTable()
}


class SMFCommentViewController: SMFBaseViewController ,SMFHomeViewDelegate{
    @IBOutlet weak var commentViewFor: MyCustomView!

    @IBOutlet weak var tetComment: SMFPlaceholderTextView!
    @IBOutlet weak var tableView: UITableView!
    var homePresenter            :      SMFHomePresenter!
    var postObjFeeds : Post?
    var postid :Int?
    var cellType : String?
    var skip = 0
    var heightXib = 0
    var commentListArray:[Comment] = [Comment]()
    @IBOutlet weak var post: MyCustomButton!
    var postView: UINib = UINib()
    
    var postLink:String?
    
    var delegate:addCommentDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.homePresenter = SMFHomePresenter(delegate: self)
        setNav(title: "Comments")
        setLeftNavBar()
        //secondController.delegate = self
        tableView.layer.borderWidth = 1.0
        tableView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 50
        if postObjFeeds == nil {
            getPostDetail()
        }else{
            homePresenter.getCommentList(postId: (postObjFeeds?.id)!)
        }
        tableView.allowsSelection = false
    }
    
    func getCommentList() {
        tetComment.text = ""
        commentListArray.removeAll()
        self.homePresenter.getCommentList(postId: (postObjFeeds?.id)!)
        tableView.reloadData()
        postObjFeeds?.commentCount = (postObjFeeds?.commentCount)! + 1
        self.delegate?.updateTable()
    }
    
    func getPostDetail(){
        Service.getPostDetail(postLink: postLink!, success: { (post) in
            print(post.id)
            self.postObjFeeds = post
            self.homePresenter.getCommentList(postId: (self.postObjFeeds?.id)!)
        }) { (error) in
            print(error!)
        }
        
    }
    
    func setLeftNavBar(){
        let backBtn = UIButton(type: .custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 18, height: 18)
        backBtn.setImage(#imageLiteral(resourceName: "backIcon"), for: .normal)
        backBtn.addTarget(self, action: #selector(SMFCommentViewController.addTapped1), for: UIControlEvents.touchUpInside)
        let back = UIBarButtonItem(customView: backBtn)
        self.navigationItem.setLeftBarButton(back, animated: true)
    }
    
    func addTapped1()
    {
        self.navigationController?.dismiss(animated: false, completion: nil)
    }
    
    
    func updateUI() {
    }
    
    
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    
    @IBAction func postAction(_ sender: Any) {
        self.homePresenter.postButtonClickedWithPost(newComment:tetComment.text , postId:   (postObjFeeds?.id)!)
        
    }
    
    
    func updateTable(newsFeedArray: [Post], totalCount: Int) {
    }
    
    
    func updateTableComment(commentArray: [Comment]) {
        self.commentListArray.append(contentsOf: commentArray)
        tableView.reloadData()
    }
    
    func updateCountLike() {
        
    }
    func updateCountDislike() {
        
    }
    func updateCountPeace() {
        
    }
    func saveIndex(index: Int) {
        postid = index
    }
    
    func refreshData() {
        
    }
     func sharePostPush(_ sender : UIButton) {
        //let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))
        let sharePostController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFSharePostViewController") as! SMFSharePostViewController
        let nib = UINib(nibName: cellType!, bundle: nil)

        sharePostController.postView = nib//getAppropriateCell(cell: cell!)
        sharePostController.postObj = postObjFeeds
        self.navigationController?.pushViewController(sharePostController, animated: true)
        
    }
    
}
extension SMFCommentViewController:UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if commentListArray.count > 0 {
            tableView.backgroundView?.isHidden = true
            return commentListArray.count
        }
        if commentListArray.count == 0{
          
            tableView.separatorStyle = .none
        }
        return commentListArray.count

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) ->
        CGFloat {
            return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "SMFCommentTableViewCell", for: indexPath) as! SMFCommentTableViewCell
        let postObj = commentListArray[indexPath.row]
        cell.CommentLabel.text = postObj.content
        cell.nameLabel.text = "\((postObj.author.profile.firstName.capitalized)) \((postObj.author.profile.lastName.capitalized))"
        let imageURl = URL(string: postObj.author.profile.profileImage)
        cell.profileImageView.sd_setImage(with: imageURl, placeholderImage: #imageLiteral(resourceName: "henryCavill"),options:.refreshCached,completed: {(image:UIImage?,error:Error?,cacheType:SDImageCacheType,imageURL:URL?) in
            
            cell.profileImageView.clipsToBounds = true
            cell.profileImageView.layer.cornerRadius = cell.profileImageView.frame.height/2
            cell.profileImageView.layer.borderWidth = 1
            cell.profileImageView.layer.masksToBounds = true
            cell.profileImageView.layer.borderColor = UIColor.white.cgColor
        })

        return cell

    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let view = postView.instantiate(withOwner: self, options: nil)[0] as! UIView
        //view.isUserInteractionEnabled = false
        if view is  SMFHistoryImageTableViewCell{
            cellType = "SMFHistoryImageTableViewCell"
            (view as! SMFHistoryImageTableViewCell).commentsCount.setTitle(String(describing: postObjFeeds?.commentCount), for: .normal)
            (view as! SMFHistoryImageTableViewCell).countLabel.text = String((postObjFeeds?.likedBy.count)!+(postObjFeeds?.dislikedBy.count)!+(postObjFeeds?.peaceBy.count)!)
             (view as! SMFHistoryImageTableViewCell).lblContent.text = postObjFeeds?.content
            
            (view as! SMFHistoryImageTableViewCell).lblUserName.text = "\((postObjFeeds?.author.profile.firstName.capitalized)!) \((postObjFeeds?.author.profile.lastName.capitalized)!)"
             (view as! SMFHistoryImageTableViewCell).imgUser.sd_setImage(with: URL(string: (postObjFeeds?.author.profile.profileImage)!))
            (view as! SMFHistoryImageTableViewCell).lblTime.text = TableViewHelper.DateToString(date: TableViewHelper.stringToDate(date: (postObjFeeds?.createdAt)!))
             (view as! SMFHistoryImageTableViewCell).contentView.isHidden = true
            (view as! SMFHistoryImageTableViewCell).btnPopup.isHidden = true
            (view as! SMFHistoryImageTableViewCell).sharePostbtn.removeTarget(self, action: #selector((view as! SMFTextTableViewCell).sharePostAction(_:)), for: UIControlEvents.touchUpInside)
             (view as! SMFHistoryImageTableViewCell).sharePostbtn.addTarget(self, action: #selector(SMFCommentViewController.sharePostPush(_:)), for: UIControlEvents.touchUpInside)
             (view as! SMFHistoryImageTableViewCell).bottonViewHeightConstraint.constant = 0
            heightXib = 500//sharePostAction
            
        }
       else if view is  SMFTextTableViewCell{
            cellType = "SMFTextTableViewCell"

            (view as! SMFTextTableViewCell).btnComment.setTitle( "\((postObjFeeds?.commentCount)!) Comments", for: .normal)
            (view as! SMFTextTableViewCell).countLabel.text = "\((postObjFeeds?.likedBy.count)!+(postObjFeeds?.dislikedBy.count)!+(postObjFeeds?.peaceBy.count)!)  |"
            
             (view as! SMFTextTableViewCell).detailLabel.text = postObjFeeds?.content
             (view as! SMFTextTableViewCell).UserImageView.sd_setImage(with: URL(string: (postObjFeeds?.author.profile.profileImage)!))
           (view as! SMFTextTableViewCell).lblUserName.text = "\((postObjFeeds?.author.profile.firstName.capitalized)!) \((postObjFeeds?.author.profile.lastName.capitalized)!)"
            (view as! SMFTextTableViewCell).lblDateTime.text = TableViewHelper.DateToString(date: TableViewHelper.stringToDate(date: (postObjFeeds?.createdAt)!))
            (view as! SMFTextTableViewCell).bottomView.isHidden = true//btnPopup
            (view as! SMFTextTableViewCell).btnPopup.isHidden = true
            (view as! SMFTextTableViewCell).sharePostbtn.removeTarget(self, action: #selector((view as! SMFTextTableViewCell).sharePostAction(_:)), for: UIControlEvents.touchUpInside)
            (view as! SMFTextTableViewCell).sharePostbtn.addTarget(self, action: #selector(SMFCommentViewController.sharePostPush(_:)), for: UIControlEvents.touchUpInside)
            (view as! SMFTextTableViewCell).bottomViewHeightConstraint.constant = 0
            heightXib = 250
            
        }
        return view
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(heightXib == 0)
        {
        return 300//Bottom view height
        }
        else
        {
        return CGFloat(heightXib) //Bottom view height
        }
    }
    
}
