//
//  HomeService.swift
//  SMF
//
//  Created by Daffolapmac-25 on 23/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class HomeService {
    
    
    /**
     This method is used for perform Login With Valid Input(Credentials)
     
     - parameter inputData: Contains info for Login
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func getNewsFeed(inputData: HomeRequestModel, homeDelegate:ResponseCallback, lat:String, long:String ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerError()
        HomeApiRequest().makeAPIRequest(reqFromData: inputData, errorResolver: errorResolver, responseCallback: homeDelegate, lat: lat, long: long)
    }
    
    
    func performHidePutWithValidInput(inputData: HomeRequestModel, homeDelegate:ResponseCallback ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerError()
        HomeApiRequest().makeAPIRequestForHide(reqFromData: inputData, errorResolver: errorResolver, responseCallback: homeDelegate)
    }
    
    func performPostWithValidInput(inputData: HomeRequestModel, homeDelegate:ResponseCallback ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerError()
        HomeApiRequest().makeAPIRequestPost(reqFromData: inputData, errorResolver: errorResolver, responseCallback: homeDelegate)
    }
    
    func performMemberPutWithValidInput(inputData: HomeRequestModel, homeDelegate:ResponseCallback ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerError()
        HomeApiRequest().makeAPIRequestForBlock(reqFromData: inputData, errorResolver: errorResolver, responseCallback: homeDelegate)
    }
    
    
    func performFlamePutWithValidInput(inputData: HomeRequestModel, homeDelegate:ResponseCallback ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerError()
        HomeApiRequest().makeAPIFlame(reqFromData: inputData, errorResolver: errorResolver, responseCallback: homeDelegate)
    }
    

    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    //TODO:- String should be mapped from localizable string file.
    private func registerError() ->ErrorResolver{
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_401, message: "Enter valid email address")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_400, message: "Invalid credentials")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1009, message: "Please check internet connection")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1011, message: "Email Id is already register")
        return errorResolver
    }
    
    
}
