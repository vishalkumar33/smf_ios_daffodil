//
//  SMFPopUpViewController.swift
//  SMF
//
//  Created by Shivam Srivastava on 02/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFPopUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var labelItem = ["Report", "Hide Post", "Block@john"]
    var imageItem = ["reportIcon", "hidePostIcon", "blockPostIcon" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.separatorStyle = .none
      self.view.layer.borderWidth = 1.0
        self.view.layer.borderColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 33
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 33
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SMFPopUpTableViewCell
        cell.popUpLabel.text = labelItem[indexPath.row]
        cell.imageView?.image = UIImage(named: imageItem[indexPath.row])
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
