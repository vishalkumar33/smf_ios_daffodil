//
//  SMFPostTableViewCell.swift
//  SMF
//
//  Created by Shivam Srivastava on 29/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

class SMFPostTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var cellImageView: UIImageView!
    
    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
