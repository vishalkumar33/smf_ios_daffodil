//
//  HomeRequestModel.swift
//  SMF
//
//  Created by Jenkins on 1/9/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//



import Foundation


//This class is used for constructing Service Request Model

class HomeRequestModel {
    
    //Note :- Property Name must be same as key used in request API
    
    let requestBody   : [String:AnyObject]?
    let requestHeader : [String:AnyObject]?
    
    //This is a Private Constrctor instantiating Service Model Request Object
    
    private init(builderObject:Builder){
        self.requestBody = builderObject.requestBody

        //Instantiating service Request model Properties with Builder Object property
        self.requestHeader = builderObject.requestHeader
    }
    
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
       
        var requestHeader = [String:AnyObject]()
        var requestBody = [String:AnyObject]()

        
        var dict: NSMutableDictionary = NSMutableDictionary()
               
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        func setpostId(postId:String) ->Builder{
            requestBody["postId"] = postId as AnyObject?
            return self
        }
        func setAction(action:String) ->Builder{
            requestBody["action"] = action as AnyObject?
            return self
        }
        func setComment(content:String) ->Builder{
            requestBody["content"] = content as AnyObject?
            return self
        }
        
        
        func setBlockUserId(blockUserId:String) ->Builder{
            requestBody["blockUserId"] = blockUserId as AnyObject?
            return self
        }
        /**
         This method returns the Service request Model
         
         - returns: Returns ServiceRequestModel Object having set Emailid and Password
         */
        
        func build() ->HomeRequestModel{
            return HomeRequestModel(builderObject: self)
        }
        
    }
    
    
    /**
     This method is used for creating End Point
     
     - returns: returning API End Point
     */
    
    func getEndPoint(lat: String, long: String) -> String {
        return "/allPosts?skip=0&limit=10&position=[\(lat),\(long)]"
    }

    
    func getUpdatePost() ->String{
        return "/updatePost"
    }
    func getHidePoint() ->String{
        return "/hidePost"
    }
    func getBlockUserPoint() -> String
    {
        return "/blockUser"
    }
    func getAddCommentPoint() ->String{
        return "/addComment"
    }

    func getRequestBody() -> [String:AnyObject] {
        return self.requestBody!
    }
    /**
     This method is used for containing Request Headers
     
     - returns: Dictionary contains header
     */
    
    func getRequestHeader() -> [String:AnyObject] {
        return self.requestHeader!
    }
    
    
}


