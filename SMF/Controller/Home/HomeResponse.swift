//
//  HomeResponse.swift
//  SMF
//
//  Created by Jenkins on 1/9/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
class HomeResponse : Mapper  {
    
    // User Info Property
    let sessId      : String
    let sessionName : String
    let token       : String
    let user        : Post?
    
    //Declaring Response Key coming from server
    private let SESSID_KEY      = "sessid"
    private let SESSIONNAME_KEY = "session_name"
    private let TOKEN_KEY       = "token"
    private let USER_KEY        = "user"
    
    
    required init?(_ response: Dictionary<String,AnyObject>) {
        sessId      = (response[SESSID_KEY] as? String) ?? ""
        sessionName = (response[SESSIONNAME_KEY] as? String) ?? ""
        token       = (response[TOKEN_KEY] as? String) ?? ""
        user        = Post(response: response as NSDictionary)
}
}
