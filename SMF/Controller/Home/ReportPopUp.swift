//
//  ReportPopUp.swift
//  SMF
//
//  Created by Vishal on 21/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
protocol ReportPopUpDelegate {
    func confirmDelegate(_ b:UIButton)
}

class ReportPopUp:UIView{
    var confirm :ReportPopUpDelegate!
    override func awakeFromNib() {
        
    }
    @IBAction func cancelAction(_ sender: Any) {
        removeFromSuperview()
    }


    @IBAction func reportAction(_ sender: Any) {
        removeFromSuperview()
        self.confirm.confirmDelegate(sender as! UIButton)
    }

}
