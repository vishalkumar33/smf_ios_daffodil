//
//  SMFPostUpdateViewController.swift
//  SMF
//
//  Created by Shivam Srivastava on 29/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit
import MediaPlayer

class SMFPostUpdateViewController: SMFBaseViewController, UITableViewDelegate, UITableViewDataSource,DataDropDownDelegate,UIGestureRecognizerDelegate {
    
    
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLastCreated: UILabel!

    @IBOutlet weak var imageVideoView: UIView!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var whatsInYourMind: UITextView!
    @IBOutlet weak var profilePicImage: UIImageView!
    var postUpdatePresenter                      : SMFPostandUpdatePresenter?
    var dropDownView :DropDown! = DropDown()
    var islocation = false
    
    let imagePicker = UIImagePickerController()
    var imageView = UIImageView()
    
    var postedImage:UIImage?
    var postVideoUrl:URL?
    
    var moviePlayer: MPMoviePlayerController!

    //Outlets
    @IBOutlet weak var postUpdateTableView: UITableView!
    @IBOutlet weak var upperView: UIView!
 
    //Variables
    var icon = ["videoIcon", "photoIcon", "publicIcon"]
    var item = ["Videos", "Photos", "Disappearing Post"]
    var hidden = false
    var tribe:Tribes?
    var crossButton:UIButton?
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationTextField.leftPadding(width: 16)
        dropDownView.frame = CGRect(x: self.locationTextField.frame.origin.x, y: self.locationTextField.frame.origin.y + 10 , width: UIScreen.main.bounds.width - 32 , height: 180.0)

        
        
        self.dropDownView.delegate = self
        self.dropDownView.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        self.postUpdatePresenter = SMFPostandUpdatePresenter(delegate: self)
        upperView.layer.borderWidth = 1.0
        upperView.layer.borderColor = UIColor( red: 200/255, green: 200/255, blue: 200/255, alpha: 0.2 ).cgColor
        postUpdateTableView.delegate = self
        postUpdateTableView.dataSource = self
        postUpdateTableView.rowHeight = 56
        postUpdateTableView.estimatedRowHeight = 56
        self.postUpdateTableView.separatorStyle = .none
        imagePicker.delegate = self
        setLeftNavBarButton()
        initView()
//        
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SMFPostUpdateViewController.handleTap(recognizer:)))
//        tap.delegate = self
//        self.view.addGestureRecognizer(tap)

    }
    
    func initView(){
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width / 2;
        self.imgUser.layer.borderColor = UIColor.black.cgColor
        self.imgUser.clipsToBounds = true
        self.imgUser.layer.borderWidth = 1
        if let name =  UserDefault.string(forKey: "userName") {
            lblName.text = name.uppercased()
        }
        if let profileURl:URL = UserDefault.url(forKey: "userProfileImage") {
            imgUser.sd_setImage(with: profileURl, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
        }
        self.title = "POST"
    }
    
    func disApperingPost()
    {
        
    }
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        if (touch.view?.isDescendant(of: dropDownView))!{
//            return false
//        }
//        return true
//    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     let cell = postUpdateTableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SMFPostTableViewCell
        
        switch indexPath.row {
        case 0:
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            imagePicker.mediaTypes = ["public.movie"]
            present(imagePicker, animated: true, completion: nil)
            break
        case 1:
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            imagePicker.mediaTypes = ["public.image"]
            present(imagePicker, animated: true, completion: nil)
            break
        case 2:
            hidden = true
            cell.cellImageView.image = #imageLiteral(resourceName: "hidePostIcon")
            break
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    @IBAction func dropDownAction(_ sender: Any) {
        
        dropDownView.dropDownViewBorderWidth = 1
        dropDownView.dropDownViewBorderColor = UIColor.appBlueBorderColor.cgColor
        dropDownView.backgroundColor = UIColor.white
        self.view.addSubview(dropDownView)
        self.dropDownView.isHidden = false
        self.dropDownView.show()
        addTrasparentBUtton()
//        islocation = !islocation
//        if(islocation)
//        {
//            self.dropDownView.isHidden = false
//            self.dropDownView.show()
//        }
//        else
//        {
//            
//            self.dropDownView.isHidden = true
//        }
        let Local : DropDownDataObject! = DropDownDataObject()
        Local.labelData = "Local" as AnyObject
        let Favorites : DropDownDataObject! = DropDownDataObject()
        Favorites.labelData = "Favorites" as AnyObject
        let NearBy : DropDownDataObject! = DropDownDataObject()
        NearBy.labelData = "NearBy" as AnyObject
        let Trending : DropDownDataObject! = DropDownDataObject()
        Trending.labelData = "Trending" as AnyObject
        dropDownView.dataObject = [Local,Favorites,NearBy,Trending]
    }
    
    func addTrasparentBUtton(){
        let transparencyButton = UIButton(frame: self.view.bounds)
        transparencyButton.backgroundColor = UIColor.clear
        self.view.insertSubview(transparencyButton, belowSubview: dropDownView)
        transparencyButton.addTarget(self, action: #selector(self.dismissHelper), for: .touchUpInside)
    }
    
    func dismissHelper(_ sender: UIButton) {
        dropDownView.removeFromSuperview()
        sender.isHidden = true
    }
    
    func data(_ val: AnyObject, index: Int) {
        self.locationTextField.text = val as? String
        islocation = false
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = postUpdateTableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SMFPostTableViewCell
        cell.cellView.layer.borderWidth = 1.0
        cell.selectionStyle = .none
        cell.cellView.layer.borderColor = UIColor( red: 200/255, green: 200/255, blue: 200/255, alpha: 0.2 ).cgColor
        cell.cellImageView.image = UIImage(named: icon[indexPath.row])
        cell.cellLabel.text = item[indexPath.row]
        return cell
    }
    
    @IBAction func postAction(_ sender: Any) {
        if(MyPostState.isPostSelected == true)
        {
        if tribe  != nil {
            self.postUpdatePresenter?.postButtonClickedWithTribePost(newPost: self.whatsInYourMind.text, groupId: (tribe?.tribeId)!)
        }
        }
        else{
            
            self.postUpdatePresenter?.postButtonClickedWithPost(newPost: self.whatsInYourMind.text, lat: "27", long: "83", hidden: hidden, feedImage: postedImage, feedUrl: postVideoUrl)


        }
        
    }
    func popHome() {
       _ =  self.navigationController?.popViewController(animated: true)
    }
    
    func handleTap(recognizer: UITapGestureRecognizer) {
        
        self.dropDownView.isHidden = true
        islocation = false
    }
    

   
}
extension SMFPostUpdateViewController : SMFPostandUpdateDelegate
{
    func showLoader() {
        self.view.showLoader()
    }
    func hideLoader() {
        self.view.hideLoader()
    }
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    func showError(value: Int) {
        
    }
}


extension SMFPostUpdateViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaType:String = info[UIImagePickerControllerMediaType] as! String
        if mediaType == kUTTypeMovie as String {
            if let url:URL = info[UIImagePickerControllerMediaURL] as? URL {
                print(url)
                self.postVideoUrl = url
                playMovie(pathURL: url as NSURL)
            }
        }else if mediaType == kUTTypeImage as String{
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                self.postedImage = pickedImage
                self.imageView.frame = self.imageVideoView.bounds
                self.imageView.image = pickedImage
                self.imageView.contentMode = .scaleAspectFill
                self.imageView.clipsToBounds = true
                self.imageVideoView.addSubview(imageView)
                self.addCrossButton()
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func addCrossButton(){
        crossButton   = UIButton(type: UIButtonType.custom) as UIButton
        let image = UIImage(named: "imageCross") as UIImage?
        crossButton?.frame = CGRect(x: self.imageVideoView.frame.width - 35, y: 1, width: 40, height: 40)
        crossButton?.setImage(image, for: .normal)
        crossButton?.addTarget(self, action: #selector(self.closePhoto), for: UIControlEvents.touchUpInside)
        self.imageVideoView.addSubview(crossButton!)
        
        
    }
    
    func closePhoto(){
        self.imageView.removeFromSuperview()
        crossButton?.removeFromSuperview()
    }
    
    
    func playMovie(pathURL:NSURL){
        self.moviePlayer = MPMoviePlayerController(contentURL: pathURL as URL!)
        self.moviePlayer?.view.frame = self.imageVideoView.bounds
        self.moviePlayer?.prepareToPlay()
        self.imageVideoView.addSubview((self.moviePlayer?.view)!)
    }
    
    
}

