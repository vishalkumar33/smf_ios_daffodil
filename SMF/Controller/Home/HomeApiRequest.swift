//
//  HomeApiRequest.swift
//  SMF
//
//  Created by Jenkins on 1/9/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class HomeApiRequest : ApiRequestProtocol {
    
    var apiRequestURL:String = ""

    func makeAPIRequest(reqFromData: HomeRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback, lat:String, long:String) {
        
        //Creating URL of API Call
        self.apiRequestURL = SMFConstant.BASE_URL  + reqFromData.getEndPoint(lat: lat, long: long)
        ServiceManager.sharedInstance.requestGETWithURL(urlString: apiRequestURL, requestHeader: reqFromData.getRequestHeader(), responseCallBack: ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback ), returningClass: HomeResponse.self)
    }
    func makeAPIRequestForHide(reqFromData: HomeRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        //Creating URL of API Call
        self.apiRequestURL = SMFConstant.BASE_URL  + reqFromData.getHidePoint()
        ServiceManager.sharedInstance.requestPUTWithURL(urlString: apiRequestURL , andRequestDictionary: reqFromData.getRequestBody(), requestHeader: reqFromData.getRequestHeader(), responseCallBack: ResponseWrapper(errorResolver: errorResolver,responseCallBack:responseCallback) , returningClass: HomeResponse.self)
    }
    func makeAPIRequestPost(reqFromData: HomeRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        //Creating URL of API Call
        self.apiRequestURL = SMFConstant.BASE_URL  + reqFromData.getAddCommentPoint()
        
        ServiceManager.sharedInstance.requestPOSTWithURL(urlString: self.apiRequestURL , andRequestDictionary: reqFromData.getRequestBody() , requestHeader: reqFromData.getRequestHeader(), responseCallBack: ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback ), returningClass: HomeResponse.self)
        
    }
    
    func makeAPIRequestForBlock(reqFromData: HomeRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        //Creating URL of API Call
        self.apiRequestURL = SMFConstant.BASE_URL  + reqFromData.getBlockUserPoint()
        ServiceManager.sharedInstance.requestPUTWithURL(urlString: apiRequestURL , andRequestDictionary: reqFromData.getRequestBody(), requestHeader: reqFromData.getRequestHeader(), responseCallBack: ResponseWrapper(errorResolver: errorResolver,responseCallBack:responseCallback) , returningClass: HomeResponse.self)
    }
    

    func makeAPIFlame(reqFromData: HomeRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        //Creating URL of API Call
        self.apiRequestURL = SMFConstant.BASE_URL  + reqFromData.getUpdatePost()
        ServiceManager.sharedInstance.requestPUTWithURL(urlString: apiRequestURL , andRequestDictionary: reqFromData.getRequestBody(), requestHeader: reqFromData.getRequestHeader(), responseCallBack: ResponseWrapper(errorResolver: errorResolver,responseCallBack:responseCallback) , returningClass: HomeResponse.self)
    }
    
    
    
    func makeAPIRequest(reqFromData: HomeRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
    }
    
    /**
     This method is used for canceling API Call
     */
    
    func cancel() {
        ServiceManager.sharedInstance.cancelTaskWithURL(urlString: self.apiRequestURL)
    }
    
    /**
     This method return whether API call is in progress or not
     
     - returns: Boolean value either true or false
     */
    
    func isInProgress() -> Bool {
        return ServiceManager.sharedInstance.isInProgress(urlString: self.apiRequestURL)
    }
}
