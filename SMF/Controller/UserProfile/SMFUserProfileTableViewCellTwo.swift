//
//  SMFUserProfileTableViewCellTwo.swift
//  SMF
//
//  Created by Shivam Srivastava on 29/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

class SMFUserProfileTableViewCellTwo: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var cellLabel: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
