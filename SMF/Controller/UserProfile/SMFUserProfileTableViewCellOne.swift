//
//  SMFUserProfileTableViewCellOne.swift
//  SMF
//
//  Created by Shivam Srivastava on 29/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

protocol SMFUserProfileTableViewCellOneDelegate {
    func followUser()
    func blockUser()
    func sendMessage()
}

class SMFUserProfileTableViewCellOne: UITableViewCell {

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var handleLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnBlock: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    
    var delegate:SMFUserProfileTableViewCellOneDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImageView.layer.borderWidth = 1.0
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor.appBlueBorderColor.cgColor
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.clipsToBounds = true
    }

    @IBAction func followUser(_ sender: Any) {
        delegate.followUser()
    }
    
    @IBAction func blockUser(_ sender: Any) {
        delegate.blockUser()
    }
   
    @IBAction func sendMessage(_ sender: Any) {
        delegate.sendMessage()
    }

}
