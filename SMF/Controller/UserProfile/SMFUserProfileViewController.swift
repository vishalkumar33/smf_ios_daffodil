import UIKit

class SMFUserProfileViewController: SMFBaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var userProfileViewPresenter : SMFMemberListPresenter?
    var memberData: MemberInfo?
    var item = ["30 November, 1995", "Male", "Single", "Private Account", "johndeo12@gmail.com", "Lane C-5 Ashima Vihar, Clement Town, Dehradun" ]
    var data :[String] = []
    
    var connections:UserConnection?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        userProfileViewPresenter = SMFMemberListPresenter(delegate: self)
        userProfileViewPresenter?.getNewsFeed()
        tableView.delegate = self
        tableView.dataSource = self
        
        setData()
        userProfileViewPresenter?.whoAmI()
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        setNav(title: "USER PROFILE")
        setLeftNavBarButton()
    }

    @IBAction func historyAction(_ sender: Any) {
        let historyViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFHomeViewController") as! SMFHomeViewController
        historyViewController.isHome = false
        self.navigationController?.pushViewController(historyViewController, animated: true)

        
    }
    override func viewDidAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
    }
    func changeDob(dob:String)-> String{
        
        if dob == "MM/DD/YYYY" {
            return "MM/DD/YYYY"
        }
        
        let month = dob.substring(to: 2)
        let date = dob.substring(with: 2..<4)
        let year = dob.substring(from: 4)
        
        let dobOfUSer = date + "/" + month + "/" + year
        
        print(dobOfUSer)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dat = dateFormatter.date(from: dobOfUSer)
        if dat != nil {
            dateFormatter.dateFormat = "dd MMMM yyyy"
            let stringDate = dateFormatter.string(from: dat!)
            return stringDate
        }else
        {
            return "MM/DD/YYYY"
        }
        
        
    }

    func setData()
    {
        let profile = memberData?.profile
        let d = profile?.dob != "" ? profile?.dob : "MM/DD/YYYY"
        let dob = changeDob(dob: d!)

        let gender = profile?.gender != "" ?
            profile?.gender : "Gender"
        let relationShip = profile?.relationStatus != "" ? profile?.relationStatus : "Relationship Status"
        let defaultAddress = "\((profile?.location)!) \((profile?.state)!) \((profile?.zip)!)"
        let location = defaultAddress != "   " ? defaultAddress : "Location"
        item = ["huhyh",dob, gender!, relationShip!, "Private Account", "frf", location]
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("INDEX Path = \(indexPath.row)")
        
        if indexPath.row == 0 {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! SMFUserProfileTableViewCellOne
            print("Cel height = \(cell1.frame.height)")
             cell1.nameLabel.text = "\((memberData?.profile?.firstName)!) \((memberData?.profile?.lastName)!)"
            cell1.handleLabel.text = "@\((memberData?.profile?.handle)!)"
            cell1.profileImageView.sd_setImage(with: NSURL(string: (memberData?.profile?.profileImage)!) as! URL, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
            cell1.delegate = self
            cell1.backgroundImageView.sd_setImage(with: NSURL(string: (memberData?.profile?.profileImage)!) as! URL, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
                
            return cell1
            
        }
            
        else
        {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! SMFUserProfileTableViewCellTwo
            cell2.cellLabel.text = item[indexPath.row]
            print("Cel height 2 = \(cell2.frame.height)")
            return cell2
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 454.0
        }else{
            return 62.0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 454.0
    }

}
extension SMFUserProfileViewController : SMFMemberListIDelegate {
    
    func AboutMe(newsFeedArray: [UserInfo]) {
        
    }
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
   
    func updateTable(newsFeedArray: [MemberInfo]) {
        
    }
    
    func aboutMe(userInfo: UserInfo) {
        self.connections = userInfo.connections
    }
}

extension SMFUserProfileViewController: SMFUserProfileTableViewCellOneDelegate {
    func followUser() {
        
    }
    
    func checkUserStatus()->Bool{
        return (self.connections?.favorites.contains(self.memberData?.id as Any))!
    }
    
    func sendMessage() {
        
        if checkUserStatus() {
            let vcObject = self.storyboard!.instantiateViewController(withIdentifier: "SMFMessageDetailViewController") as! SMFMessageDetailViewController
            vcObject.senderId = self.memberData?.id
            self.navigationController?.pushViewController(vcObject, animated: true)
        }else{
            self.showErrorAlert(alertTitle: "Error", alertMessage: "Sorry you can't chat!", VC: self)
        }
        
    }
    
    func blockUser() {
        
    }
    
    
}
