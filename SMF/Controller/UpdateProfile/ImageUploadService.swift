//
//  ImageUploadService.swift
//  SMF
//
//  Created by Daffolapmac-25 on 18/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class ImageUploadService {
    
    class func videoUploadRequest(_ videoType:String, id:String ,videoUrl:URL,success: @escaping (_ response: NSDictionary) -> Void, failure: @escaping (_ error: NSError?) -> Void)
    {
        let myUrl = URL(string: "\(SMFConstant.BASE_URL)/uploadImage")
        let request = NSMutableURLRequest(url:myUrl!);
        request.httpMethod = "POST";
        
        let param = [
            "type"  : videoType,
            "id"    : id
        ]
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        let videoData = NSData(contentsOf: videoUrl)
        if(videoData==nil)  { return; }
        request.httpBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: videoData! as Data, boundary: boundary)
        let task = URLSession.shared.dataTask(with: request as URLRequest) {data,response,error in
            if error != nil {
                print("error=\(error)")
                failure(error as NSError?)
                return
            }
            
            // You can print out response object
            print("******* response = \(response)")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print(json!)
                success(json!)
            }catch
            {
                print(error)
            }
        }
        task.resume()
    }
    
    
    
    
    class func imageUploadRequest(_ imageType:String, id:String ,image:UIImage,success: @escaping (_ response: NSDictionary) -> Void, failure: @escaping (_ error: NSError?) -> Void)
    {
        let myUrl = URL(string: "\(SMFConstant.BASE_URL)/uploadImage")
        let request = NSMutableURLRequest(url:myUrl!);
        request.httpMethod = "POST";
        
        let param = [
            "type"  : imageType,
            "id"    : id
        ]
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        if(imageData==nil)  { return; }
        request.httpBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData!, boundary: boundary)
        let task = URLSession.shared.dataTask(with: request as URLRequest) {data,response,error in
            if error != nil {
                print("error=\(error)")
                failure(error as NSError?)
                return
            }
            
            // You can print out response object
            print("******* response = \(response)")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print(json!)
                success(json!)
            }catch
            {
                print(error)
            }
        }
        task.resume()
    }
    
    
    class func createBodyWithParameters(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        let body = NSMutableData();
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        let uuid = UUID().uuidString
        let filename = "\(uuid).jpg"
        let mimetype = "image/jpg"
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")
        body.appendString("--\(boundary)--\r\n")
        
        return body as Data
    }
    
    
    class func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
}

extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
