//
//  UpdateProfilePresenter.swift
//  SMF
//
//  Created by Daffolapmac-25 on 19/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

@objc protocol SMFUpdateProfileDelegate:SMFCommonViewDelegate{
    func updateTable(data:[String])
    func switchToHome()
}

class UpdateProfilePresenter: ResponseCallback {
    
    private var UpdateViewDelegate   : SMFUpdateProfileDelegate?
    private var serviceUpdate = UpdateService()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : SMFUpdateProfileDelegate){
        UpdateViewDelegate = delegate
    }
    
    
    // MARK: ServiceManagerDelegate Methods
    
    /**
     This method handles success response of Login API
     
     - parameter responseObject: Response object that contains success response
     */
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        self.UpdateViewDelegate?.hideLoader!()
        self.UpdateViewDelegate?.switchToHome()
        
    }
    
    
    func initData(){
        var data:[String] = []
        let profileObj = GlobalVariable.userObj?.profile
        let email = GlobalVariable.userObj?.emails[0].address
        let location = "\((profileObj?.location)!) \((profileObj?.state)!) \((profileObj?.zip)!)"
        let handle = "@\((profileObj?.handle)!)"
        data = [location, email!, handle]
        UpdateViewDelegate?.updateTable(data: data)
    }
    
    
    func updateProfile(){
        
        let profileObj = GlobalVariable.userObj?.profile
        
        if profileObj?.handle != nil {
            UpdateViewDelegate?.showLoader!()
            

            let data = UpdateRequestModel.Builder().setFirstName(firstName: (profileObj?.firstName)!).setLastName(lastName: (profileObj?.lastName)!).setFullName(fullName: "\(profileObj?.firstName) \(profileObj?.lastName)").setCompany(company: (profileObj?.company)!).setStreetAddress(streetAddress: (profileObj?.location)!).setCity(city: (profileObj?.city)!).setState(state: (profileObj?.state)!).setZipCode(zipCode:  (profileObj?.zip)!).setHandle(handle: (profileObj?.handle)!).setDob(dob: (profileObj?.dob)!).setRelationship(relationship: (profileObj?.relationStatus)!).setGender(gender: (profileObj?.gender)!).setInro(intro: (profileObj?.intro)!).setProfileImage(profileImage: (profileObj?.profileImage)!).addRequestHeader(key:"Content-Type", value: "application/json").build()
            
            serviceUpdate.performUpdateWithValidInput(inputData: data , updateProfileDelegate: self)
        }else{
            UpdateViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: "Handle is not entered")
        }
    }
    
    
    /**
     This Method handle error response of Login API
     
     - parameter error: NSError Object that contains error information
     */
    
    func servicesManagerError(error : String){
        UpdateViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        //This statement is used for hiding loader
        self.UpdateViewDelegate?.hideLoader!()
    }
    
}
