//
//  WhoAmIPresenter.swift
//  SMF
//
//  Created by Jenkins on 1/18/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

import UIKit

protocol SMFWhoAmIDelegate:SMFCommonViewDelegate{
    func userAvailable(isAvail:Bool)
    func userDetail(userProfile: UserInfo)
}



class WhoAmIPresenter: ResponseCallback {
    
    private var WhoAmIViewDelegate   : SMFWhoAmIDelegate?
    private var serviceBuisnessAboutYou = ServicesAboutYou()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : SMFWhoAmIDelegate){
        WhoAmIViewDelegate = delegate
    }
    
    
    // MARK: ServiceManagerDelegate Methods
    
    /**
     This method handles success response of Login API
     
     - parameter responseObject: Response object that contains success response
     */
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        
        self.WhoAmIViewDelegate?.hideLoader!()
        if responseObject is AboutYouResponse {
            let obj:AboutYouResponse = responseObject as! AboutYouResponse
            
            if obj.availability == true {
                WhoAmIViewDelegate?.userAvailable(isAvail: true)
            }else{
                let userProfile = obj.userInfo
                WhoAmIViewDelegate?.userDetail(userProfile: userProfile!)
            }
        }
        
        
        if responseObject is UserInfo {
            
            
            print(responseObject)
        }
        
        
        //This statement is used for hiding loader
        
        
    }
    
    /**
     This Method handle error response of Login API
     
     - parameter error: NSError Object that contains error information
     */
    
    func servicesManagerError(error : String){
        
        
        WhoAmIViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        //This statement is used for hiding loader
        self.WhoAmIViewDelegate?.hideLoader!()
    }
    
    
    
    // MARK: Presenter Local methods
    
    /**
     This method calls Login API if emailId and Password both are Valid
     
     - parameter emailId:  User EmailId
     - parameter password: User Password
     */
    
    func whoAmI() ->Void{
        //Checking whether emailId and Password is valid or not
       
            //Showing Loader
            WhoAmIViewDelegate?.showLoader!()
            //Performing Sign up Operation with valid Input
            self.serviceBuisnessAboutYou.performAboutYouWithValidInput(inputData: AboutYouRequestModel.Builder().addRequestHeader(key:"Content-Type", value: "application/json").build() , aboutYouDelegate: self )
    }
    
    
    func checkAvability(value:String){
        serviceBuisnessAboutYou.getHandleAvailiblity(inputData: AboutYouRequestModel.Builder().addRequestHeader(key:"Content-Type", value: "application/json").build(), aboutYouDelegate: self, handle: value)
    }
    
    
    func setDataWithValue(firstName:String?, lastName:String?, handle:String?, DOB:String?, realtionshipStatus:String?, gender:String?, intro:String?,location:String?, state:String?, zipCode:String?){
        
        let obj = GlobalVariable.userObj?.profile
        
        if firstName != nil {
            obj?.firstName = firstName!
        }
        
        if lastName != nil {
            obj?.lastName = lastName!
        }
        
        if firstName != nil && (lastName != nil) {
            obj?.fullName = "\(firstName!) \(lastName!)"
        }
        
        if handle != nil {
            obj?.handle = handle!
        }
        
        
        
        if realtionshipStatus != nil {
            obj?.relationStatus = realtionshipStatus!
        }
        
        if gender != nil {
            obj?.gender = gender!
        }
        
        if intro != nil {
            obj?.intro = intro!
        }
        
        if location != nil {
            obj?.city = location!
        }
        
        if zipCode != nil {
            obj?.zip = zipCode!
        }
        
        if state != nil {
            obj?.state = state!
        }
        GlobalVariable.userObj?.profile = obj!
    }
    
}
