//
//  AboutYouApiRequest.swift
//  SMF
//
//  Created by Jenkins on 1/18/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation



class AboutYouApiRequest : ApiRequestProtocol {
    
    var apiRequestURL:String = ""
    
    /**
     This method is used for Interacting with API layer for Get Request
     
     - parameter request:          NSDictionary object contains request parameter
     - parameter responseCallback: responseCallback sending callback of success and Error response of API
     */
    
    func makeAPIRequest(reqFromData: AboutYouRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        //Creating URL of API Call
        self.apiRequestURL = SMFConstant.BASE_URL  + reqFromData.getEndPoint()
        ServiceManager.sharedInstance.requestGETWithURL(urlString: self.apiRequestURL, requestHeader: reqFromData.getRequestHeader(), responseCallBack: ResponseWrapper(errorResolver: errorResolver,responseCallBack:responseCallback) , returningClass: AboutYouResponse.self)

        
    }
    
    func makeAPIRequestForHandle(reqFromData: AboutYouRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback, handle:String) {
        
        //Creating URL of API Call
        self.apiRequestURL = SMFConstant.BASE_URL  + reqFromData.getEndPointForHandle(handle: handle)
        print(self.apiRequestURL)
        ServiceManager.sharedInstance.requestGETWithURL(urlString: self.apiRequestURL, requestHeader: reqFromData.getRequestHeader(), responseCallBack: ResponseWrapper(errorResolver: errorResolver,responseCallBack:responseCallback) , returningClass: AboutYouResponse.self)
    }
    
    
    /**
     This method is used for canceling API Call
     */
    
    func cancel() {
        ServiceManager.sharedInstance.cancelTaskWithURL(urlString: self.apiRequestURL)
    }
    
    /**
     This method return whether API call is in progress or not
     
     - returns: Boolean value either true or false
     */
    
    func isInProgress() -> Bool {
        return ServiceManager.sharedInstance.isInProgress(urlString: self.apiRequestURL)
    }
}
