//
//  SMFBusinessProfileTableViewCell.swift
//  SMF
//
//  Created by Jenkins on 12/28/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

class SMFBusinessProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
        // Initialization code
    }

    func setCell(){
        profileImageView.layer.borderWidth = 2
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor(red: 97, green: 189, blue: 214, alpha: 1.0).cgColor
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.clipsToBounds = true

        if let imgURL = GlobalVariable.userObj?.profile.profileImage  {
            profileImageView.sd_setImage(with: NSURL(string: imgURL) as! URL , placeholderImage: #imageLiteral(resourceName: "messagesProfile"))
            backgroundImageView.sd_setImage(with: NSURL(string: imgURL) as! URL , placeholderImage: #imageLiteral(resourceName: "messagesProfile"))
        }
        
    }

}
