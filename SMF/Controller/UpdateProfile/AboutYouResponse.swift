//
//  AboutYouResponse.swift
//  SMF
//
//  Created by Jenkins on 1/18/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class AboutYouResponse : Mapper  {
    
    // User Info Property
    let sessId      : String
    let sessionName : String
    let token       : String
    let availability: Bool
    var userInfo    :UserInfo? = nil
    
    //Declaring Response Key coming from server
    private let SESSID_KEY      = "sessid"
    private let SESSIONNAME_KEY = "session_name"
    private let TOKEN_KEY       = "token"
    private let AVAIL_KEY       = "availability"
    
    
    required init?(_ response: Dictionary<String,AnyObject>) {
        sessId          = (response[SESSID_KEY] as? String) ?? ""
        sessionName     = (response[SESSIONNAME_KEY] as? String) ?? ""
        token           = (response[TOKEN_KEY] as? String) ?? ""
        print(response)
        availability    = (response[AVAIL_KEY] as? Bool) ?? false
        print(availability)
        if !availability {
            userInfo        = UserInfo(response)!
        }
    }
}

