//
//  UpdateRequestModel.swift
//  SMF
//
//  Created by Daffolapmac-25 on 19/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class UpdateRequestModel {
    
    //Note :- Property Name must be same as key used in request API
    
    let requestBody   : [String:AnyObject]?
    let requestHeader : [String:AnyObject]?
    
    //This is a Private Constrctor instantiating Service Model Request Object
    
    private init(builderObject:Builder){
        
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builderObject.requestBody
        self.requestHeader = builderObject.requestHeader
    }
    
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var requestBody = [String:AnyObject]()
        var requestHeader = [String:AnyObject]()
        
        
        func setFirstName(firstName:String) ->Builder{
            requestBody["firstName"] = firstName as AnyObject?
            return self
        }
        
        
        func setLastName(lastName:String) ->Builder{
            requestBody["lastName"] = lastName as AnyObject?
            return self
        }

        func setFullName(fullName:String) ->Builder{
            requestBody["fullName"] = fullName as AnyObject?
            return self
        }
        
        func setCompany(company:String) ->Builder{
            requestBody["company"] = company as AnyObject?
            return self
        }
        
        func setSearchEmail(searchEmail:String) ->Builder{
            requestBody["searchEmail"] = searchEmail as AnyObject?
            return self
        }
        
        func setTitle(title:String) ->Builder{
            requestBody["title"] = title as AnyObject?
            return self
        }
        
        
        func setPhoneNumber(phoneNumber:String) ->Builder{
            requestBody["phoneNumber"] = phoneNumber as AnyObject?
            return self
        }
        
        
        func setStreetAddress(streetAddress:String) ->Builder{
            requestBody["streetAddress"] = streetAddress as AnyObject?
            return self
        }
        
        
        func setCity(city:String) ->Builder{
            requestBody["city"] = city as AnyObject?
            return self
        }
        
        
        func setState(state:String) ->Builder{
            requestBody["state"] = state as AnyObject?
            return self
        }
        
        
        func setZip(zipcode:String) ->Builder{
            requestBody["zipCode"] = zipcode as AnyObject?
            return self
        }
        
        
        func setWebAddress(webAddress:String) ->Builder{
            requestBody["webAddress"] = webAddress as AnyObject?
            return self
        }
        
        
        func setProfileImage(profileImage:String) ->Builder{
            requestBody["profileImage"] = profileImage as AnyObject?
            return self
        }
        
        
        func setLinkedinUrl(linkedInProfileUrl:String) ->Builder{
            requestBody["linkedInProfileUrl"] = linkedInProfileUrl as AnyObject?
            return self
        }
        
        
        
        func setFacebookUrl(facebookProfileUrl:String) ->Builder{
            requestBody["facebookProfileUrl"] = facebookProfileUrl as AnyObject?
            return self
        }
        
        
        func setTwitterUrl(twitterProfileUrl:String) ->Builder{
            requestBody["twitterProfileUrl"] = twitterProfileUrl as AnyObject?
            return self
        }
        
        
        func setSubscription(subscription:String) ->Builder{
            requestBody["subscription"] = subscription as AnyObject?
            return self
        }
        
        
        func setHandle(handle:String) ->Builder{
            requestBody["handle"] = handle as AnyObject?
            return self
        }
        
        
        func setDob(dob:String) ->Builder{
            requestBody["dob"] = dob as AnyObject?
            return self
        }
        
        
        func setRelationship(relationship:String) ->Builder{
            requestBody["relationship"] = relationship as AnyObject?
            return self
        }
        
        
        func setGender(gender:String) ->Builder{
            requestBody["gender"] = gender as AnyObject?
            return self
        }
        
        func setInro(intro:String) ->Builder{
            requestBody["intro"] = intro as AnyObject?
            return self
        }
        
        
        func setZipCode(zipCode:String) ->Builder{
            requestBody["zipCode"] = zipCode as AnyObject?
            return self
        }
        
        func setHiddenPost(hiddenPosts:String) ->Builder{
            requestBody["hiddenPosts"] = hiddenPosts as AnyObject?
            return self
        }
        
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method returns the Service request Model
         
         - returns: Returns ServiceRequestModel Object having set Emailid and Password
         */
        
        func build() ->UpdateRequestModel{
            return UpdateRequestModel(builderObject: self)
        }
        
    }
    
    
    /**
     This method is used for creating End Point
     
     - returns: returning API End Point
     */
    
    func getEndPoint() -> String {
        return "/updateProfile"
    }
    
    /**
     This method is used for
     
     - returns: returning request body
     */
    
    func getRequestBody() -> [String:AnyObject] {
        return self.requestBody!
    }
    
    /**
     This method is used for containing Request Headers
     
     - returns: Dictionary contains header
     */
    
    func getRequestHeader() -> [String:AnyObject] {
        return self.requestHeader!
    }
    
}
