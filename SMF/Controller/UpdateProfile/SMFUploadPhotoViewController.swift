//
//  SMFUploadPhotoViewController.swift
//  SMF
//
//  Created by Jenkins on 1/16/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit


class SMFUploadPhotoViewController: SMFBaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    @IBOutlet weak var addPhoto             : UIButton!
    @IBOutlet weak var imgProfileForeground : UIImageView!
    @IBOutlet weak var imgProfileBackground : UIImageView!
    var uploadPresenter                     : SMFUploadPhotoPresenter?
    var delegate                            : scrollToNextDelegate!
    
    let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.uploadPresenter = SMFUploadPhotoPresenter(delegate: self)
        initView()

    }
    
    func initView(){
        let url = GlobalVariable.userObj?.profile.profileImage
        imgProfileForeground.layer.borderWidth = 2
        imgProfileForeground.layer.masksToBounds = true
        imgProfileForeground.layer.borderColor = UIColor.appBlueBorderColor.cgColor
        imgProfileForeground.layer.cornerRadius = imgProfileForeground.frame.height/2
        imgProfileForeground.clipsToBounds = true
        imgProfileBackground.sd_setImage(with: NSURL(string: url!) as! URL, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
        imgProfileForeground.sd_setImage(with: NSURL(string: url!) as! URL, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
        
        
    }

    @IBAction func contiuneAction(_ sender: Any) {
         self.delegate.scroll(value: 1)
    }
    
    @IBAction func sddPhotoAction(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imgProfileBackground.image = pickedImage
            imgProfileForeground.image = pickedImage
            
            imgProfileForeground.layer.borderWidth = 2
            imgProfileForeground.layer.masksToBounds = true
            imgProfileForeground.layer.borderColor = UIColor.appBlueBorderColor.cgColor
            imgProfileForeground.layer.cornerRadius = imgProfileForeground.frame.height/2
            imgProfileForeground.clipsToBounds = true
            
            uploadPresenter?.uploadPhoto(image: pickedImage, type: "profilePhotos")
        }
        
        
        
        dismiss(animated: true, completion: nil)
    }
    
   
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

}

extension SMFUploadPhotoViewController: SMFUploadPhtoDelegate {
    
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    
}
