//
//  ServicesAboutYou.swift
//  SMF
//
//  Created by Jenkins on 1/18/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//


import Foundation



import UIKit

class ServicesAboutYou {
    
    
    /**
     This method is used for perform Login With Valid Input(Credentials)
     
     - parameter inputData: Contains info for Login
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performAboutYouWithValidInput(inputData: AboutYouRequestModel, aboutYouDelegate:ResponseCallback ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForLogin()
        AboutYouApiRequest().makeAPIRequest(reqFromData: inputData, errorResolver: errorResolver, responseCallback: aboutYouDelegate)
    }
    
    
    func getHandleAvailiblity(inputData: AboutYouRequestModel, aboutYouDelegate:ResponseCallback, handle:String ){
        let errorResolver:ErrorResolver = self.registerErrorForLogin()
        AboutYouApiRequest().makeAPIRequestForHandle(reqFromData: inputData, errorResolver: errorResolver, responseCallback: aboutYouDelegate, handle: handle)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    //TODO:- String should be mapped from localizable string file.
    private func registerErrorForLogin() ->ErrorResolver{
        let errorResolver:ErrorResolver = ErrorResolver()
     //   errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_401, message: "Enter valid email address")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_400, message: "User not Logged In")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1009, message: "Please check internet connection")
     //   errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1011, message: "Email Id is already register")
        return errorResolver
    }
    
    
}
