        //
//  SMFBusinessCardViewController.swift
//  SMF
//
//  Created by Jenkins on 12/28/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

class SMFBusinessCardViewController: SMFBaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var profileDetail:[String] = []
    var updatePresenter                      : UpdateProfilePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 70
        self.updatePresenter = UpdateProfilePresenter(delegate: self)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.updatePresenter?.initData()
    }
    
    @IBAction func finishUpdateAction(_ sender: Any) {
        updatePresenter?.updateProfile()
    }
    

   }


extension SMFBusinessCardViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 308
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell:SMFBusinessProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "profileCell") as! SMFBusinessProfileTableViewCell
        cell.selectionStyle = .none
        cell.setCell()
        return cell
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! SMFBusinessCardDetailTableViewCell
        cell.lblDetail.text = self.profileDetail[indexPath.row]
        return cell
    }
}

extension SMFBusinessCardViewController: SMFUpdateProfileDelegate {
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showLoader() {
        self.view.showLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
        
    }
    
    func updateTable(data: [String]) {
        self.profileDetail = data
        self.tableView.reloadData()
    }
    
    func switchToHome() {
        let loginViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFTabViewController") as! SMFTabViewController
        self.navigationController?.pushViewController(loginViewController, animated: true)
    }
    
}
