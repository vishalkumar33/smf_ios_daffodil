//
//  PageControlImageViewController.swift
//  SMF
//
//  Created by Jenkins on 12/28/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

struct GlobalVariable {
    static var userObj:UserInfo?
}


class PageControlImageViewController: SMFBaseViewController,scrollToNextDelegate {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    
    var responseObj: SMFSignUpResponse? = nil
    
    var tutorialPageViewController: SMFTutorialViewController? {
        didSet {
            tutorialPageViewController?.tutorialDelegate = self
            
        }
    }
    var aboutController: SMFAboutYouViewController?
    var uploadController: SMFUploadPhotoViewController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.addTarget(self, action: #selector(PageControlImageViewController.didChangePageControlValue), for: .valueChanged)
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        aboutController = (tutorialPageViewController?.orderedViewControllers[0] as! SMFAboutYouViewController)
        uploadController = (tutorialPageViewController?.orderedViewControllers[1] as! SMFUploadPhotoViewController)
        aboutController?.delegate = self
        uploadController?.delegate = self
        
        setNavWithoutTitle()
        
        
    }
    func scroll(value: Int) {
        didChangePageControlValue()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tutorialPageViewController = segue.destination as? SMFTutorialViewController {
            self.tutorialPageViewController = tutorialPageViewController
        }
    }
    
       /**
     Fired when the user taps on the pageControl to change its current page.
     */
    func didChangePageControlValue() {
        tutorialPageViewController?.scrollToNextViewController()
    }
}


extension PageControlImageViewController: TutorialPageViewControllerDelegate {
    
    func tutorialPageViewController(_ tutorialPageViewController: SMFTutorialViewController,
                                    didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func tutorialPageViewController(_ tutorialPageViewController: SMFTutorialViewController,
                                    didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
        if(MyViewState.isSignUpSelected == false)
        {
            setLeftNavBarButton()
        }
        switch index {
        case 0:
            self.title = "ABOUT YOU"
            
            break;
        case 1:
            self.title = "PROFILE PHOTO"
            break;
        case 2:
            self.title = "REVIEW PROFILE"
            break;
        default:
            break
        }
    }
    
}
