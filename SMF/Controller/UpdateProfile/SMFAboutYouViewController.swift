        //
//  SMFAboutYouViewController.swift
//  SMF
//
//  Created by Jenkins on 12/28/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

protocol scrollToNextDelegate
{
    func scroll(value:Int)
}


class SMFAboutYouViewController: SMFBaseViewController, DataDropDownDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,SMFWhoAmIDelegate{
    
    
    @IBOutlet weak var btnHandle: UIButton!
    @IBOutlet weak var btnRelation: UIButton!
    @IBOutlet weak var btnGender: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btnHandleheight: NSLayoutConstraint!
    @IBOutlet weak var textFieldFirstName: UITextField!
    @IBOutlet weak var textFieldLastName: UITextField!
    @IBOutlet weak var textFieldHandle: UITextField!
    @IBOutlet weak var textFieldDateOfBirth: UITextField!
    @IBOutlet weak var textFieldStatusRelationShip: UITextField!
    @IBOutlet weak var textFiledGender: UITextField!
    @IBOutlet weak var textViewIntroduce: UITextView!
    @IBOutlet weak var textViewLocation: UITextField!
    @IBOutlet weak var textViewState: UITextField!
    @IBOutlet weak var textViewZipCode: UITextField!
    var dropDownView :DropDown! = DropDown()
    var genderDropDownView :DropDown! = DropDown()
    var isSelected :Bool?
    var isRelation = false
    var isGender = false
    var whoAmIPresenter       : WhoAmIPresenter?
    var delegate              : scrollToNextDelegate!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.whoAmIPresenter = WhoAmIPresenter(delegate: self)

        if(MyViewState.isSignUpSelected == false)
        {
            self.whoAmIPresenter?.whoAmI()
            
        }
        setDelegate()
        
        //self.whoAmIPresenter?.whoAmI()
        
        textViewIntroduce.layer.borderWidth = 1.0
        textViewIntroduce.layer.borderColor = UIColor(red: 81, green: 80, blue: 82, alpha: 0.2).cgColor
        setView()
        setValue()
        textViewZipCode.keyboardType = UIKeyboardType.numberPad
        btnHandleheight.constant = 0
        //self.textViewIntroduce.contentInset = UIEdgeInsetsMake(20, 5, 5, 5);
        textViewIntroduce.textContainerInset =
            UIEdgeInsetsMake(18,30,0,0);
        textFieldStatusRelationShip.isUserInteractionEnabled = false
        dropDownView.frame = CGRect(x: self.textFieldStatusRelationShip.frame.origin.x, y: self.textFieldStatusRelationShip.frame.origin.y + 10 , width: UIScreen.main.bounds.width - 32 , height: 180.0)
        genderDropDownView.frame = CGRect(x: self.textFiledGender.frame.origin.x, y: self.textFiledGender.frame.origin.y + 10 , width: UIScreen.main.bounds.width - 32 , height: 84.0)
        self.mainView.addSubview(dropDownView)
        self.mainView.addSubview(genderDropDownView)
        self.dropDownView.isHidden = true
        self.dropDownView.delegate = self
        self.genderDropDownView.isHidden = true
        self.genderDropDownView.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SMFAboutYouViewController.handleTap(recognizer:)))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        
        
    }
    
    
    func initData(){
        let profile = GlobalVariable.userObj?.profile
        textFieldFirstName.text = profile?.firstName
        textFieldLastName.text = profile?.lastName
        if profile?.handle != nil && profile?.handle != "" {
            textFieldHandle.text = profile?.handle
            textFieldHandle.isUserInteractionEnabled = false
        }else{
            textFieldHandle.isUserInteractionEnabled = true
        }
        textFieldStatusRelationShip.text = profile?.relationStatus
        textFieldDateOfBirth.text = profile?.dob
        textFiledGender.text = profile?.gender
        textViewIntroduce.text = profile?.intro
        textViewState.text = profile?.state
        print(profile!.zip)
        textViewZipCode.text = String(describing: profile!.zip)
        textViewLocation.text = profile?.city
    }
    
    
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        if alertMessage == "Some error occured" {
            self.makeRed(textField: textFieldHandle)
            btnHandle.setTitle("Handle not available", for: .normal)
            btnHandleheight.constant = 30
            
            return
        }
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: dropDownView))!{
            return false
        }
        if (touch.view?.isDescendant(of: genderDropDownView))!{
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.dropDownView.isHidden = true
        self.genderDropDownView.isHidden = true
        isRelation = false
        isGender = false
        
    }
    
    func  setDelegate() {
        self.textFieldFirstName.delegate = self
        self.textFieldLastName.delegate = self
        self.textFieldHandle.delegate = self
        self.textFieldDateOfBirth.delegate = self
        self.textFiledGender.delegate = self
        self.textViewState.delegate = self
        self.textViewLocation.delegate = self
        self.textViewZipCode.delegate = self
    }
    
    func setValue(){
        if GlobalVariable.userObj != nil {
            let user = GlobalVariable.userObj?.profile
            textFieldFirstName.text = user?.firstName
            textFieldLastName.text = user?.lastName
            initData()
        }
    }
    
    
    func setView()
    {
        self.textViewIntroduce.layer.borderColor = UIColor.appGrayColor.cgColor
        self.textViewIntroduce.layer.borderWidth = 1.0;
        self.textViewIntroduce.layer.cornerRadius = 5.0;
        self.textViewIntroduce.leftPadding(width: 30)
        self.textFieldFirstName.leftPadding(width:30)
        self.textFieldLastName.leftPadding(width: 30)
        self.textFieldHandle.leftPadding(width: 30)
        self.textFieldDateOfBirth.leftPadding(width: 30)
        self.textFieldStatusRelationShip.leftPadding(width: 30)
        self.textFiledGender.leftPadding(width: 30)
        self.textViewLocation.leftPadding(width: 30)
        self.textViewState.leftPadding(width: 30)
        self.textViewZipCode.leftPadding(width: 30)
    }
    
    @IBAction func relationShipAction(_ sender: Any) {
        dropDownView.dropDownViewBorderWidth = 1
        dropDownView.dropDownViewBorderColor = UIColor.appBlueBorderColor.cgColor
        dropDownView.backgroundColor = UIColor.white
        isRelation = !isRelation
        if(isRelation)
        {
            self.dropDownView.isHidden = false
            self.dropDownView.show()
        }
        else
        {
            self.dropDownView.isHidden = true
        }
        let Single : DropDownDataObject! = DropDownDataObject()
        Single.labelData = "Single" as AnyObject
        let Committed : DropDownDataObject! = DropDownDataObject()
        Committed.labelData = "Committed" as AnyObject
        let Married : DropDownDataObject! = DropDownDataObject()
        Married.labelData = "Married" as AnyObject
        let Divorced : DropDownDataObject! = DropDownDataObject()
        Divorced.labelData = "Divorced" as AnyObject
        dropDownView.dataObject = [Single,Committed,Married,Divorced]
        isSelected  = true
        self.genderDropDownView.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let limitLenth = 5
        if textField == textViewZipCode{
           let  text = textViewZipCode.text!
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= limitLenth
        }
        else {
                return true
        }
        

       
        
    }
    
    
    
    
    @IBAction func genderAction(_ sender: Any) {
        genderDropDownView.dropDownViewBorderWidth = 1
        genderDropDownView.dropDownViewBorderColor = UIColor.appBlueBorderColor.cgColor
        genderDropDownView.backgroundColor = UIColor.white
        isGender = !isGender
        if(isGender == true)
        {
            self.genderDropDownView.isHidden = false
            self.genderDropDownView.show()
        }
        else
        {
            self.genderDropDownView.isHidden = true
        }
        let male : DropDownDataObject! = DropDownDataObject()
        male.labelData = "Male" as AnyObject
        let female : DropDownDataObject! = DropDownDataObject()
        female.labelData = "Female" as AnyObject
        genderDropDownView.dataObject = [male,female]
        isSelected = false
        self.dropDownView.isHidden = true
        
    }
    
    @IBAction func textDidEditing(_ sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(SMFAboutYouViewController.datePickerValueChanged), for: UIControlEvents.valueChanged)
        
    }
    
    
    @IBAction func contiuneAction(_ sender: Any) {
        
        if textFieldHandle.text != nil && textFieldHandle.text != "" {
            whoAmIPresenter?.setDataWithValue(firstName: textFieldFirstName.text, lastName: textFieldLastName.text, handle: textFieldHandle.text, DOB: textFieldDateOfBirth.text, realtionshipStatus: textFieldStatusRelationShip.text, gender: textFiledGender.text, intro: textViewIntroduce.text, location: textViewLocation.text , state: textViewState.text, zipCode: textViewZipCode.text!)
            self.delegate.scroll(value: 0)
        }else {
            self.showErrorAlert(alertTitle: "Error", alertMessage: "Handle can't be blank")
        }
    }
    
    
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        textFieldDateOfBirth.text = dateFormatter.string(from: sender.date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let stringDate = dateFormatter.string(from: sender.date)
        let ddate  = stringDate.replacingOccurrences(of: "/", with: "")
        GlobalVariable.userObj?.profile.dob = ddate
        print(ddate)
    }
    
    
    //MARK: - DropDown Delegate
    func data(_ val: AnyObject, index: Int) {
        if isSelected == false{
            self.textFiledGender.text = val as? String
            isGender = false
        }else{
            self.textFieldStatusRelationShip.text = val as? String
            self.textFieldStatusRelationShip.isUserInteractionEnabled = true
            isRelation = false
        }
    }
    
    func userAvailable(isAvail: Bool) {
        btnHandleheight.constant = 0
        btnHandle.setTitle("", for: .normal)
        if isAvail {
            makeGreen(textField: textFieldHandle)
        }else{
            makeRed(textField: textFieldHandle)
        }
    }
    
    func userDetail(userProfile: UserInfo) {
        GlobalVariable.userObj = userProfile
        initData()
    }
    
    
    func handleTap(recognizer: UITapGestureRecognizer) {
        self.dropDownView.isHidden = true
        self.genderDropDownView.isHidden = true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case textFieldHandle:
            if textFieldHandle.text != nil && textFieldHandle.text != "" {
                whoAmIPresenter?.checkAvability(value: textFieldHandle.text!)
            }else{
                self.makeRed(textField: textFieldHandle)
            }
            break
        default:
            break
        }
    }
    
}




