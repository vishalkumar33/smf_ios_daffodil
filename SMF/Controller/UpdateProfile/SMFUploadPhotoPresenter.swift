//
//  SMFUploadPhotoPresenter.swift
//  SMF
//
//  Created by Daffolapmac-25 on 18/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
import AFNetworking

@objc protocol SMFUploadPhtoDelegate:SMFCommonViewDelegate{
   
}
class SMFUploadPhotoPresenter {
    private var uploadPhotoDelegate    : SMFUploadPhtoDelegate?
    
    init(delegate : SMFUploadPhtoDelegate){
        uploadPhotoDelegate = delegate
    }
    
    func uploadPhoto(image:UIImage, type:String){
        if AFNetworkReachabilityManager.shared().isReachable {
            uploadPhotoDelegate?.showLoader!()
            var id = ""
            if GlobalVariable.userObj != nil {
                id = (GlobalVariable.userObj?.id)!
            }
            ImageUploadService.imageUploadRequest(type, id: id, image: image, success: { (response) in
                print(response)
                let filePathUrl:String = response["filePath"] as! String
                GlobalVariable.userObj?.profile.profileImage = filePathUrl
                DispatchQueue.main.async {
                    self.uploadPhotoDelegate?.hideLoader!()
                }
            }, failure: { (error) in
                DispatchQueue.main.async {
                    self.uploadPhotoDelegate?.hideLoader!()
                }
                print(error!)
            })
            
        }else{
            uploadPhotoDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: "Please check internet connection")
        }
    }
}
