//
//  SMFForgetPasswordPresenter.swift
//  SMF
//
//  Created by Jenkins on 1/16/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//


import Foundation
import UIKit

@objc protocol SMFForgetPasswordDelegate:SMFCommonViewDelegate{
   // func switchToHome()
    func showError(value:Int)
}

class SMFForgetPasswordPresenter: ResponseCallback {
    
    private var ForgetPasswordViewDelegate    : SMFForgetPasswordDelegate?
    private var serviceBuisnessForgetPassword = ServiceForgetPassword()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : SMFForgetPasswordDelegate){
        ForgetPasswordViewDelegate = delegate
    }
    
    
    // MARK: ServiceManagerDelegate Methods
    
    /**
     This method handles success response of Login API
     
     - parameter responseObject: Response object that contains success response
     */
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        ForgetPasswordViewDelegate?.hideLoader!()
        let sucessObject:ForgetPasswordResponse = responseObject as! ForgetPasswordResponse
        ForgetPasswordViewDelegate?.showErrorAlert!(alertTitle: "Sucess", alertMessage: (sucessObject.forgotData?.message)!)
        //This statement is used for hiding loader
       // self.ForgetPasswordViewDelegate?.hideLoader!()
       // self.ForgetPasswordViewDelegate?.switchToHome()
    }
    
    /**
     This Method handle error response of Login API
     
     - parameter error: NSError Object that contains error information
     */
    
    func servicesManagerError(error : String){
        ForgetPasswordViewDelegate?.hideLoader!()
        ForgetPasswordViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        //This statement is used for hiding loader
       // self.ForgetPasswordViewDelegate?.hideLoader!()
    }
    
    
    
    // MARK: Presenter Local methods
    
    /**
     This method calls Login API if emailId and Password both are Valid
     
     - parameter emailId:  User EmailId
     - parameter password: User Password
     */
    
    func doneButtonClickedWithEmailId(emailId:String) ->Void
    {
        
        //Checking whether emailId and Password is valid or not
        if(self.isValidEmail(emailId: emailId) == true)
        {
            //Showing Loader
            ForgetPasswordViewDelegate?.showLoader!()
            
            //Performing Login Operation with valid Input
            self.serviceBuisnessForgetPassword.performLoginWithValidInput(inputData: ForgetPasswordRequestModel.Builder().setEmail(email: emailId).addRequestHeader(key:"Content-Type", value: "application/json").build(), forgotPasswordDelegate: self)
        }
    }
    
    
    
    
    /**
     This method validate whether emailId And Password are Valid or not
     
     - parameter emailId:  EmialId that need to be validated whether it is valid or not
     - parameter password: Password that need to be validated whether it is valid or not
     
     - returns: Bool value true if both are valid otherwise function return false value
     */
    
    func isValidEmail(emailId:String) ->Bool
    {
        
        //This guard staement check whether email field is filled with email
        guard emailId.isEmptyString(str: emailId) == false
            else {
                ForgetPasswordViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: k_Email_Empty)
                return false
        }
        
        //This guard statement check whether email id is valid or not
        guard emailId.isValidEmailId(str: emailId) == true
            else {
                ForgetPasswordViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: k_Invalid_Email)
                return false
        }
        
        // This guard statement checks whether password field is filled or not
        
        return true
    }
    
    
    /**
     This method checks whether all input fields are empty or not
     
     - parameter registrationModel: registrationModel object contains input fields values
     
     - returns: return true if all the input fields are blank
     */
    
    func isAllInputFieldsAreEmpty(emailId:String) ->Bool
    {
        // Checking whether both field have some input or not
        if(emailId.isEmptyString(str: emailId))
        {
            return true
        }
        return false
    }
    
    func validateEmail(emailId:String) -> Bool {
        //This guard staement check whether email field is filled with email
        guard emailId.isEmptyString(str: emailId) == false
            else {
                ForgetPasswordViewDelegate?.showError(value: 1)
                return false
        }
        
        //This guard statement check whether email id is valid or not
        guard emailId.isValidEmailId(str: emailId) == true
            else {
                ForgetPasswordViewDelegate?.showError(value: 1)
                return false
        }
        return true
    }
    
       
}




