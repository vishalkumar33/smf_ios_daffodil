//
//  SMFForgetPasswordViewController.swift
//  SMF
//
//  Created by Jenkins on 1/16/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFForgetPasswordViewController: SMFBaseViewController {

    var forgotPasswordPresenter                      : SMFForgetPasswordPresenter?

    @IBOutlet weak var textFieldEmailId: MyCustomTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        self.forgotPasswordPresenter = SMFForgetPasswordPresenter(delegate: self)
        self.textFieldEmailId.leftPadding(width : 35)
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backGroundLayer")!)
        setNav(title: "FORGOT PASSWORD")
        setLeftNavBarButton()
        // Do any additional setup after loading the view.
    }
        
    func showError(value: Int) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView()
    {

    }

    @IBAction func doneAction(_ sender: Any) {
        
        self.forgotPasswordPresenter?.doneButtonClickedWithEmailId(emailId: self.textFieldEmailId.text! )
    }
   

}

extension SMFForgetPasswordViewController: SMFForgetPasswordDelegate {
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
}

