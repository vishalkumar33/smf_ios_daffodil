//
//  ServiceForgetPassword.swift
//  SMF
//
//  Created by Jenkins on 1/16/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation



import UIKit

class ServiceForgetPassword {
    
    
    /**
     This method is used for perform Login With Valid Input(Credentials)
     
     - parameter inputData: Contains info for Login
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performLoginWithValidInput(inputData: ForgetPasswordRequestModel, forgotPasswordDelegate:ResponseCallback ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForLogin()
        ForgetPasswordApiRequest().makeAPIRequest(reqFromData: inputData, errorResolver: errorResolver, responseCallback: forgotPasswordDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    //TODO:- String should be mapped from localizable string file.
    private func registerErrorForLogin() ->ErrorResolver{
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1011, message: "Email is not register")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_401, message: "Enter valid email address")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_400, message: "Invalid credentials")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1009, message: "Please check internet connection")
        return errorResolver
    }
    
    
}
