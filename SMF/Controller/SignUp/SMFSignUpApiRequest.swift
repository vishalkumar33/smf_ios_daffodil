//
//  SMFSignUpApiRequest.swift
//  SMF
//
//  Created by Jenkins on 1/5/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class SMFSignUpApiRequest : ApiRequestProtocol {
    
    var apiRequestURL:String = ""
    
    /**
     This method is used for Interacting with API layer for Get Request
     
     - parameter request:          NSDictionary object contains request parameter
     - parameter responseCallback: responseCallback sending callback of success and Error response of API
     */
    
    func makeAPIRequest(reqFromData: SMFSignUpRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        //Creating URL of API Call
        self.apiRequestURL = SMFConstant.BASE_URL  + reqFromData.getEndPoint()
        
        ServiceManager.sharedInstance.requestPOSTWithURL(urlString: self.apiRequestURL , andRequestDictionary: reqFromData.getRequestBody() , requestHeader: reqFromData.getRequestHeader(), responseCallBack: ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback ), returningClass: SMFSignUpResponse.self)
        
    }
    
    func makeAPIRequestForLogin(reqFromData: SMFSignUpRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        //Creating URL of API Call
        self.apiRequestURL = SMFConstant.BASE_URL  + reqFromData.getEndPointForLogin()
        
        ServiceManager.sharedInstance.requestPOSTWithURL(urlString: self.apiRequestURL , andRequestDictionary: reqFromData.getRequestBody() , requestHeader: reqFromData.getRequestHeader(), responseCallBack: ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback ), returningClass: SMFSignUpResponse.self)
        
    }
    
    
    /**
     This method is used for canceling API Call
     */
    
    func cancel() {
        ServiceManager.sharedInstance.cancelTaskWithURL(urlString: self.apiRequestURL)
    }
    
    /**
     This method return whether API call is in progress or not
     
     - returns: Boolean value either true or false
     */
    
    func isInProgress() -> Bool {
        return ServiceManager.sharedInstance.isInProgress(urlString: self.apiRequestURL)
    }
}
