//
//  SMFSignUpViewController.swift
//  SMF
//
//  Created by Shivam Srivastava on 23/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class SMFSignUpViewController: SMFBaseViewController {
    
    //Outlets
    @IBOutlet weak var firstNameTextField: MyCustomTextField!
    @IBOutlet weak var lastNameTextField: MyCustomTextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    var signUpPresenter                     : SMFSignUpPresenter?
    var responseObj                         : SMFSignUpResponse?
    var loginWithFacebookPresnter           : SMFLoginWithFacebookPresenter?
    let fbLoginManager                      : FBSDKLoginManager     =   FBSDKLoginManager()

    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.firstNameTextField.leftPadding(width: 30)
        firstNameTextField.delegate = self
        self.lastNameTextField.leftPadding(width: 30)
        lastNameTextField.delegate = self
        self.emailTextField.leftPadding(width: 30)
        emailTextField.delegate = self
        self.passwordTextField.leftPadding(width: 30)
        passwordTextField.delegate = self
        self.confirmPasswordTextField.leftPadding(width: 30)
        confirmPasswordTextField.delegate = self
        
        self.signUpPresenter = SMFSignUpPresenter(delegate: self)

        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backGroundLayer")!)
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func loginInAction(_ sender: AnyObject) {
            _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func signUpAction(_ sender: AnyObject) {

        self.signUpPresenter?.signUpButtonClicked(firstName: firstNameTextField.text! , lastName: lastNameTextField.text!, emailId: emailTextField.text!, password: passwordTextField.text!, confirmPassword: confirmPasswordTextField.text!)
        MyViewState.isSignUpSelected = true

        
    }
    
    @IBAction func loginWithFacebookAction(_ sender: Any) {
        self.signUpPresenter?.loginButtonClickedWithFacebook()
    }
    
    
}


extension SMFSignUpViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailTextField {
            if !(signUpPresenter?.validateEmail(emailId: emailTextField.text!))! {
                makeRed(textField: emailTextField)
            }else{
                makeWhite(textField: emailTextField)
            }
        }else if textField == firstNameTextField {
            if !(signUpPresenter?.validateFirstName(firstName: firstNameTextField.text!))! {
                makeRed(textField: firstNameTextField)
            }else{
                makeWhite(textField: firstNameTextField)
            }
        }else if textField == lastNameTextField {
            if !(signUpPresenter?.validateLastName(LastName: lastNameTextField.text!))!  {
                makeRed(textField: lastNameTextField)
            }else{
                makeWhite(textField: lastNameTextField)
            }
        }else if textField == passwordTextField {
            if !(signUpPresenter?.validatePassword(password: passwordTextField.text!))! {
                makeRed(textField: passwordTextField)
            }else{
                makeWhite(textField: passwordTextField)
            }
        }else if textField == confirmPasswordTextField {
            if (signUpPresenter?.validatePassword(password: confirmPasswordTextField.text!))! && ((signUpPresenter?.isEqualPasswordAndConfirm(password: passwordTextField.text!, confirmPassword: confirmPasswordTextField.text!))!) {
                makeWhite(textField: confirmPasswordTextField)
            }else{
                makeRed(textField: confirmPasswordTextField)
            }
        }
    }
}


extension SMFSignUpViewController: SMFSignUpViewDelegate,SMFLoginWithFacebookViewDelegate {
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func switchToAboutYou(responeObj: SMFSignUpResponse) {
        let pageControlVC =
            self.storyboard?.instantiateViewController(withIdentifier: "PageControlImageViewController") as! PageControlImageViewController
        
        self.navigationController?.pushViewController(pageControlVC, animated: true)
    }
    
    func loginWithFB() {
        fbLoginManager.logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email")){
                        self.getFBUserData()
                    }
                }
            }
        }
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, gender, locale, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let fbDetails = result as! NSDictionary
                    print(fbDetails)
                    self.loginWithFB(dict: fbDetails, accessToken: FBSDKAccessToken.current().tokenString)
                }
                self.fbLoginManager.logOut()
            })
        }
    }
    
    
    func loginWithFB(dict:NSDictionary, accessToken:String){
        let id: String = dict["id"] as! String
        let firstName = dict["first_name"] as! String
        let lastName = dict["last_name"] as! String
        let email = dict["email"] as! String
        let gender = dict["gender"] as! String
        let locale = dict["locale"] as! String
        let link: String = "https://www.facebook.com/app_scoped_user_id/\(id)/"
        
        loginWithFacebookPresnter?.LoginWithFacebookButtonClicked(accessToken: accessToken, id: id, email: email, firstName: firstName, lastName: lastName, facebookLing: link, gender: gender, locale: locale)
        
        
    }
    
    func switchToHome() {
        let loginViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFTabViewController") as! SMFTabViewController
        self.navigationController?.pushViewController(loginViewController, animated: true)
        
    }

    
    
    
}

