//
//  SMFSignUpPresenter.swift
//  SMF
//
//  Created by Jenkins on 1/5/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//


import Foundation
import UIKit

protocol SMFSignUpViewDelegate:SMFCommonViewDelegate{
    func loginWithFB()
    func switchToAboutYou(responeObj:SMFSignUpResponse)
}

class SMFSignUpPresenter: ResponseCallback {
    
    private var signUpViewDelegate    : SMFSignUpViewDelegate?
    private var serviceBuisnessSignUp = SMFServiceSignUp()
    
    var login = false
    
    var emailId:String?
    var password:String?
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : SMFSignUpViewDelegate){
        signUpViewDelegate = delegate
    }
    
    
    // MARK: ServiceManagerDelegate Methods
    
    /**
     This method handles success response of Login API
     
     - parameter responseObject: Response object that contains success response
     */
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        
        let obj = responseObject as! SMFSignUpResponse
        GlobalVariable.userObj = obj.user
        
        if !login {
            if (emailId != nil) && (password != nil){
                self.signIn(emailId: emailId!, password: password!)
                self.signUpViewDelegate?.switchToAboutYou(responeObj: obj)
            }
        }
        
        //This statement is used for hiding loader
        
        
        
        
    }
    
    /**
     This Method handle error response of Login API
     
     - parameter error: NSError Object that contains error information
     */
    
    func servicesManagerError(error : String){
        signUpViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        //This statement is used for hiding loader
        self.signUpViewDelegate?.hideLoader!()
    }
    
    
    
    func loginButtonClickedWithFacebook() ->Void
    {
        
        //Checking whether emailId and Password is valid or not
        self.signUpViewDelegate?.loginWithFB()
    }
    

    
    
    
    // MARK: Presenter Local methods
    
    /**
     This method calls Login API if emailId and Password both are Valid
     
     - parameter emailId:  User EmailId
     - parameter password: User Password
     */
    
    func signUpButtonClicked(firstName:String, lastName:String, emailId:String , password:String, confirmPassword:String) ->Void
    {
        //Checking whether emailId and Password is valid or not
        if(self.isValidData(firstName:firstName, lastName: lastName ,emailId: emailId, password: password,confirmPassword: confirmPassword) == true)
        {
            //Showing Loader
            signUpViewDelegate?.showLoader!()
            
            let fullName = "\(firstName) \(lastName)"
            self.login = false
            self.emailId = emailId
            self.password = password
            //Performing Sign up Operation with valid Input
            self.serviceBuisnessSignUp.performSignupWithValidInput(inputData: SMFSignUpRequestModel.Builder().setFirstName(firstName: firstName).setLastName(lastName: lastName).setFullName(fullName: fullName).setEmail(email: emailId).setPassword(password: password).addRequestHeader(key:"Content-Type", value: "application/json").build() , signUpDelegate: self)
        }
    }
    
    
    func signIn(emailId:String , password:String){
        login = true
        self.serviceBuisnessSignUp.performLoginWithValidInput(inputData: SMFSignUpRequestModel.Builder().setEmail(email: emailId).setPassword(password: password).addRequestHeader(key:"Content-Type", value: "application/json").build(), signUpDelegate: self)
        
    }
    
    
    /**
     This method validate whether emailId And Password are Valid or not
     
     - parameter emailId:  EmialId that need to be validated whether it is valid or not
     - parameter password: Password that need to be validated whether it is valid or not
     
     - returns: Bool value true if both are valid otherwise function return false value
     */
    
    func isValidData(firstName:String,lastName:String,emailId:String , password:String, confirmPassword: String) ->Bool
    {
        //This guard statement checks whether all fields have some inputs or not
        guard self.isAllInputFieldsAreEmpty(firstName: firstName,lastName: lastName ,emailId: emailId,password: password,confirmPassword: confirmPassword) == false
            else{
                signUpViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage:"All Fields are mendatory")
                return false
        }
        
        guard firstName.isEmptyString(str: firstName) == false
            else {
                signUpViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: "First Name can not be left blank")
                return false
        }
        
        guard lastName.isEmptyString(str: lastName) == false
            else {
                signUpViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: "Last Name can not be left blank")
                return false
        }

        
        //This guard staement check whether email field is filled with email
        guard emailId.isEmptyString(str: emailId) == false
            else {
                signUpViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: "Please enter Emaild Id")
                return false
        }
        
        //This guard statement check whether email id is valid or not
        guard emailId.isValidEmailId(str: emailId) == true
            else {
                signUpViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: "Please enter valid Email Id")
                return false
        }
        
        // This guard statement checks whether password field is filled or not
        guard password.isEmptyString(str: password) == false
            else{
                signUpViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: "Password can not be left blank")
                return false
        }
        
        //This guard statement checks whether password is valid or not
        guard password.isPasswordValid(str: password) == true
            else{
                signUpViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: "Passowrd should be greater than or equal to six character")
                return false
        }
        guard confirmPassword.isEmptyString(str: confirmPassword) == false
            else{
                signUpViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: "confirm Password can not be left blank")
                return false
        }
        
        
        guard self.isEqualPasswordAndConfirm(password: password, confirmPassword: confirmPassword) == true
            else{
                signUpViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage:"password && Confirm Password Should be same")
                return false
        }
        


        
        return true
    }
    
    
    /**
     This method checks whether all input fields are empty or not
     
     - parameter registrationModel: registrationModel object contains input fields values
     
     - returns: return true if all the input fields are blank
     */
    
    func isAllInputFieldsAreEmpty(firstName:String, lastName:String ,emailId:String , password:String,confirmPassword:String) ->Bool
    {
        // Checking whether both field have some input or not
        if((firstName.isEmptyString(str:firstName) && lastName.isEmptyString(str:lastName) && emailId.isEmptyString(str: emailId)) && (password.isEmptyString(str: password)) && (firstName.isEmptyString(str:confirmPassword)))
        {
            return true
        }
        return false
    }
    
    func isEqualPasswordAndConfirm(password:String,confirmPassword:String)->Bool
    {
        if password == confirmPassword {
            return true
        }
        return false
    }
    
    
    func validateEmail(emailId:String) -> Bool {
        //This guard staement check whether email field is filled with email
        guard emailId.isEmptyString(str: emailId) == false
            else {
                
                return false
        }
        
        //This guard statement check whether email id is valid or not
        guard emailId.isValidEmailId(str: emailId) == true
            else {
                
                return false
        }
        return true
    }
    
    func validatePassword(password:String) -> Bool {
        //This guard staement check whether email field is filled with email
        guard password.isEmptyString(str: password) == false
            else {
                
                return false
        }
        return true
    }
    
    func validateFirstName(firstName:String) -> Bool {
        guard firstName.isEmptyString(str: firstName) == false
            else {
                
                return false
        }
        return true
    }
    
    func validateLastName(LastName:String) -> Bool {
        guard LastName.isEmptyString(str: LastName) == false
            else {
                
                return false
        }
        return true
    }
    
    
    
    
}





