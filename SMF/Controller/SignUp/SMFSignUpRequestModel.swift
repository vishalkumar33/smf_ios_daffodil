//
//  SMFSignUpRequestModel.swift
//  SMF
//
//  Created by Jenkins on 1/5/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//


import Foundation


//This class is used for constructing Service Request Model

class SMFSignUpRequestModel {
    
    //Note :- Property Name must be same as key used in request API
    
    let requestBody   : [String:AnyObject]?
    let requestHeader : [String:AnyObject]?
    
    //This is a Private Constrctor instantiating Service Model Request Object
    
    private init(builderObject:Builder){
        
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builderObject.requestBody
        self.requestHeader = builderObject.requestHeader
    }
    
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var requestBody = [String:AnyObject]()
        var requestHeader = [String:AnyObject]()
        
        
        /**
         This method is used for setting Last Name
         
         - parameter Last Name: String parameter that is going to be set on Last Name
         
         - returns: returning Builder Object
         */
        
        func setLastName(lastName:String) ->Builder{
            requestBody["lastName"] = lastName as AnyObject?
            return self
        }
        
        /**
         This method is used for setting First Name
         
         - parameter Full Name: String parameter that is going to be set on Full Name
         
         - returns: returning Builder Object
         */
        
        func setFullName(fullName:String) ->Builder{
            requestBody["fullName"] = fullName as AnyObject?
            return self
        }
        
        /**
         This method is used for setting First Name
         
         - parameter First Name: String parameter that is going to be set on First Name
         
         - returns: returning Builder Object
         */
        
        func setFirstName(firstName:String) ->Builder{
            requestBody["firstName"] = firstName as AnyObject?
            return self
        }

        
        /**
         This method is used for setting email
         
         - parameter email: String parameter that is going to be set on Email
         
         - returns: returning Builder Object
         */
        
        func setEmail(email:String) ->Builder{
            requestBody["email"] = email as AnyObject?
            return self
        }
        
        /**
         This method is used for setting password
         
         - parameter password: String parameter that is going to be set on Password
         
         - returns: returning Builder object
         */
        
        func setPassword(password:String) ->Builder{
            requestBody["password"] = password as AnyObject?
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method returns the Service request Model
         
         - returns: Returns ServiceRequestModel Object having set Emailid and Password
         */
        
        func build() ->SMFSignUpRequestModel{
            return SMFSignUpRequestModel(builderObject: self)
        }
        
    }
    
    
    /**
     This method is used for creating End Point
     
     - returns: returning API End Point
     */
    
    func getEndPoint() -> String {
        return "/register"
    }
    
    func getEndPointForLogin() -> String {
        return "/login"
    }
    
    /**
     This method is used for
     
     - returns: returning request body
     */
    
    func getRequestBody() -> [String:AnyObject] {
        return self.requestBody!
    }
    
    /**
     This method is used for containing Request Headers
     
     - returns: Dictionary contains header
     */
    
    func getRequestHeader() -> [String:AnyObject] {
        return self.requestHeader!
    }
    
    
}


