//
//  SMFLoginPresenter.swift
//  SMF
//
//  Created by Daffolapmac-25 on 05/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
import UIKit

@objc protocol SMFLoginViewDelegate:SMFCommonViewDelegate{
    func loginWithFB()
    func switchToHome()
    func showError(value:Int)
    func switchToAbout()
}

class SMFLoginPresenter: ResponseCallback {
    
    private var loginViewDelegate    : SMFLoginViewDelegate?
    private var serviceBuisnessLogin = ServiceLogin()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : SMFLoginViewDelegate){
        loginViewDelegate = delegate
    }
    
    
    // MARK: ServiceManagerDelegate Methods
    
    /**
     This method handles success response of Login API
     
     - parameter responseObject: Response object that contains success response
     */
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        
        let obj = responseObject as! LoginResponse
        GlobalVariable.userObj = obj.user
        UserDefault.set(obj.user?.id, forKey: "userId")
        let profileURL = NSURL(string: (obj.user?.profile.profileImage)!) as! URL
        UserDefault.set(profileURL, forKey: "userProfileImage")
        let name = "\((obj.user?.profile.firstName)!) \((obj.user?.profile.lastName)!)"
        UserDefault.set(name, forKey: "userName")
        let city = obj.user?.profile.city
        UserDefault.set(city, forKey: "userCity")
        UserDefault.set(true, forKey: "isUserLoggedIn")
        let email = obj.user?.emails[0].address
        UserDefault.set(email, forKey: "userEmail")
        let locationName:[SavedLocation] = (obj.user?.profile.savedLocation)!
        print(locationName.count)
        let placesData = NSKeyedArchiver.archivedData(withRootObject: locationName)
        UserDefaults.standard.set(placesData, forKey: "places")
        var locArray = [""]
        
        for loc in locationName {
            locArray.append(loc.name!)
        }
        UserDefaults.standard.set(locArray, forKey: "userLocation")
        
        //Connect to socket
        MessageService.instance.connectToSocket(userId: UserDefault.string(forKey: "userId")!)
        
        //This statement is used for hiding loader
        self.loginViewDelegate?.hideLoader!()
        
        if (obj.user?.profile.handle != nil) && (obj.user?.profile.handle != ""){
            self.loginViewDelegate?.switchToHome()
        }else{
            self.loginViewDelegate?.switchToAbout()
        }
        
        
    }
    
    /**
     This Method handle error response of Login API
     
     - parameter error: NSError Object that contains error information
     */
    
    func servicesManagerError(error : String){
        loginViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        //This statement is used for hiding loader
        self.loginViewDelegate?.hideLoader!()
    }
    
    
    
    // MARK: Presenter Local methods
    
    /**
     This method calls Login API if emailId and Password both are Valid
     
     - parameter emailId:  User EmailId
     - parameter password: User Password
     */
    
    func loginButtonClickedWithEmailIdAndPassword(emailId:String , password:String) ->Void
    {
        
        
        
        
        //Checking whether emailId and Password is valid or not
        if(self.isValidEmailAndPassword(emailId: emailId, password: password) == true)
        {
            //Showing Loader
            loginViewDelegate?.showLoader!()
            let deviceToken = UserDefaults.standard.object(forKey: "deviceToken")
            //Performing Login Operation with valid Input
            if deviceToken != nil {
                self.serviceBuisnessLogin.performLoginWithValidInput(inputData: LoginRequestModel.Builder().setEmail(email: emailId).setPassword(password: password).setDeviceId(deviceId: deviceToken as! String).addRequestHeader(key:"Content-Type", value: "application/json").build(), loginDelegate: self)
            }else{
                self.serviceBuisnessLogin.performLoginWithValidInput(inputData: LoginRequestModel.Builder().setEmail(email: emailId).setPassword(password: password).addRequestHeader(key:"Content-Type", value: "application/json").build(), loginDelegate: self)
            }
            
        }
    }
   // .setDeviceId(deviceId: deviceToken as! String).
    /**
     This method calls Login API if emailId and Password both are Valid
     
     - parameter emailId:  User EmailId
     - parameter password: User Password
     */
    
    func loginButtonClickedWithFacebook() ->Void
    {
        
        //Checking whether emailId and Password is valid or not
        self.loginViewDelegate?.loginWithFB()
    }
    
    /**
     This method validate whether emailId And Password are Valid or not
     
     - parameter emailId:  EmialId that need to be validated whether it is valid or not
     - parameter password: Password that need to be validated whether it is valid or not
     
     - returns: Bool value true if both are valid otherwise function return false value
     */
    
    func isValidEmailAndPassword(emailId:String , password:String) ->Bool
    {
        //This guard statement checks whether all fields have some inputs or not
        guard self.isAllInputFieldsAreEmpty(emailId: emailId , password:password) == false
            else{
                loginViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: k_All_Field_Empty)
                return false
        }
        
        //This guard staement check whether email field is filled with email
        guard emailId.isEmptyString(str: emailId) == false
            else {
                loginViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: k_Email_Empty)
                return false
        }
        
        //This guard statement check whether email id is valid or not
        guard emailId.isValidEmailId(str: emailId) == true
            else {
                loginViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: k_Invalid_Email)
                return false
        }
        
        // This guard statement checks whether password field is filled or not
        guard password.isEmptyString(str: password) == false
            else{
                loginViewDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: k_Password_Empty)
                return false
        }
        
        return true
    }
    
    
    /**
     This method checks whether all input fields are empty or not
     
     - parameter registrationModel: registrationModel object contains input fields values
     
     - returns: return true if all the input fields are blank
     */
    
    func isAllInputFieldsAreEmpty(emailId:String , password:String) ->Bool
    {
        // Checking whether both field have some input or not
        if(emailId.isEmptyString(str: emailId) && password.isEmptyString(str: password))
        {
            return true
        }
        return false
    }
    
    func validateEmail(emailId:String) -> Bool {
        //This guard staement check whether email field is filled with email
        guard emailId.isEmptyString(str: emailId) == false
            else {
                loginViewDelegate?.showError(value: 1)
                return false
        }
        
        //This guard statement check whether email id is valid or not
        guard emailId.isValidEmailId(str: emailId) == true
            else {
                loginViewDelegate?.showError(value: 1)
                return false
        }
        return true
    }
    
    func validatePassword(password:String) -> Bool {
        //This guard staement check whether email field is filled with email
        guard password.isEmptyString(str: password) == false
            else {
                loginViewDelegate?.showError(value: 3)
                return false
        }
        return true
    }
    
    
    
    
    
}




