//
//  SMFLoginViewController.swift
//  SMF
//
//  Created by Shivam Srivastava on 22/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class SMFLoginViewController: SMFBaseViewController{

    //Outlets
    @IBOutlet weak var login                : UIButton!
    @IBOutlet weak var loginWithFacebook    : UIButton!
    @IBOutlet weak var emailTextField       : UITextField!
    @IBOutlet weak var passwordTextField    : UITextField!
    var loginPresenter                      : SMFLoginPresenter?
    var loginWithFacebookPresnter           : SMFLoginWithFacebookPresenter?
    let fbLoginManager                      : FBSDKLoginManager     =   FBSDKLoginManager()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //setField()
        emailTextField.delegate = self
        passwordTextField.delegate = self
        emailTextField.leftPadding(width: 30)
        passwordTextField.leftPadding(width: 30)
        emailTextField.autocorrectionType = .no
        self.loginPresenter = SMFLoginPresenter(delegate: self)
        self.loginWithFacebookPresnter = SMFLoginWithFacebookPresenter(delegate: self)
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backGroundLayer")!)
    }
    
    
    func setField(){
        emailTextField.text = "gulrez.karim@daffodilsw.com"
        passwordTextField.text = "hrhk@1234"
    }
    
    @IBAction func termsAndConditionAction(_ sender: Any) {
        let termsAndConditions =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFTermsOfConditionsViewController") as! SMFTermsOfConditionsViewController
        self.navigationController?.pushViewController(termsAndConditions, animated: true)

        
    }
    
    @IBAction func forgetAction(_ sender: Any) {
        let changePassrordViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFForgetPasswordViewController") as! SMFForgetPasswordViewController
        self.navigationController?.pushViewController(changePassrordViewController, animated: true)
    }
    
    
    @IBAction func signUpAction(_ sender: AnyObject) {
        let loginViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFSignUpViewController") as! SMFSignUpViewController
        self.navigationController?.pushViewController(loginViewController, animated: true)
    }
    
 
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false

    }

    @IBAction func logInAction(_ sender: Any) {
       
        self.loginPresenter?.loginButtonClickedWithEmailIdAndPassword(emailId: self.emailTextField.text!,password: self.passwordTextField.text!)
    }
    
    @IBAction func loginWithFacebookAction(_ sender: Any) {
        self.loginPresenter?.loginButtonClickedWithFacebook()
        
    }
    
    
    
    
}

extension SMFLoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailTextField {
            if !(loginPresenter?.validateEmail(emailId: emailTextField.text!))! {
                makeRed(textField: emailTextField)
            }else{
                makeWhite(textField: emailTextField)
            }
        }else if textField == passwordTextField {
            if !(loginPresenter?.validatePassword(password: passwordTextField.text!))! {
                makeRed(textField: passwordTextField)
            }else{
                makeWhite(textField: passwordTextField)
            }
        }
    }
}

extension SMFLoginViewController: SMFLoginViewDelegate, SMFLoginWithFacebookViewDelegate {
    
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func switchToHome() {
        let loginViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFTabViewController") as! SMFTabViewController
       
        self.navigationController?.setViewControllers([loginViewController], animated: true)
    }
    
    func switchToAbout() {
        let editProfileViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "PageControlImageViewController") as! PageControlImageViewController
        self.navigationController?.pushViewController(editProfileViewController, animated: true)
        
    }
    
    func loginWithFB() {
        fbLoginManager.logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email")){
                        self.getFBUserData()
                    }
                }
            }
        }
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, gender, locale, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let fbDetails = result as! NSDictionary
                    print(fbDetails)
                    self.loginWithFB(dict: fbDetails, accessToken: FBSDKAccessToken.current().tokenString)
                    self.showLoader()
                }
                self.fbLoginManager.logOut()
            })
        }
    }
    
    
    func loginWithFB(dict:NSDictionary, accessToken:String){
        let id: String = dict["id"] as! String
        let firstName = dict["first_name"] as! String
        let lastName = dict["last_name"] as! String
        let email = dict["email"] as! String
        let gender = dict["gender"] as! String
        let locale = dict["locale"] as! String
        let link: String = "https://www.facebook.com/app_scoped_user_id/\(id)/"
        
        loginWithFacebookPresnter?.LoginWithFacebookButtonClicked(accessToken: accessToken, id: id, email: email, firstName: firstName, lastName: lastName, facebookLing: link, gender: gender, locale: locale)
        
        
    }
    
    
    
    func showError(value: Int) {
        switch value {
        case 1:
            makeRed(textField: emailTextField)
            break
        case 2:
            makeRed(textField: emailTextField)
            break
        case 3:
            makeRed(textField: passwordTextField)
            break
        default:
            break
        }
    }
}
