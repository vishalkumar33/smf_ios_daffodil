//
//  SMFChangePasswordViewController.swift
//  SMF
//
//  Created by Jenkins on 1/10/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFChangePasswordViewController: SMFBaseViewController {

    @IBOutlet weak var textFileldoldPassword: MyCustomTextField!
    @IBOutlet weak var textFieldNewPassword: MyCustomTextField!
    @IBOutlet weak var textFiledConfirmPAssword: MyCustomTextField!
    
    @IBOutlet weak var btnDone: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNav(title:"CHANGE PASSWORD")
        setLeftNavBarButton()
        textFieldNewPassword.leftPadding(width: 40)
        textFileldoldPassword.leftPadding(width: 40)
        textFiledConfirmPAssword.leftPadding(width: 40)
        initView()
        
        // Do any additional setup after loading the view.
    }
    
    func initView(){
        textFileldoldPassword.isSecureTextEntry = true
        textFiledConfirmPAssword.isSecureTextEntry = true
        textFieldNewPassword.isSecureTextEntry = true
    }
    
    
    func initDic()-> NSMutableDictionary{
        let dic:NSMutableDictionary = NSMutableDictionary()
        dic["oldPassword"] = textFileldoldPassword.text
        dic["newPassword"] = textFiledConfirmPAssword.text
        return dic
    }

    
    @IBAction func changePassword(_ sender: Any) {
        if validateForm() {
            Service.changePassword(data: initDic(), success: { (response) in
                self.showErrorAlert(alertTitle: "Success", alertMessage: "You successfully changed the password", VC: self)
            }) { (error) in
                self.showErrorAlert(alertTitle: "Error", alertMessage: (error?.domain)!, VC: self)
            }
        }
    }
    
    func validateForm() -> Bool{
        if textFileldoldPassword.text == nil || textFieldNewPassword.text == nil || textFiledConfirmPAssword.text == nil {
            self.showErrorAlert(alertTitle: "Error", alertMessage: "Please fill all the values", VC: self)
            return false
        }else if (textFileldoldPassword.text?.isEmpty)! ||  (textFieldNewPassword.text?.isEmpty)! ||  (textFiledConfirmPAssword.text?.isEmpty)! {
            self.showErrorAlert(alertTitle: "Error", alertMessage: "Please fill all the values", VC: self)
            return false
        }else if textFieldNewPassword.text != textFiledConfirmPAssword.text {
            self.showErrorAlert(alertTitle: "Error", alertMessage: "New password is not matched with the confirm password", VC: self)
            return false
        }else {
            return true
        }
        
    }
    
    
    
        

}
