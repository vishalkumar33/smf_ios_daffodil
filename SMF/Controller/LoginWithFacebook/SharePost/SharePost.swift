//
//  SharePost.swift
//  SMF
//
//  Created by Jenkins on 2/20/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//
protocol SharePostDelegate1{
    func sharePost(_ b:UIButton)
}
import Foundation
class SharePost:UIView{
    var delegate :SharePostDelegate1!
    override func awakeFromNib() {
        
    }
    @IBAction func sendAction(_ sender: Any) {
        self.delegate.sharePost(sender as! UIButton)
    }
}
