//
//  SharePostPresenter.swift
//  SMF
//
//  Created by Vishal on 20/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//





import Foundation
import UIKit

@objc protocol SharePostDelegate:SMFCommonViewDelegate{
}

class  SharePostPresenter: ResponseCallback {
    
    private var SharePosstViewDelegate    : SharePostDelegate?
    private var serviceBuisnessPostUpdate = ServiceSharePost()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : SharePostDelegate){
        SharePosstViewDelegate = delegate
    }
    
    
    // MARK: ServiceManagerDelegate Methods
    
    /**
     This method handles success response of Login API
     
     - parameter responseObject: Response object that contains success response
     */
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        SharePosstViewDelegate?.hideLoader!()
        //This statement is used for hiding loader
        // self.ForgetPasswordViewDelegate?.hideLoader!()
        // self.ForgetPasswordViewDelegate?.switchToHome()
    }
    
    /**
     This Method handle error response of Login API
     
     - parameter error: NSError Object that contains error information
     */
    
    func servicesManagerError(error : String){
        SharePosstViewDelegate?.hideLoader!()
        SharePosstViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        //This statement is used for hiding loader
        // self.ForgetPasswordViewDelegate?.hideLoader!()
    }
    
    
    
    // MARK: Presenter Local methods
    
    /**
     This method calls Login API if emailId and Password both are Valid
     
     - parameter emailId:  User EmailId
     - parameter password: User Password
     */
    
    func postButtonClickedWithPost(newPost:String?, lat:String, long:String,postId:String) ->Void
    {
       
        SharePosstViewDelegate?.showLoader!()
        
        if newPost != nil {
            let trimmedPost = newPost?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.serviceBuisnessPostUpdate.performPostWithValidInput(inputData: SharePostRequestModel.Builder().setCaption(caption: trimmedPost!).setPostId(postId: postId).setCordinate(lat: lat, long: long).addRequestHeader(key:"Content-Type", value: "application/json").build(), sharePostDelegate: self)
        }else{
            self.serviceBuisnessPostUpdate.performPostWithValidInput(inputData: SharePostRequestModel.Builder().setPostId(postId: postId).setCordinate(lat: lat, long: long).addRequestHeader(key:"Content-Type", value: "application/json").build(), sharePostDelegate: self)
        }
        
        
    }
    
   }




