//
//  SMFSharePostTableViewCell.swift
//  SMF
//
//  Created by Jenkins on 2/20/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFSharePostTableViewCell: UITableViewCell {

    @IBOutlet weak var captionTextView: SMFPlaceholderTextView!
    override func awakeFromNib() {
        super.awakeFromNib()
               // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
