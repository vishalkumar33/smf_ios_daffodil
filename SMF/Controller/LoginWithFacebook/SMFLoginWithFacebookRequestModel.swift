//
//  SMFLoginWithFacebookRequestModel.swift
//  SMF
//
//  Created by Jenkins on 1/19/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//


import Foundation


//This class is used for constructing Service Request Model

class SMFLoginWithFacebookRequestModel {
    
    //Note :- Property Name must be same as key used in request API
    
    let requestBody   : [String:AnyObject]?
    let requestHeader : [String:AnyObject]?
    
    //This is a Private Constrctor instantiating Service Model Request Object
    
    private init(builderObject:Builder){
        
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builderObject.requestBody
        self.requestHeader = builderObject.requestHeader
    }
    
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var requestBody = [String:AnyObject]()
        var requestHeader = [String:AnyObject]()
        
        
        /**
         This method is used for setting Last Name
         
         - parameter Last Name: String parameter that is going to be set on Last Name
         
         - returns: returning Builder Object
         */
        
        func setAccessToken(accessToken:String) ->Builder{
            requestBody["accessToken"] = accessToken as AnyObject?
            return self
        }
        
        /**
         This method is used for setting First Name
         
         - parameter Full Name: String parameter that is going to be set on Full Name
         
         - returns: returning Builder Object
         */
        
        func setId(id:String) ->Builder{
            requestBody["id"] = id as AnyObject?
            return self
        }
        
        /**
         This method is used for setting First Name
         
         - parameter First Name: String parameter that is going to be set on First Name
         
         - returns: returning Builder Object
         */
        
        func setName(name:String) ->Builder{
            requestBody["name"] = name as AnyObject?
            return self
        }
        
        
        /**
         This method is used for setting email
         
         - parameter email: String parameter that is going to be set on Email
         
         - returns: returning Builder Object
         */
        
        func setEmail(email:String) ->Builder{
            requestBody["email"] = email as AnyObject?
            return self
        }
        
        /**
         This method is used for setting password
         
         - parameter password: String parameter that is going to be set on Password
         
         - returns: returning Builder object
         */
        func setFirstName(first_name:String) ->Builder{
            requestBody["first_name"] = first_name as AnyObject?
            return self
        }

        func setLastName(last_name:String) ->Builder{
            requestBody["last_name"] = last_name as AnyObject?
            return self
        }

        
        
        func setLink(link:String) ->Builder{
            requestBody["link"] = link as AnyObject?
            return self
        }
        
        func setGender(gender:String) ->Builder{
            requestBody["gender"] = gender as AnyObject?
            return self
        }
        
        func setLocale(locale:String) ->Builder{
            requestBody["locale"] = locale as AnyObject?
            return self
        }
        

        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method returns the Service request Model
         
         - returns: Returns ServiceRequestModel Object having set Emailid and Password
         */
        
        func build() ->SMFLoginWithFacebookRequestModel{
            return SMFLoginWithFacebookRequestModel(builderObject: self)
        }
        
    }
    
    
    /**
     This method is used for creating End Point
     
     - returns: returning API End Point
     */
    
    func getEndPoint() -> String {
        return "/facebook/login"
    }
    
    /**
     This method is used for
     
     - returns: returning request body
     */
    
    func getRequestBody() -> [String:AnyObject] {
        return self.requestBody!
    }
    
    /**
     This method is used for containing Request Headers
     
     - returns: Dictionary contains header
     */
    
    func getRequestHeader() -> [String:AnyObject] {
        return self.requestHeader!
    }
    
    
}


