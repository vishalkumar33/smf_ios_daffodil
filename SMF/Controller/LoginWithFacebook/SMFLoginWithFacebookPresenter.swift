//
//  SMFLoginWithFacebookPresenter.swift
//  SMF
//
//  Created by Jenkins on 1/5/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//


import Foundation
import UIKit

protocol SMFLoginWithFacebookViewDelegate:SMFCommonViewDelegate{
    func switchToHome()

}

class SMFLoginWithFacebookPresenter: ResponseCallback {
    
    private var loginWithFacebookViewDelegate    : SMFLoginWithFacebookViewDelegate?
    private var serviceLoginWithFacebook = SMFServiceLoginWithFacebook()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : SMFLoginWithFacebookViewDelegate){
        loginWithFacebookViewDelegate = delegate
    }
    
    
    // MARK: ServiceManagerDelegate Methods
    
    /**
     This method handles success response of Login API
     
     - parameter responseObject: Response object that contains success response
     */
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        
        //This statement is used for hiding loader
        loginWithFacebookViewDelegate?.hideLoader!()
        self.loginWithFacebookViewDelegate?.switchToHome()


        
    }
    
    /**
     This Method handle error response of Login API
     
     - parameter error: NSError Object that contains error information
     */
    
    func servicesManagerError(error : String){
        loginWithFacebookViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        //This statement is used for hiding loader
        self.loginWithFacebookViewDelegate?.hideLoader!()
    }
    
    
    
    // MARK: Presenter Local methods
    
    /**
     This method calls Login API if emailId and Password both are Valid
     
     - parameter emailId:  User EmailId
     - parameter password: User Password
     */
    
    func LoginWithFacebookButtonClicked(accessToken:String,id: String, email:String , firstName:String, lastName:String, facebookLing:String, gender:String, locale:String) ->Void
    {
        //Checking whether emailId and Password is valid or not
        
            //Showing Loader
            loginWithFacebookViewDelegate?.showLoader!()
            let fullName = "\(firstName) \(lastName)"
            //Performing Sign up Operation with valid Input
        
            self.serviceLoginWithFacebook.performLoginWithFacebook(inputData: SMFLoginWithFacebookRequestModel.Builder().setAccessToken(accessToken: accessToken).setId(id: id).setEmail(email: email).setFirstName(first_name: firstName).setLastName(last_name: lastName).setName(name: fullName).setLink(link: facebookLing).setGender(gender: gender).setLocale(locale: locale).addRequestHeader(key: "Content-Type", value: "application/json").build(), LoginWithFacebookDelegate: self)
    }
    
    
    
    
    
}





