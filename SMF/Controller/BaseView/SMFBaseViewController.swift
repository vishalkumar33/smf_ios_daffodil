//
//  SMFBaseViewController.swift
//  SMF
//
//  Created by Jenkins on 1/4/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit
struct MyViewState {
static var isSignUpSelected  :Bool?
}
struct MyPostState {
    static var isPostSelected :Bool?
}

struct MyPostId {
    static var isPost :Int?
    static var postId :String?

}
class SMFBaseViewController: UIViewController {
    
    
    override func viewDidLoad() {
        

    }
    
    
    func setNav(title:String)
    {
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "smf_gradient "), for: .default)
        self.navigationItem.title = title
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]

    }

    func setNavWithoutTitle(){
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "smf_gradient "), for: .default)
        self.navigationItem.title = title
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    func setLeftNavBarButton(){
        let backBtn = UIButton(type: .custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 18, height: 18)
        backBtn.setImage(#imageLiteral(resourceName: "backIcon"), for: .normal)
        backBtn.addTarget(self, action: #selector(SMFForgetPasswordViewController.addTapped), for: UIControlEvents.touchUpInside)
        let back = UIBarButtonItem(customView: backBtn)
        self.navigationItem.setLeftBarButton(back, animated: true)
    }
    
    func addTapped()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }


    /**
     This Method is used for showing Error Alert
     
     - parameter alertTitle:   This parameter contain Alert Title
     - parameter alertMessage: This parameter contain Alert Message that need to be shown
     - parameter VC:           ViewConroller On Which we have to show error alert
     */
    
    internal func showErrorAlert(alertTitle: String, alertMessage: String,VC : AnyObject?){
        
        // Checkin Availability of UIAlertController because if our target is less than IOS 8 then in that case we will use UIAlertView
        if #available(iOS 8, *) {
            
            // Instanciating UIAlertController Object and showing Alert
            let alertController = UIAlertController(title: alertTitle ,
                                                    message:alertMessage,
                                                    preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss",
                                                    style: UIAlertActionStyle.default,
                                                    handler: nil))
            VC!.present(alertController, animated: true, completion: nil)
        }
        else {
            // Instanciating UIAlertView Object and showing Alert
            let alertView = UIAlertView(title: alertTitle, message: alertMessage, delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "Dismiss")
            alertView.show()
        }
    }
    
    
    internal func makeRed(textField:UITextField){
        textField.layer.borderColor = UIColor.red.cgColor
        textField.textColor = UIColor.red
    }
    
    internal func makeWhite(textField:UITextField){
        textField.layer.borderColor = UIColor.white.cgColor
        textField.textColor = UIColor.white
    }
    
    
    
    internal func makeGreen(textField:UITextField){
        textField.layer.borderColor = UIColor.green.cgColor
        //textField.textColor = UIColor(colorLiteralRed: 80, green: 81, blue: 82, alpha: 0.2)
        textField.textColor = UIColor.green
    }
}
