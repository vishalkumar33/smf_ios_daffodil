//
//  SMFPostUpdateRequestModel.swift
//  SMF
//
//  Created by Jenkins on 1/23/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//





import Foundation


//This class is used for constructing Service Request Model

class SMFPostUpdateRequestModel {
    
    //Note :- Property Name must be same as key used in request API
    
    let requestBody   : [String:AnyObject]?
    let requestHeader : [String:AnyObject]?
    
    //This is a Private Constrctor instantiating Service Model Request Object
    
    private init(builderObject:Builder){
        
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builderObject.requestBody
        self.requestHeader = builderObject.requestHeader
    }
    
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var requestBody = [String:AnyObject]()
        var requestHeader = [String:AnyObject]()
        
        /**
         This method is used for setting email
         
         - parameter email: String parameter that is going to be set on Email
         
         - returns: returning Builder Object
         */
        
        func setPost(post:String) ->Builder{
            requestBody["content"] = post as AnyObject?
            return self
        }
        func setGroupId(group:String) ->Builder{
            requestBody["group"] = group as AnyObject?
            return self
        }
        
        func setCordinate(lat:String, long:String) -> Builder {
            let cod = [lat, long]
            requestBody["coordinates"] = cod as AnyObject?
            return self
        }
        
        func setHidden(hidden:Bool) -> Builder {
            requestBody["hidden"] = hidden as AnyObject?
            return self
        }
        
        func setImageUrl(url:String?) ->Builder{
            requestBody["imageUrl"] = url as AnyObject?
            return self
        }
        
        func setVideoUrl(videoUrl:String?) ->Builder{
            requestBody["videoUrl"] = videoUrl as AnyObject?
            return self
        }

        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method returns the Service request Model
         
         - returns: Returns ServiceRequestModel Object having set Emailid and Password
         */
        
        func build() ->SMFPostUpdateRequestModel{
            return SMFPostUpdateRequestModel(builderObject: self)
        }
        
    }
    
    
    /**
     This method is used for creating End Point
     
     - returns: returning API End Point
     */
    
    func getEndPoint() -> String {
        return "/newPost"
    }
    
    
    
    func getTribeEndPoint() ->String{
        return "/group/newPost"

    }
    /**
     This method is used for
     
     - returns: returning request body
     */
    
    func getRequestBody() -> [String:AnyObject] {
        return self.requestBody!
    }
    
    /**
     This method is used for containing Request Headers
     
     - returns: Dictionary contains header
     */
    
    func getRequestHeader() -> [String:AnyObject] {
        return self.requestHeader!
    }
    
    
}


