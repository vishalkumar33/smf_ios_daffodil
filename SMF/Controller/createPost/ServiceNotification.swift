//
//  ServiceNotification.swift
//  SMF
//
//  Created by Jenkins on 2/11/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
class ServiceNotification{

    func performMemberPutWithValidInput(inputData: NotificationRequestModel, NotificationListDelegate:ResponseCallback ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForLogin()
        NotificationApiRequest().makeAPIRequest(reqFromData: inputData, errorResolver: errorResolver, responseCallback: NotificationListDelegate)
    }


    func performPostWithValidInput(inputData: NotificationRequestModel, NotificationListDelegate:ResponseCallback ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForLogin()
        NotificationApiRequest().makeAPIRequestPost(reqFromData: inputData, errorResolver: errorResolver, responseCallback: NotificationListDelegate)
    }



    /**
     This method is used for adding set of Predefined Error coming from server
     */

    //TODO:- String should be mapped from localizable string file.
    private func registerErrorForLogin() ->ErrorResolver{
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_400, message: "Some mandatory field is Missing.")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_401, message: "User not Logged In")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1009, message: "Please check internet connection")
        //   errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1011, message: "Email Id is already register")
        return errorResolver
    }


}
