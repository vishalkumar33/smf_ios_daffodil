//
//  SMFPostandUpdatePresenter.swift
//  SMF
//
//  Created by Jenkins on 1/23/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//



import Foundation
import UIKit

@objc protocol SMFPostandUpdateDelegate:SMFCommonViewDelegate{
     func popHome()
    func showError(value:Int)
}

class SMFPostandUpdatePresenter: ResponseCallback {
    
    private var PostUpdateViewDelegate    : SMFPostandUpdateDelegate?
    private var serviceBuisnessPostUpdate = SMFServicePostUpdate()
    private var serviceTribeBusinessPostUpdate = SMFTribeServicePostUpdate()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : SMFPostandUpdateDelegate){
        PostUpdateViewDelegate = delegate
    }
    
    
    // MARK: ServiceManagerDelegate Methods
    
    /**
     This method handles success response of Login API
     
     - parameter responseObject: Response object that contains success response
     */
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        PostUpdateViewDelegate?.hideLoader!()
        self.PostUpdateViewDelegate?.popHome()
        //This statement is used for hiding loader
        // self.ForgetPasswordViewDelegate?.hideLoader!()
        // self.ForgetPasswordViewDelegate?.switchToHome()
    }
    
    /**
     This Method handle error response of Login API
     
     - parameter error: NSError Object that contains error information
     */
    
    func servicesManagerError(error : String){
        PostUpdateViewDelegate?.hideLoader!()
        PostUpdateViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        //This statement is used for hiding loader
        // self.ForgetPasswordViewDelegate?.hideLoader!()
    }
    
    func createPost(newPost:String, lat:String, long:String,hidden:Bool, filePathUrl:String, forImage:Bool) {
        let trimmedPost = newPost.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if forImage == true {
            self.serviceBuisnessPostUpdate.performPostWithValidInput(inputData: SMFPostUpdateRequestModel.Builder().setPost(post: trimmedPost).setCordinate(lat: lat, long: long).setHidden(hidden: hidden).setImageUrl(url: filePathUrl).addRequestHeader(key:"Content-Type", value: "application/json").build(), postandUpdateDelegate: self)
        }else {
            self.serviceBuisnessPostUpdate.performPostWithValidInput(inputData: SMFPostUpdateRequestModel.Builder().setPost(post: trimmedPost).setCordinate(lat: lat, long: long).setHidden(hidden: hidden).setVideoUrl(videoUrl: filePathUrl).addRequestHeader(key:"Content-Type", value: "application/json").build(), postandUpdateDelegate: self)
        }
    }
    
    
    
    func postButtonClickedWithPost(newPost:String, lat:String, long:String,hidden:Bool, feedImage:UIImage?, feedUrl:URL?) ->Void
    {
        
        if newPost.isEmpty {
            PostUpdateViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: "Please post something")
            return
        }
        if feedUrl == nil && feedImage == nil {
            PostUpdateViewDelegate?.showLoader!()
            let trimmedPost = newPost.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            print(trimmedPost)
            self.serviceBuisnessPostUpdate.performPostWithValidInput(inputData: SMFPostUpdateRequestModel.Builder().setPost(post: trimmedPost).setCordinate(lat: lat, long: long).setHidden(hidden: hidden).addRequestHeader(key:"Content-Type", value: "application/json").build(), postandUpdateDelegate: self)
        }else {
            if feedUrl != nil {
                self.uploadVideo(videoUr: feedUrl!, lat: lat, long: long, hidden: hidden, post: newPost)
            }else if feedImage != nil {
                self.uploadImage(feedImage: feedImage!, lat: lat, long: long, hidden: hidden, post: newPost)
            }
        }
    }
    
    func postButtonClickedWithTribePost(newPost:String,groupId:String) ->Void
    {
        if newPost.isEmpty {
            PostUpdateViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: "Please post something")
            return
        }
        PostUpdateViewDelegate?.showLoader!()
        let trimmedPost = newPost.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        print(trimmedPost)
        self.serviceTribeBusinessPostUpdate.performPostWithValidInput(inputData: SMFPostUpdateRequestModel.Builder().setPost(post: trimmedPost).setGroupId(group: groupId).addRequestHeader(key:"Content-Type", value: "application/json").build(), postandUpdateDelegate: self)
        
    }
    
    
    func uploadImage(feedImage:UIImage, lat:String, long:String, hidden:Bool, post:String){
        self.PostUpdateViewDelegate?.showLoader!()
        ImageUploadService.imageUploadRequest("newsFeedImages", id: "", image: feedImage, success: { (response) in
            print(response)
            let filePathUrl:String = response["filePath"] as! String
            DispatchQueue.main.async {
                self.createPost(newPost: post, lat: lat, long: long, hidden: hidden, filePathUrl: filePathUrl, forImage: true)
            }
        }) { (error) in
            DispatchQueue.main.async {
                self.PostUpdateViewDelegate?.hideLoader!()
            }
            print(error!)
        }
    }
    
    
    func uploadVideo(videoUr:URL,  lat:String, long:String, hidden:Bool,post:String){
        self.PostUpdateViewDelegate?.showLoader!()
        ImageUploadService.videoUploadRequest("newsFeedVideos", id: "", videoUrl: videoUr, success: { (response) in
            print(response)
            var filePathUrl:String = ""
            if let fileUrl:String = response["filePath"] as? String {
                filePathUrl = fileUrl
            }
            DispatchQueue.main.async {
                self.PostUpdateViewDelegate?.hideLoader!()
                self.createPost(newPost: post, lat: lat, long: long, hidden: hidden, filePathUrl: filePathUrl, forImage: false)
            }
        }) { (error) in
            DispatchQueue.main.async {
                self.PostUpdateViewDelegate?.hideLoader!()
            }
            print(error!)
        }
    }

    

    
    
    
    
    
    
}




