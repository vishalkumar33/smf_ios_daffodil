//
//  NotificationRequestModel.swift
//  SMF
//
//  Created by Jenkins on 2/11/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//


import Foundation


//This class is used for constructing Service Request Model

class NotificationRequestModel {
    
    //Note :- Property Name must be same as key used in request API
    
    let requestBody   : [String:AnyObject]?
    let requestHeader : [String:AnyObject]?
    
    //This is a Private Constrctor instantiating Service Model Request Object
    
    private init(builderObject:Builder){
        
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builderObject.requestBody
        self.requestHeader = builderObject.requestHeader
    }
    
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var requestBody = [String:AnyObject]()
        var requestHeader = [String:AnyObject]()
        
        
        
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method returns the Service request Model
         
         - returns: Returns ServiceRequestModel Object having set Emailid and Password
         */
        func setMemberId(memberId:String) ->Builder{
            requestBody["memberId"] = memberId as AnyObject?
            return self
        }
        func setStatus(status:String) ->Builder{
            requestBody["status"] = status as AnyObject?
            return self
        }
        func setGroupId(groupId:String) ->Builder{
            requestBody["groupId"] = groupId as AnyObject?
            return self
        }
        func build() -> NotificationRequestModel{
            return  NotificationRequestModel(builderObject: self)
        }
        
    }
    
    
    
    
    /**
     This method is used for creating End Point
     
     - returns: returning API End Point
     */
    
    func getClearNotification() -> String {
        return "/clearNotifications"
    }
    
    func getStatus() -> String {
        return "/changeStatus"
    }
    
    
    /**
     This method is used for
     
     - returns: returning request body
     */
    
    func getRequestBody() -> [String:AnyObject] {
        return self.requestBody!
    }
    
    /**
     This method is used for containing Request Headers
     
     - returns: Dictionary contains header
     */
    
    func getRequestHeader() -> [String:AnyObject] {
        return self.requestHeader!
    }
    
    
}


