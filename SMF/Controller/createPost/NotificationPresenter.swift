//
//  NotificationPresenter.swift
//  SMF
//
//  Created by Jenkins on 2/11/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//




import Foundation

import UIKit

protocol NotificationDelegate:SMFCommonViewDelegate{
    //func updateTable(newsFeedArray:[MemberInfo])
    func  updateTable(newsfeedArray :[UserNotification])
    func getSuccess()
    // func userDetail(userProfile: MemberInfo)
    
}



class NotificationPresenter: ResponseCallback {
    
    private var notificationDelegate   : NotificationDelegate?
    private var serviceBuisnessNotification = ServiceNotification()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : NotificationDelegate){
        notificationDelegate = delegate
    }
    
    
    // MARK: ServiceManagerDelegate Methods
    
    /**
     This method handles success response of Login API
     
     - parameter responseObject: Response object that contains success response
     */
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        self.notificationDelegate?.hideLoader!()
        self.notificationDelegate?.getSuccess()
    }
    
    /**
     This Method handle error response of Member API
     
     - parameter error: NSError Object that contains error information
     */
    
    func servicesManagerError(error : String){
        notificationDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        //This statement is used for hiding loader
        self.notificationDelegate?.hideLoader!()
    }
    
    
    
    func getNewsFeed(){
       
        let notificationArray = 0

      /*  notificationDelegate?.showLoader!()
        Service.getMemberList(success: { (response) in
            print(response)
            self.notificationDelegate?.hideLoader!()
            
        }) { (error) in
            self.notificationDelegate?.hideLoader!()
            print(error!)
        }*/
    }
  
    
    func parseMemberList(feeds:NSArray){
        var notificationArray:[MemberInfo] = [MemberInfo]()
        if feeds.count > 0 {
            let result = feeds
            print("\(result)")
            for type in result {
                if let dict = type as? NSDictionary {
                    let obj  = MemberInfo(dict as! Dictionary<String, AnyObject>)
                    notificationArray.append(obj!)
                }
                
            }
            print(notificationArray)
        }
      //  self.notificationDelegate?.updateTable(newsfeedArray: notificationArray as! [Int])
    }
    func yesButtonClicked() ->Void
    {
        
        //Showing Loader
        notificationDelegate?.showLoader!()
        self.serviceBuisnessNotification.performMemberPutWithValidInput(inputData: NotificationRequestModel.Builder().addRequestHeader(key:"Content-Type" , value: "application/json").build(), NotificationListDelegate: self)
        
    }
    
    func AcceptButtonClickedWithStatus(memberId:String,status:String ,groupId:String) ->Void
    {
        
        notificationDelegate?.showLoader!()
        self.serviceBuisnessNotification.performPostWithValidInput(inputData: NotificationRequestModel.Builder().setStatus(status: status).setGroupId(groupId: groupId).setMemberId(memberId: memberId).addRequestHeader(key:"Content-Type", value: "application/json").build(), NotificationListDelegate: self)
        
    }
        
    func getUserNotfication(){
        self.notificationDelegate?.showLoader!()
        Service.getForNotification(success: { (notification) in
            self.notificationDelegate?.hideLoader!()
            self.notificationDelegate?.updateTable(newsfeedArray: notification)
        }) { (error) in
            self.notificationDelegate?.hideLoader!()
        }
    }
    
    





}






