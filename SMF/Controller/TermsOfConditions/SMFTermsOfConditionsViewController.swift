//
//  SMFTermsOfConditionsViewController.swift
//  SMF
//
//  Created by Jenkins on 12/29/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

class SMFTermsOfConditionsViewController: SMFBaseViewController, UIWebViewDelegate {

    @IBOutlet weak var webViewUrl: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        webViewUrl.delegate = self
        setNav(title:"TERMS AND CONDITIONS")
        setLeftNavBarButton()
        self.webViewUrl.frame = self.view.bounds
        self.webViewUrl.scalesPageToFit = true
        if let url = URL(string: "http://socialmediafreedom.zendesk.com/hc/en-us/article_attachments/218402068/Terms-of-Use.docx") {
            let request = URLRequest(url: url)
            webViewUrl.loadRequest(request)
        }
        // Do any additional setup after loading the view.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.view.showLoader()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.view.hideLoader()
    }
    

   
}
