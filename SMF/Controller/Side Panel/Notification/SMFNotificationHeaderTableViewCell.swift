//
//  SMFNotificationHeaderTableViewCell.swift
//  SMF
//
//  Created by Jenkins on 12/29/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

class SMFNotificationHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNameOfDay: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
