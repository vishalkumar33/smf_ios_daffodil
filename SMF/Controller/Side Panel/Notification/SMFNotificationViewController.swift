//
//  SMFNotificationViewController.swift
//  SMF
//
//  Created by Jenkins on 12/29/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

class SMFNotificationViewController: SMFBaseViewController,NotificationDelegate,ClearNotificationDelegate,AcceptDeclineDelegate {

    @IBOutlet weak var tableView: UITableView!
    var feedArray                :      [UserNotification] = []
    var notificationPresenter      :      NotificationPresenter!
    var notifyArray : UserNotification?
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 80;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.notificationPresenter = NotificationPresenter(delegate: self)
        notificationPresenter.getUserNotfication()
        setNav(title: "NOTIFICATIONS")
        setNavRightbtn()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
    }
    
    func updateTable(newsfeedArray: [UserNotification]) {
        for newsFeed in newsfeedArray {
            if newsFeed.type == "privateGroupRequest" {
                if newsFeed.read {
                    feedArray.append(newsFeed)
                }
            }else{
                feedArray.append(newsFeed)
            }
        }
        tableView.reloadData()
    }
    
    
    func yesTapped(sender: UIButton) {
        self.view.showLoader()
        Service.clearAllNotification(success: { (response) in
            self.view.hideLoader()
            self.notificationPresenter.getUserNotfication()
        }) { (error) in
            self.view.hideLoader()
            print(error!)
        }
    }
    
    func refresh(sender:AnyObject) {
        self.refreshControl.endRefreshing()
        feedArray.removeAll(keepingCapacity: false)
        tableView.reloadData()
        notificationPresenter.getUserNotfication()
        print("ededwdewdwedooo")

    }
    
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func setNavRightbtn()
    {
        let clearBtn = UIButton(type: .custom)
        clearBtn.frame = CGRect(x: 0, y: 0, width: 60, height: 30)
        clearBtn.layer.borderWidth = 1.0
        clearBtn.layer.borderColor = UIColor.white.cgColor
        clearBtn.titleLabel!.font =  UIFont(name: "Quicksand-Medium", size: 12)
        clearBtn.setTitle("Clear All", for: .normal)
        clearBtn.setTitleColor(UIColor.white, for: .normal)
        clearBtn.addTarget(self, action: #selector(SMFNotificationViewController.loadClear_PopUp), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: clearBtn)
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        
    }
    
    func loadClear_PopUp()
    {
        let clearView = Bundle.main.loadNibNamed("SMFClearPopUpNotification", owner: self, options: nil)?[0] as! SMFClearPopUpNotification
        clearView.frame = UIScreen.main.bounds
        clearView.delegate = self
        let window = UIApplication.shared.keyWindow!
        window.addSubview(clearView)
    }
   
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }

   
}
extension SMFNotificationViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return feedArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if feedArray.count > 0
        {
            tableView.backgroundView?.isHidden = true
            return 1
        }
        tableView.backgroundView?.isHidden = false
        TableViewHelper.EmptyMessage(message:"You don't have any Notification" , tableView: tableView)
        return 0
    }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 53
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderNameCell") as! SMFNotificationHeaderTableViewCell
        return cell
    }
    func AcceptTapped(sender: UIButton) {
        print("Accept")
        notificationPresenter.getUserNotfication()
        if (notifyArray?.isNewRequest == true)
        {
            self.notificationPresenter.AcceptButtonClickedWithStatus(memberId:(notifyArray?.sourceUser?._id)!, status: "accepted", groupId: (notifyArray?.actionItemId)!)

        }
        else
        {
            showErrorAlert(alertTitle: "", alertMessage: "You already accepted")
        }
    }
    func DeclineTapped(sender: UIButton) {
        print("Decline")
        notificationPresenter.getUserNotfication()
        if (notifyArray?.isNewRequest == true)
        {
        self.notificationPresenter.AcceptButtonClickedWithStatus(memberId:(notifyArray?.sourceUser?._id)!, status: "declined", groupId: (notifyArray?.actionItemId)!)
        }
        else
        {
            showErrorAlert(alertTitle: "", alertMessage: "You already declined")

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        notifyArray = feedArray[indexPath.row]
        if (notifyArray?.type == "privateGroupRequest")
        {
              let cell = tableView.dequeueReusableCell(withIdentifier: "SMFAcceptDeclineTableViewCell", for: indexPath) as! SMFAcceptDeclineTableViewCell
            cell.delegate1 = self
            cell.setCellRequested(notification: feedArray[indexPath.row])
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! SMFNotificationDetailTableViewCell
            cell.setCell(notification: feedArray[indexPath.row])
            return cell

        }
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let notificationArray = feedArray[indexPath.row]
        let link = notificationArray.sourceUser?.notifcationLink
        let array = link?.components(separatedBy: "/")
        if array?[1] == "post"{
            goToComment(withLink: link!)
        }
      //  vc.postIdFromNot = feedArray[indexPath.row].
       // let navigationController = UINavigationController(rootViewController: vc)
   //     self.present(navigationController, animated: true, completion: nil)

    }
    
    func goToComment(withLink:String){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SMFCommentViewController") as! SMFCommentViewController
        vc.postLink = withLink
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getSuccess() {
        
    }
    
}

