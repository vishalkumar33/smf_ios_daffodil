//
//  SMFClearPopUpNotification.swift
//  SMF
//
//  Created by Jenkins on 12/29/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

protocol ClearNotificationDelegate{
    func yesTapped(sender:UIButton)
}
class SMFClearPopUpNotification: UIView {

    @IBOutlet weak var clearView: UIView!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    var delegate:ClearNotificationDelegate!
    override func awakeFromNib() {
       
        self.removeFromSuperview()
    }
    
    @IBAction func noAction(_ sender: Any)
    {
        btnNo.layer.borderColor = UIColor(red: 113, green: 195, blue: 218, alpha: 0.47).cgColor
        btnYes.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.09).cgColor
        self.removeFromSuperview()
    }
    
    
    @IBAction func yesAction(_ sender: Any) {
        btnYes.layer.borderColor = UIColor(red: 113, green: 195, blue: 218, alpha: 0.47).cgColor
        btnNo.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.09).cgColor
        self.removeFromSuperview()
        self.delegate?.yesTapped(sender: btnYes)
    }
    
}
