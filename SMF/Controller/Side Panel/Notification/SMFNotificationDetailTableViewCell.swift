//
//  SMFNotificationDetailTableViewCell.swift
//  SMF
//
//  Created by Jenkins on 12/29/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit
class SMFNotificationDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var acceptVerticalTop: NSLayoutConstraint!
   
    @IBOutlet weak var TimeBottomHeight: NSLayoutConstraint!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var btnyes: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImageView.layer.borderWidth = 1.0
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor.appBlueBorderColor.cgColor
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.clipsToBounds = true

        // Initialization code
    }

    
    func setCell(notification:UserNotification){
        let url = URL(string: (notification.sourceUser?.profile.profileImage)!)
        profileImageView.sd_setImage(with: url)
        var array = notification.notificationText.components(separatedBy: " ")
        print(array.count)
        array.remove(at: 0)
        array.remove(at: 0)
        print(array.count)
        let laststring = array.joined(separator: " ")
        
        let myString = "@\((notification.sourceUser?.profile.handle)!)"
        let color = UIColor(colorLiteralRed: 97/255, green: 189/255, blue: 214/255, alpha: 1)
        let myAttribute = [ NSForegroundColorAttributeName: color ]
        let myAttrString = NSAttributedString(string: myString, attributes: myAttribute)
        
        let lastAttribute  = NSAttributedString(string: laststring)
        let combination = NSMutableAttributedString()
        combination.append(myAttrString)
        combination.append(NSAttributedString(string: " "))
        combination.append(lastAttribute)
        // set attributed text on a UILabel
        lblName.attributedText = combination

        let date =  TableViewHelper.DateToString(date: TableViewHelper.stringToDate(date: (notification.sourceUser?.createdDat)!))
        lblTime.text = date
    }

}
