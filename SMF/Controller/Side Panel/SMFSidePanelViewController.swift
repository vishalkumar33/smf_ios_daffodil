//
//  SMFSidePanelViewController.swift
//  SMF
//
//  Created by Shivam Srivastava on 26/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit
import SDWebImage
protocol pushViewControllerDelegate {
    func pushViewController()
}

class SMFSidePanelViewController: SMFBaseViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var userNameLbl: UILabel!
    
    
    @IBOutlet weak var userEmailId: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: pushViewControllerDelegate?
    
   var imageIcon = ["myProfileIcon","messagehomeSideView", "tribes","homeSidemenuSearch", "logoutIcon" ]
   var itemTitles = ["My Profile", "Messages", "Tribes", "Search", "Logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initValue()
        //self.navigationController?.isNavigationBarHidden = true
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    func removeAllUserDefault(){
        for key in Array(UserDefaults.standard.dictionaryRepresentation().keys) {
            if key == "deviceToken" {
                continue
            }
            UserDefaults.standard.removeObject(forKey: key)
        }
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        
    }
    
    func switchToHome(){
        removeAllUserDefault()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController: UINavigationController?
        let landingVC:SMFLoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SMFLoginViewController") as! SMFLoginViewController
        navigationController = UINavigationController(rootViewController: landingVC)
        UIApplication.shared.delegate?.window!!.rootViewController = navigationController
    }
    
    
    func initValue(){
        
        if let name =  UserDefault.string(forKey: "userName") {
            userNameLbl.text = name
        }
        if let profileURl:URL = UserDefault.url(forKey: "userProfileImage") {
           // profileImageView.sd_setImage(with: profileURl, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
            //public typealias SDWebImageCompletionBlock = (UIImage?, Error?, SDImageCacheType, URL?) -> Swift.Void
            profileImageView.sd_setImage(with: profileURl, placeholderImage: #imageLiteral(resourceName: "henryCavill"),options:.refreshCached,completed: {(image:UIImage?,error:Error?,cacheType:SDImageCacheType,imageURL:URL?) in
                
                self.profileImageView.clipsToBounds = true
                self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
                self.profileImageView.layer.borderWidth = 1
                self.profileImageView.layer.masksToBounds = true
                self.profileImageView.layer.borderColor = UIColor.white.cgColor
            })
        }
        if let email = UserDefault.string(forKey: "userEmail"){
            userEmailId.text = email
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

    @IBAction func closeAction(_ sender: Any) {
        close()
    }
    
    func close(){
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
        self.navigationController?.dismiss(animated: true, completion: nil)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemTitles.count
    
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.separatorStyle = .none
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SMFSidePanelTableViewCell
        let cellBGView = UIView()
        cellBGView.backgroundColor = UIColor(red: 97.0/255.0, green: 189.0/255, blue: 214.0/255.0, alpha: 1.0)
        cell.selectionStyle = .none
        cell.selectedBackgroundView = cellBGView
        cell.imageViewCell.image = UIImage(named: imageIcon[indexPath.row])
        cell.sidePanelItems?.text = itemTitles[indexPath.row]
        print("height = \(cell.bounds.height)")
        
        return cell
    }

   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       // let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        var vcObject : UIViewController?

        switch indexPath.row {
        case 0:
            print("deded")
            vcObject = self.storyboard?.instantiateViewController(withIdentifier: "SMFProfileViewController") as! SMFProfileViewController
            pushView(vcObject: vcObject!)
            break;

        case 1:
            //vcObject = self.storyboard!.instantiateViewController(withIdentifier: "SMFMessageDetailViewController") as! SMFMessageDetailViewController
            vcObject = self.storyboard!.instantiateViewController(withIdentifier: "SMFMessageViewController") as! SMFMessageViewController
            pushView(vcObject: vcObject!)
            break;
        case 2:
            vcObject = self.storyboard!.instantiateViewController(withIdentifier:"SMFTribesListViewController" ) as! SMFTribesListViewController
            pushView(vcObject: vcObject!)
            break;
        case 3:
            vcObject = self.storyboard!.instantiateViewController(withIdentifier: "SMFSearchViewController") as! SMFSearchViewController
            pushView(vcObject: vcObject!)
            break;
        case 4:
            self.view.showLoader()
             Service.logout(success: { (response) in
                self.view.hideLoader()
                if response["message"] as! String == "User logout successfully" {
                    self.switchToHome()
                }
             }, failure: { (error) in
                self.view.hideLoader()
             })
            return
        default:
            break;
        }

        
        
        
        
    }
    
    func pushView(vcObject:UIViewController){
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.view.window!.layer.add(transition, forKey: nil)
        self.navigationController?.pushViewController(vcObject, animated: false)
    }
    
    

}
