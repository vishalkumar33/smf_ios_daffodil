//
//  SMFSearchPresenter.swift
//  SMF
//
//  Created by Jenkins on 2/3/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//




import Foundation

protocol SMFSearchViewDelegate:SMFCommonViewDelegate{
    func updateTable(newsFeedArray:[Post])
}

class SMFSearchPresenter: ResponseCallback {
    
    private var searchViewDelegate    : SMFSearchViewDelegate?
    
    init(delegate : SMFSearchViewDelegate){
        searchViewDelegate = delegate
    }
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        self.searchViewDelegate?.hideLoader!()
    }
    
    
    func servicesManagerError(error : String){
        searchViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        self.searchViewDelegate?.hideLoader!()
    }
    
    func getSearchGroupFeed(searchText:String, groupId:String ,limit:Int, skip:Int){
        searchViewDelegate?.showLoader!()
        Service.searchGroupPost(searchText: searchText, groupId: groupId, limit: limit, skip: skip, success: { (response) in
            print(response)
            self.searchViewDelegate?.hideLoader!()
            print(response.count)
            self.searchViewDelegate?.updateTable(newsFeedArray: response)
        }) { (error) in
            print(error!)
            self.searchViewDelegate?.hideLoader!()
        }
    }
    
    
    
    
    func getSearchNewsFeed(textField:String){
        searchViewDelegate?.showLoader!()
        Service.getSearchNewsList(searchText: textField, success: { (response) in
            print(response)
            self.searchViewDelegate?.hideLoader!()
            self.parseSearchNewsFeed(response: response, totalCount:  10)
        }) { (error) in
            self.searchViewDelegate?.hideLoader!()
            print(error!)
        }
    }
    
   

    func parseSearchNewsFeed(response:NSDictionary, totalCount:Int){
        
        var totalCount = response["totalfeedscount"] as! Int
        let feeds = response["result"] as! NSArray
        var feedArray:[Post] = [Post]()
        if feeds.count > 0 {
            let result = feeds
            print("\(result)")
            for type in result {
                if let dict = type as? NSDictionary {
                    let obj  = Post(response: dict)
                    feedArray.append(obj)
                }
            }
        }
        self.searchViewDelegate?.updateTable(newsFeedArray: feedArray)
    }
}
