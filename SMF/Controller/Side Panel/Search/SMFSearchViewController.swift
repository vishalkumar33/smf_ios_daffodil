//
//  SMFSearchViewController.swift
//  SMF
//
//  Created by Jenkins on 1/3/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFSearchViewController: SMFBaseViewController,UITextFieldDelegate{

    
    var searchPresenter            :      SMFSearchPresenter!
    var feedArray                :      [Post]                          =       [Post]()
    var menuOptionNameArray : [String] = ["Report", "Hide Post", "Block@john"]
    var menuOptionImageNameArray = ["reportIcon", "hidePostIcon", "blockPostIcon" ]

    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    var groupId:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNav(title: "SEARCH")
        setLeftNavBarButton()
        self.navigationController?.navigationBar.isHidden = false
        searchTextField.becomeFirstResponder()
        self.searchPresenter = SMFSearchPresenter(delegate: self)
        searchTextField.returnKeyType = UIReturnKeyType.search
        tableView.delegate = self
        tableView.dataSource = self
        searchTextField.delegate = self
        self.tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        tableView.estimatedRowHeight = 26
        searchTextField.leftPadding(width: 55)
        searchTextField.layer.borderWidth = 1.0
        searchTextField.layer.borderColor  = UIColor.appMainViewGrayColor.cgColor
        searchTextField.attributedPlaceholder = NSAttributedString(string: "Search",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.appBlueBorderColor])
        self.tableView.register(SMFHistoryTextTableViewCell.self)
        self.tableView.register(SMFHistoryImageTableViewCell.self)
        self.tableView.register(SMFHistoryVideoTableViewCell.self)
        self.tableView.register(SMFTextTableViewCell.self)
       //   searchTextField.addTarget(self, action: #selector(SMFMemberViewController.textFieldDidChange), for: UIControlEvents.editingChanged)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if groupId == nil {
            searchPresenter.getSearchNewsFeed(textField: textField.text!)
             tableView.reloadData()
        }else{
            searchPresenter.getSearchGroupFeed(searchText: textField.text!, groupId: groupId!, limit: 10, skip: 0)
            tableView.reloadData()
        }
        
        return true
    }
    
    
    

}


extension SMFSearchViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) ->
        CGFloat {
            return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let postObj = feedArray[indexPath.row]
        print(postObj.id)
        if postObj.videoPath != "" && postObj.hidden == false {
            if postObj.content != "" {
                let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFHistoryVideoTableViewCell
                
                //cell.setCell(post: postObj)
                cell.setCell1(post: postObj, indexPath: indexPath.row)
                cell.selectionStyle = .none
                cell.contentView.removeFromSuperview()
                return cell
            }else{
                return UITableViewCell()
            }
        }else if postObj.imagePath != "" && postObj.hidden == false {
            if postObj.content != "" {
                let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFHistoryImageTableViewCell
//                cell.imageDelegate = self
//                cell.countLabel.text = String(peaceCount + likeCount + dislikeCount)
                //   cell.btnPopup.addTarget(self, action: #selector(SMFHomeViewController.showPopUp(_:)), for: .touchUpInside)
                cell.setCell1(post: postObj, indexPath: indexPath.row)
                cell.selectionStyle = .none
                cell.contentView.removeFromSuperview()
                return cell
            }else{
                return UITableViewCell()
            }
        }else if postObj.hidden == true {
            if postObj.content != "" {
                let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFDisapperingPost
                
                //   cell.btnPopup.addTarget(self, action: #selector(SMFHomeViewController.showPopUp(_:)), for: .touchUpInside)
                cell.selectionStyle = .none
                cell.contentView.removeFromSuperview()
                return cell
            }else{
                return UITableViewCell()
            }
        }
        else{
            if postObj.content != "" && postObj.hidden == false {
                let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFTextTableViewCell
//                cell.countLabel.text = String(peaceCount + likeCount + dislikeCount)
//                cell.hideDelegate = self
                cell.setCell(post: postObj, indexPath: indexPath.row)
                cell.selectionStyle = .none
                cell.contentView.removeFromSuperview()
                return cell
            }else{
                return UITableViewCell()
            }
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if feedArray.count > 0 {
            tableView.backgroundView?.isHidden = true
            return 1
        }
        tableView.backgroundView?.isHidden = false
        TableViewHelper.EmptyMessage(message:"You don't have any Post" , tableView: tableView)
        
        return 0
    }
    
    
    
}
extension SMFSearchViewController: SMFSearchViewDelegate, SMFHistoryTextTableViewCellDelegate {
    
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func popUpButtonTapped(_ sender: AnyObject) {
    }
    
    func updateTable(newsFeedArray: [Post]) {
        self.feedArray = newsFeedArray
        tableView.reloadData()
    }
}


