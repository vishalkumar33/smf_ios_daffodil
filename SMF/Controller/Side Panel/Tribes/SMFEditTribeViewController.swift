//
//  SMFEditTribeViewController.swift
//  SMF
//
//  Created by Jenkins on 2/8/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit


protocol editTribeDelegate {
    func reloadData()
}


class SMFEditTribeViewController: SMFBaseViewController{

    
    @IBOutlet weak var tribeImageView: UIImageView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var tribeTextView: UITextView!
    @IBOutlet weak var tribeNameTextField: MyCustomTextField!
    @IBOutlet weak var msgLabel: UILabel!
    
    let imagePicker = UIImagePickerController()
    var tribe:Tribes?
    var editTribePresenter: EditTribePresenter!
    
    var delegate:editTribeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeImageRound()
        editTribePresenter = EditTribePresenter(delegate: self)
        initViewWithData()
        tribeTextView.layer.borderWidth = 1.0
        tribeTextView.layer.borderColor = UIColor.appGrayboderColor.cgColor
        tribeTextView.layer.cornerRadius = 5.0
        tribeTextView.clipsToBounds = true
        tribeNameTextField.leftPadding(width : 20 )
        tribeTextView.textContainerInset =
            UIEdgeInsetsMake(10,14,0,0);
        setLeftNavBarButton()
        setNav(title: "EDIT TRIBE")
    }
    
    func initViewWithData(){
        tribeImageView.sd_setImage(with: NSURL(string: (tribe?.tribeImage)!) as! URL, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
        tribeTextView.text = tribe?.tribeDescription
        tribeNameTextField.text = tribe?.tribeName
    }
    
    
    func makeImageRound(){
        tribeImageView.layer.borderWidth = 1
        tribeImageView.layer.masksToBounds = false
        tribeImageView.layer.borderColor = UIColor.black.cgColor
        tribeImageView.layer.cornerRadius = tribeImageView.frame.height/2
        tribeImageView.clipsToBounds = true
    }

    @IBAction func removeAction(_ sender: Any) {
        tribeImageView.image = #imageLiteral(resourceName: "camera")
    }
    
    
    @IBAction func editTribe(_ sender: Any) {
        
        
        if tribeImageView.image != #imageLiteral(resourceName: "camera"){
            editTribePresenter.uploadPhoto(image: tribeImageView.image!, type: "groupsImages")
        }else{
            self.showErrorAlert(alertTitle: "Error", alertMessage: "Image is required", VC: self)
        }
    }
    
    
    @IBAction func cameraAction(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    

}


extension SMFEditTribeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            tribeImageView.image = pickedImage
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    
}


extension SMFEditTribeViewController: EditTribeViewDelegate {
    
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func imageUploadStatus(url: String) {
        
        self.editTribePresenter.updateTribe(tribeId: (tribe?.tribeId!)!, name: tribeNameTextField.text!, desc: tribeTextView.text!, imageUrl: url)
        
    }
    
    func popUp(tribe: Tribes) {
        self.tribe?.tribeImage = tribe.tribeImage
        self.tribe?.tribeName = tribe.tribeName
        self.tribe?.tribeDescription = tribe.tribeDescription
        if delegate != nil {
            self.delegate?.reloadData()
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
}
