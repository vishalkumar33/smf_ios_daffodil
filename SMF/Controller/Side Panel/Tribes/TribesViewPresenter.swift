//
//  TribesViewPresenter.swift
//  SMF
//
//  Created by Vishal on 08/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

protocol SMFTribesViewDelegate:SMFCommonViewDelegate {
    func updateTable(withData data:[Tribes], forMyTribes:Bool, count:Int)
    func switchToAdmin(withTribe tribe:Tribes)
    func swithcToMember(withTribe tribe:Tribes)
    func successResponse()
}


class TribesViewPresenter: ResponseCallback {
    
    private var tribesViewDelegate    : SMFTribesViewDelegate?
    private var serviceBuisnessGetAllTribes = ServiceAllTribes()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : SMFTribesViewDelegate){
        tribesViewDelegate = delegate
    }
    
    func servicesManagerSuccessResponse(responseObject: AnyObject) {
        self.tribesViewDelegate?.hideLoader!()
        if responseObject is AllTribesResponse {
            let obj:AllTribesResponse = responseObject as! AllTribesResponse
            tribesViewDelegate?.updateTable(withData: obj.tribes, forMyTribes: false, count: obj.totalCount)
        }
    }
    
    func servicesManagerError(error: String) {
        self.tribesViewDelegate?.hideLoader!()
    }
    
    
    func getAllTribes(limit:Int, skip: Int){
        tribesViewDelegate?.showLoader!()
        serviceBuisnessGetAllTribes.getAllTribes(inputData: AllTribesRequestModel.Builder().setSkip(skip: skip).setLimit(limit: limit).addRequestHeader(key:"Content-Type", value: "application/json").build() , allTribeDelegate: self)
        
    }
    
    func switchToDetail(tribe:Tribes){
        let userId = UserDefault.string(forKey: "userId")
        if tribe.tribeOwner?.tribeOwnerId == userId {
            tribesViewDelegate?.switchToAdmin(withTribe: tribe)
        }else{
            tribesViewDelegate?.swithcToMember(withTribe: tribe)
        }
    }
    
    func initDicForJoinReqeuest(memberId:String, groupId:String) -> NSDictionary {
        let dic = NSMutableDictionary()
        dic["groupId"] = groupId
        dic["memberId"] = memberId
        return dic
    }
    
    func joinRequest(memberId:String, groupId:String){
        self.tribesViewDelegate?.showLoader!()
        Service.joinRequest(data: initDicForJoinReqeuest(memberId: memberId, groupId: groupId), success: { (response) in
            self.tribesViewDelegate?.hideLoader!()
            self.tribesViewDelegate?.successResponse()
        }) { (error) in
            if error?.code == -1011 {
                self.tribesViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: "You already send the request")
            }
            self.tribesViewDelegate?.hideLoader!()
        }
    }
    
    
    
    
    
}
