//
//  SMFAddMemberViewController.swift
//  SMF
//
//  Created by Vishal on 21/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit



class SMFAddMemberViewController: SMFBaseViewController {

    @IBOutlet weak var tblMemberList: UITableView!
    
    var addMemberPresenter:SMFAddMemberPresenter?
    var connectionArray:[Connection] = [Connection]()
    
    var selectedArray:[Connection] = [Connection]()
    
    var previousArray:[Connection] = [Connection]()
    
    var fromAddMember = false
    
    var group:Tribes?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMemberList.dataSource = self
        tblMemberList.delegate = self
        tblMemberList.separatorStyle = .none
        addMemberPresenter = SMFAddMemberPresenter(delegate: self)
        addMemberPresenter?.getConnections()
        self.title = "Add Member"
        initLeftButton()
        initRightButton()
        
    }
    
    func initLeftButton(){
        let backBtn = UIButton(type: .custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 18, height: 18)
        backBtn.setImage(#imageLiteral(resourceName: "backIcon"), for: .normal)
        backBtn.addTarget(self, action: #selector(SMFAddMemberViewController.back), for: UIControlEvents.touchUpInside)
        let back = UIBarButtonItem(customView: backBtn)
        self.navigationItem.setLeftBarButton(back, animated: true)
    }
    
    func back()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func initRightButton(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(SMFAddMemberViewController.addMember))
    }
    
    func addMember() {
        if !fromAddMember {
            let count: Int! = self.navigationController?.viewControllers.count
            let createTribeVC = (self.navigationController?.viewControllers[count-2])! as? SMFCreateTribeViewController
            createTribeVC?.selctedMemmber = self.selectedArray
            if selectedArray.count > 0 {
                createTribeVC?.isMemberSelected = true
            }
            back()
        }else{
            if selectedArray.count > 0 {
                addMemberPresenter?.requestForConncetion(groupId: (self.group?.tribeId)!, member: selectedArray)
            }else{
                self.showErrorAlert(alertTitle: "Error", alertMessage: "Please add some member")
            }
            
        }
        
        
    }
    
    
}


extension SMFAddMemberViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if connectionArray.count > 0 {
            numOfSections            = 1
            tblMemberList.backgroundView = nil
        }
        else{
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tblMemberList.bounds.size.width, height: tblMemberList.bounds.size.height))
            noDataLabel.text          = "No data available"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tblMemberList.backgroundView  = noDataLabel
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return connectionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SMFAddMemberTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SMFAddMemberTableViewCell") as! SMFAddMemberTableViewCell
        cell.selectionStyle = .none
        cell.delegate = self
        if checkDataExist(connection: connectionArray[indexPath.row]) {
            cell.setCell(connection: connectionArray[indexPath.row], index: indexPath.row)
            self.selectedArray.append(connectionArray[indexPath.row])
            cell.selcted.on = true
        }else{
            cell.setCell(connection: connectionArray[indexPath.row], index: indexPath.row)
        }
        return cell
    }
    
    func checkDataExist(connection:Connection) -> Bool{
        var falg = false
        for conec in previousArray {
            if conec.handle == connection.handle {
                falg = true
                break
            }
        }
        return falg
    }
    
    
}


extension SMFAddMemberViewController: AddMemberDelegate, SMFAddMemberTableViewCellDelegate {
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func success(tribe: Tribes) {
        print(tribe.tribeMember.count)
        group?.tribeMember = tribe.tribeMember
        print(group?.tribeMember.count)
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func updateTable(data: [Connection]) {
        self.connectionArray = data
        tblMemberList.reloadData()
    }
    
    func tapOnCheckBox(index: Int, value: Bool) {
        if value {
            selectedArray.append(connectionArray[index])
        }else{
            let obj = connectionArray[index]
            selectedArray.remove(object: obj)
        }
        print(selectedArray.count)
    }
    
    
    
    func tapOnCheckBox(index: Int) {
        print(index)
        
    }
}

extension Array where Element: AnyObject {
    mutating func remove(object: Connection) {
        if let index = index(where: { $0 === object }) {
            remove(at: index)
        }
    }
}
