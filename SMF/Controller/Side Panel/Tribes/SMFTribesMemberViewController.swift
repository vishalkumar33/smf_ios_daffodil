//
//  SMFTribesMemberViewController.swift
//  SMF
//
//  Created by Jenkins on 2/8/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFTribesMemberViewController: SMFBaseViewController {
    @IBOutlet weak var memberCollectionView: UICollectionView!

    @IBOutlet weak var addMember: UIButton!
    @IBOutlet weak var txtSearchView: AutoCompleteTextField!
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var collectionViewTopConstraint: NSLayoutConstraint!
    var forTribeMember = false
    
    var tribeViewPresenter: TribeMemberViewPresenter?
    var tribeMember:[TribeMember] = [TribeMember]()
    var owner:Tribes?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextField()
        handleTextFieldInterfaces()
        setNav(title: "MEMBERS")
        setLeftNavBarButton()
        txtSearchView.leftPadding(width: 45)
        tribeViewPresenter = TribeMemberViewPresenter(delegate: self)
        memberCollectionView.delegate = self
        memberCollectionView.dataSource = self
        tribeViewPresenter?.intView(forMember: forTribeMember)
        
    }
    
    func configureTextField(){
        txtSearchView.autoCompleteTextColor = UIColor.white
        txtSearchView.autoCompleteTextFont = UIFont(name: "Roboto-Light", size: 15.0)
        txtSearchView.autoCompleteCellHeight = 60
        txtSearchView.maximumAutoCompleteCount = 5
        txtSearchView.hidesWhenSelected = true
        txtSearchView.hidesWhenEmpty = true
        txtSearchView.enableAttributedText = true
        var attributes = [String:AnyObject]()
        attributes[NSForegroundColorAttributeName] = UIColor.white
        attributes[NSFontAttributeName] = UIFont(name: "Roboto-Bold", size: 15.0)
        txtSearchView.autoCompleteAttributes = attributes
    }
    
    
    
    
    func handleTextFieldInterfaces(){
        txtSearchView.onTextChange = {[weak self] text in
            if !text.isEmpty{
                print("vishal")
                // self!.textRideName.autoCompleteStrings = self!.listOfNames
                self?.getMemberSearchValue(searchText: text)
                return
            }
            self!.txtSearchView.autoCompleteStrings = nil
        }
        txtSearchView.onSelect = {[weak self] text, indexpath in
            print(text)
            print(indexpath.row)
            
            
            
        }
        
        
    }
    
    @IBAction func addMember(_ sender: Any) {
        let addMemberVC = self.storyboard?.instantiateViewController(withIdentifier: "SMFAddMemberViewController") as! SMFAddMemberViewController
        addMemberVC.fromAddMember = true
        addMemberVC.group = owner
        self.navigationController?.pushViewController(addMemberVC, animated: true)
    }
    
    
    
    func getMemberSearchValue(searchText:String){
        tribeViewPresenter?.getMember(searchValue: searchText)
    }
}
extension SMFTribesMemberViewController: UICollectionViewDelegate,UICollectionViewDataSource
{
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (tribeMember.count + 1)
        
    }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            let cell = memberCollectionView.dequeueReusableCell(withReuseIdentifier: "SMFTribesMemberCollectionViewCell", for: indexPath) as! SMFTribesMemberCollectionViewCell
            cell.setCellForAdmin(tribe: owner!)
            cell.layer.masksToBounds = true
            cell.layer.borderColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.2 ).cgColor
            cell.layer.borderWidth = 1.0
            return cell
        }
        
        let cell = memberCollectionView.dequeueReusableCell(withReuseIdentifier: "SMFTribesMemberCollectionViewCell", for: indexPath) as! SMFTribesMemberCollectionViewCell
        cell.setCell(tribeMember: tribeMember[indexPath.row - 1], forMember: forTribeMember, indexPath: indexPath.row)
        cell.layer.masksToBounds = true
        cell.delegate = self
        cell.layer.borderColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.2 ).cgColor
         cell.layer.borderWidth = 1.0
        return cell
    }
    
}

extension SMFTribesMemberViewController : TribeMemberViewDelegate {
    
    func showSearchView() {
        self.lineView.isHidden = false
        self.txtSearchView.isHidden = false
        self.addMember.isHidden = false
        
    }
    
    
    func hideSearchView() {
        self.collectionViewTopConstraint.constant = self.collectionViewTopConstraint.constant - 41
        self.lineView.isHidden = true
        self.txtSearchView.isHidden = true
        self.addMember.isHidden = true
        self.imgSearch.isHidden = true
    }
    
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
}

extension SMFTribesMemberViewController: SMFTribesMemberCollectionViewCellDelegate {
    func getIndexPath(indexPath: Int) {
        showAlert(tribeMember: tribeMember[indexPath-1])
    }
    
    func showAlert(tribeMember:TribeMember){
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Do you really want to remove this member ?", preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelAction)
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Remove", style: .destructive) { action -> Void in
            self.removeMember(tribemember: tribeMember)
        }
        actionSheetController.addAction(takePictureAction)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func removeMember(tribemember:TribeMember){
        tribeViewPresenter?.removeMember(memberId: tribemember.tribeMemberId!, groupId: (owner?.tribeId)!)
    }
}
