//
//  CreateTribePresenter.swift
//  SMF
//
//  Created by Vishal on 08/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
import AFNetworking

protocol CreateTribeViewDelegate:SMFCommonViewDelegate {
    func imageUploadStatus(url:String)
    func popUp()
}

class CreateTribePresenter:ResponseCallback {
    
    private var createTribeDelegate    : CreateTribeViewDelegate?
    private var serviceBuisnessCreateTribes = ServiceCreateTribe()
    
    init(delegate : CreateTribeViewDelegate){
        createTribeDelegate = delegate
    }
    
    func servicesManagerSuccessResponse(responseObject: AnyObject) {
        self.createTribeDelegate?.hideLoader!()
        createTribeDelegate?.popUp()
        
        
    }
    
    func servicesManagerError(error: String) {
        self.createTribeDelegate?.hideLoader!()
    }
    
    
    func createTribes(name:String, desc:String, memberArray:[NSDictionary]?, imgUrl:String, status:Bool){
        if(self.isValidData(name:name, desc: desc) == true){
            createTribeDelegate?.showLoader!()
            serviceBuisnessCreateTribes.createTribe(inputData: CreateTribeRequestModel.Builder().setName(name: name).setStatus(status: status).setImage(imageUrl: imgUrl).setDescription(desc: desc).setMember(name: memberArray).addRequestHeader(key:"Content-Type", value: "application/json").build(), createTribeDelegate: self)
        }
    }
    
    
    func uploadPhoto(image:UIImage, type:String){
            if AFNetworkReachabilityManager.shared().isReachable {
                createTribeDelegate?.showLoader!()
                let id = ""
                ImageUploadService.imageUploadRequest(type, id: id, image: image, success: { (response) in
                    print(response)
                    let filePathUrl:String = response["filePath"] as! String
                    DispatchQueue.main.async {
                        self.createTribeDelegate?.hideLoader!()
                        self.createTribeDelegate?.imageUploadStatus(url: filePathUrl)
                    }
                }, failure: { (error) in
                    DispatchQueue.main.async {
                        self.createTribeDelegate?.hideLoader!()
                    }
                    print(error!)
                })
                
            }else{
                createTribeDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: "Please check internet connection")
            }
    }
    
    func updateTribe(tribeId:String, name:String, desc:String, imageUrl:String){
        
    }
    
    func isValidData(name:String,desc:String) ->Bool
    {
        //This guard statement checks whether all fields have some inputs or not
        guard self.isAllInputFieldsAreEmpty(name: name,desc: desc) == false
            else{
                createTribeDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage:"All Fields are mendatory")
                return false
        }
        
        

        guard name.isEmptyString(str: name) == false
            else {
                createTribeDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: "Name of this group is required")
                return false
        }
        guard desc.isEmptyString(str: desc) == false
            else {
                createTribeDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: "Desription of this group is required")
                return false
        }
        return true
    }
    

    
    func isAllInputFieldsAreEmpty(name:String, desc:String) ->Bool
    {
        // Checking whether both field have some input or not
        if((name.isEmptyString(str:name) && desc.isEmptyString(str:desc) ))
        {
            return true
        }
        return false
    }

    
    
}
