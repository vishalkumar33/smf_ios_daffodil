//
//  MyTribesRequestModel.swift
//  SMF
//
//  Created by Vishal on 08/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class MyTribesRequestModel {
    
    let requestBody   : [String:AnyObject]?
    let requestHeader : [String:AnyObject]?
    let limit         : Int?
    let skip          : Int?
    
    
    private init(builderObject:Builder){
        
        self.requestBody = builderObject.requestBody
        self.requestHeader = builderObject.requestHeader
        self.limit = builderObject.limit
        self.skip = builderObject.skip
        
    }
    
    
    internal class Builder{
        
        var requestBody = [String:AnyObject]()
        var requestHeader = [String:AnyObject]()
        var limit         = 10
        var skip          = 0
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        func setLimit(limit:Int)->Builder{
            self.limit = limit
            return self
        }
        
        func setSkip(skip:Int)->Builder{
            self.skip = skip
            return self
        }
        
        
        func build() ->MyTribesRequestModel{
            return MyTribesRequestModel(builderObject: self)
        }
        
    }
    
    
    
    /**
     This method is used for creating End Point
     
     - returns: returning API End Point
     */
    
    func getEndPoint() -> String {
        let userId = UserDefault.string(forKey: "userId")
        return "/tribes?limit=\(self.limit!)&skip=\(self.skip!)&myTribes=\(userId!)"
    }
    
    /**
     This method is used for
     
     - returns: returning request body
     */
    
    func getRequestBody() -> [String:AnyObject] {
        return self.requestBody!
    }
    
    /**
     This method is used for containing Request Headers
     
     - returns: Dictionary contains header
     */
    
    func getRequestHeader() -> [String:AnyObject] {
        return self.requestHeader!
    }
    
}
