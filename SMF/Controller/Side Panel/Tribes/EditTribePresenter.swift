//
//  EditTribePresenter.swift
//  SMF
//
//  Created by Vishal on 09/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
import AFNetworking

protocol EditTribeViewDelegate:SMFCommonViewDelegate {
    func imageUploadStatus(url:String)
    func popUp(tribe:Tribes)
}

class EditTribePresenter:ResponseCallback {
    
    private var editTribeDelegate    : EditTribeViewDelegate?
    private var serviceBuisnessCreateTribes = ServiceEditTribe()
    
    init(delegate : EditTribeViewDelegate){
        editTribeDelegate = delegate
    }
    
    func servicesManagerSuccessResponse(responseObject: AnyObject) {
        self.editTribeDelegate?.hideLoader!()
        let obj:EditTribeResponse = responseObject as! EditTribeResponse
        editTribeDelegate?.popUp(tribe: obj.tribe!)
    }
    
    func servicesManagerError(error: String) {
        self.editTribeDelegate?.hideLoader!()
    }
    
    
    func uploadPhoto(image:UIImage, type:String){
        if AFNetworkReachabilityManager.shared().isReachable {
            editTribeDelegate?.showLoader!()
            let id = ""
            ImageUploadService.imageUploadRequest(type, id: id, image: image, success: { (response) in
                print(response)
                let filePathUrl:String = response["filePath"] as! String
                DispatchQueue.main.async {
                    self.editTribeDelegate?.hideLoader!()
                    self.editTribeDelegate?.imageUploadStatus(url: filePathUrl)
                }
            }, failure: { (error) in
                DispatchQueue.main.async {
                    self.editTribeDelegate?.hideLoader!()
                }
                print(error!)
            })
            
        }else{
            editTribeDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: "Please check internet connection")
        }
    }
    
    func updateTribe(tribeId:String, name:String, desc:String, imageUrl:String){
        if(self.isValidData(name:name, desc: desc) == true){
        serviceBuisnessCreateTribes.editTribe(inputData: EditTribeRequestmodel.Builder().setName(name: name).setGroupId(groupId: tribeId).setImage(imageUrl: imageUrl).setDescription(desc: desc).addRequestHeader(key:"Content-Type", value: "application/json").build(), editTribeDelegate: self)
        }
    }
    func isValidData(name:String,desc:String) ->Bool
    {
        //This guard statement checks whether all fields have some inputs or not
        guard self.isAllInputFieldsAreEmpty(name: name,desc: desc) == false
            else{
                editTribeDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage:"All Fields are mendatory")
                return false
        }
        
        
        
        guard name.isEmptyString(str: name) == false
            else {
                editTribeDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: "Name of this group is required")
                return false
        }
        guard desc.isEmptyString(str: desc) == false
            else {
                editTribeDelegate?.showErrorAlert!(alertTitle: "Alert", alertMessage: "Desription of this group is required")
                return false
        }
        return true
    }
        
        func isAllInputFieldsAreEmpty(name:String, desc:String) ->Bool
        {
            // Checking whether both field have some input or not
            if((name.isEmptyString(str:name) && desc.isEmptyString(str:desc) ))
            {
                return true
            }
            return false
        }
        

    
    
}
