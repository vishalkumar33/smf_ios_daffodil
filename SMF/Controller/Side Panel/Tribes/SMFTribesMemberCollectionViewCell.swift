//
//  SMFTribesMemberCollectionViewCell.swift
//  SMF
//
//  Created by Jenkins on 2/8/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

protocol SMFTribesMemberCollectionViewCellDelegate {
    func getIndexPath(indexPath:Int)
}

class SMFTribesMemberCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var adminLabel: UILabel!
    @IBOutlet weak var memberImageView: UIImageView!
    @IBOutlet weak var btnCross: UIButton!
    
    @IBOutlet weak var memberNameLabel: UILabel!
    
    @IBOutlet weak var memberHandleLabel: UILabel!
    
    var delegate:SMFTribesMemberCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        memberImageView.layer.masksToBounds = false
        memberImageView.layer.cornerRadius = memberImageView.frame.height/2
        memberImageView.clipsToBounds = true
    }
    
    @IBAction func removMember(_ sender: Any) {
        delegate?.getIndexPath(indexPath: (sender as AnyObject).tag)
    }
    
    func resetCell(){
        btnCross.tag = -1
    }
    
    
    func setCell(tribeMember:TribeMember, forMember:Bool, indexPath:Int){
        resetCell()
        let memberProile = tribeMember.tribeMemberDetail?.tribeOwnerProfile
        if memberProile?.profileImage != nil {
            let imgURL = NSURL(string: (memberProile?.profileImage)!) as! URL
            self.memberImageView.sd_setImage(with: imgURL, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
        }
        if memberProile?.firstName != nil {
            self.memberNameLabel.text = "\((memberProile?.firstName)!) \((memberProile?.lastName)!)"
        }
        if memberProile?.handle != nil {
            self.memberHandleLabel.text = "@\((memberProile?.handle)!)"
        }
        if tribeMember.tribeMemberId == UserDefault.string(forKey: "userId"){
            self.adminLabel.isHidden = false
        }else{
            self.adminLabel.isHidden = true
        }
        if forMember {
            self.btnCross.isHidden = true
        }else {
            self.btnCross.isHidden = false
        }
        btnCross.tag = indexPath
        
    }
    
    func setCellForAdmin(tribe:Tribes){
        let owner = tribe.tribeOwner?.tribeOwnerProfile
        self.btnCross.isHidden = true
        if owner?.profileImage != nil {
            let imgURL = NSURL(string: (owner?.profileImage)!) as! URL
            self.memberImageView.sd_setImage(with: imgURL, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
        }
        if owner?.firstName != nil {
            self.memberNameLabel.text = "\((owner?.firstName.capitalized)!) \((owner?.lastName.capitalized)!)"
        }
        if owner?.handle != nil {
            self.memberHandleLabel.text = "@\((owner?.handle)!)"
        }
            self.adminLabel.isHidden = false
    }
    
}
