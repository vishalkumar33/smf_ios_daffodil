//
//  SMFAdminTribeDetailViewController.swift
//  SMF
//
//  Created by Jenkins on 2/8/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit
import AFNetworking

protocol RemoveMemberDelegate {
    func removeMember()
}


class SMFAdminTribeDetailViewController: SMFBaseViewController,TribeDetailPresenterDelegate{
    
    @IBOutlet weak var tribeSearchTextField: MyCustomTextField!
    @IBOutlet weak var tableView: UITableView!
    var tribe:Tribes?
    var tribeDetailPresenter      :      TribeDetailPresenter!
    var menuOptionNameArray : [String] = ["Edit Tribe", "Permanently Close Tribe"]
    var menuOptionImageNameArray = ["edit", "cross"]
    var limit = 0
    var skip = 0
    var tribesArray                :      [Post]                          =       [Post]()
    
    var delegate:RemoveMemberDelegate?
    
    var editTribeObj:SMFEditTribeViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        setNav(title: (tribe?.tribeName)!)
        setLeftNavBarButton()
        tribeSearchTextField.leftPadding(width : 16)
        self.tribeDetailPresenter = TribeDetailPresenter(delegate: self)
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.register(SMFHistoryTextTableViewCell.self)
        self.tableView.register(SMFHistoryImageTableViewCell.self)
        self.tableView.register(SMFHistoryVideoTableViewCell.self)
        self.tableView.register(SMFTextTableViewCell.self)
        getTribesFeed()
        self.tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "SMFLoaderTableViewCell",bundle: nil), forCellReuseIdentifier: "SMFLoaderTableViewCell")
        self.tableView.showsVerticalScrollIndicator = false
        setRightNavBarButton()
        changeToWeChatStyle()
        // Do any additional setup after loading the view.
    }
    
    func updateTable1(tripFeedArray: [Post], totalCount: Int) {
        self.tribesArray = tripFeedArray
        tableView.reloadData()
    }
    
    func successResponse() {
        self.delegate?.removeMember()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func  setRightNavBarButton(){
        let tribeMemberBtn = UIButton(type: .custom)
        tribeMemberBtn.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        tribeMemberBtn.setImage(#imageLiteral(resourceName: "setting"), for: .normal)
        let item1 = UIBarButtonItem(customView: tribeMemberBtn)
        let tribeEditBtn = UIButton(type: .custom)
        tribeEditBtn.frame = CGRect(x: 25, y: 0, width: 30, height: 30)
        tribeEditBtn.setImage(#imageLiteral(resourceName: "tribeMember"), for: .normal)
        let item2 = UIBarButtonItem(customView: tribeEditBtn)
        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
        tribeMemberBtn.addTarget(self, action: #selector(SMFAdminTribeDetailViewController.settingTapped), for: UIControlEvents.touchUpInside)
        tribeEditBtn.addTarget(self, action: #selector(SMFAdminTribeDetailViewController.goToMember), for: UIControlEvents.touchUpInside)
    }
    
    func goToMember(){
        let memberViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFTribesMemberViewController") as! SMFTribesMemberViewController
        memberViewController.forTribeMember = false
        if tribe?.tribeMember != nil {
            memberViewController.tribeMember = (tribe?.tribeMember)!
        }
        memberViewController.owner = tribe
        self.navigationController?.pushViewController(memberViewController, animated: true)
    }
    
    func settingTapped(_ sender: AnyObject){
        TribePopMenu.showForSender(sender: sender as! UIView, with: menuOptionNameArray, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            print(selectedIndex)
            switch selectedIndex {
            case 0:
                self.switchToEditTribe()
                break
            case 1:
                self.showAlert()
                break
            default:
                break
            }
        }) {
            
        }
    }
    
    func showAlert(){
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Do you really want to delete this tribe ?", preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelAction)
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Remove", style: .destructive) { action -> Void in
            self.tribeDetailPresenter.removeTribePermanently(groupId: (self.tribe?.tribeId)!)
        }
        actionSheetController.addAction(takePictureAction)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func switchToEditTribe(){
       // let editViewController =
         //   self.storyboard?.instantiateViewController(withIdentifier: "SMFEditTribeViewController") as! SMFEditTribeViewController
        editTribeObj?.tribe = self.tribe
        self.navigationController?.pushViewController(editTribeObj!, animated: true)
    }
    
    
    
    
    func changeToWeChatStyle() {
        let config = FTConfiguration1.shared
        config.textColor = UIColor.black
        config.backgoundTintColor = UIColor.white
        config.borderColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        config.menuWidth = 186
        config.borderWidth = 0.5
        config.menuSeparatorColor = UIColor.white
        config.textFont = UIFont.systemFont(ofSize: 11)
        config.menuRowHeight = 33
        config.cornerRadius = 10
        
    }
    func showLoader() {
        self.view.showLoader()
    }
    
    //Hide the Loader
    func hideLoader() {
        self.view.hideLoader()
    }
    
    //Show teh Error Alert
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    func getTribesFeed() {
        if AFNetworkReachabilityManager.shared().isReachable{
            self.tribeDetailPresenter.getTribeDetail(authorId: (tribe?.tribeId)!,limit:self.limit,skip: self.skip, lat: "28", long: "73")
        }
        else{
            self.getTribeFeedsNetworkHandler()
        }
    }
    
    
    func getTribeFeedsNetworkHandler(){
        AFNetworkReachabilityManager.shared().setReachabilityStatusChange { (status: AFNetworkReachabilityStatus) -> Void in
            switch status {
            case .notReachable:
                print("Not reachable")
            case .reachableViaWiFi, .reachableViaWWAN:
                self.tribeDetailPresenter.getTribeDetail(authorId: (self.tribe?.tribeId)!,limit:self.limit,skip: self.skip, lat: "28", long: "73")
                print("Reachable")
            case .unknown:
                print("Unknown")
            }
        }
    }
}

extension SMFAdminTribeDetailViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tribesArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) ->
        CGFloat {
            return UITableViewAutomaticDimension
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if tribesArray.count > 0 {
            tableView.backgroundView?.isHidden = true
            return 1
        }
        tableView.backgroundView?.isHidden = false
        TableViewHelper.EmptyMessage(message:"You don't have any Post of tribes" , tableView: tableView)
        
        return 0
    }
    
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let postObj = tribesArray[indexPath.row]
        if postObj.videoPath != "" {
            return UITableViewCell()
        }else if postObj.imagePath != "" {
            return UITableViewCell()
        }else{
            if postObj.content != "" {
                let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFTextTableViewCell
               
                cell.setCell(post: postObj, indexPath: indexPath.row)
                cell.selectionStyle = .none
                cell.contentView.removeFromSuperview()
                return cell
            }else{
                return UITableViewCell()
            }
        }
    }

    

}
