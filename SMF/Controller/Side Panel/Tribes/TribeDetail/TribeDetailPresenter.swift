//
//  TribeDetailPresenter.swift
//  SMF
//
//  Created by Jenkins on 2/8/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//



import Foundation

protocol TribeDetailPresenterDelegate:SMFCommonViewDelegate {
    
    func successResponse()
    func updateTable1(tripFeedArray:[Post], totalCount:Int)

}

class TribeDetailPresenter:ResponseCallback {
    
    private var tribesDetailViewDelegate    : TribeDetailPresenterDelegate?
    
    init(delegate : TribeDetailPresenterDelegate){
        tribesDetailViewDelegate = delegate
    }
    
    func servicesManagerSuccessResponse(responseObject: AnyObject) {
        self.tribesDetailViewDelegate?.hideLoader!()
    }
    
    func servicesManagerError(error: String) {
        self.tribesDetailViewDelegate?.hideLoader!()
    }
    
    func initDicForJoinReqeuest(memberId:String, groupId:String) -> NSDictionary {
        let dic = NSMutableDictionary()
        dic["groupId"] = groupId
        dic["memberId"] = memberId
        return dic
    }
    
    func joinRequest(memberId:String, groupId:String){
        self.tribesDetailViewDelegate?.showLoader!()
        Service.joinRequest(data: initDicForJoinReqeuest(memberId: memberId, groupId: groupId), success: { (response) in
            self.tribesDetailViewDelegate?.hideLoader!()
            self.tribesDetailViewDelegate?.successResponse()
        }) { (error) in
            if error?.code == -1011 {
                self.tribesDetailViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: "You already send the request")
            }
            self.tribesDetailViewDelegate?.hideLoader!()
        }
    }
    


   
    
   
    func getTribeDetail(authorId:String,limit:Int,skip:Int,lat:String, long:String)
    {
            Service.getTribeDetail(tribeId: authorId, limit: limit, skip: skip, lat: lat, long: long, success: { (response) in
                print(response)
                self.tribesDetailViewDelegate?.hideLoader!()
                self.parseResponse(data: response)
        }) { (error) in
            self.tribesDetailViewDelegate?.hideLoader!()
            print(error!)
        }
        
    }
    
    
    func parseResponse(data:NSDictionary){
        print(data)
        let totalCount = data["totalfeedscount"]
        let result = data["result"] as! NSArray
        var tribePostArray:[Post] = [Post]()
        
        for res in result{
            let obj = Post(response: res as! NSDictionary)
            tribePostArray.append(obj)
        }
        print(tribePostArray.count)
        self.tribesDetailViewDelegate?.updateTable1(tripFeedArray: tribePostArray, totalCount:  totalCount as! Int)

    }
    
    func initDicForRemove(groupId:String) -> NSMutableDictionary{
        let dic:NSMutableDictionary = NSMutableDictionary()
        dic["groupId"] = groupId
        return dic
    }
    
    
    func removeTribePermanently(groupId:String){
        self.tribesDetailViewDelegate?.showLoader!()
        Service.deleteTribe(data: initDicForRemove(groupId: groupId), success: { (response) in
            print(response)
            self.tribesDetailViewDelegate?.hideLoader!()
            self.tribesDetailViewDelegate?.successResponse()
        }) { (error) in
            self.tribesDetailViewDelegate?.hideLoader!()
        }
    }
    
    
}
