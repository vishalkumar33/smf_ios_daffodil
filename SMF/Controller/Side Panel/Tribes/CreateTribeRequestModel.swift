//
//  CreateTribeRequestModel.swift
//  SMF
//
//  Created by Vishal on 08/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class CreateTribeRequestModel{
    
    let requestBody   : [String:AnyObject]?
    let requestHeader : [String:AnyObject]?
    let limit         : Int?
    let skip          : Int?
    
    
    private init(builderObject:Builder){
        
        self.requestBody = builderObject.requestBody
        self.requestHeader = builderObject.requestHeader
        self.limit = builderObject.limit
        self.skip = builderObject.skip
        
    }
    
    
    internal class Builder{
        
        var requestBody = [String:AnyObject]()
        var requestHeader = [String:AnyObject]()
        var limit         = 10
        var skip          = 0
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        func setName(name:String) -> Builder {
            self.requestBody["name"] = name as AnyObject?
            return self
        }
        
        func setDescription(desc:String) -> Builder {
            self.requestBody["desc"] = desc as AnyObject?
            return self
        }
        
        func setStatus(status:Bool) -> Builder {
            self.requestBody["privateGroup"] = status as AnyObject?
            return self
        }
        
        func setMember(name:[NSDictionary]?) -> Builder {
            self.requestBody["members"] = name as AnyObject?
            return self
        }
        
        func setImage(imageUrl:String) -> Builder {
            self.requestBody["imageUrl"] = imageUrl as AnyObject?
            return self
        }
        
        
        func build() ->CreateTribeRequestModel{
            return CreateTribeRequestModel(builderObject: self)
        }
        
    }
    
    
    
    /**
     This method is used for creating End Point
     
     - returns: returning API End Point
     */
    
    func getEndPoint() -> String {
        return "/createGroup"
    }
    
    /**
     This method is used for
     
     - returns: returning request body
     */
    
    func getRequestBody() -> [String:AnyObject] {
        return self.requestBody!
    }
    
    /**
     This method is used for containing Request Headers
     
     - returns: Dictionary contains header
     */
    
    func getRequestHeader() -> [String:AnyObject] {
        return self.requestHeader!
    }
    
}
