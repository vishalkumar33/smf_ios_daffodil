//
//  SMFAddMemberPresenter.swift
//  SMF
//
//  Created by Vishal on 21/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

protocol AddMemberDelegate:SMFCommonViewDelegate {
    func updateTable(data:[Connection])
    func success(tribe:Tribes)
}

class SMFAddMemberPresenter {
    
    private var addMemberViewDelegate    : AddMemberDelegate?
    
    init(delegate: AddMemberDelegate ) {
        self.addMemberViewDelegate = delegate
    }
    
    
    func getConnections(){
        addMemberViewDelegate?.showLoader!()
        Service.getConnection(success: { (connectionArray) in
            self.addMemberViewDelegate?.hideLoader!()
            self.addMemberViewDelegate?.updateTable(data: connectionArray)
            print(connectionArray.count)
        }) { (error) in
            self.addMemberViewDelegate?.hideLoader!()
            print(error!)
        }
    }
    
    func initDic(groupId:String, selectedMember:[Connection]) -> NSMutableDictionary{
        var memberArray:[NSDictionary] = [NSDictionary]()
        for obj in selectedMember {
            let dic:NSMutableDictionary = NSMutableDictionary()
            dic["id"] = obj.id
            dic["status"] = "pending"
            memberArray.append(dic)
        }
        
        let dict:NSMutableDictionary = NSMutableDictionary()
        dict["groupId"] = groupId
        dict["member"] = memberArray
        print(dict)
        return dict
    }
    
    func requestForConncetion(groupId:String, member:[Connection]){
        self.addMemberViewDelegate?.showLoader!()
        Service.addMember(data: initDic(groupId: groupId, selectedMember: member), success: { (response) in
            self.addMemberViewDelegate?.hideLoader!()
            self.addMemberViewDelegate?.success(tribe: response)
            print(response)
        }) { (error) in
            self.addMemberViewDelegate?.hideLoader!()
            self.addMemberViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: (error?.domain)!)
        }
    }
    
}
