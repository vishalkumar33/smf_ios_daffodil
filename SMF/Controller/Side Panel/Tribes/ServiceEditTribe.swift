//
//  ServiceEditTribe.swift
//  SMF
//
//  Created by Vishal on 09/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class ServiceEditTribe {
    
    
    /**
     This method is used for perform Login With Valid Input(Credentials)
     
     - parameter inputData: Contains info for Login
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func editTribe(inputData: EditTribeRequestmodel, editTribeDelegate:ResponseCallback ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForCreateTribe()
        EditTribeApiRequest().makeAPIRequest(reqFromData: inputData, errorResolver: errorResolver, responseCallback: editTribeDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    //TODO:- String should be mapped from localizable string file.
    private func registerErrorForCreateTribe() ->ErrorResolver{
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1011, message: "Email is not register")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_401, message: "Enter valid email address")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_400, message: "Invalid credentials")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1009, message: "Please check internet connection")
        return errorResolver
    }
    
    
    
    
    
}
