//
//  SMFAddMemberTableViewCell.swift
//  SMF
//
//  Created by Vishal on 21/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit
import BEMCheckBox

protocol SMFAddMemberTableViewCellDelegate {
    func tapOnCheckBox(index:Int, value:Bool)
}


class SMFAddMemberTableViewCell: UITableViewCell, BEMCheckBoxDelegate {

    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblHandle: UILabel!
    @IBOutlet weak var selcted: BEMCheckBox!
    
    var delegate: SMFAddMemberTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgUSer.layer.masksToBounds = false
        imgUSer.layer.cornerRadius = imgUSer.frame.height/2
        imgUSer.clipsToBounds = true
        
        selcted.delegate = self
        
    }
    
    func resetCell(){
        selcted.tag = -1
    }
    
    func setCell(connection:Connection, index:Int){
        resetCell()
        let imageUrl = URL(string: connection.profileImage!)
        imgUSer.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "henryCavill"))
        lblName.text = connection.fullName
        lblHandle.text = "@\((connection.handle)!)"
        selcted.tag = index
    }
    
    func didTap(_ checkBox: BEMCheckBox) {
        print(checkBox.tag)
        self.delegate.tapOnCheckBox(index: checkBox.tag, value: checkBox.on)
    }
    
    
    
}


