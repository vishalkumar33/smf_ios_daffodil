//
//  SMFTribeDetailViewController.swift
//  SMF
//
//  Created by Jenkins on 2/8/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit
import AFNetworking

class SMFTribeDetailViewController: SMFBaseViewController,TribeDetailPresenterDelegate ,SMFHomeViewDelegate{
  
    let kSeparatorId = 123
    let kSeparatorHeight: CGFloat = 10
    func updateUI() {
    
    }
    var flameView :LikePopUp!

    
    var flag = 0

  
    @IBOutlet weak var tribepostbtn: MyCustomButton!
    var homePresenter :SMFHomePresenter!
    var menuOptionNameArray : [String] = ["Members", "Join Tribe"]
    var menuOptionImageNameArray = ["popUpMember", "joinTribe"]
    var hasMoreData = false
    var isRefreshedData = false

    @IBOutlet weak var tableView: UITableView!
    var tribe:Tribes?
    var tribeDetailPresenter      :      TribeDetailPresenter!
     var postObj : Post?
    var skip = 0
    var limit = 10
    var tribePostArray                :      [Post]                          =       [Post]()
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        setNav(title: (tribe?.tribeName)!)
        popUp()
        self.tribeDetailPresenter = TribeDetailPresenter(delegate: self)
        self.homePresenter = SMFHomePresenter(delegate: self)
        getTribesFeed()
        self.tableView.estimatedRowHeight = 300
        self.tableView.register(SMFHistoryTextTableViewCell.self)
        self.tableView.register(SMFHistoryImageTableViewCell.self)
        self.tableView.register(SMFHistoryVideoTableViewCell.self)
        self.tableView.register(SMFTextTableViewCell.self)
        self.tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "SMFLoaderTableViewCell",bundle: nil), forCellReuseIdentifier: "SMFLoaderTableViewCell")
        self.tableView.showsVerticalScrollIndicator = false
        initRefresh()

        setLeftNavBarButton()
        setRightNavBarButton()
        // Do any additional setup after loading the view.
    }
    func initRefresh(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl!.addTarget(self, action: #selector(SMFTribeDetailViewController.refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }

    func refresh() {
        if (self.refreshControl != nil) {
            let formatter: DateFormatter = DateFormatter()
            formatter.dateFormat = "MMM d, h:mm a"
            let title: String = "Last update: \(formatter.string(from: Date()))"
            let attrsDictionary: [String : AnyObject] = [
                NSForegroundColorAttributeName : UIColor.black
            ]
            let attributedTitle: NSAttributedString = NSAttributedString(string: title, attributes: attrsDictionary)
            self.refreshControl!.attributedTitle = attributedTitle
            self.refreshControl!.endRefreshing()
        }
        self.skip = 0
        self.hasMoreData = false
        self.isRefreshedData = true
        getTribesFeed()
    }
    
    func updateTableComment(commentArray: [Comment]) {
        
    }
    func updateCountLike() {
        
    }
    func updateCountDislike() {
        
    }
    func updateCountPeace() {
        
    }
    func getCommentList() {
        
    }
    @IBAction func tribePostAction(_ sender: Any) {
        
        let postUpadateController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFPostUpdateViewController") as! SMFPostUpdateViewController
        MyPostState.isPostSelected = true

        postUpadateController.tribe = tribe
        self.navigationController?.pushViewController(postUpadateController, animated: true)

    }
    
    func sharePostPush(index: Int) {
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))
        let sharePostController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFSharePostViewController") as! SMFSharePostViewController
        sharePostController.postView = getAppropriateCell(cell: cell!)
        sharePostController.postObj = tribePostArray[index]
        self.navigationController?.pushViewController(sharePostController, animated: true)
        
    }
    func  setRightNavBarButton(){
        let tribePopUpBtn = UIButton(type: .custom)
        tribePopUpBtn.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        tribePopUpBtn.setImage(#imageLiteral(resourceName: "PopUpTribe"), for: .normal)
        let item1 = UIBarButtonItem(customView: tribePopUpBtn)
        let tribeEditBtn = UIButton(type: .custom)
        tribeEditBtn.frame = CGRect(x: 20, y: 0, width: 30, height: 30)
        tribeEditBtn.setImage(#imageLiteral(resourceName: "TribeSearch"), for: .normal)
        let item2 = UIBarButtonItem(customView: tribeEditBtn)
        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
       tribePopUpBtn.addTarget(self, action: #selector(SMFTribeDetailViewController.settingTapped), for: UIControlEvents.touchUpInside)
        tribeEditBtn.addTarget(self, action: #selector(SMFTribeDetailViewController.switchToSearchTribe), for: UIControlEvents.touchUpInside)
    }
    func settingTapped(_ sender: AnyObject)
    {
        
        TribePopMenu.showForSender(sender: sender as! UIView, with: menuOptionNameArray, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            print(selectedIndex)
            switch selectedIndex {
            case 0:
                self.switchToMemberTribe()
                break
            case 1:
                self.joinTribe()
                break
            default:
                break
            }
        }) {
            
        }
    }
    func switchToMemberTribe(){
        let tribeMemberVC =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFTribesMemberViewController") as! SMFTribesMemberViewController
        tribeMemberVC.forTribeMember = true
        if tribe?.tribeMember != nil {
            tribeMemberVC.tribeMember = (tribe?.tribeMember)!
            tribeMemberVC.owner = tribe
        }
        self.navigationController?.pushViewController(tribeMemberVC, animated: true)
    }
    
    func switchToSearchTribe(){
        let searchVC =
           self.storyboard!.instantiateViewController(withIdentifier: "SMFSearchViewController") as! SMFSearchViewController
        searchVC.groupId = tribe?.tribeId
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    
    
    func joinTribe(){
        let userId = UserDefault.string(forKey: "userId")
        tribeDetailPresenter.joinRequest(memberId: userId!, groupId: (tribe?.tribeId)!)
    }
    

    func popUp()
    {
        let config = FTConfiguration1.shared
        config.textColor = UIColor.black
        config.backgoundTintColor = UIColor.white
        config.borderColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        config.menuWidth = 105
        config.borderWidth = 0.5
        config.menuSeparatorColor = UIColor.white
        config.textFont = UIFont.systemFont(ofSize: 11)
        config.menuRowHeight = 33
        config.cornerRadius = 10
 
    }
    
    func getTribesFeed() {
        
        if AFNetworkReachabilityManager.shared().isReachable{
            self.tribeDetailPresenter.getTribeDetail(authorId: (tribe?.tribeId)!,limit:self.limit,skip: self.skip, lat: "28", long: "73")
        }
        else{
            self.getTribeFeedsNetworkHandler()
        }
        
    }
    
    
    func getTribeFeedsNetworkHandler(){
        AFNetworkReachabilityManager.shared().setReachabilityStatusChange { (status: AFNetworkReachabilityStatus) -> Void in
            switch status {
            case .notReachable:
                print("Not reachable")
            case .reachableViaWiFi, .reachableViaWWAN:
                self.tribeDetailPresenter.getTribeDetail(authorId: (self.tribe?.tribeId)!,limit:self.limit,skip: self.skip, lat: "28", long: "73")
                print("Reachable")
            case .unknown:
                print("Unknown")
            }
        }
    }
    
    func updateTable1(tripFeedArray: [Post], totalCount: Int) {
        if refreshControl.isRefreshing {
            self.tribePostArray.removeAll(keepingCapacity: false)
            self.refreshControl.endRefreshing()
        }
        self.tribePostArray = tripFeedArray
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }

   
    func successResponse() {
        let TribeView:TribeNamePopup = Bundle.main.loadNibNamed("TribeNamePopUp", owner: self, options: nil)?[0] as! TribeNamePopup
        TribeView.frame = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        window.addSubview(TribeView)
    }
    
    
    func showLoader() {
        self.view.showLoader()
    }
    
    //Hide the Loader
    func hideLoader() {
        self.view.hideLoader()
    }
    
    //Show teh Error Alert
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func refreshData() {
        
    }
    
}


extension SMFTribeDetailViewController:UITableViewDelegate,UITableViewDataSource {
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hasMoreData == true{
            return tribePostArray.count + 1
        }
    else{
        return tribePostArray.count
    }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) ->
        CGFloat {
            return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.viewWithTag(kSeparatorId) == nil //add separator only once
        {
            let separatorView = UIView(frame: CGRect(x: 0, y: cell.frame.height - kSeparatorHeight, width: cell.frame.width, height: kSeparatorHeight))
            separatorView.tag = kSeparatorId
            separatorView.backgroundColor = UIColor.white
            separatorView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            cell.addSubview(separatorView)
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if  (indexPath as NSIndexPath).row < self.tribePostArray.count{
            return 300
        }
        else{
            return 50
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if tribePostArray.count > 0 {
            tableView.backgroundView?.isHidden = true
            return 1
        }
        tableView.backgroundView?.isHidden = false
        TableViewHelper.EmptyMessage(message:"You don't have any Post of tribes" , tableView: tableView)
        return 0
        
    }
    
    
    func getAppropriateCell(cell:UITableViewCell)->UINib
    {
        
        if let _:SMFTextTableViewCell = cell as? SMFTextTableViewCell{
            let nib = UINib(nibName: "SMFTextTableViewCell", bundle: nil)
            return nib
        }else if let _ :SMFHistoryVideoTableViewCell = cell as? SMFHistoryVideoTableViewCell
        {
            let nib = UINib(nibName: "SMFHistoryVideoTableViewCell", bundle: nil)
            return nib
        }
        else if let _ :SMFHistoryImageTableViewCell = cell as? SMFHistoryImageTableViewCell
        {
            let nib = UINib(nibName: "SMFHistoryImageTableViewCell", bundle: nil)
            return nib
        }
        else if let _ :SMFDisapperingPost = cell as? SMFDisapperingPost
        {
            let nib = UINib(nibName: "SMFTextTableViewCell", bundle: nil)
            return nib
        }
        return UINib()
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath as NSIndexPath).row < tribePostArray.count {
            postObj = tribePostArray[indexPath.row]
            print(postObj?.id)
            if postObj?.videoPath != "" && postObj?.hidden == false {
                if postObj?.content != "" {
                    let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFHistoryVideoTableViewCell
                    cell.videoDelegate = self
                    cell.setCell1(post: postObj!, indexPath: indexPath.row)
                    cell.selectionStyle = .none
                    cell.contentView.removeFromSuperview()
                    return cell
                }else{
                    return UITableViewCell()
                }
            }else if postObj?.imagePath != "" && postObj?.hidden == false {
                if postObj?.content != "" {
                    let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFHistoryImageTableViewCell
                    cell.imageDelegate = self
                   
                    
                    //   cell.btnPopup.addTarget(self, action: #selector(SMFHomeViewController.showPopUp(_:)), for: .touchUpInside)
                    cell.setCell1(post: postObj!, indexPath: indexPath.row)
                    cell.selectionStyle = .none
                    cell.contentView.removeFromSuperview()
                    return cell
                }else{
                    return UITableViewCell()
                }
            }else if postObj?.hidden == true {
                if postObj?.content != "" {
                    let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFDisapperingPost
                    
                    //   cell.btnPopup.addTarget(self, action: #selector(SMFHomeViewController.showPopUp(_:)), for: .touchUpInside)
                    cell.selectionStyle = .none
                    cell.contentView.removeFromSuperview()
                    return cell
                }else{
                    return UITableViewCell()
                }
            }
            else{
                if postObj?.content != "" && postObj?.hidden == false {
                    let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFTextTableViewCell
                    let baseView = UIView()
                    baseView.backgroundColor = UIColor.red
                    baseView.frame = self.view.bounds
                    //  tableView.backgroundView = baseView
                    // let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(gesture:)))
                    // tableView.addGestureRecognizer(swipeRightGesture)
                    // =
                    let flamePopup = cell.flamePopUp!
                  
                   
                    //                    cell.addSubview(flameView)
                    //                    tableView.bringSubview(toFront: cell)
                    
                    cell.hideDelegate = self
                    cell.setCell(post: postObj!, indexPath: indexPath.row)
                    cell.selectionStyle = .none
                    cell.contentView.removeFromSuperview()
                    return cell
                }else{
                    return UITableViewCell()
                }
            }
        } else if hasMoreData == true{
            let cell: SMFLoaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SMFLoaderTableViewCell")! as! SMFLoaderTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }else {
            let cell:UITableViewCell = UITableViewCell()
            cell.backgroundColor = UIColor.clear
            return cell
            
        }

    }
    func updateTable(newsFeedArray: [Post], totalCount: Int) {
            }
   
 
}
extension SMFTribeDetailViewController:SMFHistoryTextTableViewCellDelegate,HidePostDelegate,ActionDelegate,ReportPopUpDelegate, SMFHistoryImageTableViewCellDelegate,SMFHistoryVideoTableViewCellDelegate
{
     func flamePopUpLoad(index: Int) {
        
        let postObj = self.tribePostArray[index]
        // tableView.addGestureRecognizer(swipeRightGesture)
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! SMFTextTableViewCell
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let flamePopup = cell.flamePopUp!
        flameView = Bundle.main.loadNibNamed("LikePopUp", owner: self, options: nil)?[0] as! LikePopUp
        flameView.frame = CGRect(x: flamePopup.frame.minX, y: flamePopup.frame.minY, width: 260, height: 49)
        flameView.actionDelegate = self
        flameView.likeCountLabel.text = String(postObj.likedBy.count)
        flameView.dislikeCountLabel.text = String(postObj.dislikedBy.count)
        flameView.peaceCountLabel.text = String(postObj.peaceBy.count)
        
        if postObj.likedBy.contains( UserDefault.string(forKey: "userId")!)
        {
            flameView.likeImageView.image = #imageLiteral(resourceName: "like")
        }
        else
        {
            flameView.likeImageView.image = #imageLiteral(resourceName: "thumbsup")
        }
        if postObj.dislikedBy.contains( UserDefault.string(forKey: "userId")!)
        {
            flameView.dislikeImageView.image = #imageLiteral(resourceName: "thumbsdown")
        }
        else
        {
            flameView.dislikeImageView.image = #imageLiteral(resourceName: "dislike")
        }
        if postObj.peaceBy.contains( UserDefault.string(forKey: "userId")!)
        {
            flameView.peaceImageView.image = #imageLiteral(resourceName: "peaceBlue")
        }
        else
        {
            flameView.peaceImageView.image = #imageLiteral(resourceName: "win")
            
        }
        
        flameView.likeButton.tag = index
        flameView.dislikeButton.tag = index
        flameView.peaceButton.tag = index
       
        cell.addSubview(flameView)
        tableView.bringSubview(toFront: cell)
        flag = 1

    }
   
     func hidePost(index: Int) {
        let postobj1 = tribePostArray[index]
        print(postobj1.id)
        self.homePresenter.HidePostButtonClicked(postId: (postobj1.id) )
    }
    
    func reportPost(index: Int) {
        let reportView = Bundle.main.loadNibNamed("ReportPopUp", owner: self, options: nil)?[0] as! ReportPopUp
        reportView.frame = UIScreen.main.bounds
        reportView.confirm = self
        let window = UIApplication.shared.keyWindow!
        window.addSubview(reportView)
        let postobj1 = tribePostArray[index]
        print(postobj1.id)
    }

    
     func popUpButtonTapped(_ sender: AnyObject) {
        
         
    }

    func confirmDelegate(_ b: UIButton) {
        self.homePresenter.confirmReport(postId: (postObj?.id)!)
    }
    func likeTapped(_ b: UIButton) {
        let postObj = self.tribePostArray[b.tag]
        let likeCount = postObj.likedBy.count
        if postObj.likedBy.contains( UserDefault.string(forKey: "userId")!)
        {
            postObj.likedBy.remove(UserDefault.string(forKey: "userId")!)
            self.flameView.likeImageView.image = #imageLiteral(resourceName: "thumbsup")
        }
        else
        {
            postObj.likedBy.add(UserDefault.string(forKey: "userId")!)
            self.flameView.likeImageView.image = #imageLiteral(resourceName: "like")
        }
        
        self.flameView.likeCountLabel.text = String(postObj.likedBy.count)
        self.tableView.reloadData()
        self.homePresenter.tappedRelateToFlame(postId: (postObj.id), action: "like", content: "")
    }
    
    func popHome() {
        
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if (flag == 1){
            flameView.closePopUp()
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView){
        if (flag == 1){
            flameView.closePopUp()
        }
    }
    func dislikeTapped(_ b: UIButton) {
        let postObj = self.tribePostArray[b.tag]
        let disLikeCount = postObj.dislikedBy.count
        if postObj.dislikedBy.contains( UserDefault.string(forKey: "userId")!)
        {
            postObj.dislikedBy.remove(UserDefault.string(forKey: "userId")!)
            self.flameView.dislikeImageView.image = #imageLiteral(resourceName: "dislike")
        }
        else
        {
            postObj.dislikedBy.add(UserDefault.string(forKey: "userId")!)
            self.flameView.dislikeImageView.image = #imageLiteral(resourceName: "thumbsdown")
        }
        var cell = tableView.cellForRow(at: IndexPath(row: b.tag, section: 0))
        if (cell?.isKind(of: SMFTextTableViewCell.self))!
        {
            let newcell = cell as! SMFTextTableViewCell
            newcell.countLabel.text = String(postObj.likedBy.count+postObj.dislikedBy.count+postObj.peaceBy.count)
        }
        else if (cell?.isKind(of: SMFHistoryImageTableViewCell.self))!
        {
            let newcell = cell as! SMFHistoryImageTableViewCell
            newcell.countLabel.text = String(postObj.likedBy.count+postObj.dislikedBy.count+postObj.peaceBy.count)
        }
        else
        {
            let newcell = cell as! SMFHistoryVideoTableViewCell
            newcell.countLabel.text = String(postObj.likedBy.count+postObj.dislikedBy.count+postObj.peaceBy.count)
        }
        self.flameView.dislikeCountLabel.text = String(postObj.dislikedBy.count)
        
        self.homePresenter.tappedRelateToFlame(postId: (postObj.id), action: "dislike", content: "")
        

        
    }
    func peaceTapped(_ b: UIButton) {
        let postObj = self.tribePostArray[b.tag]
        let likeCount = postObj.peaceBy.count
        if postObj.peaceBy.contains( UserDefault.string(forKey: "userId")!)
        {
            postObj.peaceBy.remove(UserDefault.string(forKey: "userId")!)
            self.flameView.peaceImageView.image = #imageLiteral(resourceName: "win")
            
        }
        else
        {
            postObj.peaceBy.add(UserDefault.string(forKey: "userId")!)
            flameView.peaceImageView.image = #imageLiteral(resourceName: "peaceBlue")
        }
        let cell = tableView.cellForRow(at: IndexPath(row: b.tag, section: 0)) as! SMFTextTableViewCell
        self.flameView.peaceCountLabel.text = String(postObj.peaceBy.count)
        cell.countLabel.text = String(postObj.likedBy.count+postObj.dislikedBy.count+postObj.peaceBy.count)
        

        
    }
    
     func flamePopUpLoadImage(index:Int) {
        let postObj = self.tribePostArray[index]
        // tableView.addGestureRecognizer(swipeRightGesture)
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! SMFHistoryImageTableViewCell
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let flamePopup = cell.flamePopUp!
        flameView = Bundle.main.loadNibNamed("LikePopUp", owner: self, options: nil)?[0] as! LikePopUp
        flameView.frame = CGRect(x: flamePopup.frame.minX, y: flamePopup.frame.minY, width: 260, height: 49)
        flameView.actionDelegate = self
        flameView.likeCountLabel.text = String(postObj.likedBy.count)
        flameView.dislikeCountLabel.text = String(postObj.dislikedBy.count)
        flameView.peaceCountLabel.text = String(postObj.peaceBy.count)
        
        if postObj.likedBy.contains( UserDefault.string(forKey: "userId")!)
        {
            flameView.likeImageView.image = #imageLiteral(resourceName: "like")
        }
        else
        {
            flameView.likeImageView.image = #imageLiteral(resourceName: "thumbsup")
        }
        if postObj.dislikedBy.contains( UserDefault.string(forKey: "userId")!)
        {
            flameView.dislikeImageView.image = #imageLiteral(resourceName: "thumbsdown")
        }
        else
        {
            flameView.dislikeImageView.image = #imageLiteral(resourceName: "dislike")
        }
        if postObj.peaceBy.contains( UserDefault.string(forKey: "userId")!)
        {
            flameView.peaceImageView.image = #imageLiteral(resourceName: "peaceBlue")
        }
        else
        {
            flameView.peaceImageView.image = #imageLiteral(resourceName: "win")
            
        }
        
        flameView.likeButton.tag = index
        flameView.dislikeButton.tag = index
        flameView.peaceButton.tag = index
        cell.addSubview(flameView)
        tableView.bringSubview(toFront: cell)
        flag = 1
    }
    
    func pushCommentPage(index:Int)
    {
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SMFCommentViewController") as! SMFCommentViewController
        let navigationController = UINavigationController(rootViewController: vc)
        
        self.present(navigationController, animated: true, completion: nil)
        //    self.delegate.saveIndex(index: index)
        MyPostId.isPost     = index
        vc.postView = getAppropriateCell(cell: cell!)
        MyPostId.postId = tribePostArray[index].id
        vc.postObjFeeds = tribePostArray[index]
        
    }
    func blockUser() {
        self.homePresenter.blockUserClicked(blockUserId: (postObj?.author.id)!)

    }

    

}
