//
//  SMFTribesListCollectionViewCell.swift
//  SMF
//
//  Created by Jenkins on 2/7/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFTribesListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tribeImageView: UIImageView!
    @IBOutlet weak var tribeImageNameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var noOfMemberLabel: UILabel!
    @IBOutlet weak var tribeDetailLabel: UILabel!
    @IBOutlet weak var imgLock: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        intCell()
    }
    
    func intCell(){
        tribeImageView.layer.masksToBounds = false
        tribeImageView.layer.borderColor = UIColor.appBlueBorderColor.cgColor
        tribeImageView.layer.cornerRadius = tribeImageView.frame.height/2
        tribeImageView.clipsToBounds = true
    }
    
    func setCell(tribes:Tribes){
        self.tribeImageView.sd_setImage(with: NSURL(string:tribes.tribeImage!) as! URL, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
        self.tribeDetailLabel.text = tribes.tribeDescription
        self.tribeImageNameLabel.text = tribes.tribeName?.uppercased()
        let count = tribes.tribeMember.count + 1
        self.noOfMemberLabel.text = "\(count)"
        if tribes.tribePrivateGroup == true {
            self.imgLock.isHidden = false
        }else{
            self.imgLock.isHidden = true
        }
    }
}
