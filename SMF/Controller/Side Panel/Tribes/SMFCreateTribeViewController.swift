//
//  SMFCreateTribeViewController.swift
//  SMF
//
//  Created by Jenkins on 2/8/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFCreateTribeViewController: SMFBaseViewController {

   
    @IBOutlet weak var btnAddMember: UIButton!
    @IBOutlet weak var tokenView: KSTokenView!
    @IBOutlet weak var tribeNameTextField: MyCustomTextField!
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var tickbtn: MyCustomButton!
    @IBOutlet weak var tribeTextView: UITextView!
    @IBOutlet weak var addMemberTextField: MyCustomTextField!
    
    
    
    var createTribePresenter: CreateTribePresenter!
    let imagePicker = UIImagePickerController()
    var isCheck = false
    
    var isMemberSelected = false
    
    var selctedMemmber:[Connection] = [Connection]()
    
    let addMemberButton = UIButton(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNav(title: "CREATE TRIBE")
        tribeNameTextField.leftPadding(width: 20)
        addMemberTextField.leftPadding(width: 20)
        tribeTextView.textContainerInset =
            UIEdgeInsetsMake(10,14,0,0);
        createTribePresenter = CreateTribePresenter(delegate: self)
        setLeftNavBarButton()
        makeImageRound()
        imagePicker.delegate = self
        tribeTextView.layer.borderWidth = 1.0
        tribeTextView.layer.borderColor = UIColor.appGrayboderColor.cgColor
        tribeTextView.layer.cornerRadius = 5.0
        tribeTextView.clipsToBounds = true
        
        
        addMemberTextField.isHidden = true
        initTokenView()
       
        
       // addMemberTextField.addTarget(self, action:#selector(SMFCreateTribeViewController.showAddMember), for: UIControlEvents.editingDidBegin)
        
    
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tokenView.deleteAllTokens()
        if isMemberSelected {
            if selctedMemmber.count > 0 {
                for connection in selctedMemmber {
                    let token = KSToken(title: "@\(connection.handle!)" , object: title as AnyObject?)
                    token.backgroundColor = UIColor.blue
                    tokenView.addToken(token)
                }
            }
        }
    }
    
    func initTokenView(){
        tokenView.delegate = self
        tokenView.placeholder = "Add Members"
        tokenView.style = .squared
        tokenView.layer.borderColor = UIColor(colorLiteralRed: 81/255, green: 80/255, blue: 82/255, alpha: 1).cgColor
        tokenView.layer.borderWidth = 1.3
        tokenView.layer.cornerRadius = 2

    }
    
    
    func makeImageRound(){
        cameraImage.layer.borderWidth = 1
        cameraImage.layer.masksToBounds = false
        cameraImage.layer.borderColor = UIColor.black.cgColor
        cameraImage.layer.cornerRadius = cameraImage.frame.height/2
        cameraImage.clipsToBounds = true
    }
    
    func showAddMember(){
        addMemberTextField.resignFirstResponder()
        let addMemberVC = self.storyboard?.instantiateViewController(withIdentifier: "SMFAddMemberViewController") as! SMFAddMemberViewController
        addMemberVC.previousArray = selctedMemmber
        self.navigationController?.pushViewController(addMemberVC, animated: true)
    }
    
    
    @IBAction func addMemberToTribe(_ sender: Any) {
        showAddMember()
    }

  
    @IBAction func markPrivateAction(_ sender: Any) {

        isCheck = !isCheck
        if(isCheck)
        {
           tickbtn.setImage(#imageLiteral(resourceName: "check"), for: UIControlState.normal)
        }
        else
        {
           tickbtn.setImage(nil, for: UIControlState.normal)

        }

    }
    
   
    @IBAction func createTribeAction(_ sender: Any) {
        if cameraImage.image != #imageLiteral(resourceName: "camera") {
            if createTribePresenter.isValidData(name: tribeNameTextField.text!, desc: tribeTextView.text) {
                createTribePresenter.uploadPhoto(image: cameraImage.image!, type: "groupsImages")
            }
        }else{
            self.showErrorAlert(alertTitle: "Error", alertMessage: "Image is required", VC: self)
        }
    }
    
    @IBAction func cameraAction(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    

}

extension SMFCreateTribeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            cameraImage.image = pickedImage
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}



extension SMFCreateTribeViewController: CreateTribeViewDelegate {
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func imageUploadStatus(url: String) {
        
        var memberArray:[NSDictionary] = [NSDictionary]()
        for obj in selctedMemmber {
            let dic:NSMutableDictionary = NSMutableDictionary()
            dic["id"] = obj.id
            dic["status"] = "pending"
            memberArray.append(dic)
        }
        
        self.createTribePresenter.createTribes(name: tribeNameTextField.text!, desc: tribeTextView.text, memberArray: memberArray, imgUrl: url, status: isCheck)
    }
    
    func popUp() {
        _ = self.navigationController?.popViewController(animated: true)
    }
}


extension SMFCreateTribeViewController: KSTokenViewDelegate {
    
    func tokenView(_ token: KSTokenView, performSearchWithString string: String, completion: ((_ results: Array<AnyObject>) -> Void)?) {
        return
//        var data: Array<String> = []
//        for value: String in names {
//            if value.lowercased().range(of: string.lowercased()) != nil {
//                data.append(value)
//            }
//        }
//        completion!(data as Array<AnyObject>)
    }
    
    
    func tokenView(_ token: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        return object as! String
    }
    
    
    
    func tokenView(_ tokenView: KSTokenView, shouldAddToken token: KSToken) -> Bool {
        var shouldAddToken = true
        if (token.title == "f") {
            shouldAddToken = false
        }
        return shouldAddToken
    }
    
}



