//
//  MyTribesPresneter.swift
//  SMF
//
//  Created by Vishal on 08/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class MyTribesPresneter:ResponseCallback {
    
    private var tribesViewDelegate    : SMFTribesViewDelegate?
    private var serviceBuisnessGetMyTribes = ServiceMyTribes()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : SMFTribesViewDelegate){
        tribesViewDelegate = delegate
    }
    
    func servicesManagerSuccessResponse(responseObject: AnyObject) {
        self.tribesViewDelegate?.hideLoader!()
        if responseObject is AllTribesResponse {
            let obj:AllTribesResponse = responseObject as! AllTribesResponse
            tribesViewDelegate?.updateTable(withData: obj.tribes, forMyTribes: true, count: obj.totalCount)
        }
    }
    
    func servicesManagerError(error: String) {
        self.tribesViewDelegate?.hideLoader!()
    }
    
    
    func getMyTribes(limit:Int, skip: Int){
        tribesViewDelegate?.showLoader!()
        serviceBuisnessGetMyTribes.getMyTribes(inputData: MyTribesRequestModel.Builder().setSkip(skip: skip).setLimit(limit: limit).addRequestHeader(key:"Content-Type", value: "application/json").build(), allTribeDelegate: self)
        
    }
    
    func searchTribe(text:String){
        self.tribesViewDelegate?.showLoader!()
        Service.searchTribe(searchText: text, success: { (tribesArray, totalCount) in
            self.tribesViewDelegate?.hideLoader!()
            self.tribesViewDelegate?.updateTable(withData: tribesArray, forMyTribes: true, count: totalCount)
        }) { (error) in
            self.tribesViewDelegate?.hideLoader!()
        }
    }
    
}
