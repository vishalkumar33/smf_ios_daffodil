//
//  SMFTribesListViewController.swift
//  SMF
//
//  Created by Jenkins on 2/7/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFTribesListViewController: SMFBaseViewController {

    @IBOutlet weak var allTribeBtn: MyCustomButton!
    
    @IBOutlet weak var myTribeBtn: MyCustomButton!
    
    @IBOutlet weak var createTribeBtn: MyCustomButton!
    @IBOutlet weak var tribesCollectionView: UICollectionView!
    
    @IBOutlet weak var searchTextField: MyCustomTextField!
    
    var tribeViewPresenter : TribesViewPresenter?
    var myTribeViewPresenter : MyTribesPresneter?
    
    var tribArray:[Tribes] = [Tribes]()
    var forMyTribe = false
    
    var refreshControl: UIRefreshControl!
    
    var forSearch:Bool = false
    
    var skip = 0
    var hasMoreData = false
    
    var selectedIndex:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.leftPadding(width:45)
        setbackgroundColorOnSelection(sender:allTribeBtn)
        setbackgroundColorNoSolection(sender: myTribeBtn)
        setbackgroundColorNoSolection(sender: createTribeBtn)
        
        searchTextField.returnKeyType = .search
        searchTextField.delegate = self

        searchTextField.attributedPlaceholder = NSAttributedString(string: "Search",
                                                                   attributes: [NSForegroundColorAttributeName: UIColor.appBlueBorderColor])
        tribeViewPresenter = TribesViewPresenter(delegate: self)
        myTribeViewPresenter = MyTribesPresneter(delegate: self)
        tribesCollectionView.delegate = self
        tribesCollectionView.dataSource = self
        tribesCollectionView.showsVerticalScrollIndicator = false
        
        tribesCollectionView.register(UINib(nibName:"SMFPageLoaderCollectionReusableView", bundle:nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "SMFPageLoaderCollectionReusableView")
        
        initRefresh()
        setNav(title: "TRIBES")
        setLeftNavBarButton()
        getTribes()

    }
    
    
    func initRefresh(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl!.addTarget(self, action: #selector(SMFTribesListViewController.refresh), for: .valueChanged)
        tribesCollectionView.addSubview(refreshControl)
    }
    
    
    
    
    func refresh() {
        self.skip = 0
        self.hasMoreData = false
        getTribes()
    }
    
    
    func getTribes(){
        if forMyTribe == true {
            myTribeViewPresenter?.getMyTribes(limit: 10, skip: skip)
        }else{
            tribeViewPresenter?.getAllTribes(limit: 10, skip: skip)
        }
        
    }

    @IBAction func allTribesAction(_ sender: Any) {
        setbackgroundColorOnSelection(sender:allTribeBtn)
        setbackgroundColorNoSolection(sender: myTribeBtn)
        setbackgroundColorNoSolection(sender: createTribeBtn)
        forMyTribe = false
        self.skip = 0
        self.hasMoreData = false
        tribArray.removeAll(keepingCapacity: false)
        tribesCollectionView.reloadData()
        getTribes()
    }
    
    @IBAction func myTribesAction(_ sender: Any) {
        setbackgroundColorOnSelection(sender: myTribeBtn)
        setbackgroundColorNoSolection(sender: allTribeBtn)
        setbackgroundColorNoSolection(sender: createTribeBtn)
        forMyTribe = true
        self.skip = 0
        self.hasMoreData = false
        tribArray.removeAll(keepingCapacity: false)
        tribesCollectionView.reloadData()
        getTribes()
    }
    
    @IBAction func createTribesAction(_ sender: Any) {
        let createTribe =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFCreateTribeViewController") as! SMFCreateTribeViewController
        self.navigationController?.pushViewController(createTribe, animated: true)
    }
    
    func setbackgroundColorOnSelection(sender:UIButton)
    {
        sender.backgroundColor = UIColor.appseaformColor
        sender.setTitleColor(UIColor.white, for: UIControlState.normal)
        sender.layer.borderColor = UIColor.white.cgColor
    }
    
    func setbackgroundColorNoSolection(sender:UIButton)
    {
        sender.backgroundColor = UIColor.white
        sender.setTitleColor(UIColor.appseaformColor, for: UIControlState.normal)
        sender.layer.borderColor = UIColor.appseaformColor.cgColor

    }

}

extension SMFTribesListViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        var numOfSections: Int = 0
        if tribArray.count > 0 {
            numOfSections            = 1
            collectionView.backgroundView = nil
        }
        else{
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height))
            noDataLabel.text          = "No data available"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            collectionView.backgroundView  = noDataLabel
        }
        return numOfSections
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,referenceSizeForFooterInSection section: Int) -> CGSize{
        let width = UIScreen.main.bounds.width
        if hasMoreData {
            return CGSize(width: width, height: 50)
        }else{
            return CGSize.zero
        }
    }
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tribArray.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = tribesCollectionView.dequeueReusableCell(withReuseIdentifier: "SMFTribesListCollectionViewCell", for: indexPath) as! SMFTribesListCollectionViewCell
        cell.layer.masksToBounds = true
        cell.layer.borderColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.2 ).cgColor
        cell.layer.borderWidth = 1.0
        
        cell.setCell(tribes: tribArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        let tribe  = tribArray[indexPath.row]
        if tribe.tribePrivateGroup == true {
            if self.validateUser(tribe: tribe) == false {
                showAlertViewToJoinTribe(groupId: tribe.tribeId!)
            }else{
                tribeViewPresenter?.switchToDetail(tribe: tribArray[indexPath.row])
            }
        }else {
            tribeViewPresenter?.switchToDetail(tribe: tribArray[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if hasMoreData == true {
            getTribes()
            switch kind {
            case UICollectionElementKindSectionFooter:
                let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SMFPageLoaderCollectionReusableView", for: indexPath)
                return footerView
            default:
                assert(false, "Unexpected element kind")
            }
        }else{
            return UIView() as! UICollectionReusableView
        }
        return UIView() as! UICollectionReusableView
    }
    
    func validateUser(tribe:Tribes)->Bool{
        let userId = UserDefault.string(forKey: "userId")
        if tribe.tribeOwner?.tribeOwnerId == userId {
            return true
        }
        return false
    }
    
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
//        let frame : CGRect = self.tribesCollectionView.frame
//        let margin  = (frame.width - 90 * 3) / 6.0
//        return UIEdgeInsetsMake(0, margin, 0, margin) // margin between cells
//    }
    
    func showAlertViewToJoinTribe(groupId:String){
        let userId = UserDefault.string(forKey: "userId")
        let alertController = UIAlertController(title: "Sorry", message: "This is the private group to see detail please join the club ??", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            self.tribeViewPresenter?.joinRequest(memberId: userId!, groupId: groupId)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true) {
        }
    }
    
    
    
}

extension SMFTribesListViewController: SMFTribesViewDelegate {
    func showLoader() {
        self.view.showLoader()
    }
    
    func  hideLoader() {
        self.view.hideLoader()
    }
    
    func updateTable(withData data: [Tribes], forMyTribes: Bool, count: Int) {
        if forSearch == true {
            self.tribArray.removeAll(keepingCapacity: false)
            forSearch = false
        }
        if refreshControl.isRefreshing {
            self.tribArray.removeAll(keepingCapacity: false)
            self.refreshControl.endRefreshing()
        }
        self.tribArray.append(contentsOf: data)
        if count > self.tribArray.count {
            hasMoreData = true
            skip = skip + 10
        }else {
            hasMoreData = false
        }
        tribesCollectionView.reloadData()
    }
    
    
    
    func switchToAdmin(withTribe tribe: Tribes) {
        
        let tribeEdit =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFEditTribeViewController") as! SMFEditTribeViewController
        tribeEdit.delegate = self
        
        let tribeDetail =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFAdminTribeDetailViewController") as! SMFAdminTribeDetailViewController
        tribeDetail.tribe = tribe
        tribeDetail.delegate = self
        tribeDetail.editTribeObj = tribeEdit
        self.navigationController?.pushViewController(tribeDetail, animated: true)
    }
 
    
    func swithcToMember(withTribe tribe: Tribes) {
        let tribeDetail =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFTribeDetailViewController") as! SMFTribeDetailViewController
        tribeDetail.tribe = tribe
        self.navigationController?.pushViewController(tribeDetail, animated: true)
    }
    
    func successResponse() {
        let TribeView:TribeNamePopup = Bundle.main.loadNibNamed("TribeNamePopUp", owner: self, options: nil)?[0] as! TribeNamePopup
        TribeView.frame = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        window.addSubview(TribeView)
    }
    
}

extension SMFTribesListViewController: UITextFieldDelegate {
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
            self.forSearch = true
            myTribeViewPresenter?.searchTribe(text: textField.text!)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
            if textField.text != "" {
                self.forSearch = true
                myTribeViewPresenter?.searchTribe(text: textField.text!)
            }
        }
        return true
    }
}

extension SMFTribesListViewController: RemoveMemberDelegate{
    func removeMember() {
        if selectedIndex != nil {
            self.tribArray.remove(at: selectedIndex!)
            tribesCollectionView.reloadData()
        }
    }
}

extension SMFTribesListViewController: editTribeDelegate{
    func reloadData() {
        self.tribesCollectionView.reloadData()
    }
}


