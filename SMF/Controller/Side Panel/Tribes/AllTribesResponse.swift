//
//  AllTribesResponse.swift
//  SMF
//
//  Created by Vishal on 08/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class AllTribesResponse: Mapper {
    
    // User Info Property
    let sessId      : String
    let sessionName : String
    let token       : String
    var tribes:[Tribes] = [Tribes]()
    let totalCount:Int
    
    //Declaring Response Key coming from server
    private let SESSID_KEY      = "sessid"
    private let SESSIONNAME_KEY = "session_name"
    private let TOKEN_KEY       = "token"
    private let TOTAL_COUNT_KEY = "totalTribescount"
    
    
    required init?(_ response: Dictionary<String,AnyObject>) {
        print(response)
        sessId      = (response[SESSID_KEY] as? String) ?? ""
        sessionName = (response[SESSIONNAME_KEY] as? String) ?? ""
        token       = (response[TOKEN_KEY] as? String) ?? ""
        totalCount  = (response[TOTAL_COUNT_KEY] as? Int) ?? 0
        if let tribeResul = response["result"] as? NSArray{
            for tribe in tribeResul{
                let tribeObj = Tribes(tribe as! Dictionary<String, AnyObject>)
                self.tribes.append(tribeObj!)
            }
        }
    }
    
}
