//
//  EditTribeRequestmodel.swift
//  SMF
//
//  Created by Vishal on 09/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class EditTribeRequestmodel {
    
    let requestBody   : [String:AnyObject]?
    let requestHeader : [String:AnyObject]?
    
    private init(builderObject:Builder){
        
        self.requestBody = builderObject.requestBody
        self.requestHeader = builderObject.requestHeader
       
        
    }
    
    
    internal class Builder{
        
        var requestBody = [String:AnyObject]()
        var requestHeader = [String:AnyObject]()
        
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        
        func setGroupId(groupId:String) -> Builder {
            self.requestBody["groupId"] = groupId as AnyObject?
            return self
        }
        
        func setName(name:String) -> Builder {
            self.requestBody["name"] = name as AnyObject?
            return self
        }
        
        func setDescription(desc:String) -> Builder {
            self.requestBody["desc"] = desc as AnyObject?
            return self
        }
        
        func setImage(imageUrl:String) -> Builder {
            self.requestBody["imageUrl"] = imageUrl as AnyObject?
            return self
        }
        
        
        func build() ->EditTribeRequestmodel{
            return EditTribeRequestmodel(builderObject: self)
        }
        
    }
    
    
    
    /**
     This method is used for creating End Point
     
     - returns: returning API End Point
     */
    
    func getEndPoint() -> String {
        return "/updateTribe"
    }
    
    /**
     This method is used for
     
     - returns: returning request body
     */
    
    func getRequestBody() -> [String:AnyObject] {
        return self.requestBody!
    }
    
    /**
     This method is used for containing Request Headers
     
     - returns: Dictionary contains header
     */
    
    func getRequestHeader() -> [String:AnyObject] {
        return self.requestHeader!
    }
}
