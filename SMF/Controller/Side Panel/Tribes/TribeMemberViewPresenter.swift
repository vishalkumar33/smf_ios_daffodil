//
//  TribeMemberViewPresenter.swift
//  SMF
//
//  Created by Vishal on 10/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


protocol TribeMemberViewDelegate:SMFCommonViewDelegate {
    func showSearchView()
    func hideSearchView()
}


class TribeMemberViewPresenter {
    
    
    private var tribesMemberViewDelegate    : TribeMemberViewDelegate?
    
    init(delegate : TribeMemberViewDelegate){
        tribesMemberViewDelegate = delegate
    }
    
    func intView(forMember:Bool){
        if forMember == false {
            self.tribesMemberViewDelegate?.showSearchView()
        }else{
            self.tribesMemberViewDelegate?.hideSearchView()
        }
    }
    
    func getMember(searchValue:String){
        
        Service.getTribeMemberSearchList(searchText: searchValue, success: { (response) in
            print(response)
        }) { (error) in
            print(error!)
        }
        
    }
    
    func parseMember(response:NSDictionary){
        let result:NSArray = response["result"] as! NSArray
    }
    
    func dictForRemvoeMember(groupId:String, memberId:String) -> NSMutableDictionary{
        let dic:NSMutableDictionary = NSMutableDictionary()
        dic["groupId"] = groupId
        dic["memberId"] = memberId
        return dic
    }
    
    func removeMember(memberId:String, groupId:String){
        self.tribesMemberViewDelegate?.showLoader!()
        Service.removeMember(data: dictForRemvoeMember(groupId: groupId, memberId: memberId), success: { (tribeDetail) in
            self.tribesMemberViewDelegate?.hideLoader!()
            self.tribesMemberViewDelegate?.showErrorAlert!(alertTitle: "Success", alertMessage: "You successfully remove the member")
        }) { (error) in
            self.tribesMemberViewDelegate?.hideLoader!()
        }
    }
    
    
    
    
}
