//
//  SMFMessageTableViewCell.swift
//  SMF
//
//  Created by Shivam Srivastava on 28/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

class SMFMessageTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgProfile.layer.masksToBounds = false
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        imgProfile.clipsToBounds = true
        
        // Initialization code
    }
    
    func setCell(message:Message){
        let name = "\((message.recipient?.profile.firstName)!) \((message.recipient?.profile.lastName)!)"
        nameLabel.text = name
        let url = URL(string: (message.recipient?.profile.profileImage)!)
        imgProfile.sd_setImage(with: url)
        contentLabel.text = message.content!
    }
    
    func setCellUserMessage(message:UserMessage){
        let url = URL(string: message.messageReceiverProfile!)
        nameLabel.text = message.messageReceiverName
        imgProfile.sd_setImage(with: url)
        contentLabel.text = message.messageContent
    }


}
