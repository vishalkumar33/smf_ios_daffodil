//
//  SMFMessageRecevingTableViewCell.swift
//  SMF
//
//  Created by Vishal on 19/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFMessageRecevingTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setCell(size:CGSize, withText:String, profileImage:String){
        
        self.viewWithTag(1)?.removeFromSuperview()
        self.viewWithTag(2)?.removeFromSuperview()
        
        let cellHeight = size.height + 20
        let userImage = UIImageView()
        userImage.tag = 1
        let url = URL(string: profileImage)
        userImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
        //userImage.image = #imageLiteral(resourceName: "henryCavill")
        let imageY = ((cellHeight/2) - 20)
        userImage.frame = CGRect(x: 8, y: imageY, width: 40, height: 40)
        userImage.layer.cornerRadius = 20
        userImage.clipsToBounds = true
        self.addSubview(userImage)
        
        let label = UILabel(frame: .zero)
        label.tag = 2
        label.font = UIFont(name: "Quicksand-Medium", size: 13)
        label.backgroundColor = UIColor.white
        label.text = withText
        label.numberOfLines = 0
        label.frame = CGRect(x: 58, y: 10, width: size.width, height: size.height)
        self.addSubview(label)
        
        self.layoutIfNeeded()
    }
    
    
    func setCellForSending(){
        self.flipY()
    }
    
    

    
    
}

extension SMFMessageRecevingTableViewCell: NibLoadableView {
    
}
