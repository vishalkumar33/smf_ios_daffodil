//
//  SMFMessageViewController.swift
//  SMF
//
//  Created by Shivam Srivastava on 28/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

class SMFMessageViewController: SMFBaseViewController  {

    @IBOutlet weak var messageTableView: UITableView!
    
    var messageViewPresenter: MessageViewPresenter!
    
    var messageListArray:[Message] = [Message]()
    
    var userMessageList:[UserMessage] = [UserMessage]()
    
    var item = [1, 2, 3, 4, 5, 6,7,8,9,10]
    
    var skip = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageViewPresenter = MessageViewPresenter(delegate: self)
        self.navigationController?.isNavigationBarHidden = false
        setLeftNavBarButton()
        setNav(title: "Message")
        messageTableView.delegate = self
        messageTableView.dataSource = self
        messageTableView.rowHeight = UITableViewAutomaticDimension
        messageTableView.estimatedRowHeight = 84
        messageTableView.separatorStyle = .none
        messageTableView.layer.masksToBounds = true
        messageTableView.layer.borderColor = UIColor( red: 200/255, green: 200/255, blue: 200/255, alpha: 0.2 ).cgColor
        messageTableView.layer.borderWidth = 1.0
        
        getMessage()
        
        
    }
    
    func getMessage(){
        messageViewPresenter.getMessage(skip: skip, limit: 10)
    }
    
}

extension SMFMessageViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if userMessageList.count > 0 {
            numOfSections = 1
            messageTableView.backgroundView = nil
        }
        else{
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: messageTableView.bounds.size.width, height: messageTableView.bounds.size.height))
            noDataLabel.text          = "No message available"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            messageTableView.backgroundView  = noDataLabel
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userMessageList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = messageTableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SMFMessageTableViewCell
        cell.setCellUserMessage(message: userMessageList[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switchToMessageDetail(index: indexPath.row)
    }
    
    func switchToMessageDetail(index:Int){
        let vcObject = self.storyboard!.instantiateViewController(withIdentifier: "SMFMessageDetailViewController") as! SMFMessageDetailViewController
        vcObject.senderId = self.userMessageList[index].messageReceiverId
        vcObject.userMessage = self.userMessageList[index]
        self.navigationController?.pushViewController(vcObject, animated: true)
    }
    
}

extension SMFMessageViewController: SMFMessageViewDelegate {
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func updateTable(data: [Message], totalCount: Int) {
        messageListArray.append(contentsOf: data)
        messageTableView.reloadData()
    }
    
    func updateTable(data: [UserMessage]) {
        userMessageList = data
        messageTableView.separatorStyle = .none
        messageTableView.reloadData()
    }
}
