//
//  SMFSendingTableViewCell.swift
//  SMF
//
//  Created by Vishal on 20/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

class SMFSendingTableViewCell: UITableViewCell {

   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    func setMessageLabel(){
        
    }
    
    func setCell(size:CGSize, withText:String, profileImage:String){
        
        
        self.viewWithTag(1)?.removeFromSuperview()
        self.viewWithTag(2)?.removeFromSuperview()
        
        

        let screenWidth = UIScreen.main.bounds.width
        
        let cellHeight = size.height + 20
        
        let userImage = UIImageView()
        userImage.tag = 1
        let url = URL(string: profileImage)
        userImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "henryCavill"))
        let imageX = screenWidth - 70
        let imageY = ((cellHeight/2) - 20)
        userImage.frame = CGRect(x: imageX, y: imageY, width: 40, height: 40)
        userImage.layer.cornerRadius = 20
        userImage.clipsToBounds = true
        self.addSubview(userImage)
        
        let label = UILabel(frame: .zero)
        label.tag = 2
        label.font = UIFont(name: "Quicksand-Medium", size: 13)
        label.backgroundColor = UIColor.white
        label.numberOfLines = 0
        label.text = withText
        let labelX = imageX -  10 - size.width
        label.frame = CGRect(x: labelX, y: 10, width: size.width, height: size.height)
        self.addSubview(label)
        
        self.layoutIfNeeded()
    
    }

    
}

extension SMFSendingTableViewCell: NibLoadableView {
    
    
}
