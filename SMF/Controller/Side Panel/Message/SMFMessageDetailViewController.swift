//
//  SMFMessageDetailViewController.swift
//  SMF
//
//  Created by Vishal on 17/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit
import IQKeyboardManager
import GrowingTextView

class SMFMessageDetailViewController: SMFBaseViewController {
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var inputToolbar: UIToolbar!
    let textView = GrowingTextView()
    
    
    private var isVisibleKeyboard = true
    private var constant:CGFloat = 0.0
    
    var messageArray:[ChatMessage] = [ChatMessage]()
    
    var dummyText = ["I added a search bar to my table view, and as the user searches, I want the table view to scroll to the text that your typed", "How do I make the scroll view scroll automatically", "I am trying to flip", "This code flips it vertically but I could not flip it horizontally","The issue was not in piece of code, so my fix is in the question itself. It might still be useful for others", "Maybe an even more neat solution in some cases is to make an extension","Which can by called directly from the UILabel object like this", "The", "I've just put this in a playground and it works for me", "Vishal","I added a search bar to my table view, and as the user searches, I want the table view to scroll to the text that your typed","How do I make the scroll view scroll automatically", "I am trying to flip"]
    
    var messageDetailViewPresenter: SMFMessageDetailPresenter!
    
    var senderId:String?
    
    var isSender:Bool?
    
    var userMessage:UserMessage?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.messageDetailViewPresenter = SMFMessageDetailPresenter(delegate: self)
        constant = bottomConstraint.constant
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        addTextView()
        tblChat.separatorStyle = .none
        tblChat.dataSource = self
        tblChat.delegate = self
        tblChat.layer.borderWidth = 1
        tblChat.layer.borderColor = UIColor.lightGray.cgColor
        self.tblChat.register(SMFMessageRecevingTableViewCell.self)
        self.tblChat.register(SMFSendingTableViewCell.self)
        self.tblChat.separatorStyle = .none
        tableViewScrollToBottom(animated: true)
        self.title = userMessage?.messageReceiverName
        NotificationCenter.default.addObserver(self, selector: #selector(SMFMessageDetailViewController.messageReceived), name: NSNotification.Name(rawValue: "messageReceived"), object: nil)
        setLeftNavBar()
       

    }
    
    func setLeftNavBar(){
        let backBtn = UIButton(type: .custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 18, height: 18)
        backBtn.setImage(#imageLiteral(resourceName: "backIcon"), for: .normal)
        backBtn.addTarget(self, action: #selector(SMFMessageDetailViewController.back), for: UIControlEvents.touchUpInside)
        let back = UIBarButtonItem(customView: backBtn)
        self.navigationItem.setLeftBarButton(back, animated: true)
    }
    
    func back()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func addTextView(){
        
        textView.delegate = self
        textView.layer.cornerRadius = 4.0
        
        textView.maxLength = 200
        textView.maxHeight = 70
        textView.trimWhiteSpaceWhenEndEditing = true
        textView.placeHolder = "Type a message..."
        textView.placeHolderColor = UIColor(colorLiteralRed: 151/255, green: 151/255, blue: 151/255, alpha: 0.7)
        textView.placeHolderLeftMargin = 5.0
        textView.font = UIFont(name: "Quicksand-Medium", size: 12.2)
        
        inputToolbar.addSubview(textView)
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        button.titleLabel?.text = "Send"
        inputToolbar.addSubview(button)
        
        textView.translatesAutoresizingMaskIntoConstraints = false
        inputToolbar.translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["textView": textView]
        let hConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[textView]-8-|", options: [], metrics: nil, views: views)
        let vConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[textView]-8-|", options: [], metrics: nil, views: views)
        inputToolbar.addConstraints(hConstraints)
        inputToolbar.addConstraints(vConstraints)
        self.view.layoutIfNeeded()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillChangeFrame(_ notification: Notification) {
        let endFrame = ((notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        bottomConstraint.constant = view.bounds.height - endFrame.origin.y
        bottomConstraint.constant = constant
        self.view.layoutIfNeeded()
    }
    
   
    func messageReceived(notification: NSNotification){
        isSender = false
        let data = notification.userInfo?["data"] as! NSArray
        print("From detail \(data)")
        let chatMessage = ChatMessage(response: data.firstObject as! NSDictionary)
        messageArray.append(chatMessage)
        tblChat.reloadData()
        tableViewScrollToBottom(animated: true)
//        tableView.reloadData()
    }
    
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    func tapGestureHandler() {
        inputToolbar.endEditing(true)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        let text = textView.text
        if text != nil {
            let message = text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let userId = UserDefault.string(forKey: "userId")
            if senderId != nil  {
                MessageService.instance.sendMessage(message: message!, receiptId: userId!, senderId: senderId!)
            }
            isSender = true
            messageArray.append(ChatMessage(content: message!))

            
            tblChat.reloadData()
            tableViewScrollToBottom(animated: true)
        }
        textView.text = ""
    }
    
    func tableViewScrollToBottom(animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let numberOfSections = self.tblChat.numberOfSections
            let numberOfRows = self.tblChat.numberOfRows(inSection: numberOfSections-1)
            if numberOfRows > 0 {
                let indexPath = NSIndexPath(row: numberOfRows-1, section: numberOfSections-1)
                self.tblChat.scrollToRow(at: indexPath as IndexPath, at: UITableViewScrollPosition.bottom, animated: animated)
            }
        }
    }
    
    
}

extension SMFMessageDetailViewController: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.inputToolbar.layoutIfNeeded()
        }, completion: nil)
    }
    
    
}

extension SMFMessageDetailViewController: UITextViewDelegate {
    
}

extension SMFMessageDetailViewController: MessageDetailViewDelegate {
    
}


extension SMFMessageDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let content = messageArray[indexPath.row].content
        return messageDetailViewPresenter.getHeight(text:content!)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chatMessage = messageArray[indexPath.row]
        
        if chatMessage.author == "" {
            let cell:SMFSendingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SMFSendingTableViewCell") as! SMFSendingTableViewCell
            cell.selectionStyle = .none
            cell.setCell(size: TableViewHelper.getMessageSize(message: chatMessage.content!), withText: chatMessage.content!, profileImage: (userMessage?.messageSenderProfile)!)
            //cell.setCell(size: TableViewHelper.getMessageSize(message: chatMessage.content!), withText: chatMessage.content!)
            return cell
        }
        
//        if isSender! {
//            let cell:SMFSendingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SMFSendingTableViewCell") as! SMFSendingTableViecellwCell
//            cell.selectionStyle = .none
//            cell.setCell(size: TableViewHelper.getMessageSize(message: chatMessage.content!), withText: chatMessage.content!)
//            return cell
//        }
        let cell:SMFMessageRecevingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SMFMessageRecevingTableViewCell") as! SMFMessageRecevingTableViewCell
        cell.selectionStyle = .none
        cell.setCell(size: TableViewHelper.getMessageSize(message: chatMessage.content!), withText: chatMessage.content!, profileImage: (userMessage?.messageReceiverProfile)!)
       // cell.setCell(size: TableViewHelper.getMessageSize(message: chatMessage.content!), withText: chatMessage.content!)
        return cell
        
    }
    
}













