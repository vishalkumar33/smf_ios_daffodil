//
//  SMFMessageDetailPresenter.swift
//  SMF
//
//  Created by Vishal on 20/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


protocol MessageDetailViewDelegate: SMFCommonViewDelegate {
    
}

class SMFMessageDetailPresenter {
    
    private var messageDetailViewDelegate    : MessageDetailViewDelegate?
    
    init(delegate : MessageDetailViewDelegate){
        messageDetailViewDelegate = delegate
    }
    
    func getHeight(text:String)->CGFloat {
        let sizeOfText =  TableViewHelper.getMessageSize(message: text)
        if sizeOfText.height > 40 {
            return (sizeOfText.height + 20)
        }else{
            return 60
        }
    }
    
    
}
