//
//  MessageViewPresenter.swift
//  SMF
//
//  Created by Vishal on 17/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

protocol SMFMessageViewDelegate:SMFCommonViewDelegate{
    func updateTable(data:[Message], totalCount:Int)
    func updateTable(data:[UserMessage])
}

class MessageViewPresenter {
    
    private var messageViewDelegate    : SMFMessageViewDelegate?
    
    init(delegate : SMFMessageViewDelegate){
        messageViewDelegate = delegate
    }
    
    
    func getMessage(skip:Int, limit:Int){
        messageViewDelegate?.showLoader!()
        Service.getAllMessage(skip: skip, limit: limit, success: { (messageArray, count) in
            self.messageViewDelegate?.hideLoader!()
            self.initUserImage(messageArray: messageArray)
            
            print(count)
        }) { (error) in
            self.messageViewDelegate?.hideLoader!()
            print(error!)
        }
    }
    
    
    
    func initUserImage(messageArray:[Message]){
        var userMessage:[UserMessage] = [UserMessage]()
        var receiptArray:[String] = [String]()
        for message in messageArray {
            let recepName = "\((message.recipient?.profile.firstName)!) \((message.recipient?.profile.lastName)!)"
            let authName = "\((message.author?.profile.firstName)!) \((message.author?.profile.lastName)!)"
            receiptArray.append((message.recipient?.id)!)
            let obj = UserMessage(content: message.content!,
                                  senderId: (message.author?.id)!,
                                  senderName: authName,
                                  senderProfile: (message.author?.profile.profileImage)!,
                                  receiverId: (message.recipient?.id)!,
                                  receiverName: recepName,
                                  recevierProfile: (message.recipient?.profile.profileImage)!
            )
            userMessage.append(obj)
        }
        
        print(receiptArray.count)
        let receiptUniqueArray = receiptArray.unique
        print(receiptUniqueArray.count)
        var newmessageArray:[UserMessage] = [UserMessage]()
        
        for recipt in receiptUniqueArray {
            for message in userMessage {
                if message.messageReceiverId == recipt{
                    let obj = UserMessage(content: message.messageContent!, senderId: message.messageSenderId!, senderName: message.messageSenderName!, senderProfile: message.messageSenderProfile!, receiverId: message.messageReceiverId!, receiverName: message.messageReceiverName!, recevierProfile: message.messageReceiverProfile!)
                    newmessageArray.append(obj)
                    break
                }
            }
        }
        self.messageViewDelegate?.updateTable(data: newmessageArray)
        
    }
    
    
}


public extension Sequence {
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        var categories: [U: [Iterator.Element]] = [:]
        for element in self {
            let key = key(element)
            if case nil = categories[key]?.append(element) {
                categories[key] = [element]
            }
        }
        return categories
    }
    
    

}

extension Array where Element : Equatable {
    var unique: [Element] {
        var uniqueValues: [Element] = []
        forEach { item in
            if !uniqueValues.contains(item) {
                uniqueValues += [item]
            }
        }
        return uniqueValues
    }
}

