//
//  SMFMemberCollectionViewCell.swift
//  SMF
//
//  Created by Shivam Srivastava on 29/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

protocol SMFMemberCollectionViewCellDelegate {
    func geIndex(index:Int, button:UIButton)
}

class SMFMemberCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var memberImageView: UIImageView!
    
    @IBOutlet weak var memberNameLabel: UILabel!
    
    
    @IBOutlet weak var memberEmailLabel: UILabel!

    @IBOutlet weak var memberAddressLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    var delegate: SMFMemberCollectionViewCellDelegate?
    
    @IBAction func flwUnflwAction(_ sender: Any) {
        print("dewghdwdededdewdewdwwdwyyyy")
        self.delegate?.geIndex(index: (sender as AnyObject).tag, button: sender as! UIButton)
       
    }
    
    override func awakeFromNib() {
       
    }
    
    func resetCell(){
        followButton.tag = -1
    }
    
    func setCell(indexP: Int){
        resetCell()
        memberImageView.layer.shadowColor = UIColor.black.cgColor
        memberImageView.layer.shadowOpacity = 1
        memberImageView.layer.shadowOffset = CGSize.zero
        memberImageView.layer.shadowRadius = 10
        followButton.tag = indexP
        memberImageView.layer.shadowPath = UIBezierPath(rect: memberImageView.bounds).cgPath
        
    }
    

}
