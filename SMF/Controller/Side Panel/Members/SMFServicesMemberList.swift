//
//  SMFServicesMemberList.swift
//  SMF
//
//  Created by Jenkins on 1/24/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


import UIKit

class SMFServicesMemberList {
    
    
    /**
     This method is used for perform Login With Valid Input(Credentials)
     
     - parameter inputData: Contains info for Login
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performMemberPostWithValidInput(inputData: SMFMemberListRequestModel, memberListDelegate:ResponseCallback ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForLogin()
        SMFMemberListApiRequest().makeAPIRequest(reqFromData: inputData, errorResolver: errorResolver, responseCallback: memberListDelegate)
    }
    
    func performMemberPutWithValidInput(inputData: SMFMemberListRequestModel, memberListDelegate:ResponseCallback ) ->Void {
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForLogin()
        SMFMemberListApiRequest().makeAPIRequestForFollow(reqFromData: inputData, errorResolver: errorResolver, responseCallback: memberListDelegate)
    }
    

    
    
       
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    //TODO:- String should be mapped from localizable string file.
    private func registerErrorForLogin() ->ErrorResolver{
        let errorResolver:ErrorResolver = ErrorResolver()
           errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_400, message: "Some mandatory field is Missing.")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_401, message: "User not Logged In")
        errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1009, message: "Please check internet connection")
        //   errorResolver.registerErrorCode(errorCode: ErrorCodes.Error_1011, message: "Email Id is already register")
        return errorResolver
    }
    
    
}
