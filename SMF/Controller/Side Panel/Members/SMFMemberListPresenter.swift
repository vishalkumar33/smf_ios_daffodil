//
//  SMFMemberListPresenter.swift
//  SMF
//
//  Created by Jenkins on 1/24/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//


import Foundation

import UIKit

protocol SMFMemberListIDelegate:SMFCommonViewDelegate{
    func updateTable(newsFeedArray:[MemberInfo])
   // func userDetail(userProfile: MemberInfo)
    func AboutMe(newsFeedArray:[UserInfo])
    
    func aboutMe(userInfo:UserInfo)


}



class SMFMemberListPresenter: ResponseCallback {
    
    private var MemberListDelegate   : SMFMemberListIDelegate?
    private var serviceBuisnessMemberList = SMFServicesMemberList()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : SMFMemberListIDelegate){
        MemberListDelegate = delegate
    }
    
    
    // MARK: ServiceManagerDelegate Methods
    
    /**
     This method handles success response of Login API
     
     - parameter responseObject: Response object that contains success response
     */
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        self.MemberListDelegate?.hideLoader!()
        let sucessObject:SMFMemberListResponse = responseObject as! SMFMemberListResponse
        print(sucessObject.follow.message)
    }
    
    /**
     This Method handle error response of Member API
     
     - parameter error: NSError Object that contains error information
     */
    
    func servicesManagerError(error : String){
        MemberListDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        //This statement is used for hiding loader
        self.MemberListDelegate?.hideLoader!()
    }
    
    
    
    func getNewsFeed(){
        MemberListDelegate?.showLoader!()
        Service.getMemberList(success: { (response) in
            print(response)
            self.MemberListDelegate?.hideLoader!()
            self.parseMemberList(feeds: response )
            
        }) { (error) in
            self.MemberListDelegate?.hideLoader!()
            print(error!)
            }
    }
    
    
    func whoAmI(){
        MemberListDelegate?.showLoader!()
        Service.whoAMI(success: { (userInfo) in
            print(userInfo.connections.favorites.count)
            print(userInfo.connections.follwing.count)
            self.MemberListDelegate?.aboutMe(userInfo: userInfo)
        }) { (error) in
            print(error!)
        }
    }
    
    
    
    
    func getWhoAMI(){
        MemberListDelegate?.showLoader!()
        Service.getWhoAMI(success: { (response) in
            print(response)
            self.MemberListDelegate?.hideLoader!()
          
            self.parseAboutList(feeds: response )

        }) { (error) in
            self.MemberListDelegate?.hideLoader!()
            print(error!)
        }
    }
    
    func parseAboutList(feeds:NSDictionary?){
        var feedArray:[UserInfo] = [UserInfo]()
        if (feeds?.count)! > 0 {
            
            //print("\(result)")
            if let resultDict = feeds {
                let obj = UserInfo(resultDict as! Dictionary<String, AnyObject>)
                feedArray.append(obj!)
            }
            
            }
            print(feedArray)
        self.MemberListDelegate?.AboutMe(newsFeedArray: feedArray)
    }
    
    func getSearchMemberFeed(textField:String){
        MemberListDelegate?.showLoader!()
        Service.getMemberSearchList(searchText: textField, success: { (response) in
            print(response)
            self.MemberListDelegate?.hideLoader!()
            self.parseMemberList(feeds: response )
        }) { (error) in
            self.MemberListDelegate?.hideLoader!()
            print(error!)
        }
        
    }

    func parseMemberList(feeds:NSArray){
        var feedArray:[MemberInfo] = [MemberInfo]()
        if feeds.count > 0 {
            let result = feeds
            print("\(result)")
            for type in result {
                if let dict = type as? NSDictionary {
                    let obj  = MemberInfo(dict as! Dictionary<String, AnyObject>)
                    feedArray.append(obj!)

                }

            }
            print(feedArray)
        }
        self.MemberListDelegate?.updateTable(newsFeedArray: feedArray)
    }
    
    func followUpButtonClicked(action:String, followedUserId:String) ->Void
    {
        
            //Showing Loader
            MemberListDelegate?.showLoader!()
           // self.serviceBuisnessMemberList.performMemberPutWithValidInput(inputData: SMFMemberListRequestModel.Builder().setAction(action: action).setFollowedUserId(followedUserId: followedUserId).addRequestHeader(key:"Content-Type" , value: "application/json").build(), memberListDelegate: self)
        let followDic:NSMutableDictionary = NSMutableDictionary()
        followDic["followedUserId"] = followedUserId
        followDic["action"] = action
        
        print(followDic)
        
        Service.followUser(data: followDic, success: { (response) in
            print(response)
        }) { (error) in
            print(error!)
        }
        
        
        
        }
    }



    

    
    
    

