//
//  SMFMemberViewController.swift
//  SMF
//
//  Created by Shivam Srivastava on 28/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit
import SDWebImage
class SMFMemberViewController: SMFBaseViewController,SMFMemberListIDelegate,UITextFieldDelegate,SMFWhoAmIDelegate, SMFMemberCollectionViewCellDelegate {

    //Outlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var lineView: UIView!
    
    @IBOutlet weak var collectionViewTopConstrain: NSLayoutConstraint!
    @IBOutlet weak var memberCollectionView: UICollectionView!
    
    //Variables
    var filtered                  :[MemberInfo] = [MemberInfo]()
    var memberListPresenter      :      SMFMemberListPresenter!
    var feedArray                :      [MemberInfo]    =       [MemberInfo]()
    var aboutArray                :      [UserInfo]    =       [UserInfo]()

    var memberData: MemberInfo?
    var selectedMember: MemberInfo?
    var whoAmIPresenter       : WhoAmIPresenter?
    
    var connections:UserConnection?
    
    var refreshControl: UIRefreshControl!
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        self.whoAmIPresenter = WhoAmIPresenter(delegate: self)
        self.memberListPresenter = SMFMemberListPresenter(delegate: self)
        //memberListPresenter.getWhoAMI()
        memberListPresenter.whoAmI()
        
        self.navigationController?.isNavigationBarHidden = false
         setNav(title: "MEMBERS")
        memberCollectionView.delegate = self
        memberCollectionView.dataSource = self
        memberCollectionView.showsVerticalScrollIndicator = false
       // searchTextField.addTarget(self, action: #selector(SMFMemberViewController.textFieldDidChange), for: UIControlEvents.editingChanged)
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        memberCollectionView.addSubview(refreshControl)

    }
    
    func refresh() {
//        memberListPresenter.whoAmI()
//        self.refreshControl.endRefreshing()
//        feedArray.removeAll(keepingCapacity: false)
        
    }
    
    func userAvailable(isAvail:Bool)
    {
        
    }
    func userDetail(userProfile: UserInfo)
    {
        
    }
    
    //called when the textField value is changed
    func textFieldDidChange(textField: UITextField) {
       memberListPresenter.getNewsFeed()
        memberCollectionView.reloadData()
    }
    
    //celled when textField End Editing
    func textFieldDidEndEditing(_ textField: UITextField) {
      //  memberListPresenter.getSearchMemberFeed(textField: textField.text!)
      //  memberCollectionView.reloadData()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchTextField.becomeFirstResponder()
        searchTextField.returnKeyType = .search
       
    }
    

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        memberListPresenter.getSearchMemberFeed(textField: textField.text!)
        memberCollectionView.reloadData()
        if(searchTextField.text?.characters.count == 0)
        {
            memberListPresenter.getNewsFeed()
            memberCollectionView.reloadData()
        }
        return true
    }
    
    //set the nav Right Bar Button
    
    //Show the Loader
    func showLoader() {
        self.view.showLoader()
    }
    
    //Hide the Loader
    func hideLoader() {
        self.view.hideLoader()
    }
    
    //Show teh Error Alert
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    func AboutMe(newsFeedArray: [UserInfo]) {
        self.aboutArray = newsFeedArray
        
     }
    
    func aboutMe(userInfo: UserInfo) {
        self.connections = userInfo.connections
        memberListPresenter.getNewsFeed()
    }
    
    func updateTable(newsFeedArray: [MemberInfo]) {
        self.feedArray = newsFeedArray
        memberCollectionView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }
}

extension SMFMemberViewController: UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let userProfileViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "SMFUserProfileViewController") as! SMFUserProfileViewController
        userProfileViewController.memberData = feedArray[indexPath.row]
        self.navigationController?.pushViewController(userProfileViewController, animated: true)
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = memberCollectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! SMFMemberCollectionViewCell
        memberData =  feedArray[indexPath.row]

        cell.layer.borderColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.2 ).cgColor
        cell.layer.borderWidth = 1.0
        cell.memberImageView.layer.cornerRadius = 39
        
        cell.delegate = self
        
        cell.followButton.layer.cornerRadius = 5
        cell.followButton.layer.borderWidth = 1
        cell.setCell(indexP: indexPath.row)
        
        if (connections?.favorites.count)! > 0 {
            if (connections?.favorites.contains(memberData?.id as Any))! {
                cell.followButton.layer.borderColor = UIColor.init(red: 97/255, green: 189/255, blue: 214/255, alpha: 1.0).cgColor
                cell.followButton.backgroundColor = UIColor(colorLiteralRed: 97/255, green: 189/255, blue: 214/255, alpha: 0.7)
                cell.followButton.isSelected = true
            }else{
                 cell.followButton.layer.borderColor = UIColor.init(red: 97/255, green: 189/255, blue: 214/255, alpha: 1.0).cgColor
                cell.followButton.backgroundColor = UIColor.white
                cell.followButton.isSelected = false
            }
        }
       
        cell.memberNameLabel.text = "\((memberData?.profile?.firstName.uppercased())!) \((memberData?.profile?.lastName.uppercased())!)"
        cell.memberEmailLabel.text = "@\((memberData?.profile?.handle)!)"
        cell.memberAddressLabel.text = "\((memberData?.profile?.city)!), \((memberData?.profile?.state)!)"
   
        cell.memberImageView.sd_setImage(with: NSURL(string: (memberData?.profile?.profileImage)!) as! URL, placeholderImage: #imageLiteral(resourceName: "henryCavill"),options:.refreshCached,completed: {(image:UIImage?,error:Error?,cacheType:SDImageCacheType,imageURL:URL?) in
            cell.memberImageView.layer.shadowOffset = CGSize.init(width: -5, height: -5)
            cell.memberImageView.layer.shadowColor = UIColor.black.cgColor
            cell.memberImageView.layer.shadowRadius = 5
            cell.memberImageView.layer.shadowOpacity = 1
        
             cell.memberImageView.clipsToBounds = true
            
        })
        
        
        return cell
    }
    
    func geIndex(index: Int, button: UIButton) {
        let member = feedArray[index]
        folUnflwbtn(sender: button, member: member)
    }
    
    
    
    //called when follow and Following button tapped
    func folUnflwbtn(sender :UIButton, member:MemberInfo)
    {
        // Save Data
        
        sender.isSelected  = !sender.isSelected;
        if (sender.isSelected)
        {
           memberListPresenter.followUpButtonClicked(action: "follow", followedUserId: (member.id))
            self.whoAmIPresenter?.whoAmI()
            
            sender.backgroundColor = UIColor(colorLiteralRed: 97/255, green: 189/255, blue: 214/255, alpha: 0.7)
        }
        else
        {
          memberListPresenter.followUpButtonClicked(action: "unfollow", followedUserId: (member.id))
            self.whoAmIPresenter?.whoAmI()
            sender.backgroundColor = UIColor.white
        }
    }
    
    //MARK:- returns the number of section
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if feedArray.count > 0 {
            memberCollectionView.backgroundView?.isHidden = true
            return 1
        }
        memberCollectionView.backgroundView?.isHidden = false
        CollectionViewHelper.EmptyMessage(message: "You don't have Any member", collectionView: memberCollectionView)
        return 0
    }
    

}
