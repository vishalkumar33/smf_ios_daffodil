//
//  SMFSidePanelTableViewCell.swift
//  
//
//  Created by Shivam Srivastava on 26/12/16.
//
//

import UIKit

class SMFSidePanelTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewCell: UIImageView!
    
    @IBOutlet weak var sidePanelItems: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
