//
//  SMFHistoryTextTableViewCell.swift
//  SMF
//
//  Created by Jenkins on 12/30/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

protocol SMFHistoryTextTableViewCellDelegate {
    func popUpButtonTapped(_ sender: AnyObject)
}

class SMFHistoryTextTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var btnPopup: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    
    
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblShareTitle: UILabel!
    @IBOutlet weak var btnUrl: UIButton!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var btnLocation: UIButton!
   
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var lblNoOfLikes: UILabel!
    
    @IBOutlet weak var commentPostView: UIView!
    
    @IBOutlet weak var TextViewcomment: UITextView!
    
    @IBOutlet weak var btnPost: UIButton!
    
    var deleagate: SMFHistoryTextTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.mainView.layer.borderWidth = 1.0
        self.mainView.layer.borderColor = UIColor.appMainViewGrayColor.cgColor
        self.shareView.layer.borderWidth = 0.5
        self.shareView.layer.borderColor = UIColor.appbtnBorderColor.cgColor
        self.btnPost.layer.borderWidth = 1.0
        self.btnPost.layer.borderColor = UIColor.appbtnBorderColor.cgColor
        self.commentPostView.layer.borderWidth = 0.5
        self.commentPostView.layer.borderColor = UIColor.appbtnBorderColor.cgColor
    }
    
    @IBAction func showPopUp(_ sender: Any) {
        self.deleagate.popUpButtonTapped(sender as AnyObject)
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension SMFHistoryTextTableViewCell: NibLoadableView {
    
}
