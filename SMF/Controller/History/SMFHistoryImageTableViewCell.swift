//
//  SMFHistoryImageTableViewCell.swift
//  SMF
//
//  Created by Jenkins on 12/30/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit
protocol SMFHistoryImageTableViewCellDelegate{
    func hidePost(index:Int)
    func reportPost(index:Int)
    func flamePopUpLoadImage(index:Int)
    func pushCommentPage(index:Int)
    func sharePostPush(index :Int)
    func blockUser()
}
class SMFHistoryImageTableViewCell: UITableViewCell,UITextFieldDelegate {
    var imageDelegate:SMFHistoryImageTableViewCellDelegate!
    var menuOptionNameArray : [String] = ["Report", "Hide Post", "Block@john"]
    var menuOptionImageNameArray = ["reportIcon", "hidePostIcon", "blockPostIcon" ]
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var bottonViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentTextView: MyCustomTextField!
    @IBOutlet weak var btnPopup: UIButton!
    
    @IBOutlet weak var commentsCount: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var postbtn: MyCustomButton!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var btnLink: UIButton!
    
    @IBOutlet weak var flamePopUp: UIButton!
    
    @IBOutlet weak var sharePostbtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgUser.layer.masksToBounds = false
        imgUser.layer.cornerRadius = imgUser.frame.height/2
        imgUser.clipsToBounds = true
        self.mainView.layer.borderWidth = 1.0
        self.mainView.layer.borderColor = UIColor.appMainViewGrayColor.cgColor
        commentTextView.delegate = self
        changeToWeChatStyle()

        
    }
    @IBAction func postAction(_ sender: Any) {
        
        
    }
    func resetCell(){
        self.btnPopup.tag = -1
        self.flamePopUp.tag = -1
        self.commentTextView.tag = -1
        self.sharePostbtn.tag = -1
    }
    @IBAction func sharePostAction(_ sender: Any) {
        let index = sharePostbtn.tag
        self.imageDelegate.sharePostPush(index:index)

    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let index = commentTextView.tag
        self.imageDelegate.pushCommentPage(index:index)
    }
        
    
    @IBAction func popUpAction(_ sender: Any) {
        showPopUp(sender as AnyObject)
    }
    
    func showPopUp(_ sender: AnyObject) {
        let index = sender.tag
        FTPopOverMenu.showForSender(sender: sender as! UIView, with: menuOptionNameArray, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            print(selectedIndex)
            switch selectedIndex {
            case 0:
                self.imageDelegate.reportPost(index:index!)
                break
            case 1:
                self.imageDelegate.hidePost(index: index!)
                break
                
                
            case 2:
                self.imageDelegate.blockUser()
            default:
                break
            }
            
        }) {
            
        }
    }

    
    func setCell1(post:Post, indexPath:Int){
        lblUserName.text = "\((post.author.profile.firstName.capitalized)) \((post.author.profile.lastName.capitalized))"
        if let imgURL = post.author.profile?.profileImage  {
            imgUser.sd_setImage(with: NSURL(string: imgURL) as! URL , placeholderImage: #imageLiteral(resourceName: "messagesProfile"))
            
        }
        lblContent.text = post.content
        print(post.imagePath)
        if post.imagePath != ""  {
            imgPost.sd_setImage(with: NSURL(string: post.imagePath) as! URL , placeholderImage: #imageLiteral(resourceName: "messagesProfile"))
        }
       
        lblTime.text = TableViewHelper.DateToString(date: TableViewHelper.stringToDate(date: post.createdAt))
        self.btnPopup.tag = indexPath
        self.flamePopUp.tag = indexPath
        self.commentTextView.tag = indexPath
        self.sharePostbtn.tag = indexPath
    }

    @IBAction func flamePopUpAction(_ sender: Any) {
        let index = flamePopUp.tag
        self.imageDelegate.flamePopUpLoadImage(index: index)

    }
    func changeToWeChatStyle() {
        let config = FTConfiguration.shared
        config.textColor = UIColor.black
        config.backgoundTintColor = UIColor.white
        config.borderColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.25)
        config.menuWidth = 168
        config.borderWidth = 1.0
        config.menuSeparatorColor = UIColor.white
        config.textFont = UIFont.systemFont(ofSize: 11)
        config.menuRowHeight = 33
        config.cornerRadius = 10
        
        
    }
}

extension SMFHistoryImageTableViewCell: NibLoadableView {
    
}
