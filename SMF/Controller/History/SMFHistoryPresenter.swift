//
//  SMFHistoryPresenter.swift
//  SMF
//
//  Created by Jenkins on 2/3/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//


import Foundation

protocol SMFHistoryViewDelegate:SMFCommonViewDelegate{
    func updateTable(newsFeedArray:[Post])
}

class SMFHistoryPresenter: ResponseCallback {
    
    private var historyViewDelegate    : SMFHistoryViewDelegate?
    private var homeService = HomeService()
    
    init(delegate : SMFHistoryViewDelegate){
        historyViewDelegate = delegate
    }
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        self.historyViewDelegate?.hideLoader!()
    }
    
    
    func servicesManagerError(error : String){
        historyViewDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        self.historyViewDelegate?.hideLoader!()
    }
    
    func getHistoryNewsFeed(){
        historyViewDelegate?.showLoader!()
        let authorId = UserDefault.value(forKey: "userId") as! String
        Service.getHistoryDetail(author_id: authorId, success:{ (response) in
            print(response)
            self.historyViewDelegate?.hideLoader!()
            self.parseHistoryNewsFeed(response: response )
        }) { (error) in
            self.historyViewDelegate?.hideLoader!()
            print(error!)
        }
    }
    
    func parseHistoryNewsFeed(response:NSDictionary){
        
        let data:NSDictionary  = response 
        let count:Int = data["totalfeedscount"] as! Int
        let feeds = data["result"] as! NSArray
        
        var feedArray:[Post] = [Post]()
        if feeds.count > 0 {
            let result = feeds
            print("\(result)")
            for type in result {
                if let dict = type as? NSDictionary {
                    let obj  = Post(response: dict)
                    feedArray.append(obj)
                }
            }
        }
        self.historyViewDelegate?.updateTable(newsFeedArray: feedArray)
    }
    
    
    
       
    func showPopUp(_ sender: AnyObject){
        
    }
    
}
