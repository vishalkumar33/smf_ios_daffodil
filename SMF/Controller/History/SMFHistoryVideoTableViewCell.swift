//
//  SMFHistoryVideoTableViewCell.swift
//  SMF
//
//  Created by Jenkins on 12/30/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

protocol SMFHistoryVideoTableViewCellDelegate {
    func hidePost(index:Int)
    func reportPost(index:Int)
   // func flamePopUpLoad1(index:Int)
    func pushCommentPage(index:Int)
    func sharePostPush(index :Int)
    func blockUser()
}
class SMFHistoryVideoTableViewCell: UITableViewCell,UITextFieldDelegate {
    var menuOptionNameArray : [String] = ["Report", "Hide Post", "Block@john"]
    var menuOptionImageNameArray = ["reportIcon", "hidePostIcon", "blockPostIcon" ]
    
    var videoDelegate: SMFHistoryVideoTableViewCellDelegate!
    @IBOutlet weak var btnPopup: UIButton!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var imgVideoThumbnail: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    
    @IBOutlet weak var sharePostbtn: UIButton!
    @IBOutlet weak var commentTextView: UITextField!
    @IBOutlet weak var flamePopUp: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.layer.borderWidth = 1.0
        self.mainView.layer.borderColor = UIColor.appMainViewGrayColor.cgColor
        commentTextView.delegate = self
        changeToWeChatStyle()
        
    }
    func resetCell(){
        self.btnPopup.tag = -1
        self.flamePopUp.tag = -1
        self.commentTextView.tag = -1
        self.sharePostbtn.tag = -1
    }
    @IBAction func flamePopUpAction(_ sender: Any) {
    }
    
    
    func setCell1(post:Post, indexPath:Int){
        lblUserName.text = "\((post.author.profile.firstName.capitalized)) \((post.author.profile.lastName.capitalized))"
        if let imgURL = post.author.profile?.profileImage  {
            profileImageView.sd_setImage(with: NSURL(string: imgURL) as! URL , placeholderImage: #imageLiteral(resourceName: "messagesProfile"))
            
        }
        lblContent.text = post.content
        print(post.videoPath)
        if  post.videoPath != "" {
            imgVideoThumbnail.sd_setImage(with: NSURL(string: post.videoPath) as! URL , placeholderImage: #imageLiteral(resourceName: "messagesProfile"))
        }
        //   lblUserName.text = post.author.profile.firstName.uppercased()
        lblTime.text = TableViewHelper.DateToString(date: TableViewHelper.stringToDate(date: post.createdAt))

        self.btnPopup.tag = indexPath
        self.flamePopUp.tag = indexPath
        self.commentTextView.tag = indexPath
        self.sharePostbtn.tag = indexPath
    }
    
    
    @IBAction func showPopUp(_ sender: Any) {
        showPopUp1(sender as AnyObject)
    }
    
    
    func showPopUp1(_ sender: AnyObject) {
        let index = sender.tag
        FTPopOverMenu.showForSender(sender: sender as! UIView, with: menuOptionNameArray, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            print(selectedIndex)
            switch selectedIndex {
            case 0:
                self.videoDelegate.reportPost(index:index!)
                break
            case 1:
                self.videoDelegate.hidePost(index: index!)
                break
            case 2:
                self.videoDelegate.blockUser()
            default:
                break
            }
            
        }) {
            
        }
    }
    

    func changeToWeChatStyle() {
        let config = FTConfiguration.shared
        config.textColor = UIColor.black
        config.backgoundTintColor = UIColor.white
        config.borderColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.25)
        config.menuWidth = 168
        config.borderWidth = 1.0
        config.menuSeparatorColor = UIColor.white
        config.textFont = UIFont.systemFont(ofSize: 11)
        config.menuRowHeight = 33
        config.cornerRadius = 10
        
        
    }



}
extension SMFHistoryVideoTableViewCell: NibLoadableView {
    
}
