//
//  SMFHistoryViewController.swift
//  SMF
//
//  Created by Jenkins on 12/29/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit

class SMFHistoryViewController: SMFBaseViewController{
    @IBOutlet weak var tableView: UITableView!
    
    var historyPresenter            :      SMFHistoryPresenter!
    var feedArray                :      [Post]                          =       [Post]()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.historyPresenter = SMFHistoryPresenter(delegate: self)
        historyPresenter.getHistoryNewsFeed()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        tableView.estimatedRowHeight = 26
        setNav(title: "HISTORY")
        setLeftNavBarButton()
        self.tableView.register(SMFHistoryTextTableViewCell.self)
        self.tableView.register(SMFHistoryImageTableViewCell.self)
        self.tableView.register(SMFHistoryVideoTableViewCell.self)
        self.tableView.register(SMFTextTableViewCell.self)
    }
   
  
    override func viewWillAppear(_ animated: Bool) {
      
    }
      override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}



extension SMFHistoryViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedArray.count
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) ->
        CGFloat {
        return UITableViewAutomaticDimension
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let postObj = feedArray[indexPath.row]
        if postObj.videoPath != "" {
            return UITableViewCell()
        }else if postObj.imagePath != "" {
            return UITableViewCell()
        }else{
            if postObj.content != "" {
                let cell = tableView.dequeueReusableCell(forIndexPath: indexPath as NSIndexPath) as SMFTextTableViewCell
                cell.setCell(post: postObj, indexPath: indexPath.row)
                cell.selectionStyle = .none
                cell.isUserInteractionEnabled = false
                cell.contentView.removeFromSuperview()
                return cell
            }else{
                return UITableViewCell()
            }
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if feedArray.count > 0 {
            tableView.backgroundView?.isHidden = true
            return 1
        }
        tableView.backgroundView?.isHidden = false
        TableViewHelper.EmptyMessage(message:"You don't have any friends" , tableView: tableView)
        
        return 0
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.viewWithTag(kSeparatorId) == nil //add separator only once
        {
            let separatorView = UIView(frame: CGRect(x: 0, y: cell.frame.height - kSeparatorHeight, width: cell.frame.width, height: kSeparatorHeight))
            separatorView.tag = kSeparatorId
            separatorView.backgroundColor = UIColor.white
            separatorView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            cell.addSubview(separatorView)
        }

    }
   
}
extension SMFHistoryViewController: SMFHistoryViewDelegate, SMFHistoryTextTableViewCellDelegate {
    
    func showLoader() {
        self.view.showLoader()
    }
    
    func hideLoader() {
        self.view.hideLoader()
    }
    
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func popUpButtonTapped(_ sender: AnyObject) {
        historyPresenter.showPopUp(sender)
    }
 
    func updateTable(newsFeedArray: [Post]) {
        self.feedArray = newsFeedArray
        tableView.reloadData()
    }
}






