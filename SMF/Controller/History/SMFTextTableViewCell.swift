//
//  SMFTextTableViewCell.swift
//  SMF
//
//  Created by Shivam Srivastava on 24/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit

protocol HidePostDelegate{
    func hidePost(index:Int)
    func reportPost(index:Int)
    func flamePopUpLoad(index:Int)
    func pushCommentPage(index:Int)
    func sharePostPush(index :Int)
    func blockUser()
}
class SMFTextTableViewCell: UITableViewCell,UITextFieldDelegate {
    var count = 0
    var flag = 0
    @IBOutlet weak var countLabel: UILabel!
    var menuOptionNameArray : [String] = ["Report", "Hide Post", "Block@john"]
    var menuOptionImageNameArray = ["reportIcon", "hidePostIcon", "blockPostIcon" ]
    var hideDelegate :HidePostDelegate!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var btnPopup: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
  
    @IBOutlet weak var sharePostbtn: UIButton!
    @IBOutlet weak var UserImageView: UIImageView!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnShowMore: UIButton!
    @IBOutlet weak var flamePopUp: UIButton!
    var likeObj :LikePopUp!
    @IBOutlet weak var commentTextField: MyCustomTextField!
    @IBOutlet weak var sharePostIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commentTextField.delegate = self
        UserImageView.layer.masksToBounds = false
        UserImageView.layer.cornerRadius = UserImageView.frame.height/2
        UserImageView.clipsToBounds = true
        self.mainView.layer.borderWidth = 1.0
        self.mainView.layer.borderColor = UIColor.appMainViewGrayColor.cgColor
        textLengthCheck()
        changeToWeChatStyle()
    }
    
    @IBAction func sharePostAction(_ sender: Any) {
        let index = sharePostbtn.tag
        if let _ = self.hideDelegate
        {
        self.hideDelegate.sharePostPush(index:index)
        }
    }
    
    @IBAction func showMoreAction(_ sender: UIButton){
        if count == 0{
            count += 1
            flag = 1
            detailLabel.numberOfLines = 0
            sender.setTitle("Show less", for: .normal)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tableReload"), object: nil)
        }else {
            count -= 1
            flag = 1
            detailLabel.numberOfLines = 3
            sender.setTitle("Show More", for: .normal)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tableReload"), object: nil)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let index = commentTextField.tag
        self.hideDelegate.pushCommentPage(index:index)
    }
    
    func resetCell(){
        self.btnPopup.tag = -1
        self.flamePopUp.tag = -1
        self.commentTextField.tag = -1
        self.sharePostbtn.tag = -1
    }
    
    func setCell(post:Post, indexPath:Int){
        resetCell()
            lblUserName.text = "\((post.author.profile.firstName.capitalized)) \((post.author.profile.lastName.capitalized))"
            if let imgURL = post.author.profile?.profileImage  {
                UserImageView.sd_setImage(with: NSURL(string: imgURL) as! URL , placeholderImage: #imageLiteral(resourceName: "messagesProfile"))
                
            }
            detailLabel.text = post.content
            //   lblUserName.text = post.author.profile.firstName.uppercased()
            lblDateTime.text = TableViewHelper.DateToString(date: TableViewHelper.stringToDate(date: post.createdAt))
         btnComment.setTitle("| \(post.commentCount) Comments" , for: UIControlState.normal)
            textLengthCheck()
        self.btnPopup.tag = indexPath
        self.flamePopUp.tag = indexPath
        self.commentTextField.tag = indexPath
        self.sharePostbtn.tag = indexPath
        
    }
    
    
    @IBAction func sharePost(_ sender: Any) {
        let index = sharePostbtn.tag
        self.hideDelegate.sharePostPush(index:index)
      }
    
    
    @IBAction func flamePopUp(_ sender: Any) {
         let index = flamePopUp.tag
        
        //self.likeObj.isHidden = false
        self.hideDelegate.flamePopUpLoad(index: index)
    }
    
    
    func textLengthCheck(){
        print("character = \(detailLabel.text?.characters.count)")
       if flag != 1 {
        if (detailLabel.text?.characters.count)! >= 160 {
            detailLabel.numberOfLines = 3
            btnShowMore.isHidden = false
            flag = 0
        }else{
            btnShowMore.isHidden = true
        }
    }
    }
    
    @IBAction func showPopUpAction(_ sender: Any) {
        showPopUp(sender as AnyObject)
    }
    
    func showPopUp(_ sender: AnyObject) {
        let index = sender.tag
        FTPopOverMenu.showForSender(sender: sender as! UIView, with: menuOptionNameArray, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            print(selectedIndex)
            switch selectedIndex {
            case 0:
                self.hideDelegate.reportPost(index:index!)
                break
            case 1:
                self.hideDelegate.hidePost(index: index!)
                break
                
                
            case 2:
                self.hideDelegate.blockUser()
            default:
                break
            }

        }) {
        }
    }
    
    
    func changeToWeChatStyle() {
        let config = FTConfiguration.shared
        config.textColor = UIColor.black
        config.backgoundTintColor = UIColor.white
        config.borderColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.25)
        config.menuWidth = 168
        config.borderWidth = 1.0
        config.menuSeparatorColor = UIColor.white
        config.textFont = UIFont.systemFont(ofSize: 11)
        config.menuRowHeight = 33
        config.cornerRadius = 10
        
        
    }

}
extension SMFTextTableViewCell: NibLoadableView {
    
}

