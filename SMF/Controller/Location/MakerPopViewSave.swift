//
//  MakerPopViewSave.swift
//  SMF
//
//  Created by Jenkins on 2/14/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
protocol SaveLocationDelegate{
    func saveTappedLocation(sender:UIButton,name:String)
}
class MakerPopViewSave: UIView {
    
    
    
    @IBOutlet weak var locationLAbel: UILabel!
    
    @IBOutlet weak var savebtn: UIButton!
    @IBOutlet weak var dropPinLabel: UILabel!
    @IBOutlet weak var dropAddressLabel: UILabel!
    var delegate:SaveLocationDelegate!
    
    @IBOutlet weak var nameofLocationTextField: UITextField!
    override func awakeFromNib() {
       
    }
    
    @IBAction func saveAction(_ sender: Any) {
        self.delegate?.saveTappedLocation(sender: savebtn,name: locationLAbel.text!)
        removeFromSuperview()
    }
    
}
