//
//  LocationPresenter.swift
//  SMF
//
//  Created by Jenkins on 2/13/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//





import Foundation
import UIKit

protocol LocationDelegate:SMFCommonViewDelegate{
    func updateLocation(locationName:[SavedLocation])
}

class LocationPresenter: ResponseCallback {
    
    private var locationDelegate    : LocationDelegate?
    private var serviceBuisnessLocation = ServiceLocation()
    
    // Initialize loginView Delegate with View Controller Delegate Object
    init(delegate : LocationDelegate){
        locationDelegate = delegate
    }
    
    
    // MARK: ServiceManagerDelegate Methods
    
    /**
     This method handles success response of Login API
     
     - parameter responseObject: Response object that contains success response
     */
    
    func servicesManagerSuccessResponse(responseObject : AnyObject){
        locationDelegate?.hideLoader!()
        if responseObject is LocationResponse {
            let obj = responseObject as! LocationResponse
            locationDelegate?.updateLocation(locationName: (obj.userProfile?.savedLocation)!)
            let locations:[SavedLocation] = (obj.userProfile?.savedLocation)!
            
        }
        //This statement is used for hiding loader
        // self.ForgetPasswordViewDelegate?.hideLoader!()
        // self.ForgetPasswordViewDelegate?.switchToHome()
    }
    
    /**
     This Method handle error response of Login API
     
     - parameter error: NSError Object that contains error information
     */
    
    func servicesManagerError(error : String){
        locationDelegate?.hideLoader!()
        locationDelegate?.showErrorAlert!(alertTitle: "Error", alertMessage: error)
        //This statement is used for hiding loader
        // self.ForgetPasswordViewDelegate?.hideLoader!()
    }
    
    
    
    // MARK: Presenter Local methods
    
    /**
     This method calls Login API if emailId and Password both are Valid
     
     - parameter emailId:  User EmailId
     - parameter password: User Password
     */
    
    func addLocationClickedWithPost(lat:Double, long:Double,name:String,location:String) ->Void
    {
       
        locationDelegate?.showLoader!()
        self.serviceBuisnessLocation.performPostWithValidInput(inputData: LocationRequestModel.Builder().setName(name: name).setLocation(location: location).setCordinate(lat: lat, long: long).addRequestHeader(key:"Content-Type", value: "application/json").build(), locationDelegate: self)
        
    }
    
    
}




