//
//  SMFAcceptDeclineTableViewCell.swift
//  SMF
//
//  Created by Jenkins on 2/15/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit
protocol AcceptDeclineDelegate{
    func AcceptTapped(sender:UIButton)
    func DeclineTapped(sender:UIButton)
}
class SMFAcceptDeclineTableViewCell: UITableViewCell {

    @IBOutlet weak var declineBtn: MyCustomButton!
    @IBOutlet weak var acceptBtn: MyCustomButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    var delegate1:AcceptDeclineDelegate!

    @IBOutlet weak var userImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        userImageView.layer.borderWidth = 1.0
        userImageView.layer.masksToBounds = false
        userImageView.layer.borderColor = UIColor.appBlueBorderColor.cgColor
        userImageView.layer.cornerRadius = userImageView.frame.height/2
        userImageView.clipsToBounds = true
        // Initialization code
    }
  
    @IBAction func declineAction(_ sender: Any) {
        self.delegate1.DeclineTapped(sender: declineBtn)

        
    }
    @IBAction func acceptAction(_ sender: Any) {
        
        self.delegate1.AcceptTapped(sender: acceptBtn)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCellRequested(notification:UserNotification){
        nameLabel.text = notification.notificationText
        userImageView.image = UIImage(named: (notification.sourceUser?.profile.profileImage)!)
        
    }
}
