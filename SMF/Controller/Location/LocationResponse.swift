//
//  LocationResponse.swift
//  SMF
//
//  Created by Jenkins on 2/13/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//




import Foundation
class LocationResponse : Mapper  {
    
    // User Info Property
    let sessId      : String
    let sessionName : String
    let token       : String
    let userProfile: UserProfile?
    var location  : [SavedLocation] = [SavedLocation]()
    
    //Declaring Response Key coming from server
    private let SESSID_KEY      = "sessid"
    private let SESSIONNAME_KEY = "session_name"
    private let TOKEN_KEY       = "token"
    private let USER_KEY        = "user"
    
    
    required init?(_ response: Dictionary<String,AnyObject>) {
        sessId      = (response[SESSID_KEY] as? String) ?? ""
        sessionName = (response[SESSIONNAME_KEY] as? String) ?? ""
        token       = (response[TOKEN_KEY] as? String) ?? ""
        print(response)
        userProfile = UserProfile(response: response["profile"] as! NSDictionary)
        
       
    }
}

