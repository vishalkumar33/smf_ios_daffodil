//
//  SMFLocationViewController.swift
//  SMF
//
//  Created by Jenkins on 2/11/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import UIKit
import GoogleMaps
class SMFLocationViewController: SMFBaseViewController,GMSMapViewDelegate,DataDropDownDelegate,UIGestureRecognizerDelegate,AddLocationDelegate,SaveLocationDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var mapbtn: UIButton!
    @IBOutlet weak var mapSatellite: UIButton!
    @IBOutlet weak var locationTextField: MyCustomTextField!
    var dropDownView :DropDown! = DropDown()
    var islocation = false
    var isFirstTime = true
    var locationPresenter                      : LocationPresenter?
    var lag :Double?
    var long :Double?
    var namelocation:String?
    var locationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var locationFlag:Bool?
    let movableMarker = GMSMarker()
    @IBOutlet weak var mapView: GMSMapView!
    var locArray:[String] = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self

        
        
        
        setNav(title: "ADD A LOCATION")
        locationTextField.leftPadding(width: 16)
        
        self.locationPresenter = LocationPresenter(delegate: self)
        
        self.locationTextField.leftPadding(width: 16)
        dropDownView.frame = CGRect(x: self.locationTextField.frame.origin.x, y: self.locationTextField.frame.origin.y + 30 , width: UIScreen.main.bounds.width - 32 , height: 180.0)
        
        self.view.addSubview(dropDownView)
        self.dropDownView.delegate = self
        self.dropDownView.isHidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SMFPostUpdateViewController.handleTap(recognizer:)))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
             // Do any additional setup after loading the view.
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if isFirstTime
        {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
            let camera = GMSCameraPosition.camera(withLatitude: locValue.latitude,
                                                  longitude: locValue.longitude,
                                                  zoom: 8)
             mapView.camera = camera
            isFirstTime = false
        }
    }
    @IBAction func mapAction(_ sender: Any) {
          mapbtn.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 1.0), for: UIControlState.normal)
        mapSatellite.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.4), for: UIControlState.normal)

          mapView.mapType = kGMSTypeNormal
    }
    func saveTapped(sender: UIButton, name: String) {
        if(name != "" && namelocation != nil){
            if lag != nil {
                if long != nil {
                    self.locationPresenter?.addLocationClickedWithPost(lat: lag!, long: long!, name: name, location: namelocation!)
                }else{
                    showErrorAlert(alertTitle: "ERROR", alertMessage: "Unable to find longitude")
                }
            }else{
                showErrorAlert(alertTitle: "ERROR", alertMessage: "Unable to find lattitude")
            }
        }
        else{
            showErrorAlert(alertTitle: "ERROR", alertMessage: "Unable to find location")
        }
        
    }

    @IBAction func satelliteAction(_ sender: Any) {
        mapbtn.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.4), for: UIControlState.normal)
        mapSatellite.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 1.0), for: UIControlState.normal)

            mapView.mapType = kGMSTypeSatellite
         //mapView
        
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            
            break
        // ...
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            let alertController = UIAlertController(
                title: "dwd",
                message: "msg",
                preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            let openAction = UIAlertAction(title: "Settings", style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                }
            }
            alertController.addAction(openAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func saveTappedLocation(sender: UIButton, name: String) {
        let markerView = Bundle.main.loadNibNamed("MarkerPopUpView", owner: self, options: nil)?[0] as! MarkerPopUpView
        markerView.frame = mapView.frame
        self.view.addSubview(markerView)
        markerView.layer.shadowColor = UIColor.black.cgColor
        markerView.layer.shadowOpacity = 0.5
        markerView.layer.shadowOffset = CGSize.zero
        markerView.layer.shadowRadius = 10
        self.view.bringSubview(toFront: markerView)
        markerView.delegate = self
       

    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let markerView = Bundle.main.loadNibNamed("MarkerPopViewSave", owner: self, options: nil)?[0] as! MakerPopViewSave
        if namelocation != nil {
            markerView.locationLAbel.text = namelocation!
        }
        markerView.frame = mapView.frame
        self.view.addSubview(markerView)
        self.view.bringSubview(toFront: markerView)
        markerView.layer.shadowColor = UIColor.black.cgColor
        markerView.layer.shadowOpacity = 0.5
        markerView.layer.shadowOffset = CGSize.zero
        markerView.layer.shadowRadius = 10
        markerView.delegate = self
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.dropDownView.isHidden = true
        islocation = false
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        movableMarker.map = mapView
        getAddressForLatLng(position.target.latitude,longitude: position.target.longitude)
        lag = position.target.latitude
        long = position.target.longitude
        movableMarker.icon = #imageLiteral(resourceName: "marker")
        print("\(position.target.latitude) \(position.target.longitude)")
        
        movableMarker.position = position.target
    }
    func getAddressForLatLng(_ latitude: Double, longitude: Double) {
        let location = CLLocationCoordinate2DMake(latitude, longitude)
        var locationString:String?
        let gmsGeocoder = GMSGeocoder()
        
            gmsGeocoder.reverseGeocodeCoordinate(location) { (placemarks, error) in
                if placemarks != nil {
                   
                    for addressObj: GMSAddress in placemarks!.results()! {
                        let lines = addressObj.lines
                        locationString = lines?.joined(separator: " ")
                        locationString = locationString?.trimmingCharacters(in: CharacterSet.whitespaces)
                        print(locationString!)
                        self.namelocation = locationString!
                       
                        
                    
                    }                  
                }
            
        }
    }
    

    @IBAction func dropDownAction(_ sender: Any) {
        if let locat:[String] =  UserDefaults.standard.object(forKey: "userLocation") as! [String]? {
            updateNewLocation(withLocation: locat)
        }
        
    }
    
    func updateNewLocation(withLocation name:[String]?){
        dropDownView.dropDownViewBorderWidth = 1
        dropDownView.dropDownViewBorderColor = UIColor.appBlueBorderColor.cgColor
        dropDownView.backgroundColor = UIColor.white
        islocation = !islocation
        let Local : DropDownDataObject! = DropDownDataObject()
        Local.labelData = "Local" as AnyObject
        let Favorites : DropDownDataObject! = DropDownDataObject()
        Favorites.labelData = "Favorites" as AnyObject
        let NearBy : DropDownDataObject! = DropDownDataObject()
        NearBy.labelData = "NearBy" as AnyObject
        let Trending : DropDownDataObject! = DropDownDataObject()
        Trending.labelData = "Trending" as AnyObject
        
        dropDownView.dataObject = [Local,Favorites,NearBy,Trending]
        if name != nil {
            if (name?.count)! > 0 {
                for locName in name! {
                    let obj : DropDownDataObject! = DropDownDataObject()
                    obj.labelData = locName as AnyObject!
                    dropDownView.dataObject.append(obj)
                }
            }
        }
        dropDownView.dropDownTableView.reloadData()
        if(islocation)
        {
            self.dropDownView.isHidden = false
            self.dropDownView.show()
             self.view.bringSubview(toFront: self.dropDownView)
        }
        else
        {
            
            self.dropDownView.isHidden = true
        }
    }
    
    func data(_ val: AnyObject, index: Int) {
        self.locationTextField.text = val as? String
        islocation = false
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: dropDownView))!{
            return false
        }
       
        return true
    }
    func handleTap(recognizer: UITapGestureRecognizer) {
        self.dropDownView.isHidden = true
        islocation = false
    }
    

}
extension SMFLocationViewController:LocationDelegate{
    func showLoader() {
        self.view.showLoader()
    }
    func hideLoader() {
        self.view.hideLoader()
    }
    func showErrorAlert(alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle: alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func updateLocation(locationName: [SavedLocation]) {
        locArray = [""]
        for loc in locationName {
            locArray.append(loc.name!)
        }
        UserDefaults.standard.set(locArray, forKey: "userLocation")
    }

}
