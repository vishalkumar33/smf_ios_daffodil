//
//  MarkerPopUpView.swift
//  SMF
//
//  Created by Jenkins on 2/11/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
protocol AddLocationDelegate{
    func saveTapped(sender:UIButton,name:String)
}
class MarkerPopUpView: UIView {
  
    @IBOutlet weak var msgLabel: UILabel!
    
    @IBOutlet weak var savebtn: MyCustomButton!
    @IBOutlet weak var dropPinLabel: UILabel!
    @IBOutlet weak var dropAddressLabel: UILabel!
    var delegate:AddLocationDelegate!

    @IBOutlet weak var nameofLocationTextField: UITextField!
    override func awakeFromNib() {
    }
   
    @IBAction func saveAction(_ sender: Any) {
     
        self.delegate?.saveTapped(sender: savebtn,name: nameofLocationTextField.text!)
        removeFromSuperview()
    }

    @IBAction func cancelAction(_ sender: Any) {
        removeFromSuperview()

    }
}
