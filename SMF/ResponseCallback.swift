//
//  AGUCommonServiceManagerDelegate.swift
//  Aguila_IOS
//
//  Created by VedPandey on 26/04/16.
//  Copyright © 2016 Daffodilsw Applications. All rights reserved.
//

import Foundation

@objc protocol ResponseCallback {
       @objc optional func servicesManagerSuccessResponse(responseObject : AnyObject)
       @objc optional func servicesManagerError(error : String)
}

