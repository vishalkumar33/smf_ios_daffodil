

//This Struct is used for parsing successful response of API

class LoginResponse : Mapper  {
    
    // User Info Property
    let sessId      : String
    let sessionName : String
    let token       : String
    let user        : UserInfo?

    //Declaring Response Key coming from server
    private let SESSID_KEY      = "sessid"
    private let SESSIONNAME_KEY = "session_name"
    private let TOKEN_KEY       = "token"
    private let USER_KEY        = "user"

    
    required init?(_ response: Dictionary<String,AnyObject>) {
        sessId      = (response[SESSID_KEY] as? String) ?? ""
        sessionName = (response[SESSIONNAME_KEY] as? String) ?? ""
        token       = (response[TOKEN_KEY] as? String) ?? ""
        user        = UserInfo(response)
    }
}

