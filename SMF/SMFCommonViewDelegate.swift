//
//  AGUCommonViewDelegate.swift
//  IOSServiceLayerDemoCode
//
//  Created by Vishal on 04/08/16.
//  Copyright © 2016 Daffodilsw Applications. All rights reserved.
//

import Foundation

@objc protocol SMFCommonViewDelegate {
    @objc optional func showLoader()
    @objc optional func hideLoader()
    @objc optional func showErrorAlert(alertTitle : String , alertMessage : String)
}

