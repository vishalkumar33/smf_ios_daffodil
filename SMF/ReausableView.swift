//
//  ReausableView.swift
//  SMF
//
//  Created by Jenkins on 1/9/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView  {
    static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

extension UICollectionViewCell: ReusableView {
    
}

extension UITableViewCell: ReusableView {
    
}
