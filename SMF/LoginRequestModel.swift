

import Foundation


//This class is used for constructing Service Request Model

class LoginRequestModel {
    
    //Note :- Property Name must be same as key used in request API
    
     let requestBody   : [String:AnyObject]?
     let requestHeader : [String:AnyObject]?
    
    //This is a Private Constrctor instantiating Service Model Request Object
    
    private init(builderObject:Builder){
        
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builderObject.requestBody
        self.requestHeader = builderObject.requestHeader
    }

    
    // This inner class is used for setting upper class properties
    internal class Builder{
    
        var requestBody = [String:AnyObject]()
        var requestHeader = [String:AnyObject]()
        
        /**
         This method is used for setting email
         
         - parameter email: String parameter that is going to be set on Email
         
         - returns: returning Builder Object
         */
        
        func setEmail(email:String) ->Builder{
            requestBody["email"] = email as AnyObject?
            return self
        }
        
        /**
         This method is used for setting password
         
         - parameter password: String parameter that is going to be set on Password
         
         - returns: returning Builder object
         */
        
        func setPassword(password:String) ->Builder{
            requestBody["password"] = password as AnyObject?
            return self
        }
        
        func setDeviceId(deviceId:String) -> Builder{
            let dic = NSMutableDictionary()
            dic["deviceToken"] = deviceId
            dic["deviceType"] = "ios"
            requestBody["deviceInfo"] = dic as AnyObject?
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method returns the Service request Model
         
         - returns: Returns ServiceRequestModel Object having set Emailid and Password
         */
        
        func build() ->LoginRequestModel{
            return LoginRequestModel(builderObject: self)
        }
        
    }
    
        
    /**
     This method is used for creating End Point
     
     - returns: returning API End Point
     */
    
    func getEndPoint() -> String {
        return "/login"
    }
    
    /**
     This method is used for
     
     - returns: returning request body
     */
    
    func getRequestBody() -> [String:AnyObject] {
        return self.requestBody!
    }
    
    /**
     This method is used for containing Request Headers
     
     - returns: Dictionary contains header
     */
    
    func getRequestHeader() -> [String:AnyObject] {
        return self.requestHeader!
    }


}


