
//This class is used for parsing success response of API
class UserInfo : AnyObject , Mapper {
    
    // User Info Property
    let id              : String                    
    let password        : String
    let status          : UserStatus
    var profile         : UserProfile
    let settings        : UserSettings
    let pinned          : NSArray
    let notifications   : NSArray
    let workspaces      : NSArray
    let connections     : UserConnection
    let secure          : UserSecure
    var services        : NSDictionary  = NSDictionary()
    var facebookDetail  : UserFacebook?  = nil
    var emails          : [UserEmail]
    
    
    
    //Declaring Response Key coming from server
    private let ID_KEY              = "_id"
    private let PASSWORD_KEY        = "password"
    private let STATUS_KEY          = "status"
    private let PROFILE_KEY         = "profile"
    private let SETTINGS_KEY        = "settings"
    private let PINNED_KEY          = "pinned"
    private let NOTIFICATIONS_KEY   = "notifications"
    private let WORKSPACES_KEY      = "workspaces"
    private let CONNECTIONS_KEY     = "connections"
    private let SECURE_KEY          = "secure"
    private let EMAILS_KEY          = "emails"
    private let SERVICES_KEY        = "services"
    private let FACEBOOK_KEY        = "facebook"
    
    required init?(_ response: Dictionary<String,AnyObject>) {
        print(response)
        id              = (response[ID_KEY] as? String) ?? ""
        password        = (response[PASSWORD_KEY] as? String) ?? ""
        status          = UserStatus(response: (response[STATUS_KEY] as! NSDictionary))
        profile         = UserProfile(response: (response[PROFILE_KEY] as! NSDictionary))
        settings        = UserSettings(response: (response[SETTINGS_KEY] as! NSDictionary))
        pinned          = (response[PINNED_KEY] as? NSArray) ?? []
        notifications   = (response[NOTIFICATIONS_KEY] as? NSArray) ?? []
        workspaces      = (response[WORKSPACES_KEY] as? NSArray) ?? []
        connections     = UserConnection(response: (response[CONNECTIONS_KEY] as! NSDictionary))
        secure          = UserSecure(response: (response[SECURE_KEY] as! NSDictionary))
        
        if let service: NSDictionary = response[SERVICES_KEY] as? NSDictionary {
            services        = service
            facebookDetail  = UserFacebook(response: services)
        }
        emails          = [UserEmail]()
        if let emailsArray = response[EMAILS_KEY] as? [NSDictionary]{
            for dic in emailsArray{
                let value = UserEmail(response: dic)
                emails.append(value)
            }
        }
    }
}
