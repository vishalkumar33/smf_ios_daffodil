//
//  AppDelegate.swift
//  SMF
//
//  Created by Shivam Srivastava on 22/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit
import IQKeyboardManager
import AFNetworking
import FBSDKLoginKit
import GoogleMaps
import Fabric
import Crashlytics
import UserNotifications
import SocketIO
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    

    var window: UIWindow?
    var navBarImage :UINavigationBar?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
       
        Fabric.with([Crashlytics.self])
        AFNetworkReachabilityManager.shared().startMonitoring()
        initViewController()
        setUpKeybaordManager()
        setUpStatusBar()
        GMSServices.provideAPIKey("AIzaSyDpwJihzCJnaYkgndsdov1KP80WYnlHYXo")
        
   
        if #available(iOS 10, *) {
            
            //Notifications get posted to the function (delegate):  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void)"
            
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                application.registerForRemoteNotifications()
                guard error == nil else {
                    //Display Error.. Handle Error.. etc..
                    return
                }
                
                if granted {
                    //Do stuff here..
                }
                else {
                    //Handle user denying permissions..
                }
            }
            
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
        

        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    func initViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle(for: type(of: self)))
        let navigationController: UINavigationController?
        if UserDefault.bool(forKey: "isUserLoggedIn") {
            let homeVC:SMFTabViewController = storyBoard.instantiateViewController(withIdentifier: "SMFTabViewController") as! SMFTabViewController
                navigationController = UINavigationController(rootViewController: homeVC)
          MessageService.instance.connectToSocket(userId: UserDefault.string(forKey: "userId")!)
        }
        else {
            let landingVC:SMFLoginViewController = storyBoard.instantiateViewController(withIdentifier: "SMFLoginViewController") as! SMFLoginViewController
                navigationController = UINavigationController(rootViewController: landingVC)
        }
        self.window?.rootViewController = navigationController
    }
    
    func setUpKeybaordManager(){
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        IQKeyboardManager.shared().toolbarManageBehaviour = IQAutoToolbarManageBehaviour.byPosition
        IQKeyboardManager.shared().shouldShowTextFieldPlaceholder = true
        IQKeyboardManager.shared().keyboardDistanceFromTextField = 10.0
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
    }
    
    func setUpStatusBar(){
        UIApplication.shared.statusBarStyle = .lightContent
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor.init(red: 77 / 255, green: 154 / 255, blue: 164 / 255, alpha: 1.0)
        }
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    
    class func sharedAppDelegate() -> AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
        
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL! , sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
       
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print(token)
        let deviceTokenString = token
        print("device token....\(deviceTokenString)")
        
        
        print(deviceTokenString)
        UserDefaults.standard.set(deviceTokenString, forKey: "deviceToken")
    }
    
    
}


