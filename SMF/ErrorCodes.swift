

import Foundation

enum ErrorCodes:Int {
    
    case Error_Request_Time_Out = -1001
    case Error_400              = 400
    case Error_401              = 401
    case Error_404              = 404
    case Error_403              = 403
    case Error_409              = 409
    case Error_406              = 406
    case Error_451              = 451
    case Error_500              = 500
    case Error_501              = 501
    case Error_1009             = -1009
    case Error_1011             = -1011
}
