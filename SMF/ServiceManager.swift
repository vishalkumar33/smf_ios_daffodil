import UIKit
import AFNetworking


class ServiceManager {
    
    
    static let sharedInstance = ServiceManager()
    private(set) var delegate : ApiResponseReceiver?

    private(set) var globalManager:AFHTTPSessionManager!
    
    private func sessionManager() -> AFHTTPSessionManager
    {
        if globalManager==nil
        {
            globalManager = AFHTTPSessionManager()
        }
        
        globalManager.requestSerializer =  AFJSONRequestSerializer()
        globalManager.responseSerializer = AFJSONResponseSerializer()
        globalManager.responseSerializer.acceptableContentTypes = Set([ "application/json"])

        return globalManager
    }

    /**
     This method cancel all the Api calls , currently running
     */
    
    func cancelAllOperations() ->Void
    {
        globalManager.operationQueue.cancelAllOperations()
    }
    
    
    /**
     This method cancel Api call specific to url
     
     - parameter urlString: Url String that is used for Api call
     */
    
    func cancelTaskWithURL(urlString:String) ->Void
    {
        //Iterating all the tasks in Session Manager
        for task in self.globalManager.tasks{
            //Checking task URL if it's matches with URL we cancel that specific task
            if task.originalRequest?.url?.absoluteString.contains(urlString) != nil {
                task.cancel()
            }
        }
    }
    
    /**
     This method checks whether API is in Progress or not
     
     - parameter urlString: Url String that is used for Api call
     
     - returns: Returning true or false depend whether API is running or not
     */
    
    func isInProgress(urlString:String) ->Bool{
        //Iterating all the tasks in Session Manager
        for task in self.globalManager.tasks{
            //Checking task URL if it's matches with URL we cancel that specific task
            if(task.originalRequest?.url?.absoluteString.contains(urlString)) != nil {
                if(task.state == .running){
                    return true
                }
            }
        }
        return false
    }
    
    /**
     This method return NSError object in case if internet connection is not available
     
     - returns: NSError Object
     */
    
    private func getNetworkError() -> NSError {
        return NSError(domain: "network_error", code: -1009, userInfo:nil)
    }

    /**
     Method is used for Get Request Api Call
     
     - parameter urlString:    URL String that is used for Api call
     - parameter successBlock: return success response
     - parameter failureBlock: return failure response
     */
    
    func requestGETWithURL<T:Mapper>(urlString:String , requestHeader:[String:AnyObject], responseCallBack:ApiResponseReceiver , returningClass:T.Type)-> Void{
        
        self.delegate = responseCallBack

        // Checking the rechability of Network
        if AFNetworkReachabilityManager.shared().isReachable {
            
            // Instantiate session manager
            let manager:AFHTTPSessionManager = self.sessionManager()
            
            //Iterating request header dictionary and adding into API Manager
            for (key, value) in requestHeader {
                    manager.requestSerializer.setValue(value as? String, forHTTPHeaderField: key)
            }
            
            var error:NSError?
        
            // Creating Immutable Get NSURL Request
            let request:NSURLRequest = manager.requestSerializer.request(withMethod: "GET", urlString: urlString, parameters: [ : ], error: &error)
            
            // Calling Api with NSURLRequest and session Manager and fetching Response from server
            self.dataTaskWithRequestAndSessionManager(request: request, sessionManager: manager, returningClass:returningClass)
            
        }
        else{
            // Generating common network error
            self.delegate?.onError(errorResponse: getNetworkError())
        }
    }
    
    
    /**
     Method is used for Get Request Api Call with parameter
     
     - parameter urlString:         URL String that is used for Api call
     - parameter requestDictionary: dictionary used as a parameter
     - parameter successBlock:      return success response
     - parameter failureBlock:      return failure response
     */
    
    func requestGETWithParameter<T:Mapper>(urlString:String , andRequestDictionary requestDictionary:[String : AnyObject] , requestHeader:[String:AnyObject] , responseCallBack:ApiResponseReceiver , returningClass:T.Type)-> Void{
        
        self.delegate = responseCallBack
        
        // Checking the rechability of Network
        if AFNetworkReachabilityManager.shared().isReachable {
            
            // Instantiate session manager Object
            let manager:AFHTTPSessionManager = self.sessionManager()
            
            //Iterating request header dictionary and adding into API Manager
            for (key, value) in requestHeader {
                manager.requestSerializer.setValue(value as? String, forHTTPHeaderField: key)
            }

            var error:NSError?
            
            // Creating Immutable Get NSURL Request
            let request:NSURLRequest = manager.requestSerializer.request(withMethod: "GET", urlString: urlString, parameters: requestDictionary, error: &error)
            
            // Calling Api with NSURLRequest and session Manager and fetching Response from server
            self.dataTaskWithRequestAndSessionManager(request: request, sessionManager: manager, returningClass:returningClass)
        }
        else{
            // Generating common network error
            self.delegate?.onError(errorResponse: getNetworkError())
        }
    }
    
    /**
     Method is used for Post Request Api Call with parameter
     
     - parameter urlString:         URL String that is used for Api call
     - parameter requestDictionary: dictionary used as a parameter
     - parameter successBlock:      return success response
     - parameter failureBlock:      return failure response
     */
    
    func requestPOSTWithURL<T:Mapper>(urlString:String , andRequestDictionary requestDictionary:[String : AnyObject],requestHeader:[String:AnyObject] , responseCallBack:ApiResponseReceiver , returningClass:T.Type) ->Void{
        
        self.delegate = responseCallBack
        print(requestDictionary)

        // Checking the rechability of Network
        if AFNetworkReachabilityManager.shared().isReachable {
            
            // Instantiate session manager Object
            let manager:AFHTTPSessionManager = self.sessionManager()
            
            //Iterating request header dictionary and adding into API Manager
            for (key, value) in requestHeader {
                manager.requestSerializer.setValue(value as? String, forHTTPHeaderField: key)
            }

            var error:NSError?
            
            // Creating Immutable POST NSURL Request
            let request:NSURLRequest = manager.requestSerializer.request(withMethod: "POST", urlString: urlString, parameters: requestDictionary, error: &error)
            
            // Calling Api with NSURLRequest and session Manager and fetching Response from server
            self.dataTaskWithRequestAndSessionManager(request: request, sessionManager: manager , returningClass:returningClass)
            
            }
        else{
            // Generating common network error
            self.delegate?.onError(errorResponse: getNetworkError())
        }
    }
    
    /**
     Method is used for Delete Request Api Call with parameter
     
     - parameter urlString:         URL String that is used for Api call
     - parameter requestDictionary: dictionary used as a parameter
     - parameter successBlock:      return success response
     - parameter failureBlock:      return failure response
     */
    
    func requestDELETEWithURL<T:Mapper>(urlString:String, andRequestDictionary requestDictionary:[String : AnyObject], requestHeader:[String:AnyObject] ,responseCallBack:ApiResponseReceiver , returningClass:T.Type) -> Void{
        
        self.delegate = responseCallBack

        // Checking the rechability of Network
        if AFNetworkReachabilityManager.shared().isReachable {
            
            // Instantiate session manager Object
            let manager:AFHTTPSessionManager = self.sessionManager()
            
            //Iterating request header dictionary and adding into API Manager
            for (key, value) in requestHeader {
                manager.requestSerializer.setValue(value as? String, forHTTPHeaderField: key)
            }

            var error:NSError?
            
            // Creating Immutable DELETE NSURL Request
            let request:NSURLRequest = manager.requestSerializer.request(withMethod: "DELETE", urlString: urlString, parameters: requestDictionary, error: &error)
            
            // Calling Api with NSURLRequest and session Manager and fetching Response from server
            self.dataTaskWithRequestAndSessionManager(request: request, sessionManager: manager, returningClass:returningClass)
            
        }
        else{
            // Generating common network error
            self.delegate?.onError(errorResponse: getNetworkError())
        }
    }
    
    /**
     Method is used for Put Request Api Call with parameter
     
     - parameter urlString:         URL String that is used for Api call
     - parameter requestDictionary: dictionary used as a parameter
     - parameter successBlock:      return success response
     - parameter failureBlock:      return failure response
     */
    
    func requestPUTWithURL<T:Mapper>(urlString:String, andRequestDictionary requestDictionary:[String : AnyObject], requestHeader:[String:AnyObject], responseCallBack:ApiResponseReceiver , returningClass:T.Type) -> Void{
        
        
        print(requestDictionary)
        
        self.delegate = responseCallBack

        // Checking the rechability of Network
        if AFNetworkReachabilityManager.shared().isReachable {
            
            // Instantiate session manager Object
            let manager:AFHTTPSessionManager = self.sessionManager()
            
            //Iterating request header dictionary and adding into API Manager
            for (key, value) in requestHeader {
                manager.requestSerializer.setValue(value as? String, forHTTPHeaderField: key)
            }

            var error:NSError?
            
            // Creating Immutable PUT NSURL Request
            let request:NSURLRequest = manager.requestSerializer.request(withMethod: "PUT", urlString: urlString, parameters: requestDictionary, error: &error)
            
            // Calling Api with NSURLRequest and session Manager and fetching Response from server
            self.dataTaskWithRequestAndSessionManager(request: request, sessionManager: manager, returningClass:returningClass)
        }
        else{
            // Generating common network error
            self.delegate?.onError(errorResponse: getNetworkError())
        }
    }
    
    /**
     Method is used for Multipart Request Api Call with parameter
     
     - parameter urlString:            URL String that is used for Api call
     - parameter imageData:            data uploads in multipart
     - parameter andRequestDictionary: dictionary used as a parameter
     - parameter imageName:            image that has to be uploaded
     - parameter successBlock:         return success response
     - parameter failureBlock:         return failure response
     - parameter progressBlock:        return progress response
     */
    
    func requestMultipartRequestWithURL(urlString:String, andImageData imageData:NSData, andRequestDictionary:[String : NSData?],requestHeader:[String:AnyObject], withImageName imageName:String , withSuccessBlock successBlock:@escaping (_ response:AnyObject) ->Void, andFailureBlock failureBlock:@escaping (_ error:NSError) ->Void, andProgressBlock progressBlock:@escaping (_ progress:Double) -> Void){
        
        // Checking the rechability of Network
        if AFNetworkReachabilityManager.shared().isReachable {
            
            // Instantiate session manager Object
            let manager:AFHTTPSessionManager = self.sessionManager()
            
            //Iterating request header dictionary and adding into API Manager
            for (key, value) in requestHeader {
                manager.requestSerializer.setValue(value as? String, forHTTPHeaderField: key)
            }

            //Setting multipart request Header
            manager.requestSerializer.setValue("multipart/form-data", forKey: "Content-Type")
            
            // Creating Immutable Multipart POST NSURL Request
            let request:NSURLRequest = manager.requestSerializer.multipartFormRequest(withMethod: "POST", urlString: urlString, parameters: nil, constructingBodyWith: { (formData:AFMultipartFormData!) -> Void in
                
                formData.appendPart(withFileData: imageData as Data, name: imageName, fileName: "file.jpg", mimeType: "image/jpeg")
                
                }, error: nil)
            
            
            var uploadTask: URLSessionUploadTask
            
            // Calling Api with NSURLRequest and upload progress
            uploadTask = manager.uploadTask(withStreamedRequest: request as URLRequest, progress: { (uploadProgress) in
                DispatchQueue.global(qos: .background).async {
                    DispatchQueue.main.async {
                        //Update the progress
                        progressBlock (uploadProgress.fractionCompleted)
                    }
                }
            }, completionHandler: { (response, responseObject, error) in
                
                // Checking whether API Response contains Success response or Error Response
                if( (error == nil) && (responseObject != nil)){
                    successBlock(responseObject! as AnyObject)
                    
                }else {
                    failureBlock (error! as NSError)
                }
            })
            
            //Resuming Uploading
            uploadTask.resume()
        }
        else{
            // Generating common network error
            failureBlock(getNetworkError())
        }
    }
    
    /**
     Calling Api with NSURLRequest and session Manager and fetching Response from server
     
     - parameter request:        NSURLRequest request used for interacting with server
     - parameter sessionManager: AFHTTPSessionManager that contains API Header and Content Type
     */
    private func dataTaskWithRequestAndSessionManager<T:Mapper>(request:NSURLRequest, sessionManager:AFHTTPSessionManager, returningClass: T.Type) -> Void {
        
        
        sessionManager.dataTask(with: request as URLRequest) { (response, responseObject, error) -> Void in
            // Checking whether API Response contains Success response or Error Response
            

            
            if( (error == nil) && (responseObject != nil)){
                self.delegate?.onSuccess(response: T(responseObject as! Dictionary<String,AnyObject>)!)
            }else {
                self.delegate?.onError(errorResponse: error! as NSError)
            }
        }.resume()
    }

}
