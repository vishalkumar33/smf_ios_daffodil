//
//  NibLoadableView.swift
//  SMF
//
//  Created by Jenkins on 1/9/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

protocol NibLoadableView: class {
    static var nibName: String { get }
}

extension NibLoadableView  {
    static var nibName: String {
        return String(describing: self)
    }
}


