//
//  ForgotData.swift
//  SMF
//
//  Created by Jenkins on 1/16/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
class ForgotData : AnyObject, Mapper{
    let email              : String
    let message            : String
    
    
    private let Email_KEY           = "email"
    private let MESSAGE_KEY         = "message"
  
    
    required init?(_ response: Dictionary<String,AnyObject>) {
        email              = (response[Email_KEY] as? String) ?? ""
        message            = (response[MESSAGE_KEY] as? String) ?? ""
    }
    
    
    
}
