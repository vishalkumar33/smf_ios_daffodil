//
//  MemberInfo.swift
//  SMF
//
//  Created by Jenkins on 1/24/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

//This class is used for parsing success response of API
class MemberInfo : AnyObject , Mapper {
    
    // User Info Property
    let id              : String
    var profile         : UserProfile?
    
    //Declaring Response Key coming from server
    private let ID_KEY              = "_id"
    private let PROFILE_KEY         = "profile"

    required init?(_ response: Dictionary<String,AnyObject>) {
        print(response)
        id              = (response[ID_KEY] as? String) ?? ""
        profile         = UserProfile(response: (response[PROFILE_KEY] as! NSDictionary))
    }
    
    
    
}
