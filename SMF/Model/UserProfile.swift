//
//  UserProfile.swift
//  SMF
//
//  Created by Daffolapmac-25 on 05/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class UserProfile {
    
    var firstName           : String
    var lastName            : String
    var fullName            : String
    var subscription        : String
    var twitterProfileUrl   : String
    var facebookProfileUrl  : String
    var linkedInProfileUrl  : String
    var webAddress          : String
    var state               : String
    var city                : String
    var streetAddress       : String
    var title               : String
    var company             : String
    var profileImage        : String
    var hiddenPost          : NSArray
    var searchEmail         : String
    var zip                 : String
    var phoneNumber         : String
    var handle              : String = ""
    var dob                 : String = ""
    var relationStatus      : String = ""
    var intro               : String = ""
    var location            : String = ""
    var gender              : String
    var savedLocation       : [SavedLocation]
    
    //Declaring Response Key coming from server
    private let FIRSTNAME_KEY           = "firstName"
    private let LASTNAME_KEY            = "lastName"
    private let FULLNAME_KEY            = "fullName"
    private let SUBSCRIPTION_KEY        = "subscription"
    private let TWITTERPROFILEURL_KEY   = "twitterProfileUrl"
    private let FACEBOOKPROFILEURL_KEY  = "facebookProfileUrl"
    private let LINKEDINPROFILEURL_KEY  = "linkedInProfileUrl"
    private let WEBADDRESS_KEY          = "webAddress"
    private let STATE_KEY               = "state"
    private let CITY_KEY                = "city"
    private let STREETADDRESS_KEY       = "streetAddress"
    private let TITLE_KEY               = "title"
    private let COMPANY_KEY             = "company"
    private let PROFILE_IMAGE_KEY       = "profileImage"
    private let HIDDEN_POST_KEY         = "hiddenPosts"
    private let SEARCH_EMAIL_KEY        = "searchEmail"
    private let ZIPCODE_KEY             = "zipCode"
    private let PHONE_KEY               = "phoneNumber"
    private let GENDER_KEY              = "gender"
    private let HANDLE_KEY              = "handle"
    private let RELATION_KEY            = "relationship"
    private let DOB_KEY                 = "dob"
    private let INTRO_KEY               = "intro"
    private let SAVEDLOCATION_KEY       = "savedLocations"
    
    init(response: NSDictionary) {
        firstName           = (response[FIRSTNAME_KEY] as? String) ?? ""
        lastName            = (response[LASTNAME_KEY] as? String) ?? ""
        fullName            = (response[FULLNAME_KEY] as? String) ?? ""
        subscription        = (response[SUBSCRIPTION_KEY] as? String) ?? ""
        twitterProfileUrl   = (response[TWITTERPROFILEURL_KEY] as? String) ?? ""
        facebookProfileUrl  = (response[FACEBOOKPROFILEURL_KEY] as? String) ?? ""
        linkedInProfileUrl  = (response[LINKEDINPROFILEURL_KEY] as? String) ?? ""
        webAddress          = (response[WEBADDRESS_KEY] as? String) ?? ""
        state               = (response[STATE_KEY] as? String) ?? ""
        city                = (response[CITY_KEY] as? String) ?? ""
        streetAddress       = (response[STREETADDRESS_KEY] as? String) ?? ""
        title               = (response[TITLE_KEY] as? String) ?? ""
        company             = (response[COMPANY_KEY] as? String) ?? ""
        profileImage        = (response[PROFILE_IMAGE_KEY] as? String) ?? ""
        hiddenPost          = (response[HIDDEN_POST_KEY] as? NSArray) ?? []
        searchEmail         = (response[SEARCH_EMAIL_KEY] as? String) ?? ""
        zip                 = (response[ZIPCODE_KEY] as? String) ?? "0"
        phoneNumber         = (response[PHONE_KEY] as? String) ?? ""
        gender              = (response[GENDER_KEY] as? String) ?? ""
        handle              = (response[HANDLE_KEY] as? String) ?? ""
        relationStatus      = (response[RELATION_KEY] as? String) ?? ""
        dob                 = (response[DOB_KEY] as? String) ?? ""
        intro               = (response[INTRO_KEY] as? String) ?? ""
        
        savedLocation          = [SavedLocation]()
        if let savedlocationsArray = response[SAVEDLOCATION_KEY] as? [NSDictionary]{
            for dic in savedlocationsArray{
                let value = SavedLocation(response: dic)
                savedLocation.append(value)
            }
        }
    }
    

    
    
}
