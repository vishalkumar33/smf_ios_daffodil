//
//  FollowUser.swift
//  SMF
//
//  Created by Jenkins on 1/24/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation



class FollowUser {
    
    let followedUserId     : String
    let action          : String
    let message            : String

    //Declaring Response Key coming from server
    private let FOLLOWUSERID_KEY     = "followedUserId"
    private let ACTION_KEY           = "action"
    private let MESSAGE_KEY             = "message"
    init(response:NSDictionary) {
        followedUserId     = (response[FOLLOWUSERID_KEY] as? String) ?? ""
        action          = (response[ACTION_KEY] as? String) ?? ""
        message          = (response[MESSAGE_KEY] as? String) ?? ""

    }
}
