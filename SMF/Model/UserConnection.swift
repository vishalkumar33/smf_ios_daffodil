//
//  UserConnection.swift
//  SMF
//
//  Created by Daffolapmac-25 on 05/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class UserConnection{
    
    let follwing    : NSArray
    let blocked     : NSArray
    let connected   : NSArray
    let favorites   : NSArray
    
    //Declaring Response Key coming from server
    private let FOLLOWING_KEY     = "following"
    private let BLOCKED_KEY  = "blocked"
    private let CONNECTED_KEY   = "connected"
    private let FAVORITES_KEY   = "favorites"
    init(response: NSDictionary) {
        follwing    = (response[FOLLOWING_KEY] as? NSArray) ?? []
        blocked     = (response[BLOCKED_KEY] as? NSArray) ?? []
        connected   = (response[CONNECTED_KEY] as? NSArray) ?? []
        favorites   =   (response[FAVORITES_KEY]) as? NSArray ?? []
    }
    
}
