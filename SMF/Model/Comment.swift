//
//  Comment.swift
//  SMF
//
//  Created by Jenkins on 2/18/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//




class Comment {
    let id              : String
    let content         : String
    var author          : Author  = Author()
    let updatedAt       : String
    let peaceBy         : NSArray
    let hidden          : Bool
    let removed         : Bool
    let dislikedBy      : NSArray
    let likedBy         : NSArray
    let createdAt       : String
   
    
    
    private let ID_KEY           = "_id"
    private let CONTENT_KEY      = "content"
    private let AUTHOR_KEY       = "author"
    private let UPDATED_KEY      = "updatedAt"
    private let PEACEBY_KEY      = "peaceBy"
    private let HIDDEN_KEY       = "hidden"
    private let REMOVED_KEY      = "removed"
    private let DISLIKEDBY_KEY   = "dislikedBy"
    private let LIKEDBY_KEY      = "likedBy"
    private let CREATEDAT_KEY    = "createdAt"
    
    init(response: NSDictionary) {
        print(response)
        id              = (response[ID_KEY] as? String) ?? ""
        content         = (response[CONTENT_KEY] as? String) ?? ""
        if let auth = response[AUTHOR_KEY] as? NSDictionary{
            author          = Author(dict:auth)
        }
       
        updatedAt       = (response[UPDATED_KEY] as? String) ?? ""
        peaceBy         = (response[PEACEBY_KEY] as? NSArray) ?? []
        hidden          = (response[HIDDEN_KEY] as? Bool) ?? true
        removed         = (response[REMOVED_KEY] as? Bool) ?? true
        dislikedBy      = (response[DISLIKEDBY_KEY] as? NSArray) ?? []
        likedBy         = (response[LIKEDBY_KEY] as? NSArray) ?? []
        createdAt       = (response[CREATEDAT_KEY] as? String) ?? ""
        
    }
    
    
    
}
