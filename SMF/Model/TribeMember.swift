//
//  TribeMember.swift
//  SMF
//
//  Created by Vishal on 08/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class TribeMember {
    var tribeOwnerId:String?
    var tribeMemberId:String?
    var tribeMemberStatus:String?
    var tribeMemberDetail:TribeOwner?
    
    init(response:NSDictionary) {
        if let id = response["id"] as? NSDictionary {
            tribeMemberDetail = TribeOwner(response: id)
        }
        if let memberId = response["_id"] as? String{
            tribeMemberId = memberId
        }
        if let status = response["status"] as? String{
            tribeMemberStatus = status
        }
        
    }
    
}
