//
//  Author.swift
//  SMF
//
//  Created by Daffolapmac-25 on 25/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class Author {
    var id:String?
    var profile:UserProfile!
    
    
    init(dict:NSDictionary) {
        if let id = dict["_id"] as? String{
            self.id = id
        }
        self.profile = UserProfile(response: dict["profile"] as! NSDictionary)
    }
    
    init(){
        
    }
    
}
