//
//  UserEmails.swift
//  SMF
//
//  Created by Daffolapmac-25 on 05/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class UserEmail:NSObject {
    
    let address     : String
    let id          : String
    let verified    :String
    
    //Declaring Response Key coming from server
    private let ADDRESS_KEY     = "address"
    private let ID_KEY          = "_id"
    private let VERIFIED_KEY    = "verified"
    
    init(response:NSDictionary) {
        address     = (response[ADDRESS_KEY] as? String) ?? ""
        id          = (response[ID_KEY] as? String) ?? ""
        verified    = (response[VERIFIED_KEY] as? String) ?? ""
    }
}
