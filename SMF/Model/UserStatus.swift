//
//  UserStatus.swift
//  SMF
//
//  Created by Daffolapmac-25 on 05/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class UserStatus {
    let idle    :Bool
    let online  :Bool
    
    //Declaring Response Key coming from server
    private let IDLE_KEY     = "idle"
    private let ONLINE_KEY   = "online"
    
    init(response:NSDictionary){
        idle     = (response[IDLE_KEY] as? Bool) ?? false
        online   = (response[ONLINE_KEY] as? Bool) ?? false
    }
}
