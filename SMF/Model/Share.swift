//
//  Share.swift
//  SMF
//
//  Created by Vishal on 20/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
//
//  SharePost.swift
//  SMF
//
//  Created by Vishal on 20/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation



class Share {
    let id              : String
    let content         : String
    var author          : Author  = Author()
    let updatedAt       : String
    let caption         : String
    let peaceBy         : NSArray
    let hidden          : Bool
    let removed         : Bool
    let dislikedBy      : NSArray
    let likedBy         : NSArray
    let realOwner       : String
    let createdAt       : String
    let permissions     : String
    let visible         : Bool
    var locations       : LocationType = LocationType()
    
    
    
    private let ID_KEY           = "_id"
    private let CONTENT_KEY      = "content"
    private let AUTHOR_KEY       = "author"
    private let UPDATED_KEY      = "updatedAt"
    private let PEACEBY_KEY      = "peaceBy"
    private let HIDDEN_KEY       = "hidden"
    private let REMOVED_KEY      = "removed"
    private let DISLIKEDBY_KEY   = "dislikedBy"
    private let LIKEDBY_KEY      = "likedBy"
    private let CREATEDAT_KEY    = "createdAt"
    private let LOCATION_KEY     = "location"
    private let CAPTION_KEY      = "caption"
    private let REALOWNER_KEY     = "realOwner"
    private let PERMISSION_KEY      = "permissions"
    private let VISIBLE_KEY      = "visible"
    
    init(response: NSDictionary) {
        print(response)
        id              = (response[ID_KEY] as? String) ?? ""
        content         = (response[CONTENT_KEY] as? String) ?? ""
        if let auth = response[AUTHOR_KEY] as? NSDictionary{
            author          = Author(dict:auth)
        }
        updatedAt       = (response[UPDATED_KEY] as? String) ?? ""
        caption       = (response[CAPTION_KEY] as? String) ?? ""
        realOwner       = (response[REALOWNER_KEY] as? String) ?? ""
        peaceBy         = (response[PEACEBY_KEY] as? NSArray) ?? []
        hidden          = (response[HIDDEN_KEY] as? Bool) ?? true
        removed         = (response[REMOVED_KEY] as? Bool) ?? true
        dislikedBy      = (response[DISLIKEDBY_KEY] as? NSArray) ?? []
        likedBy         = (response[LIKEDBY_KEY] as? NSArray) ?? []
        createdAt       = (response[CREATEDAT_KEY] as? String) ?? ""
        permissions       = (response[PERMISSION_KEY] as? String) ?? ""
        visible         = (response[VISIBLE_KEY] as? Bool) ?? true
        
        if let _        = response[LOCATION_KEY] as? NSDictionary
        {
            locations       = LocationType(response: (response[LOCATION_KEY] as! NSDictionary))
        }
        
    }
    
    
    
}
