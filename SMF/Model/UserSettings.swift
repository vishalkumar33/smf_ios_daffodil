//
//  UserSettings.swift
//  SMF
//
//  Created by Daffolapmac-25 on 05/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class UserSettings {
    
    let deactivated     : Bool
    let profilePrivate  : Bool
    let addressPublic   : Bool
    let emailPublic     : Bool
    let phonePublic     : Bool
    
    //Declaring Response Key coming from server
    private let DEACTIVATED_KEY     = "deactivated"
    private let PROFILEPRIVATE_KEY  = "profilePrivate"
    private let ADDRESSPUBLIC_KEY   = "addressPublic"
    private let EMAILPUBLIC_KEY     = "emailPublic"
    private let PHONEPUBLIC_KEY     = "phonePublic"
    
    init(response: NSDictionary) {
        deactivated     = (response[DEACTIVATED_KEY] as? Bool) ?? false
        profilePrivate  = (response[PROFILEPRIVATE_KEY] as? Bool) ?? false
        addressPublic   = (response[ADDRESSPUBLIC_KEY] as? Bool) ?? false
        emailPublic     = (response[EMAILPUBLIC_KEY] as? Bool) ?? false
        phonePublic     = (response[PHONEPUBLIC_KEY] as? Bool) ?? false
    }
   
    
}
