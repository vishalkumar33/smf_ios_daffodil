//
//  UserSecure.swift
//  SMF
//
//  Created by Daffolapmac-25 on 05/01/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class UserSecure {
    let passwordResetToken  : String
    let numberVerify        : String
    let smsCode             : String
    
    //Declaring Response Key coming from server
    private let PASSWORDRESETTOKEN_KEY  = "passwordResetToken"
    private let NUMBERVERIFY_KEY        = "numberVerify"
    private let SMSCODE_KEY             = "smsCode"
    
    
    init(response:NSDictionary) {
        passwordResetToken  = (response[PASSWORDRESETTOKEN_KEY] as? String) ?? ""
        numberVerify        = (response[NUMBERVERIFY_KEY] as? String) ?? ""
        smsCode             = (response[SMSCODE_KEY] as? String) ?? ""
    }
}
