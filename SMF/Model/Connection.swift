//
//  Connection.swift
//  SMF
//
//  Created by Vishal on 21/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class Connection {
    
    var id:String?
    var fullName:String?
    var handle:String?
    var profileImage:String?
    var isSelected = false
    
    init(response:NSDictionary) {
        if let id = response["_id"] as? String {
            self.id = id
        }
        if let profileDic = response["profile"] as? NSDictionary {
            if let name = profileDic["fullName"] as? String {
                self.fullName = name
            }
            if let handle = profileDic["handle"] as? String {
                self.handle = handle
            }
            if let image = profileDic["profileImage"] as? String {
                self.profileImage = image
            }
        }
    }
}
