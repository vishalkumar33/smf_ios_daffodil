//
//  UserFacebook.swift
//  SMF
//
//  Created by Jenkins on 1/19/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class  UserFacebook {
    let accessToken         : String
    let expiresAt           : String
    let id                  : String
    let email               : String
    let name                : String
    let first_name          : String
    let last_name           : String
    let link                : String
    let gender              : String
    let locale              : String
    
    //Declaring Response Key coming from server
    private let ACCESSTOKEN_KEY        = "accessToken"
    private let EXPIRESAT_KEY          = "expiresAt"
    private let ID_KEY                 = "id"
    private let EMAIL_KEY              = "email"
    private let NAME_KEY               = "name"
    private let FIRSTNAME_KEY          = "first_name"
    private let LASTNAME_KEY           = "last_name"
    private let LINK_KEY               = "link"
    private let GENDER_KEY             = "gender"
    private let LOCALE_KEY             = "locale"
    
    
    init(response:NSDictionary) {
        accessToken  = (response[ACCESSTOKEN_KEY] as? String) ?? ""
        expiresAt    = (response[EXPIRESAT_KEY] as? String) ?? ""
        id           = (response[ID_KEY] as? String) ?? ""
        email        = (response[EMAIL_KEY] as? String) ?? ""
        name         = (response[NAME_KEY] as? String) ?? ""
        first_name   = (response[FIRSTNAME_KEY] as? String) ?? ""
        last_name    = (response[LASTNAME_KEY] as? String) ?? ""
        link         = (response[LINK_KEY] as? String) ?? ""
        gender       = (response[GENDER_KEY] as? String) ?? ""
        locale       = (response[LOCALE_KEY] as? String) ?? ""

    }
}
