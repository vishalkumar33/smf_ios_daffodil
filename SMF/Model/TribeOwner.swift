//
//  TribeOwner.swift
//  SMF
//
//  Created by Vishal on 08/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class TribeOwner {
    var tribeOwnerId:String?
    var tribeOwnerProfile:UserProfile?
    
    init(response:NSDictionary) {
        if let id = response["_id"] as? String {
            tribeOwnerId = id
        }
        if let profile = response["profile"] as? NSDictionary{
            tribeOwnerProfile = UserProfile(response: profile)
        }
    }
}
