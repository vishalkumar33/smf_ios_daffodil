//
//  LocationType.swift
//  SMF
//
//  Created by Jenkins on 1/9/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
class LocationType
{
    let coordinates: NSArray?
    let type: String?
    
    private let COORDINATES_KEY     = "coordinates"
    private let TYPE_KEY            = "type"
    
    init()
    {
        coordinates = nil
        type = nil
    }
    
    required init(response:NSDictionary) {
        coordinates     = (response[COORDINATES_KEY] as? NSArray) ?? []
        type          = (response[TYPE_KEY] as? String) ?? ""
    }
    
    
}
