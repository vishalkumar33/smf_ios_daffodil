//
//  SourceUser.swift
//  SMF
//
//  Created by Jenkins on 2/16/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
class  SourceUser {
    var _id:String?
    var createdDat: String?
    var profile         : UserProfile
    var notifcationLink :String?
    private let PROFILE_KEY         = "profile"

    init(response:NSDictionary) {
        print(response)
        if let id = response["_id"] as? String {
            _id = id
        }
        if let created = response["createdAt"] as? String{
            self.createdDat = created
        }
        profile         = UserProfile(response: (response[PROFILE_KEY] as! NSDictionary))
        
        if let notfication = response["notifications"] as? NSDictionary {
            self.notifcationLink = notfication["link"] as? String ?? ""
        }
    }
}
