//
//  TribeInviteMember.swift
//  SMF
//
//  Created by Vishal on 21/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class TribeInviteMember {
    
    var id:String?
    var status:String?
    
    init(id:String, status:String) {
        self.id = id
        self.status = status
    }
}
