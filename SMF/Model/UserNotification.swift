//
//  Notification.swift
//  SMF
//
//  Created by Vishal on 14/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class UserNotification {
    var createdId:Int = 0
    var sourceUser:SourceUser?
    var read:Bool = false
    var link:String = ""
    var notificationText:String = ""
    var actionItemId:String = ""
    var icon:String = ""
    var isNewRequest :Bool = false
    var direct:Bool = false
    var type:String = ""
    
    init(response:NSDictionary) {
        print(response)
        if let createdId = response["createdAt"] as? Int{
            self.createdId = createdId
        }
        if let SourceDetail = response["sourceUser"] as? NSDictionary {
            sourceUser = SourceUser(response: SourceDetail)
        }
        if let read = response["read"] as? Bool{
            self.read = read
        }
        
        if let link = response["link"] as? String{
            self.link = link
        }
        
        if let text = response["text"] as? String{
            self.notificationText = text
        }
        if let actionItemId = response["actionItemId"] as? String{
            self.actionItemId = actionItemId
        }
        if let icon = response["icon"] as? String{
            self.icon = icon
        }
        if let direct = response["direct"] as? Bool{
            self.direct = direct
        }
        if let isNewRequest = response["isNewRequest"] as? Bool{
            self.isNewRequest = isNewRequest
        }
        if let type = response["type"] as? String{
            self.type = type
        }
        
    }
}
