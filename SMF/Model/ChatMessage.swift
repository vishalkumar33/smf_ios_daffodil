//
//  ChatMessage.swift
//  SMF
//
//  Created by Vishal on 20/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class ChatMessage {
    var id:String?
    var author:String?
    var content:String?
    var createdAt:String?
    var read:Bool?
    var receipt:String?
    
    
    init(response:NSDictionary) {
        if let id = response["id"] as? String {
            self.id = id
        }
        if let author = response["author"] as? String {
            self.author = author
        }
        if let content = response["content"] as? String {
            self.content = content
        }
        if let createdAt = response["createdAt"] as? String {
            self.createdAt = createdAt
        }
        if let read = response["read"] as? Bool {
            self.read = read
        }
        if let recipient = response["recipient"] as? String {
            self.receipt = recipient
        }
    }
    
    init(content:String){
        self.id = ""
        self.author = ""
        self.content = content
        self.createdAt = ""
        self.read = false
        self.receipt = ""
    }
}
