//
//  Post.swift
//  SMF
//
//  Created by Jenkins on 1/9/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//



class Post {

    let id              : String 
    let content         : String
    var author          : Author  = Author()
    let updatedAt       : String
    let peaceBy         : NSMutableArray
    let hidden          : Bool
    let removed         : Bool
    let dislikedBy      : NSMutableArray
    let likedBy         : NSMutableArray
    let createdAt       : String
    var locations       : LocationType = LocationType()
    let imagePath       : String
    let videoPath       : String
    var group           : Tribes = Tribes()
    var commentCount    :Int
    
    
    private let ID_KEY           = "_id"
    private let CONTENT_KEY      = "content"
    private let AUTHOR_KEY       = "author"
    private let UPDATED_KEY      = "updatedAt"
    private let PEACEBY_KEY      = "peaceBy"
    private let HIDDEN_KEY       = "hidden"
    private let REMOVED_KEY      = "removed"
    private let DISLIKEDBY_KEY   = "dislikedBy"
    private let LIKEDBY_KEY      = "likedBy"
    private let CREATEDAT_KEY    = "createdAt"
    private let LOCATION_KEY     = "location"
    private let IMAGE_PATH_KEY   = "imageUrl"
    private let VIDEOPATH_KEY    = "videoUrl"
    private let GROUP_KEY        = "group"
    private let COMMENT_COUNT_KEY = "commentsCount"
    
    init(response: NSDictionary) {
        print(response)
        id              = (response[ID_KEY] as? String) ?? ""
        content         = (response[CONTENT_KEY] as? String) ?? ""
        if let auth = response[AUTHOR_KEY] as? NSDictionary{
            author          = Author(dict:auth)
        }
        if let tribe = response[GROUP_KEY] as? NSDictionary {
            group = Tribes(tribe as! Dictionary<String, AnyObject>)!
        }
        updatedAt       = (response[UPDATED_KEY] as? String) ?? ""
        peaceBy         = (response[PEACEBY_KEY] as? NSArray)?.mutableCopy() as?NSMutableArray ?? []
        hidden          = (response[HIDDEN_KEY] as? Bool) ?? true
        removed         = (response[REMOVED_KEY] as? Bool) ?? true
        dislikedBy      = (response[DISLIKEDBY_KEY] as? NSArray)?.mutableCopy() as?NSMutableArray ?? []
        likedBy         = (response[LIKEDBY_KEY] as? NSArray)?.mutableCopy() as?NSMutableArray ?? []
        createdAt       = (response[CREATEDAT_KEY] as? String) ?? ""
        if let _        = response[LOCATION_KEY] as? NSDictionary
        {
            locations       = LocationType(response: (response[LOCATION_KEY] as! NSDictionary))
        }
        imagePath       = (response[IMAGE_PATH_KEY] as? String) ?? ""
        videoPath       = (response[VIDEOPATH_KEY] as? String) ?? ""
        commentCount    = (response[COMMENT_COUNT_KEY] as? Int) ?? 0
    }



}
