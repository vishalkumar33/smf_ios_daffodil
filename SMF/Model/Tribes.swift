//
//  Tribes.swift
//  SMF
//
//  Created by Vishal on 08/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation

class Tribes {
    
    var tribeId:String?
    var tribeName:String?
    var tribeDescription:String?
    var tribeImage:String?
    var tribePrivateGroup:Bool?
    var tribeOwner:TribeOwner?
    var tribeMember:[TribeMember] = [TribeMember]()
    
    init(){
        
    }
    
    
    required init?(_ response: Dictionary<String,AnyObject>) {
        print(response)
        tribeId = (response["_id"] as? String) ?? ""
        tribeName = (response["name"] as? String) ?? ""
        tribeDescription = (response["desc"] as? String) ?? ""
        tribeImage = (response["imageUrl"] as? String) ?? ""
        tribePrivateGroup = (response["privateGroup"] as? Bool) ?? false
        if let ownerDetail = response["ownerId"] as? NSDictionary {
            tribeOwner = TribeOwner(response: ownerDetail)
        }
        if let memberArray = response["members"] as? NSArray {
            for member in memberArray {
                let memberObj = TribeMember(response: member as! NSDictionary)
                tribeMember.append(memberObj)
            }
        }
    }
}
