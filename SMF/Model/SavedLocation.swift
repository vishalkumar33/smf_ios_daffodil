//
//  SavedLocation.swift
//  SMF
//
//  Created by Jenkins on 2/13/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
class SavedLocation: NSObject, NSCoding {
    
    let latLng     : NSArray?
    let name          : String?
    let location    :String?
    
    //Declaring Response Key coming from server
    private let LATLNG_KEY         = "latLng"
    private let NAME_KEY          = "name"
    private let LOCATION_KEY    = "location"
    
    init(response:NSDictionary) {
        latLng      = (response[LATLNG_KEY] as? NSArray) ?? []
        name        = (response[NAME_KEY] as? String) ?? ""
        location    = (response[LOCATION_KEY ] as? String) ?? ""
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        self.latLng = aDecoder.decodeObject(forKey: "latLng") as! NSArray?
        self.location = aDecoder.decodeObject(forKey: "location") as! String?
        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(latLng, forKey: "latLng")
        aCoder.encode(location, forKey: "location")
        aCoder.encode(name, forKey: "name")
    }
   
}
