//
//  UserMessage.swift
//  SMF
//
//  Created by Vishal on 23/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class UserMessage {
    
    let messageContent:String?
    let messageSenderId:String?
    let messageSenderName:String?
    let messageSenderProfile:String?
    let messageReceiverId:String?
    let messageReceiverName:String?
    let messageReceiverProfile:String?
    
    
    init(content:String, senderId:String, senderName:String, senderProfile:String, receiverId:String, receiverName:String, recevierProfile:String) {
        self.messageContent = content
        self.messageSenderId = senderId
        self.messageSenderName = senderName
        self.messageSenderProfile = senderProfile
        self.messageReceiverId = receiverId
        self.messageReceiverName = receiverName
        self.messageReceiverProfile = recevierProfile
    }
    
    
}
