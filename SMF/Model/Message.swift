//
//  Message.swift
//  SMF
//
//  Created by Vishal on 17/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation


class Message {
    
    var id:String?
    var content:String?
    var recipient:Author?
    var author:Author?
    var read:Bool?
    var createdAt:String?
    
    init(response:NSDictionary) {
        if let id = response["_id"] as? String {
            self.id = id
        }
        if let content = response["content"] as? String {
            self.content = content
        }
        if let receipt = response["recipient"] as? NSDictionary {
            self.recipient = Author(dict: receipt)
        }
        if let author = response["author"] as? NSDictionary {
            self.author = Author(dict: author)
        }
        if let read = response["read"] as? Bool {
            self.read = read
        }
        if let created = response["createdAt"] as? String {
            self.createdAt = created
        }
    }
}
