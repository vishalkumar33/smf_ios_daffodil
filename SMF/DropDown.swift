
import UIKit

protocol DataDropDownDelegate: class {
    func data(_ val: AnyObject, index: Int)
}



class DropDown: CustomizeDropDown, UITableViewDelegate, UITableViewDataSource  {
    
    
    var delegate: DataDropDownDelegate? = nil
    
    override func awakeFromNib() {
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    //MARK: - Show Dropdown
    /*
     *  This function is used to Show the Dropdown
    */
    func show()
    {
        
//        let dataCount = dataObject.count
//        let cellHeight = dropDownCellHeight ?? dropDownDefaultCellHeight
//        
//        
//        if dataCount == 0{
//            self.frame.size.height = cellHeight!
//        }else{
//            self.frame.size.height = CGFloat(dataCount) * cellHeight!
//        }
        dropDownViewHeight = self.frame.size.height
        self.dropDownTableView.frame  =   CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        self.dropDownTableView.delegate      =   self
        self.dropDownTableView.dataSource    =   self
        self.dropDownTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        initializeDropDownViewProperties()
        self.addSubview(self.dropDownTableView)
        self.frame.size.height = CGFloat(0)
        showDropDownWithAnimation()
    }
  
    
    //MARK: - Show Shadow Dropdown
    /*
     *  This function is used to Show Shadow to the Dropdown
     */
    func showShadow(){
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = self.shadow_Offset
        self.layer.shadowOpacity = self.shadow_Opacity
        self.layer.shadowRadius = shadow_Radius ?? defaultShadowRadius
    }
    
    
    //MARK: - Hide Dropdown
    /*
     *  This function is used to Hide the Dropdown
     */
    func hide(){
        
         isHidden = true
    }
    
    //MARK: - Show Dropdown With Animation
    /*
     *  This function is used to Show the Dropdown with Animation
     */
    func showDropDownWithAnimation(){
        UIView.animate (withDuration: self.interval ?? self.defaultInterval, delay: 0.0, options: UIViewAnimationOptions.curveLinear ,animations: {
                    self.frame.size.height = self.dropDownViewHeight
                }, completion: { _ in
                    self.clipsToBounds = false
                   // self.showShadow()
                    self.dropDownTableView.layer.cornerRadius = self.dropDownViewCornerRadius ?? self.dropDownDefaultViewCornerRedius
            })   
        
        
    }
    func hideDropDownWithAnimation(){
        UIView.animate (withDuration: self.interval ?? self.defaultInterval, delay: 0.0, options: UIViewAnimationOptions.curveLinear ,animations: {
            self.frame.size.height =    0
        }, completion: { _ in
            self.clipsToBounds = false
            self.showShadow()
            self.dropDownTableView.layer.cornerRadius = self.dropDownViewCornerRadius ?? self.dropDownDefaultViewCornerRedius
            self.isHidden = true
            
        })
        
        
    }

    
    
    //MARK: - Initialize Dropdown View Properties
    /*
     *  This function is used to set the Dropdown view Properties
     */
    func initializeDropDownViewProperties(){
        self.layer.cornerRadius = self.dropDownViewCornerRadius ?? dropDownDefaultViewCornerRedius
        self.layer.borderWidth = self.dropDownViewBorderWidth ?? dropDownDefaultViewBorderWidth
        self.layer.borderColor = self.dropDownViewBorderColor ?? dropDownViewDefaultBorderColor
        self.clipsToBounds = true
    }
    
    //MARK: - Initialize Dropdown Table View Properties
    /*
     *  This function is used to set the Dropdown tableview Properties
     */
    func initializeDropDownTableViewproperties(){
        self.dropDownTableView.layer.cornerRadius = self.dropDownViewCornerRadius ?? dropDownDefaultViewCornerRedius
        self.dropDownTableView.layer.borderWidth = self.dropDownViewBorderWidth ?? dropDownDefaultViewBorderWidth
        self.dropDownTableView.layer.borderColor = self.dropDownViewBorderColor ?? dropDownViewDefaultBorderColor
        self.dropDownTableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.dropDownTableView.separatorColor = UIColor.black
    }
    
    
    //MARK: - Initialize Dropdown TableView Cell Properties
    /*
     *  This function is used to set the Dropdown TableView cell Properties
     */
    func initializeDropDownCellProperties(_ cell: UITableViewCell){
        
        cell.textLabel?.textColor              = dropDownCellTextColor ?? dropDownDefaultCellTextColor
        cell.textLabel?.textAlignment          = dropDownCellTextAligment ?? dropDownDefaultCellTextAligment
        cell.backgroundColor                    = dropDownCellBackgroundColor ?? dropDownDefaultBackgroundColor
      
        cell.textLabel?.font                   = UIFont(name: dropDownCellLabelFont ?? dropDownDefaultLabelFont, size: CGFloat(14))
        cell.textLabel?.font.withSize(dropDownCellLabelFontSize ?? dropDownCellDefaultLabelFontSize)
        cell.imageView?.layer.cornerRadius = dropDownCellImageViewCornerRadius ?? dropDownDefaultCellImageViewCornerRadius
        cell.imageView?.layer.borderWidth = dropDownCellImageBorderWidth ?? dropDownDefaultCellImageBorderWidth
        cell.imageView?.layer.borderColor = dropDownCellImageBorderColor ?? dropDownDefaultCellImageBorderColor
        cell.imageView?.clipsToBounds = true
    }
    
    
    //MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return dataObject.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return dropDownCellHeight ?? dropDownDefaultCellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        cell.selectionStyle = .none
        if self.dataObject[(indexPath as NSIndexPath).row].imageData != nil{
            cell.imageView?.image = UIImage(named: dataObject[(indexPath as NSIndexPath).row].imageData as! String)
        }
        cell.textLabel?.text = self.dataObject[(indexPath as NSIndexPath).row].labelData as? String
        initializeDropDownCellProperties(cell)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        changeSelectionColor(index: indexPath)
        self.value = dataObject[(indexPath as NSIndexPath).row].labelData
    
        
        self.index = (indexPath as NSIndexPath).row + 1
        delegate?.data(value, index: index)
        self.hide()
    }
    
    
    func changeSelectionColor(index:IndexPath){
        let cell:UITableViewCell = dropDownTableView.cellForRow(at: index)!
        cell.backgroundColor = UIColor.appBlueBorderColor
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell:UITableViewCell = dropDownTableView.cellForRow(at: indexPath)!
        cell.backgroundColor = UIColor.appfillColor
    }

}
