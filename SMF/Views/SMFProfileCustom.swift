//
//  SMFProfileCustom.swift
//  SMF
//
//  Created by Shivam Srivastava on 28/12/16.
//  Copyright © 2016 daffodilsw. All rights reserved.
//

import UIKit


class SMFProfileCustom: UIView {


    @IBOutlet weak var profileImageBackground: UIImageView!
    @IBOutlet weak var profileView: UIView!
    
    @IBOutlet weak var btnEditProfile: UIButton!
    
    @IBOutlet weak var lblLastUpdated: UILabel!
    @IBOutlet weak var lblHandle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var btnHistory: UIButton!
    @IBAction func editProfileAction(_ sender: Any) {
        
    }
    
    
    @IBAction func changePasswordAction(_ sender: Any) {
    }
    
    @IBAction func historyAction(_ sender: Any) {
        print("erded")
    }
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print("")
        print("Inside SMFProfile Class")
        profileView.layer.cornerRadius = 41
        profileImage.layer.cornerRadius = 40
        profileImage.layer.masksToBounds = true
    
    }
    
    
    class func instanciateWithXib()->SMFProfileCustom{
            return UINib(nibName: "SMFProfileCustom", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SMFProfileCustom
    }
    
    
    override func layoutSubviews() {
        print("run when UIView appears on screen")
        // you can update your label's text or etc.
    }

}
