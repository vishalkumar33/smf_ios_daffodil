//
//  HTTPClient.swift
//  LookForward
//
//  Created by Daffomacbook-25 on 10/11/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import Foundation
import AFNetworking


import UIKit

class HTTPClient: NSObject {
    
    static let sharedInstance = HTTPClient()
    var globalManager:AFHTTPSessionManager!
    
    
    func operationManager() -> AFHTTPSessionManager? {
        if globalManager==nil{
            globalManager = AFHTTPSessionManager()
        }
        globalManager.requestSerializer = AFJSONRequestSerializer()
        
        //            AFJSONRequestSerializer()
        
        globalManager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        if let user:UserModel = UserInformationUtility.sharedInstance.getCurrentUser(){
//            if let expiryDate:Date = user.authTokenExpires?.getDateFromString(Constant.DateFormat.dateFormat){
//                if expiryDate.isExpired() == false{
//                    globalManager.requestSerializer.setValue("\(user.email!)", forHTTPHeaderField: "api_key")
//                    globalManager.requestSerializer.setValue("\(user.authToken!)", forHTTPHeaderField: "api_secret")
//                }
//                else{
//                    UserInformationUtility.sharedInstance.deleteCurrentUser()
//                    UserInformationUtility.sharedInstance.deleteCurrentStat()
//                    UserInformationUtility.sharedInstance.deleteCurrentStoredNewCustomers()
//                    return nil
//                }
//            }
//        }
        //        globalManager.responseSerializer = AFHTTPResponseSerializer()
        //        globalManager.responseSerializer.setValue("application/json", forKey: "Content-Type")
        //        globalManager.responseSerializer.setValue("text/plain", forKey: "Content-Type")
        //        globalManager.responseSerializer.setValue("text/javascript", forKey: "Content-Type")
        //
        // globalManager.responseSerializer = AFJSONResponseSerializer(readingOptions: .allowFragments)
        globalManager.responseSerializer.acceptableContentTypes = Set(["text/html","text/plain" , "application/json"])
        return globalManager
    }
    
    
    
    func requestGETWithURL( _ urlString:String , withSuccessBlock successBlock:@escaping ( _ response:AnyObject)-> Void, andFailureBlock failureBlock:@escaping ( _ error:NSError)-> Void){
        if AFNetworkReachabilityManager.shared().isReachable {
            if let manager = self.operationManager(){
                manager.get(urlString, parameters: nil, progress: nil, success: { (task, response) in
                    if response != nil{
                        successBlock(response! as AnyObject)
                    }
                    }, failure: { (task:URLSessionDataTask?, error:Error) in
                        print(error)
                        failureBlock(error as NSError)
                })
            }else{
                    failureBlock(NSError())
                }
        }else{
            failureBlock(getNetworkError())
        }
    }
    
    
    
    
    
    func requestGETWithParameter( _ urlString:String , andRequestDictionary requestDictionary:[String : AnyObject] , withSuccessBlock successBlock:@escaping ( _ response:AnyObject)-> Void, andFailureBlock failureBlock:@escaping ( _ error:NSError)-> Void){
        if AFNetworkReachabilityManager.shared().isReachable {
            if let manager = self.operationManager(){
                manager.operationQueue.cancelAllOperations()
                manager.get(urlString, parameters: requestDictionary, progress: nil, success: { (task, response) in
                    if response != nil{
                        successBlock(response as AnyObject)
                    }
                }) { (task:URLSessionDataTask?, error:Error) in
                    print(error)
                    failureBlock(error as NSError)
                }
            }else{
                failureBlock(NSError())
            }
        }
        else{
            failureBlock(getNetworkError())
        }
    }
    
    
    
    
    
    
    
    func requestPOSTWithURL( _ urlString:String , andRequestDictionary requestDictionary:[String : AnyObject],withSuccessBlock successBlock:@escaping ( _ response:AnyObject)-> Void, andFailureBlock failureBlock:@escaping ( _ error:NSError)-> Void){
        print(requestDictionary)
        if let manager = self.operationManager(){
            manager.operationQueue.cancelAllOperations()
            manager.post(urlString, parameters: requestDictionary, progress: nil, success: { (task, response) in
                if response != nil{
                    print(response!)
                    successBlock(response as AnyObject)
                }
            }) { (task:URLSessionDataTask?, error:Error) in
                print(error)
                failureBlock(error as NSError )
            }
        }else{
            failureBlock(NSError())
        }
    }
    
    func requestPOST( _ urlString:String , andRequestDictionary requestDictionary:[String : AnyObject],withSuccessBlock successBlock:@escaping ( _ response:AnyObject)-> Void, andFailureBlock failureBlock:@escaping ( _ error:NSError)-> Void){
        print(requestDictionary)
        if let manager = self.operationManager(){
            manager.operationQueue.cancelAllOperations()
            manager.post(urlString, parameters: requestDictionary, progress: nil, success: { (task, response) in
                if response != nil{
                    print(response!)
                    successBlock(response as AnyObject)
                }
            }) { (task:URLSessionDataTask?, error:Error) in
                print(error)
                failureBlock(error as NSError )
            }
        }else{
            failureBlock(NSError())
        }
    }
    
    func requestPOSTWithoutSession( _ urlString:String ,sessionId:String?,apiKey:String, andRequestDictionary requestDictionary:[String : AnyObject],withSuccessBlock successBlock:@escaping ( _ response:AnyObject)-> Void, andFailureBlock failureBlock:@escaping ( _ error:NSError)-> Void){
        print(requestDictionary)
        if let manager = self.operationManager(){
            manager.operationQueue.cancelAllOperations()
            manager.requestSerializer.setValue(sessionId, forHTTPHeaderField: "session-id")
            manager.requestSerializer.setValue(apiKey, forHTTPHeaderField: "api-key")
            manager.post(urlString, parameters: requestDictionary, progress: nil, success: { (task, response) in
                if response != nil{
                    print(response!)
                    successBlock(response as AnyObject)
                }
            }) { (task:URLSessionDataTask?, error:Error) in
                print(error)
                failureBlock(error as NSError )
            }
        }else{
            failureBlock(NSError())
        }
    }
    
    
    func requestGET( _ urlString:String,sessionId:String,apiKey:String , withSuccessBlock successBlock:@escaping ( _ response:AnyObject)-> Void, andFailureBlock failureBlock:@escaping ( _ error:NSError)-> Void){
        if let manager = self.operationManager(){
            manager.requestSerializer.setValue(sessionId, forHTTPHeaderField: "session-id")
            manager.requestSerializer.setValue(apiKey, forHTTPHeaderField: "api-key")
            manager.get(urlString, parameters: nil, progress: nil, success: { (task, response) in
                if response != nil{
                    print(response!)
                    successBlock(response! as AnyObject)
                }
            }, failure: { (task:URLSessionDataTask?, error:Error) in
                print(error)
                failureBlock(error as NSError)
            })
        }else{
            failureBlock(NSError())
        }
    }
    
    func requestGETWithoutSession( _ urlString:String,sessionId:String?,apiKey:String , withSuccessBlock successBlock:@escaping ( _ response:AnyObject)-> Void, andFailureBlock failureBlock:@escaping ( _ error:NSError)-> Void){
        if let manager = self.operationManager(){
            manager.requestSerializer.setValue(sessionId, forHTTPHeaderField: "session-id")
            manager.requestSerializer.setValue(apiKey, forHTTPHeaderField: "api-key")
            manager.get(urlString, parameters: nil, progress: nil, success: { (task, response) in
                if response != nil{
                    print(response!)
                    successBlock(response! as AnyObject)
                }
            }, failure: { (task:URLSessionDataTask?, error:Error) in
                print(error)
                failureBlock(error as NSError)
            })
        }else{
            failureBlock(NSError())
        }
    }
    
    func requestPUT( _ urlString:String , andRequestDictionary requestDictionary:[String : AnyObject]?,withSuccessBlock successBlock:@escaping ( _ response:AnyObject)-> Void, andFailureBlock failureBlock:@escaping ( _ error:NSError)-> Void){
        if let manager = self.operationManager(){
            manager.operationQueue.cancelAllOperations()
            manager.put(urlString, parameters: requestDictionary, success: { (task, response) in
                if response != nil{
                    print(response!)
                    successBlock(response as AnyObject)
                }
            }, failure: { (task:URLSessionDataTask?, error:Error) in
                print(error)
                failureBlock(error as NSError )
            })
        }else{
            failureBlock(NSError())
        }
    }
    
    
    
    
    
    func getNetworkError() -> NSError {
        return NSError()
//        return NSError(domain: "network_error", code: ErrorCodes.error_1009.rawValue, userInfo:nil)
    }
    
    
    
    
    
    // method to get auth token error
    
//    func getAuthTokenError() -> NSError {
//        
//        UserInformationUtility.sharedInstance.deleteCurrentUser()
//        
//        UserInformationUtility.sharedInstance.deleteCurrentStat()
//        
//        UserInformationUtility.sharedInstance.deleteCurrentStoredNewCustomers()
//        
//        return NSError(domain: "authtoken_expires", code: ErrorCodes.error_106.rawValue, userInfo:nil)
//        
//    }
    
    // Method to cancel all operations.
    
    func cancellAllNetworkOperations(){
        if let manager = self.operationManager(){
            manager.operationQueue.cancelAllOperations()
        }
    }
    
}
