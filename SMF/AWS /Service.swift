import UIKit
import AFNetworking


class Service {
    
    class func getNewFeed(_ lat:String, skip:Int, choice:String? ,long:String,success: @escaping (_ response: NSArray, _ count:Int) -> Void, failure: @escaping (_ error: NSError?) -> Void )
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String?
            if choice != nil {
                urlString = "\(SMFConstant.BASE_URL)/allPosts?skip=\(skip)&limit=10&position=[\(lat),\(long)]&range=[0,250]&\(choice)=true"
            }else{
                urlString = "\(SMFConstant.BASE_URL)/allPosts?skip=\(skip)&limit=10&position=[\(lat),\(long)]&range=[0,250]"
            }
            print(urlString)
            let encodedUrlString:String = urlString!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                print(response)
                
                
                let data:NSDictionary  = response as! NSDictionary
                let count:Int = data["totalfeedscount"] as! Int
                let result = data["result"] as! NSArray
                success(result, count)
                
            }) { (error) in
                failure(error)
            }
        }
        else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
    }
    
    class func getMemberList(success: @escaping (_ response:NSArray) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/members?limit=50&skip=0"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                success(response as! NSArray)
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        

    }
    class func getCommentList(_ postId:String,success: @escaping (_ response:NSArray) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        let urlString:String = "\(SMFConstant.BASE_URL)/comments?postId=\(postId)"
        let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
            success(response as! NSArray)
        }) { (error) in
            failure(error)
        }
        
    }
    
    
    class func getWhoAMI(success: @escaping (_ response:NSDictionary) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        let urlString:String = "\(SMFConstant.BASE_URL)/whoAmI"
        let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
            success(response as! NSDictionary)
        }) { (error) in
            failure(error)
        }
        
    }
    
    
    class func whoAMI(success: @escaping (_ response:UserInfo) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        let urlString:String = "\(SMFConstant.BASE_URL)/whoAmI"
        let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
            success(Parser.parseUser(data: response as! NSDictionary))
        }) { (error) in
            failure(error)
        }
        
    }
    
    
    

    
    
   
    class func getMemberSearchList(searchText:String,success: @escaping (_ response:NSArray) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/searchMember?searchValue=\(searchText)&limit=1&skip=0"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                success(response as! NSArray)
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
        
    }
    
    class func getSearchNewsList(searchText:String,success: @escaping (_ response:NSDictionary) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/searchPost?searchValue=\(searchText)&limit=1&skip=0"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                success(response as! NSDictionary)
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
        
    }
    
    
    class func searchGroupPost(searchText:String,groupId:String, limit:Int, skip:Int,success: @escaping (_ response:[Post]) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/group/searchPost?groupId=\(groupId)&searchValue=\(searchText)&limit=\(limit)&skip=\(skip)"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                success(Parser.ParseGroupTribe(data: response as! NSDictionary))
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
        
    }
    

    
    class func getHistoryDetail(author_id:String,success: @escaping (_ response: NSDictionary) -> Void, failure: @escaping (_ error: NSError?) -> Void )
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/userHistory?author=\(author_id)&limit=10&skip=0"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                success(response as! NSDictionary)
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
    }
    
    class func getTribeDetail(tribeId:String,limit:Int,skip:Int,lat:String,long:String,success: @escaping (_ response: NSDictionary) -> Void, failure: @escaping (_ error: NSError?) -> Void )
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/group/allposts/?groupId=\(tribeId)&skip=\(skip)&limit=\(limit)&position=[\(lat),\(long)] "
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                success(response as! NSDictionary)
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
    }
    
    
    
    
    
    class func getTribeMemberSearchList(searchText:String,success: @escaping (_ response:NSDictionary) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/memberSearch?searchValue=\(searchText)"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                success(response as! NSDictionary)
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
        
    }
    
    class func whoAmIForNotification(success: @escaping (_ response: [UserNotification]) -> Void, failure: @escaping (_ error: NSError?) -> Void )
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/whoAmI"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                success(Parser.parseNotification(data: response as! NSDictionary) )
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
    }
    
    
    class func searchTribe(searchText:String,success: @escaping (_ response:[Tribes], _ totalCount:Int) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/searchTribe?searchValue=\(searchText)&limit=20&skip=0"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                let totalCount = response["totalfeedscount"] as! Int
                success(Parser.ParseSearchTribe(data: response as! NSDictionary), totalCount)
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
        
    }
    
    
    class func joinRequest(data:NSDictionary,success: @escaping (_ response:Tribes) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable  {
            let urlString:String = "\(SMFConstant.BASE_URL)/joinRequest"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestPOSTWithURL(encodedUrlString, andRequestDictionary: data as! [String : AnyObject], withSuccessBlock: { (response) in
                success(Parser.ParseTribe(data: response as! NSDictionary))
                print(response)
            }) { (error) in
                failure(error)
                print(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
        
    }
    
    
    class func removeMember(data:NSDictionary,success: @escaping (_ response:Tribes) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/removeMember"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestPOSTWithURL(encodedUrlString, andRequestDictionary: data as! [String : AnyObject], withSuccessBlock: { (response) in
                success(Parser.ParseTribe(data: response as! NSDictionary))
                print(response)
            }) { (error) in
                failure(error)
                print(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
        
    }
    
    
    class func Report(data:NSDictionary,success: @escaping (_ response:NSDictionary) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/reportPost"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestPOSTWithURL(encodedUrlString, andRequestDictionary: data as! [String : AnyObject], withSuccessBlock: { (response) in
                success(response as! NSDictionary)
                print(response)
                
            }) { (error) in
                failure(error)
                print(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
        
    }
    

    
    
    class func deleteTribe(data:NSDictionary,success: @escaping (_ response:NSDictionary) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable  {
            let urlString:String = "\(SMFConstant.BASE_URL)/closeTribe"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestPOSTWithURL(encodedUrlString, andRequestDictionary: data as! [String : AnyObject], withSuccessBlock: { (response) in
                success(response as! NSDictionary)
                print(response)
            }) { (error) in
                failure(error)
                print(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
        
    }
    
    
    class func getForNotification(success: @escaping (_ response: [UserNotification]) -> Void, failure: @escaping (_ error: NSError?) -> Void )
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/getNotification"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                print(response)
                success(Parser.parseNotification(data: response as! NSDictionary) )
                print(response)
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
    }
    
    class func getAllMessage(skip:Int, limit:Int, success: @escaping (_ response: [Message], _ totalCount:Int) -> Void, failure: @escaping (_ error: NSError?) -> Void )
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/messages"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                print(response)
                var totalCount = 0
                if let count = response["totalmessagescount"] as? Int {
                    totalCount = count
                }
                success(Parser.ParseMessage(data: response as! NSDictionary), totalCount)
                print(response)
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
    }
    
    
    class func logout(success: @escaping (_ response: NSDictionary) -> Void, failure: @escaping (_ error: NSError?) -> Void )
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/logout"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                success(response as! NSDictionary)
                print(response)
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
    }
    
    class func getConnection(success: @escaping (_ response: [Connection]) -> Void, failure: @escaping (_ error: NSError?) -> Void )
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)/memberSearch"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                success(Parser.ParseConnection(data: response as! NSDictionary))
                print(response)
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
    }
    
    
    class func changePassword(data:NSDictionary,success: @escaping (_ response:NSDictionary) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable  {
            let urlString:String = "\(SMFConstant.BASE_URL)/changePassword"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestPOSTWithURL(encodedUrlString, andRequestDictionary: data as! [String : AnyObject], withSuccessBlock: { (response) in
                success(response as! NSDictionary)
                print(response)
            }) { (errorRes) in
                if let errResponse: String = String(data: (errorRes.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as! NSData) as Data, encoding: String.Encoding.utf8) {
                    NSLog("%@", errResponse)
                    if let jsonData: Dictionary<String,AnyObject> = errResponse.parseJSONString as? Dictionary<String, AnyObject> {
                        print(jsonData)
                        if jsonData["message"] as! String == "Old Password not Matched" {
                            let error = NSError(domain: "Old Password not Matched", code: 402, userInfo: nil)
                            failure(error)
                        }
                    }else{
                        failure(errorRes)
                        print(errorRes)
                    }
                }else{
                    failure(errorRes)
                    print(errorRes)
                }
                
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
    }
    
    class func followUser(data:NSDictionary,success: @escaping (_ response: NSDictionary) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable  {
            let urlString:String = "\(SMFConstant.BASE_URL)/followUser"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestPUT(encodedUrlString, andRequestDictionary: data as? [String : AnyObject], withSuccessBlock: { (response) in
                success(response as! NSDictionary)
                print(response)
            }) { (errorRes) in
                if let errResponse: String = String(data: (errorRes.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as! NSData) as Data, encoding: String.Encoding.utf8) {
                    NSLog("%@", errResponse)
                }else{
                    failure(errorRes)
                    print(errorRes)
                }
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            
            
            failure(error)
        }
        
        
    }
    
    
    class func addMember(data:NSDictionary,success: @escaping (_ response: Tribes) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable  {
            let urlString:String = "\(SMFConstant.BASE_URL)/addMember"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestPUT(encodedUrlString, andRequestDictionary: data as! [String : AnyObject], withSuccessBlock: { (response) in
                success( Parser.ParseTribe(data: response as! NSDictionary))
                print(response)
            }) { (errorRes) in
                if let errResponse: String = String(data: (errorRes.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as! NSData) as Data, encoding: String.Encoding.utf8) {
                    NSLog("%@", errResponse)
                    if let jsonData: Dictionary<String,AnyObject> = errResponse.parseJSONString as? Dictionary<String, AnyObject> {
                        print(jsonData)
                        if jsonData["message"] as! String == "member already exists" {
                            let error = NSError(domain: "member already exists", code: 402, userInfo: nil)
                            failure(error)
                        }
                        if jsonData["message"] as! String == "Some mandatory field is Missing." {
                            let error = NSError(domain: "Some mandatory field is Missing", code: 400, userInfo: nil)
                            failure(error)
                        }
                    }else{
                        failure(errorRes)
                        print(errorRes)
                    }
                }else{
                    failure(errorRes)
                    print(errorRes)
                }
                
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            
            
            failure(error)
        }
        
        
    }
    
    
    class func getPostDetail(postLink:String,success: @escaping (_ response:Post) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable {
            let urlString:String = "\(SMFConstant.BASE_URL)\(postLink)"
            print(urlString)
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestGETWithURL(encodedUrlString, withSuccessBlock: { (response) in
                success(Parser.parsePost(data: response as! NSDictionary))
            }) { (error) in
                failure(error)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
    }
    
    
    class func clearAllNotification(success: @escaping (_ response: NSDictionary) -> Void,failure: @escaping (_ error: NSError?) -> Void)
    {
        if AFNetworkReachabilityManager.shared().isReachable  {
            let urlString:String = "\(SMFConstant.BASE_URL)/clearNotifications"
            let encodedUrlString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HTTPClient.sharedInstance.requestPUT(encodedUrlString, andRequestDictionary: nil, withSuccessBlock: { (response) in
                success(response as! NSDictionary)
                print(response)
            }) { (errorRes) in
                failure(errorRes)
            }
        }else{
            let error = NSError(domain: "Please check internet connection", code: -1009, userInfo: nil)
            failure(error)
        }
        
        
    }
    
    
     
    
}

extension String {
    
    var parseJSONString: Any? {
        
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if let jsonData = data {
            
            // Will return an object or nil if JSON decoding fails
            do {
                return try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers)
            }
            catch {
                return nil
            }
        } else {
            // Lossless conversion of the string was not possible
            return nil
        }
    }
}




