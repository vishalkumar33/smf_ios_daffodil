//
//  MessageService.swift
//  SMF
//
//  Created by Vishal on 17/02/17.
//  Copyright © 2017 daffodilsw. All rights reserved.
//

import Foundation
import SocketIO


class MessageService {
    
     private var socket :SocketIOClient!
    
     private init() {
        
    }

    static var instance: MessageService = MessageService()
    

    func connectToSocket(userId:String)
    {
        MessageService.instance.socket = SocketIOClient(socketURL: URL(string: "http://139.162.5.142:5400")!, config: [.log(true), .forcePolling(true)])
        MessageService.instance.socket.on("connect") {data, ack in
            print("socket connected")
             MessageService.instance.setUserId(userId: userId)
            
            MessageService.instance.socket.on("received-message") {data, ack in
                print("message received")
                print(data)
               
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "messageReceived"), object: nil, userInfo: ["data": data])
            }
            MessageService.instance.socket.on("received-reply-message") {data, ack in
                print("message received")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "messageReplyReceived"), object: nil, userInfo: ["data": data])
            }
        }
        
      MessageService.instance.socket.connect()
    }
    
    func setUserId(userId:String){
        MessageService.instance.socket.emit("setUserId", with: [userId])
    }
    
    func sendMessage(message:String, receiptId:String, senderId:String){
        MessageService.instance.socket.emit("send-message", with: [["content":message,	"recipient" : senderId,	"author" : receiptId]])
    }
    
    
    
    

}
