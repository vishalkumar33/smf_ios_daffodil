//
//  Parser.swift
//  wicked_ride_ios
//
//  Created by Mohit Kumar on 14/12/15.
//  Copyright © 2015 vishal. All rights reserved.
//

import UIKit



class Parser {
    
    
    class func parseNotification(data:NSDictionary) -> [UserNotification]{
        let notificationArray = data["notifications"] as! NSArray!
        
        var userNotificationArray:[UserNotification] = [UserNotification]()
        
        if (notificationArray?.count)! > 0 {
            for notification in notificationArray! {
                let obj = UserNotification(response: notification as! NSDictionary)
                userNotificationArray.append(obj)
            }
        }
        return userNotificationArray
    }
    
    class func ParseSearchTribe(data:NSDictionary) -> [Tribes]{
        let reslultArray = data["result"] as! NSArray!
        
        var tribeArray:[Tribes] = [Tribes]()
        
        if (reslultArray?.count)! > 0 {
            for result in reslultArray! {
                let obj = Tribes((result as! NSDictionary) as! Dictionary<String, AnyObject>)
                tribeArray.append(obj!)
            }
        }
        return tribeArray
    }
    
    
    class func ParseGroupTribe(data:NSDictionary) -> [Post]{
        let reslultArray = data["result"] as! NSArray!
        
        var postArray:[Post] = [Post]()
        
        if (reslultArray?.count)! > 0 {
            for result in reslultArray! {
                let obj = Post(response: result as! NSDictionary)
                postArray.append(obj)
            }
        }
        return postArray
    }
    
    
    class func ParseTribe(data:NSDictionary) -> Tribes{
        return Tribes(data as! Dictionary<String, AnyObject>)!
    }
    
    class func parseUser(data:NSDictionary) -> UserInfo{
        return UserInfo(data as! Dictionary<String, AnyObject>)!
    }
    
    class func ParseMessage(data:NSDictionary) -> [Message]{
        let reslultArray = data["result"] as! NSArray!
        
        var messageArray:[Message] = [Message]()
        
        if (reslultArray?.count)! > 0 {
            for result in reslultArray! {
                let obj = Message(response: result as! NSDictionary)
                messageArray.append(obj)
            }
        }
        return messageArray
    }
    
    class func ParseConnection(data:NSDictionary) -> [Connection]{
        let reslultArray = data["result"] as! NSArray!
        
        var connectionArray:[Connection] = [Connection]()
        
        if (reslultArray?.count)! > 0 {
            for result in reslultArray! {
                let obj = Connection(response: result as! NSDictionary)
                connectionArray.append(obj)
            }
        }
        return connectionArray
    }
    
    class func parsePost(data:NSDictionary) -> Post {
        return Post(response: data)
    }
}
